import { Document } from './oas3/objects/document/document.model';
import { petStoreOas3 } from './mock/petStore.oas.js';
import { StringField } from './oas3/fields/string.field';
import { Responses } from './oas3/objects/components/responses.model';

import { Contact } from './oas3/objects/document/contact.model';
// console.log(
//   JSON.stringify(petStoreDoc, (key, value) => {
//     if (key.indexOf('_') < 0 || key === '_oasNodeId') {
//       return value;
//     }
//   })
// );

import { OasSchemaFactory } from './oas3/objects/common/schema/factories/oasSchemaFactory';
//
// console.log(OasSchemaFactory.createSchema().extract());
// console.log(OasSchemaFactory.createSchema().format.value);
// console.log(OasSchemaFactory.createSchema().nullable.value);
import { ComponentFactory } from './oas3/objects/components/componentFactory';
import { OasExtensibleNode } from './oas3/base/oasExtensibleNode';
import { OasNode } from './oas3/base/oasNode';
import { OasMapNode } from './oas3/base/oasMapNode';

let petStoreDoc = new Document(petStoreOas3);
let responses = petStoreDoc.paths['/pet'].post.responses;
responses.addExtension('x-canceled', 'Canceled');
console.log(petStoreDoc.nodeObjectType);
console.log(Responses.nodeObjectType);
console.log(responses instanceof Responses);
console.log(responses instanceof OasNode);
console.log(responses instanceof OasMapNode);

let newV = petStoreDoc.servers.value[0].variables.addVariable('env', {
  default: 'dev',
  description: 'Development',
  enum: ['dev', 'test', 'prod'],
});

console.log(newV.parent.path);
console.log(JSON.stringify(petStoreDoc.extract()));

console.log(petStoreDoc.servers.value[0].oasNodeId);
console.log(petStoreDoc.servers.value[0].variables.path);
console.log(petStoreDoc.servers.value[0].variables.version.enum.value[0].oasNodeId);
console.log(petStoreDoc.servers.value[0].variables.version.enum.extract());
console.log(petStoreDoc.servers.value[0].variables.version.extract());
console.log(petStoreDoc.servers.extract()[0].variables.version);
console.log(petStoreDoc.servers.value[0].variables.version.enum.value[0].extract());
console.log(petStoreDoc.info.license);
console.log('extract ids');
// console.log(petStoreDoc.servers.extract(true));
console.log(JSON.stringify(petStoreDoc.extract()));
petStoreDoc.paths['/pet'].put.responses.changeItemKey('405', '500');
console.log(JSON.stringify(petStoreDoc.extract()));

let a = ComponentFactory.createComponent({
  params: {
    type: 'http',
    description: '',
    scheme: 'Bearer',
    bearerFormat: '',
  },
  parent: this,
  type: 'securityScheme',
});
console.log(petStoreDoc.getPathKeyByErrorPath('info.contact.email'));
console.log(petStoreDoc.getPathKeyByErrorPath('servers.0.variables.version.default'));
console.log(petStoreDoc.getPathKeyByErrorPath('servers.1.variables.version.enum.0'));
console.log(petStoreDoc.getPathKeyByErrorPath('servers.url.ddd.ddd'));

console.log(
  (petStoreDoc.components.requestBodies.UserArray.content[
    'application/json'
  ].schema.maxItems.value = undefined)
);
console.log(
  petStoreDoc.components.requestBodies.UserArray.content[
    'application/json'
  ].schema.maxItems.extract()
);

console.log(petStoreDoc.components.examples.extract());
console.log(petStoreDoc.components.examples.tagName.value.extract());
console.log(petStoreDoc.components.examples.tagName.exampleValue.extract());
console.log(petStoreDoc.components.examples.tagName.exampleValue.path);
console.log(petStoreDoc.components.examples.tagName.exampleValue.oasNodePath);

console.log(petStoreDoc.components.schemas.Dog);
let not = OasSchemaFactory.createSchema({ not: { $ref: '' } });
let allOf = OasSchemaFactory.createSchema({
  allOf: [{ $ref: 'n/a' }, { $ref: '1n/a' }, { $ref: '2n/a' }],
});
console.log(not.extract());
console.log(allOf.extract());
console.log(allOf.allOf.extractSegmentation([0], true));

let x1 = new (OasExtensibleNode(OasNode))({ 'x-controller-tag': 'PET' });

x1.addExtension('x-controller-name', 'getPetById');
x1.addExtension('x-controller-dd', 'getPetByIddd');
console.log(x1.extract());

let proxy = new Proxy(x1, {
  get: (target, property) => {
    if (/^x-*/.test(property)) {
      return target._extensions[property];
    } else {
      return target[property];
    }
  },
  set: (target, property, value) => {
    if (/^x-*/.test(property)) {
      target.addExtension(property, value);
    } else {
      target[property] = value;
    }
    return true;
  },
});

// console.log(x1._extensions.extract());
// x1._extensions['x-1'] = new StringField('x-1', x1);
// proxy['x-2'] = 'x-2';
// console.log(proxy['x-2']);
//
// console.log(x1._extensions.extract());

let ex1 = new (OasExtensibleNode(OasNode))({ 'x-controller-tag': 'PET' }, null, 'doc');
console.log(ex1.extract());
petStoreDoc['x-controller-tag1'] = new StringField('x-1', ex1, 'x-controller-tag1');
petStoreDoc.addExtension('x-2', 'x-controller-tag222');
console.log(petStoreDoc.extract());
console.log(petStoreDoc['x-2'].oasNodePath);
console.log(petStoreDoc.extensiveProps);
petStoreDoc.servers.value[0].addExtension('x-scheme', 'tcp');
console.log(petStoreDoc.servers.value[0].extensiveProps);
console.log(petStoreDoc.servers.value[0].extract());
petStoreDoc.servers.value[0].addExtension('x-schemes', 'tcps');

console.log(petStoreDoc.servers.value[0].extensiveProps);
console.log(petStoreDoc.servers.value[0].extract());
petStoreDoc.servers.value[0].removeExtension('x-schemes');
console.log(petStoreDoc.servers.value[0].extensiveProps);
console.log(petStoreDoc.servers.value[0].extract());
