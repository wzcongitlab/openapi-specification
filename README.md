**该模块包含OAS文档节点或原子字段定义, 单元测试(**.spec.js), 集成测试(test/**.test.js)**  

#### 目录

--mock  该模块的测试数据  
--oas3  Oas标准数据结构定义
----base  节点对象(包括对象和原子节点)的基类  
----config  Oas相关常量, 状态码, 媒体类型, httpMethod的配置  
----fields  定义oas的原子字段, eg. StringField, NumberField. etc  
----helpers  Oas相关数据操作的辅助类  
----objects  定义openapi 3.0.1官方描述的oas节点对象, eg. Info, Components. etc  
----validation  Oas数据校验的校验信息数据结构  
--test  OasCore模块集成测试  
--utils  Oas数据操作小工具集  

#### 使用vscode调试index.js
1.`npm install`  
2.在vscode菜单栏选择调试  
3.添加/修改配置至项目根目录 >.vscode/launch.json  
`

{
    "version": "0.2.0",
    "configurations": [
        {
            "type": "node",
            "request": "launch",
            "name": "启动程序",
            "runtimeExecutable": "${workspaceRoot}\\src\\routes\\ApiDesign\\ApiEditor\\OasCore\\node_modules\\.bin\\babel-node",
            "program": "${workspaceFolder}\\src\\routes\\ApiDesign\\ApiEditor\\OasCore\\index.js"
        }
    ]
}

`  
4.增加断点, 启动调试.
