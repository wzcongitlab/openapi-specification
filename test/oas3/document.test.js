import { Utils } from '../../utils/utils';
import { Document } from '../../oas3/objects/document/document.model';
import { petStoreOas } from '../../mock/petStore.oas.js';

let chai = require('chai');

chai.should();

describe('Document', () => {
  let document;
  let params = petStoreOas;

  describe('#nodePaths', () => {
    beforeEach(() => {
      document = new Document(params);
    });

    describe('#nodeTreeLayout', () => {
      it('', () => {});
    });

    describe('#nodeTreeValues', () => {
      it('', () => {});
    });
  });

  describe('#validations', () => {
    beforeEach(() => {
      document = new Document(params);
    });
  });
});
