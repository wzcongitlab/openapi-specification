import { CONSTANTS } from './constants';

export const booleanOptions = [
  {
    value: CONSTANTS.trueAsString,
    text: CONSTANTS.trueAsString,
  },
  {
    value: CONSTANTS.falseAsString,
    text: CONSTANTS.falseAsString,
  },
];

export const booleanOptionsWithNA = [
  {
    value: CONSTANTS.unavailable,
    text: CONSTANTS.unavailable,
  },
  ...booleanOptions,
];
