import { CONSTANTS } from './constants';
import { JSON_SCHEMA_TYPES } from './oasConfig';

export const STRING_SCHEMA_FORMATS = {
  byte: {
    value: 'byte',
    text: 'Byte',
  },
  binary: {
    value: 'binary',
    text: 'Binary',
  },
  date: {
    value: 'date',
    text: 'Date',
  },
  dateTime: {
    value: 'date-time',
    text: 'DateTime',
  },
  password: {
    value: 'password',
    text: 'Password',
  },
};

// Oas schema的字段的类型;
export const SCHEMA_ATOM_FIELD_TYPE_FORMAT_MAPPING = [
  {
    typeValue: CONSTANTS.emptyStr,
    typeText: CONSTANTS.unavailable,
    formatOptions: [
      {
        value: CONSTANTS.emptyStr,
        text: CONSTANTS.unavailable,
      },
    ],
  },
  {
    typeValue: JSON_SCHEMA_TYPES.string,
    typeText: 'String',
    formatOptions: [
      {
        value: CONSTANTS.unavailable,
        text: CONSTANTS.unavailable,
      },
      ...Object.values(STRING_SCHEMA_FORMATS),
    ],
  },
  {
    typeValue: JSON_SCHEMA_TYPES.integer,
    typeText: 'Integer',
    formatOptions: [
      {
        value: CONSTANTS.unavailable,
        text: CONSTANTS.unavailable,
      },
      {
        value: 'int32',
        text: 'Int32',
      },
      {
        value: 'int64',
        text: 'Int64',
      },
    ],
  },
  {
    typeValue: JSON_SCHEMA_TYPES.boolean,
    typeText: 'Boolean',
    formatOptions: null,
  },
  {
    typeValue: JSON_SCHEMA_TYPES.number,
    typeText: 'Number',
    formatOptions: [
      {
        value: CONSTANTS.unavailable,
        text: CONSTANTS.unavailable,
      },
      {
        value: 'float',
        text: 'Float',
      },
      {
        value: 'double',
        text: 'Double',
      },
    ],
  },
];

export const SCHEMA_TYPE_FORMAT_MAPPING = [
  ...SCHEMA_ATOM_FIELD_TYPE_FORMAT_MAPPING,
  {
    typeValue: JSON_SCHEMA_TYPES.array,
    typeText: 'Array',
    formatOptions: null,
  },
];

export const SCHEMA_OBJECT_FORMAT_MAPPING = {
  typeValue: JSON_SCHEMA_TYPES.object,
  typeText: 'Object',
  formatOptions: null,
};

export const SCHEMA_OBJECT_TYPE = {
  'All Of': {
    name: 'allOf',
    title: 'All Of',
  },
  'Any Of': {
    name: 'anyOf',
    title: 'Any Of',
  },
  'One Of': {
    name: 'oneOf',
    title: 'One Of',
  },
  Not: {
    name: 'not',
    title: 'Not',
  },
  'Reference Object': {
    name: '$ref',
    title: 'Reference Object',
  },
};
