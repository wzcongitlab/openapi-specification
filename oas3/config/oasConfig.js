// [事件对象数据] 事件相关的oas编辑单位类型和对应的oas节点名称的关系;
export const OAS_OBJECT_TYPES = {
  other: {
    name: 'other',
    series: 'other',
    seriesTitle: 'Api Basics',
    nodeNames: ['openapi', 'info', 'servers', 'tags', 'externalDocs', 'security'],
  },
  path: {
    name: 'path',
    series: 'paths',
    seriesTitle: 'Paths',
    nodeNames: ['paths'],
  },
  component: {
    name: 'component',
    series: 'components',
    seriesTitle: 'Components',
    nodeNames: ['components'],
  },
};

// [事件对象数据] 事件发生时能定位到的oas节点对象的类型
export const OAS_EVENT_CONTENT_TYPES = {
  atomic: 'atomic',
  object: 'object',
  array: 'array',
};

export const OAS_COMPONENT_TYPES = {
  callback: {
    name: 'callback',
    title: 'Callback',
    series: 'callbacks',
    seriesTitle: 'Callbacks',
  },
  example: {
    name: 'example',
    title: 'Example',
    series: 'examples',
    seriesTitle: 'Examples',
  },
  header: {
    name: 'header',
    title: 'Header',
    series: 'headers',
    seriesTitle: 'Headers',
  },
  link: {
    name: 'link',
    title: 'Link',
    series: 'links',
    seriesTitle: 'Links',
  },
  parameter: {
    name: 'parameter',
    title: 'Parameter',
    series: 'parameters',
    seriesTitle: 'Parameters',
  },
  requestBody: {
    name: 'requestBody',
    title: 'RequestBody',
    series: 'requestBodies',
    seriesTitle: 'RequestBodies',
  },
  response: {
    name: 'response',
    title: 'Response',
    series: 'responses',
    seriesTitle: 'Responses',
  },
  schema: {
    name: 'schema',
    title: 'Schema',
    series: 'schemas',
    seriesTitle: 'Schemas',
  },
  securityScheme: {
    name: 'securityScheme',
    title: 'SecurityScheme',
    series: 'securitySchemes',
    seriesTitle: 'SecuritySchemes',
  },
};

export const OAS_COMPONENT_TYPE_LIST = Object.values(OAS_COMPONENT_TYPES).map(
  componentType => componentType
);

export const OAS_COMPONENT_SERIES_LIST = Object.values(OAS_COMPONENT_TYPES).map(
  componentType => componentType.series
);

export const PARAMETER_TYPES = {
  content: {
    name: 'content',
    title: 'Content',
  },
  schema: {
    name: 'schema',
    title: 'Schema',
  },
};

export const OAS_OBJECT_REF_PREFIXES = {
  path: '#/components/paths/',
  callback: '#/components/callbacks/',
  example: '#/components/examples/',
  header: '#/components/headers/',
  link: '#/components/links/',
  parameter: '#/components/parameters/',
  requestBody: '#/components/requestBodies/',
  response: '#/components/responses/',
  schema: '#/components/schemas/',
  securityScheme: '#/components/securitySchemes/',
};

export const SECURITY_SCHEME_TYPES = Object.freeze({
  apiKey: {
    name: 'apiKey',
    title: 'Api Key',
  },
  http: {
    name: 'http',
    title: 'HTTP',
  },
  oauth2: {
    name: 'oauth2',
    title: 'OAuth2',
  },
  openIdConnect: {
    name: 'openIdConnect',
    title: 'OpenId Connect',
  },
});

export const SECURITY_SCHEME_TYPE_LIST = Object.values(SECURITY_SCHEME_TYPES);

export const OAUTH_FLOW_TYPES = {
  implicit: 'implicit',
  password: 'password',
  clientCredentials: 'clientCredentials',
  authorizationCode: 'authorizationCode',
};

export const OAUTH_FLOW_TYPE_TITLES = {
  implicit: 'Implicit',
  password: 'Password',
  clientCredentials: 'Client Credentials',
  authorizationCode: 'Authorization Code',
};

export const OAUTH_FLOW_TYPE_LIST = Object.values(OAUTH_FLOW_TYPES);

export const OPERATION_TYPES = {
  get: {
    name: 'get',
    title: 'GET',
  },
  put: {
    name: 'put',
    title: 'PUT',
  },
  post: {
    name: 'post',
    title: 'POST',
  },
  delete: {
    name: 'delete',
    title: 'DELETE',
  },
  options: {
    name: 'options',
    title: 'OPTIONS',
  },
  head: {
    name: 'head',
    title: 'GET',
  },
  patch: {
    name: 'patch',
    title: 'PATCH',
  },
  trace: {
    name: 'trace',
    title: 'TRACE',
  },
};

export const OPERATION_TYPE_LIST = Object.keys(OPERATION_TYPES);

export const JSON_SCHEMA_TYPES = {
  integer: 'integer',
  string: 'string',
  number: 'number',
  boolean: 'boolean',
  object: 'object',
  array: 'array',
};

// 参考: rfc7235
// https://www.iana.org/assignments/http-authschemes/http-authschemes.xhtml
export const HTTP_SCHEMES = {
  basic: {
    name: 'basic',
    title: 'Basic',
  },
  bearer: {
    name: 'bearer',
    title: 'Bearer',
  },
  digest: {
    name: 'digest',
    title: 'Digest',
  },
  hoba: {
    name: 'hoba',
    title: 'HOBA',
  },
  mutual: {
    name: 'mutual',
    title: 'Mutual',
  },
  negotiate: {
    name: 'negotiate',
    title: 'Negotiate',
  },
  oauth: {
    name: 'OAuth',
    title: 'oauth',
  },
  scramSha1: {
    name: 'scrum-sha-1',
    title: 'SCRAM-SHA-1',
  },
  scramSha256: {
    name: 'scrum-sha-256',
    title: 'SCRAM-SHA-256',
  },
  vapid: {
    name: 'vapid',
    title: 'vapid',
  },
};

export const HTTP_SCHEME_LIST = Object.values(HTTP_SCHEMES);

export const API_KEY_LOCATIONS = {
  query: {
    name: 'query',
    title: 'query',
  },
  header: {
    name: 'header',
    title: 'header',
  },
  cookie: {
    name: 'cookie',
    title: 'cookie',
  },
};

export const API_KEY_LOCATION_LIST = Object.values(API_KEY_LOCATIONS);

export const PARAMETER_LOCATIONS = {
  query: 'query',
  header: 'header',
  path: 'path',
  cookie: 'cookie',
};

export const PARAMETER_LOCATION_LIST = Object.values(PARAMETER_LOCATIONS);

export const PARAMETER_STYLES = {
  simple: 'simple',
  form: 'form',
  label: 'label',
  matrix: 'matrix',
  spaceDelimited: 'spaceDelimited',
  pipeDelimited: 'pipeDelimited',
  deepObject: 'deepObject',
};

export const PARAMETER_STYLE_LIST = Object.values(PARAMETER_STYLES);

export const DATE_FORMAT = 'YYYY-MM-DD';

export const DATETIME_FORMAT = 'YYYY-MM-DD HH:mm:ss';

export const OPERATION_TABS_STYLE_LIST = [
  OAS_COMPONENT_TYPES.requestBody.name,
  OAS_COMPONENT_TYPES.parameter.series,
  OAS_COMPONENT_TYPES.response.series,
  'security',
  OAS_COMPONENT_TYPES.callback.series,
  'tags',
  'externalDocs',
  'servers',
];
