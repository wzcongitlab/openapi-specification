// 配置Oas定义的所有对象的页面展示的标题和跳转到文档的锚点;
export const OAS_OBJECTS = {
  callbacks: {
    name: 'callbacks',
    title: 'Callbacks',
    documentationAnchor: '#callbackObject',
  },
  components: {
    name: 'components',
    title: 'Components',
    documentationAnchor: '#componentsObject',
  },
  contact: {
    name: 'contact',
    title: 'Contact',
    documentationAnchor: '#contactObject',
  },
  discriminator: {
    name: 'discriminator',
    title: 'Discriminator',
    documentationAnchor: '#discriminatorObject',
  },
  encoding: {
    name: 'encoding',
    title: 'Encoding',
    documentationAnchor: '#encodingObject',
  },
  examples: {
    name: 'examples',
    title: 'Examples',
    documentationAnchor: '#exampleObject',
  },
  externalDocs: {
    name: 'externalDocs',
    title: 'External Docs',
    documentationAnchor: '#externalDocumentationObject',
  },
  headers: {
    name: 'headers',
    title: 'Headers',
    documentationAnchor: '#headerObject',
  },
  info: {
    name: 'info',
    title: 'Info',
    documentationAnchor: '#infoObject',
  },
  license: {
    name: 'license',
    title: 'License',
    documentationAnchor: '#licenseObject',
  },
  links: {
    name: 'links',
    title: 'Links',
    documentationAnchor: '#linkObject',
  },
  mediaType: {
    name: 'mediaType',
    title: 'Media Type',
    documentationAnchor: '#mediaTypeObject',
  },
  content: {
    name: 'content',
    title: 'Content',
    documentationAnchor: '#mediaTypeObject',
  },
  oauthFlow: {
    name: 'oauthFlow',
    title: 'OAuthFlow',
    documentationAnchor: '#oauthFlowObject',
  },
  oauthFlows: {
    name: 'oauthFlows',
    title: 'OAuthFlows',
    documentationAnchor: '#oauthFlowsObject',
  },
  operations: {
    name: 'operations',
    title: 'Operations',
    documentationAnchor: '#operationObject',
  },
  get: {
    name: 'get',
    title: 'Get',
    documentationAnchor: '#operationObject',
  },
  put: {
    name: 'put',
    title: 'Put',
    documentationAnchor: '#operationObject',
  },
  post: {
    name: 'post',
    title: 'Post',
    documentationAnchor: '#operationObject',
  },
  delete: {
    name: 'delete',
    title: 'Delete',
    documentationAnchor: '#operationObject',
  },
  options: {
    name: 'options',
    title: 'Options',
    documentationAnchor: '#operationObject',
  },
  head: {
    name: 'head',
    title: 'Head',
    documentationAnchor: '#operationObject',
  },
  patch: {
    name: 'patch',
    title: 'Patch',
    documentationAnchor: '#operationObject',
  },
  trace: {
    name: 'trace',
    title: 'Trace',
    documentationAnchor: '#operationObject',
  },
  parameters: {
    name: 'parameters',
    title: 'Parameters',
    documentationAnchor: '#parameterObject',
  },
  pathItem: {
    name: 'pathItem',
    title: 'PathItem',
    documentationAnchor: '#pathItemObject',
  },
  paths: {
    name: 'paths',
    title: 'Paths',
    documentationAnchor: '#pathsObject',
  },
  reference: {
    name: 'reference',
    title: 'Reference',
    documentationAnchor: '#referenceObject',
  },
  requestBody: {
    name: 'requestBody',
    title: 'RequestBody',
    documentationAnchor: '#requestBodyObject',
  },
  requestBodies: {
    name: 'requestBodies',
    title: 'RequestBodies',
    documentationAnchor: '#requestBodyObject',
  },
  response: {
    name: 'response',
    title: 'Response',
    documentationAnchor: '#responseObject',
  },
  responses: {
    name: 'responses',
    title: 'Responses',
    documentationAnchor: '#responsesObject',
  },
  schema: {
    name: 'schema',
    title: 'Schema',
    documentationAnchor: '#schemaObject',
  },
  schemas: {
    name: 'schemas',
    title: 'Schemas',
    documentationAnchor: '#schemaObject',
  },
  properties: {
    name: 'properties',
    title: 'Properties',
    documentationAnchor: '#schemaObject',
  },
  security: {
    name: 'security',
    title: 'Security',
    documentationAnchor: '#securityRequirementObject',
  },
  securitySchemes: {
    name: 'securitySchemes',
    title: 'SecuritySchemes',
    documentationAnchor: '#securitySchemeObject',
  },
  servers: {
    name: 'servers',
    title: 'Servers',
    documentationAnchor: '#serverObject',
  },
  variables: {
    name: 'variables',
    title: 'Variables',
    documentationAnchor: '#serverVariableObject',
  },
  tags: {
    name: 'tags',
    title: 'Tags',
    documentationAnchor: '#tagObject',
  },
  xml: {
    name: 'xml',
    title: 'XML',
    documentationAnchor: '#xmlObject',
  },
  flows: {
    name: 'flows',
    title: 'Flows',
    documentationAnchor: '#oauthFlowObject',
  },
  oauthFlowScopes: {
    name: 'oauthFlowScopes',
    title: 'OAuth Flow Scopes',
    documentationAnchor: null,
  },
  extensions: {
    name: 'extensions',
    title: 'Extensions',
    documentationAnchor: '#specificationExtensions',
  },
  pattern: {
    name: 'pattern',
    title: 'Pattern',
    documentationAnchor: null,
  },
  operationReference: {
    name: 'operationReference',
    title: 'Operation Reference',
    documentationAnchor: null,
  },
};
