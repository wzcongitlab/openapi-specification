import { CONSTANTS } from './constants';
import { PARAMETER_STYLES, PARAMETER_LOCATIONS } from './oasConfig';

export const PARAMETER_LOCATIONS_STYLES = {
  [PARAMETER_LOCATIONS.query]: {
    type: PARAMETER_LOCATIONS.query,
    styleOptions: [
      {
        value: CONSTANTS.unavailable,
        text: CONSTANTS.unavailable,
      },
      {
        value: PARAMETER_STYLES.form,
        text: PARAMETER_STYLES.form,
      },
      {
        value: PARAMETER_STYLES.spaceDelimited,
        text: PARAMETER_STYLES.spaceDelimited,
      },
      {
        value: PARAMETER_STYLES.pipeDelimited,
        text: PARAMETER_STYLES.pipeDelimited,
      },
      {
        value: PARAMETER_STYLES.deepObject,
        text: PARAMETER_STYLES.deepObject,
      },
    ],
  },
  [PARAMETER_LOCATIONS.header]: {
    type: PARAMETER_LOCATIONS.header,
    styleOptions: [
      {
        value: CONSTANTS.unavailable,
        text: CONSTANTS.unavailable,
      },
      {
        value: PARAMETER_STYLES.simple,
        text: PARAMETER_STYLES.simple,
      },
    ],
  },
  [PARAMETER_LOCATIONS.path]: {
    type: PARAMETER_LOCATIONS.path,
    styleOptions: [
      {
        value: CONSTANTS.unavailable,
        text: CONSTANTS.unavailable,
      },
      {
        value: PARAMETER_STYLES.matrix,
        text: PARAMETER_STYLES.matrix,
      },
      {
        value: PARAMETER_STYLES.label,
        text: PARAMETER_STYLES.label,
      },
      {
        value: PARAMETER_STYLES.simple,
        text: PARAMETER_STYLES.simple,
      },
    ],
  },
  [PARAMETER_LOCATIONS.cookie]: {
    type: PARAMETER_LOCATIONS.cookie,
    styleOptions: [
      {
        value: CONSTANTS.unavailable,
        text: CONSTANTS.unavailable,
      },
      {
        value: PARAMETER_STYLES.form,
        text: PARAMETER_STYLES.form,
      },
    ],
  },
};

export const PARAMETER_TYPE_STYLE_MAPPING = [
  {
    type: CONSTANTS.unavailable,
    styleOptions: [
      {
        value: CONSTANTS.unavailable,
        text: CONSTANTS.unavailable,
      },
    ],
  },
  ...Object.values(PARAMETER_LOCATIONS_STYLES),
];
