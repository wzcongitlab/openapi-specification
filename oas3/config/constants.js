/*
sep: separator(分隔符)的简写；
pathSep: 节点path分割符, pathKeySep节点pathKey分割符;
*/
export const CONSTANTS = {
  emptyStr: '',
  emailExample: 'aa@cc.com',
  unavailable: 'N/A',
  notValid: 'N/V',
  trueAsString: 'true',
  falseAsString: 'false',
  jsonFieldTypes: {
    undefined: 'undefined',
    string: 'string',
    number: 'number',
    object: 'object',
    boolean: 'boolean',
  },
  oas: {
    version: '3.0.2',
    // pathSep 系统保留字符, 用于OAS文档节点路径层次的分隔符;
    pathSep: '﹒',
    pathSepLength: 1,
    pathSepRegex: /﹒/g,
    reservedPropPrefix: '_',
    pathKeySep: '﹒',
    extensivePropRegex: /^x-.+/,
    objectParentProp: '_parent',
    valueProp: 'value',
    equivalentValueProps: ['exampleValue'],
    oasNodeIdKey: '_oasNodeId',
    rootNodeName: 'document',
    booleanFieldType: 'BooleanField',
    anyFieldType: 'AnyField',
    refStringPrefix: '#/',
    refPathSep: '/',
    refPathSepRegex: /\//gm,
    uiReservedProps: [
      // 所有oadNode的UI保留属性;
      '_documentation',
      '_parent',
      '_oasNodeId',
      '_nodeName',
      '_path',
      '_validation',
      // extensiveNode的UI保留属性;
      '_extensiveProps',
      // parameter对象的UI保留属性;
      '_enableContent',
      // oasMapNode对象的UI保留属性;
      '_itemKeys',
    ],
  },
};
