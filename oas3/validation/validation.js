import { CONSTANTS } from '../config/constants';
import { Utils } from '../../utils/utils';

class ValidationInfo {
  constructor(summary = CONSTANTS.emptyStr, description = CONSTANTS.emptyStr) {
    this.summary = summary;
    this.description = description;
  }
}

class Error extends ValidationInfo {
  /* error 用于提示用户oas编辑器页面上的逻辑错误, 必填项等;
     例如: serverVariable的default值应是enum的成员之一;
          oas的title是必填字段;
          url的格式不正确;
   */
  constructor(summary = CONSTANTS.emptyStr, description = CONSTANTS.emptyStr) {
    super(summary, description);
  }
}

class Warning extends ValidationInfo {
  /* warning 用于提示用户关于当前编辑的oas节点的警告信息,
     例如: property与object的嵌套层次不建议超过3层; 重复的枚举值定义等;
   */
  constructor(summary = CONSTANTS.emptyStr, description = CONSTANTS.emptyStr) {
    super(summary, description);
  }
}

export class Validation {
  constructor(
    { hasError = false, summary = CONSTANTS.emptyStr, description = CONSTANTS.emptyStr } = {},
    oasNode = null
  ) {
    this.path = oasNode.path;
    this.pathKey = oasNode.pathKey;
    this.update({
      hasError,
      summary,
      description,
    });
  }

  reset() {
    this.update();
  }

  update({
    hasError = false,
    summary = CONSTANTS.emptyStr,
    description = CONSTANTS.emptyStr,
  } = {}) {
    this.hasError = hasError;
    this.hasWarning = false;
    this.warning = null;
    this.error = hasError ? new Error(summary, description) : null;
    if (!hasError && (Utils.isNotEmptyString(summary) || Utils.isNotEmptyString(description))) {
      this.hasWarning = true;
      this.warning = new Warning(summary, description);
    }
  }
}
