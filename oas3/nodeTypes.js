import { AnyField } from './fields/any.field';
import { BooleanField } from './fields/boolean.field';
import { EmailField } from './fields/email.field';
import { ExampleValueField } from './fields/exampleValue.field';
import { NumberField } from './fields/number.field';
import { IntegerField } from './fields/integer.field';
import { RichTextField } from './fields/richText.field';
import { RefField } from './fields/ref.field';
import { RuntimeExpressionField } from './fields/runtimeExpression.field';
import { StringField } from './fields/string.field';
import { EncodingMap } from './objects/common/encodingMap.model';
import { EnumValues } from './objects/common/enumValues.model';
import { Encoding } from './objects/common/encoding.model';
import { ExternalDocs } from './objects/common/externalDocs.model';
import { NumberEnumValues } from './objects/common/numberEnumValues.model';
import { Media } from './objects/common/media.model';
import { PayloadContent } from './objects/common/payloadContent.model';
import { RequiredFields } from './objects/common/requiredFields.model';
import { UrlField } from './fields/url.field';
import { Reference } from './objects/common/reference.model';
import { SecurityRequirement } from './objects/common/securityRequirement.model';
import { SecurityRequirementScopes } from './objects/common/securityRequirementScopes.model';
import { Security } from './objects/common/security.model';
import { ServerVariable } from './objects/common/serverVariable.model';
import { Servers } from './objects/common/servers.model';
import { Server } from './objects/common/server.model';
import { ServerVariables } from './objects/common/serverVariables.model';
import { Tag } from './objects/common/tag.model';
import { Document } from './objects/document/document.model';
import { Contact } from './objects/document/contact.model';
import { Tags } from './objects/common/tags.model';
import { License } from './objects/document/license.model';
import { Info } from './objects/document/info.model';
import { Callbacks } from './objects/components/callbacks.model';
import { Callback } from './objects/components/callback.model';
import { Example } from './objects/components/example.model';
import { Components } from './objects/components/components.model';
import { Headers } from './objects/components/headers.model';
import { Examples } from './objects/components/examples.model';
import { Link } from './objects/components/link.model';
import { Header } from './objects/components/header.model';
import { Links } from './objects/components/links.model';
import { Parameter } from './objects/components/parameter.model';
import { Parameters } from './objects/components/parameters.model';
import { LinkParameters } from './objects/components/linkParameters.model';
import { RequestBody } from './objects/components/requestBody.model';
import { Response } from './objects/components/response.model';
import { RequestBodies } from './objects/components/requestBodies.model';
import { OperationTags } from './objects/paths/opeartionTags.model';
import { Responses } from './objects/components/responses.model';
import { Schemas } from './objects/components/schemas.model';
import { Operation } from './objects/paths/opreation.model';
import { PathItem } from './objects/paths/pathItem.model';
import { ParameterList } from './objects/paths/parameterList.model';
import { Paths } from './objects/paths/paths.model';
import { AllOf } from './objects/common/schema/allOf.model';
import { BooleanSchema } from './objects/common/schema/booleanSchema.model';
import { ArraySchema } from './objects/common/schema/arraySchema.model';
import { CompositionalSchema } from './objects/common/schema/compositionalSchema.model';
import { EmptyJsonSchema } from './objects/common/schema/emptyJsonSchema.model';
import { Discriminator } from './objects/common/schema/discriminator.model';
import { DiscriminatorMapping } from './objects/common/schema/discriminatorMapping.model';
import { Not } from './objects/common/schema/not.model';
import { JsonSchema } from './objects/common/schema/jsonSchema.model';
import { IntegerSchema } from './objects/common/schema/integerSchema.model';
import { ObjectSchema } from './objects/common/schema/objectSchema.model';
import { NumberSchema } from './objects/common/schema/numberSchema.model';
import { StringSchema } from './objects/common/schema/stringSchema.model';
import { AnyOf } from './objects/common/schema/anyOf.model';
import { OasSchema } from './objects/common/schema/oasSchema.model';
import { Properties } from './objects/common/schema/properties.model';
import { Xml } from './objects/common/schema/xml.model';
import { OneOf } from './objects/common/schema/oneOf.model';
import { AuthorizationCodeOAuthFlow } from './objects/components/securitySchemes/authorizationCodeOAuthFlow.model';
import { ClientCredentialsOAuthFlow } from './objects/components/securitySchemes/clientCredentialsOAuthFlow.model';
import { ApiKeySecurityScheme } from './objects/components/securitySchemes/apiKeySecurityScheme.model';
import { EmptySecuritySchemeModel } from './objects/components/securitySchemes/emptySecurityScheme.model';
import { OAuth2SecurityScheme } from './objects/components/securitySchemes/oauth2SecurityScheme.model';
import { OAuthFlow } from './objects/components/securitySchemes/oauthFlow.model';
import { OAuthFlowScopes } from './objects/components/securitySchemes/oauthFlowScopes.model';
import { OAuthFlows } from './objects/components/securitySchemes/oauthFlows.model';
import { HttpSecurityScheme } from './objects/components/securitySchemes/httpSecurityScheme.model';
import { SecurityScheme } from './objects/components/securitySchemes/securityScheme.model';
import { ImplicitOAuthFlow } from './objects/components/securitySchemes/implicitOAuthFlow.model';
import { PasswordOAuthFlow } from './objects/components/securitySchemes/passwordOAuthFlow.model';
import { SecuritySchemes } from './objects/components/securitySchemes/securitySchemes.model';
import { OpenIdSecurityScheme } from './objects/components/securitySchemes/openIdSecurityScheme.model';

export const OAS_NODE_TYPES = {
  AnyField,
  BooleanField,
  EmailField,
  ExampleValueField,
  NumberField,
  IntegerField,
  RichTextField,
  RefField,
  RuntimeExpressionField,
  StringField,
  EncodingMap,
  EnumValues,
  Encoding,
  ExternalDocs,
  NumberEnumValues,
  Media,
  PayloadContent,
  RequiredFields,
  UrlField,
  Reference,
  SecurityRequirement,
  SecurityRequirementScopes,
  Security,
  ServerVariable,
  Servers,
  Server,
  ServerVariables,
  Tag,
  Document,
  Contact,
  Tags,
  License,
  Info,
  Callbacks,
  Callback,
  Example,
  Components,
  Headers,
  Examples,
  Link,
  Header,
  Links,
  Parameter,
  Parameters,
  LinkParameters,
  RequestBody,
  Response,
  RequestBodies,
  OperationTags,
  Responses,
  Schemas,
  Operation,
  PathItem,
  ParameterList,
  Paths,
  AllOf,
  BooleanSchema,
  ArraySchema,
  CompositionalSchema,
  EmptyJsonSchema,
  Discriminator,
  DiscriminatorMapping,
  Not,
  JsonSchema,
  IntegerSchema,
  ObjectSchema,
  NumberSchema,
  StringSchema,
  AnyOf,
  OasSchema,
  Properties,
  Xml,
  OneOf,
  AuthorizationCodeOAuthFlow,
  ClientCredentialsOAuthFlow,
  ApiKeySecurityScheme,
  EmptySecuritySchemeModel,
  OAuth2SecurityScheme,
  OAuthFlow,
  OAuthFlowScopes,
  OAuthFlows,
  HttpSecurityScheme,
  SecurityScheme,
  ImplicitOAuthFlow,
  PasswordOAuthFlow,
  SecuritySchemes,
  OpenIdSecurityScheme,
};
