import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Security } from './security.model';

const assert = chai.assert;

describe('OpenApi top level Security', () => {
  let security;
  let params = [
    {
      api_key: [],
    },
    {
      petstore_auth: ['write:pets', 'read:pets'],
    },
  ];

  let petstoreAuth = {
    petstore_auth: ['write:pets', 'read:pets'],
  };

  describe('#fields', () => {
    describe('#data extraction', () => {
      it('can extract correct oas data', () => {
        security = new Security(params);
        assert.equal(Utils.deepEqual(params, security.extract()), true);
      });
    });
    describe('#functions', () => {
      it('can add security requirement item to security object', () => {
        security = new Security([]);
        security.addSecurityRequirement(petstoreAuth);
        let len = security.length;
        let newItem = security.value[len - 1];
        assert.equal(Utils.deepEqual(petstoreAuth, newItem.extract()), true);
      });
      it('can remove security requirement item from security object', () => {
        security = new Security([]);
        security.addSecurityRequirement(petstoreAuth);
        let len = security.length;
        let newItem = security.value[len - 1];
        assert.equal(Utils.deepEqual(petstoreAuth, newItem.extract()), true);
        security.removeSecurityRequirement(0);
        assert.equal(security.isEmpty, true);
      });
    });
  });
});
