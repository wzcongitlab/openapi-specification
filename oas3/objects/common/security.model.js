import { OasListNode } from '../../base/oasListNode';
import { SecurityRequirement } from './securityRequirement.model';

export class Security extends OasListNode {
  static get nodeObjectType() {
    return 'Security';
  }

  constructor(params = [], parent, nodeName = 'security') {
    super(parent, nodeName);
    if (params instanceof Array) {
      params.forEach(p => this.addSecurityRequirement(p));
    }
  }

  addSecurityRequirement(sr) {
    if (sr instanceof SecurityRequirement) {
      sr.parent = this;
      return this.addItem(sr);
    }
    let newSecurityRequirement = new SecurityRequirement(sr, this);
    return this.addItem(newSecurityRequirement);
  }

  removeSecurityRequirement(index) {
    return this.removeItem(index);
  }
}
