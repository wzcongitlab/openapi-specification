import { OasListNode } from '../../base/oasListNode';
import { StringField } from '../../fields/string.field';

export class RequiredFields extends OasListNode {
  static get nodeObjectType() {
    return 'RequiredFields';
  }

  constructor(params = [], parent, nodeName = 'required') {
    super(parent, nodeName);
    this.update(params);
  }

  addField(f) {
    if (f instanceof StringField) {
      f.parent = this;
      return this.addItem(f);
    }
    let newField = new StringField(f, this);
    return this.addItem(newField);
  }

  removeField(index) {
    return this.removeItem(index);
  }

  update(requiredFields) {
    this.value = [];
    if (requiredFields instanceof Array) {
      requiredFields.forEach(p => this.addField(p));
    }
  }
}
