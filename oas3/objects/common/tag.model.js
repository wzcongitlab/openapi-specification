import { OasNode } from '../../base/oasNode';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';
import { ExternalDocs } from './externalDocs.model';
import { StringField } from '../../fields/string.field';
import { RichTextField } from '../../fields/richText.field';
import { OAS_ERRORS } from '../../config/oasErrors';

export class Tag extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'Tag';
  }

  constructor(params = {}, parent = null, nodeName = 'tag') {
    super(params, parent, nodeName);
    this.name = new StringField(params.name, this, 'name');
    this.description = new RichTextField(params.description, this, 'description');
    this.externalDocs = params.externalDocs ? new ExternalDocs(params.externalDocs, this) : null;
  }

  addExternalDocs(externalDocs) {
    this.externalDocs = new ExternalDocs(externalDocs, this);
  }

  removeExternalDocs() {
    this.externalDocs = null;
  }

  validateNode() {
    this.name.validation.reset();
    if (this.name.isEmpty) {
      this.name.validation.update({
        hasError: true,
        summary: OAS_ERRORS.DOCUMENT_TAG_NAME_REQUIRED,
      });
    }
  }
}
