import { OasMapNode } from '../../base/oasMapNode';
import { Media } from './media.model';
import { SIMPLE_MIME_TYPES } from '../../config/mimeTypes';

export class PayloadContent extends OasMapNode {
  static get nodeObjectType() {
    return 'PayloadContent';
  }

  constructor(params = {}, parent = null, nodeName = 'content') {
    super(parent, nodeName);
    for (let mimeType of Object.keys(params)) {
      if (SIMPLE_MIME_TYPES.includes(mimeType)) {
        this.addMediaType(mimeType, params[mimeType]);
      }
    }
  }

  addMediaType(mediaType, params) {
    this.addItem(mediaType, new Media(params, this, mediaType));
  }

  removeMediaType(mediaType) {
    this.removeItem(mediaType);
  }

  availableMediaTypes() {
    return SIMPLE_MIME_TYPES.filter(mt => {
      return !this.itemKeys.includes(mt);
    });
  }
}
