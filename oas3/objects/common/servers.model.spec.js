import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Servers } from './servers.model';
import { Server } from './server.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Servers', () => {
  let serversData01 = [];

  let serversData02 = [];

  let servers;

  let serverData = {
    url: 'http://mindoc.skyinno.com/',
    description: 'ddd',
    variables: {},
  };

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        servers = new Servers(serversData01);
        assert.equal(Utils.deepEqual(serversData01, servers.extract()), true);
      });
    });
    describe('#functions', () => {
      it('can add server item to servers', () => {
        servers = new Servers([]);
        servers.addServer(serverData);
        assert.equal(servers.length === 1, true);
        assert.equal(servers.value[0] instanceof Server, true);
        assert.equal(Utils.deepEqual(servers.value[0].extract(), serverData), true);
      });
      it('can remove server item from servers', () => {
        servers = new Servers([]);
        servers.addServer(serverData);
        assert.equal(servers.length === 1, true);
        assert.equal(servers.value[0] instanceof Server, true);
        assert.equal(Utils.deepEqual(servers.value[0].extract(), serverData), true);
        servers.removeServer(0);
        assert.equal(servers.isEmpty, true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate servers data', () => {
      it('can pass validation when given valid data', () => {
        servers = new Servers(serversData01);
        servers.validate();
        assert.equal(servers.validation.hasError, false);
        assert.equal(servers.validation.hasWarning, false);
      });
    });
  });
});
