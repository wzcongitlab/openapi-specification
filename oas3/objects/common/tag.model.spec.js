import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Tag } from './tag.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Tag', () => {
  let tagData01 = {
    name: 'pet',
    description: 'some description',
    externalDocs: {
      url: 'http://www.kato.com/docs/user-guide',
      description: 'some description about user-guide',
    },
  };

  let tagData02 = {
    name: '',
    description: 'some description',
    externalDocs: {
      url: 'http://www.kato.com/docs/user-guide',
      description: 'some description about user-guide',
    },
  };

  let tag;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        tag = new Tag(tagData01);
        assert.equal(Utils.deepEqual(tagData01, tag.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate tag data', () => {
      it('can get error named DOCUMENT_TAG_NAME_REQUIRED', () => {
        tag = new Tag(tagData02);
        tag.validate();
        assert.equal(tag.name.validation.hasError, true);
        assert.equal(tag.name.validation.error.summary, OAS_ERRORS.DOCUMENT_TAG_NAME_REQUIRED);
      });

      it('can pass validation when given valid tag name', () => {
        tag = new Tag(tagData01);
        tag.validate();
        assert.equal(tag.name.validation.hasError, false);
        assert.equal(tag.name.validation.error, null);
      });
    });
  });
});
