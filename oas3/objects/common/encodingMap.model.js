import { OasMapNode } from '../../base/oasMapNode';
import { Encoding } from './encoding.model';

export class EncodingMap extends OasMapNode {
  static get nodeObjectType() {
    return 'EncodingMap';
  }

  constructor(params = {}, parent = null, nodeName = 'encoding') {
    super(parent, nodeName);
    for (let key of Object.keys(params)) {
      this.addEncoding(key, params[key]);
    }
  }

  addEncoding(key, encoding) {
    if (encoding instanceof Encoding) {
      encoding.parent = this;
      this.addItem(key, encoding);
    } else {
      let newEncoding = new Encoding(encoding, this);
      this.addItem(key, newEncoding);
    }
  }

  removeEncoding(key) {
    this.removeItem(key);
  }
}
