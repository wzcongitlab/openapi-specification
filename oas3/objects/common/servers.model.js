import { OasListNode } from '../../base/oasListNode';
import { Server } from './server.model';

export class Servers extends OasListNode {
  static get nodeObjectType() {
    return 'Servers';
  }

  constructor(params = [], parent, nodeName = 'servers') {
    super(parent, nodeName);
    if (params instanceof Array) {
      params.forEach(p => this.addServer(p));
    }
  }

  addServer(server) {
    if (server instanceof Server) {
      server.parent = this;
      return this.addItem(server);
    }
    let newServer = new Server(server, this);
    return this.addItem(newServer);
  }

  removeServer(index) {
    return this.removeItem(index);
  }
}
