import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Tags } from './tags.model';
import { Tag } from './tag.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Tags', () => {
  let tagsData01 = [
    {
      name: 'pet',
      description: 'some description',
      externalDocs: {
        url: 'http://www.kato.com/docs/user-guide',
        description: 'some description about user-guide',
      },
    },
    {
      name: 'pet',
      description: 'some description',
      externalDocs: {
        url: 'http://www.kato.com/docs/user-guide',
        description: 'some description about user-guide',
      },
    },
  ];

  let tagsData02 = [
    {
      name: 'pet',
      description: 'some description',
      externalDocs: {
        url: 'http://www.kato.com/docs/user-guide',
        description: 'some description about user-guide',
      },
    },
    {
      name: 'pet1',
      description: 'some description',
      externalDocs: {
        url: 'http://www.kato.com/docs/user-guide',
        description: 'some description about user-guide',
      },
    },
  ];

  let tags;

  let tagData = {
    name: 'pet',
    description: 'ops on pets',
  };

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        tags = new Tags(tagsData01);
        assert.equal(Utils.deepEqual(tagsData01, tags.extract()), true);
      });
    });
    describe('#functions', () => {
      it('can add tag item to tags', () => {
        tags = new Tags([]);
        tags.addTag(tagData);
        assert.equal(tags.length === 1, true);
        assert.equal(tags.value[0] instanceof Tag, true);
        assert.equal(Utils.deepEqual(tags.value[0].extract(), tagData), true);
      });
      it('can remove tag item from tags', () => {
        tags = new Tags([]);
        tags.addTag(tagData);
        assert.equal(tags.length === 1, true);
        assert.equal(tags.value[0] instanceof Tag, true);
        assert.equal(Utils.deepEqual(tags.value[0].extract(), tagData), true);
        tags.removeTag(0);
        assert.equal(tags.isEmpty, true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate tag data', () => {
      it('can get error named DOCUMENT_TAGS_NAME_NOT_UNIQUE', () => {
        tags = new Tags(tagsData01);
        tags.validate();
        assert.equal(tags.value[0].name.validation.hasError, true);
        assert.equal(
          tags.value[0].name.validation.error.summary,
          OAS_ERRORS.DOCUMENT_TAGS_NAME_NOT_UNIQUE
        );
        assert.equal(tags.value[1].name.validation.hasError, true);
        assert.equal(
          tags.value[1].name.validation.error.summary,
          OAS_ERRORS.DOCUMENT_TAGS_NAME_NOT_UNIQUE
        );
      });

      it('can pass validation when given valid names of tag', () => {
        tags = new Tags(tagsData02);
        tags.validate();
        for (let t of tags.value) {
          assert.equal(t.name.validation.hasError, false);
          assert.equal(t.name.validation.error, null);
          assert.equal(t.name.validation.hasWarning, false);
          assert.equal(t.name.validation.warning, null);
        }
      });
    });
  });
});
