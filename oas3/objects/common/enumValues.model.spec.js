import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { EnumValues } from './enumValues.model';
import { OAS_WARNINGS } from '../../config/oasErrors';

const assert = chai.assert;

describe('EnumValues Class', () => {
  let enumValues;

  let enumData01 = ['v1', 'v2', 'v2', 'v2'];
  let enumData02 = ['v1', 'v2'];

  describe('#fields', () => {
    beforeEach(() => {
      enumValues = new EnumValues(enumData01);
    });

    describe('#data extraction', () => {
      it('can extract correct data.', () => {
        assert.equal(Utils.deepEqual(enumValues.extract(), enumData01), true);
      });
    });
  });

  describe('#validations', () => {
    describe('Validate enumValues', () => {
      it('duplicated enum items can get warning named DUPLICATED_ENUMS', () => {
        enumValues = new EnumValues(enumData01);
        enumValues.validate();
        assert.equal(enumValues.value[1].validation.hasWarning, true);
        assert.equal(enumValues.value[2].validation.hasWarning, true);
        assert.equal(enumValues.value[3].validation.hasWarning, true);
        assert.equal(enumValues.value[1].validation.warning.summary, OAS_WARNINGS.DUPLICATED_ENUMS);
        assert.equal(enumValues.value[2].validation.warning.summary, OAS_WARNINGS.DUPLICATED_ENUMS);
        assert.equal(enumValues.value[3].validation.warning.summary, OAS_WARNINGS.DUPLICATED_ENUMS);
      });

      it('can pass validation when given valid enum values', () => {
        enumValues = new EnumValues(enumData02);
        enumValues.validate();
        for (let e of enumValues.value) {
          assert.equal(e.validation.hasError, false);
          assert.equal(e.validation.hasWarning, false);
          assert.equal(e.validation.warning, null);
          assert.equal(e.validation.error, null);
        }
      });
    });
  });
});
