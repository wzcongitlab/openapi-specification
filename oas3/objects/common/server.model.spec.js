import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Server } from './server.model';
import { OAS_WARNINGS, OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Server', () => {
  let server;

  let server01 = {
    url: 'http://petstore.swagger.io/v2',
    description: 'v2 server',
    variables: {
      version: {
        default: 'v1',
        description: 'version',
        enum: ['v1', 'v2'],
      },
    },
  };

  let server02 = {
    url: '',
    description: 'v2 server',
    variables: {
      version: {
        default: 'v1',
        description: 'version',
        enum: ['v1', 'v2'],
      },
    },
  };

  describe('#fields', () => {
    beforeEach(() => {
      server = new Server(server01);
    });
    describe('#data extraction', () => {
      it('can extract correct data.', () => {
        assert.equal(Utils.deepEqual(server.extract(), server01), true);
      });
    });
  });

  describe('#validations', () => {
    describe('validate server data', () => {
      it('can get warning named SERVER_VARIABLE_NOT_USED', () => {
        server = new Server(server01);
        server.validate();
        assert.equal(server.variables.version.validation.hasWarning, true);
        assert.equal(server.variables.version.validation.hasError, false);
        assert.equal(
          server.variables.version.validation.warning.summary,
          OAS_WARNINGS.SERVER_VARIABLE_NOT_USED
        );
      });

      it('can get error named SERVER_URL_REQUIRED', () => {
        server = new Server(server02);
        server.validate();
        assert.equal(server.url.validation.hasWarning, false);
        assert.equal(server.url.validation.hasError, true);
        assert.equal(server.url.validation.error.summary, OAS_ERRORS.SERVER_URL_REQUIRED);
      });

      it('can pass validation when given valid server url', () => {
        server = new Server(server01);
        server.validate();
        assert.equal(server.url.validation.hasWarning, false);
        assert.equal(server.url.validation.hasError, false);
        assert.equal(server.url.validation.error, null);
        assert.equal(server.url.validation.warning, null);
      });
    });
  });
});
