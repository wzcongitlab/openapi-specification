import { OasMapNode } from '../../base/oasMapNode';
import { ServerVariable } from './serverVariable.model';

export class ServerVariables extends OasMapNode {
  static get nodeObjectType() {
    return 'ServerVariables';
  }

  constructor(params = {}, parent, nodeName = 'variables') {
    super(parent, nodeName);
    for (let key of Object.keys(params)) {
      this.addVariable(key, params[key]);
    }
  }

  addVariable(key, variable) {
    if (variable instanceof ServerVariable) {
      variable.parent = this;
      return this.addItem(key, variable);
    } else {
      return this.addItem(key, new ServerVariable(variable, this, key));
    }
  }

  removeVariable(key) {
    return this.removeItem(key);
  }
}
