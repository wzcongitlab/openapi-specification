import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { ServerVariable } from './serverVariable.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('ServerVariable', () => {
  let dataFields = ['default', 'description', 'enum'];
  let serverVariableData01 = {
    default: 'v1',
    description: 'version',
    enum: ['v1', 'v2'],
  };
  let serverVariableData02 = {
    default: '',
    description: 'version',
    enum: [],
  };
  let serverVariableData03 = {
    default: 'test',
    description: 'version',
    enum: ['v1', 'v2'],
  };
  let serverVariable;

  describe('#fields', () => {
    beforeEach(() => {
      serverVariable = new ServerVariable(serverVariableData01);
    });
    describe('data extraction', () => {
      it('can extract correct data.', () => {
        assert.equal(Utils.deepEqual(serverVariableData01, serverVariable.extract()), true);
      });
    });
  });

  describe('#validations', () => {
    describe('validate serverVariable data', () => {
      it('can ge error named SERVER_VARIABLE_DEFAULT_REQUIRED', () => {
        let sv = new ServerVariable(serverVariableData02);
        sv.validate();
        assert.equal(sv.default.validation.hasError, true);
        assert.equal(sv.default.validation.hasWarning, false);
        assert.equal(sv.default.validation.warning, null);
        assert.equal(
          sv.default.validation.error.summary,
          OAS_ERRORS.SERVER_VARIABLE_DEFAULT_REQUIRED
        );
      });

      it('can ge error named SERVER_VARIABLE_DEFAULT_NOT_IN_ENUM', () => {
        let sv = new ServerVariable(serverVariableData03);
        sv.validate();
        assert.equal(sv.default.validation.hasError, false);
        assert.equal(sv.default.validation.hasWarning, true);
        assert.equal(sv.default.validation.error, null);
        assert.equal(
          sv.default.validation.warning.summary,
          OAS_ERRORS.SERVER_VARIABLE_DEFAULT_NOT_IN_ENUM
        );
      });

      it('can pass validation when given valid default value and enum values', () => {
        let sv = new ServerVariable(serverVariableData01);
        sv.validate();
        for (let key of dataFields) {
          assert.equal(sv[key].validation.hasError, false);
          assert.equal(sv[key].validation.hasWarning, false);
          assert.equal(sv[key].validation.warning, null);
          assert.equal(sv[key].validation.error, null);
        }
        for (let e of sv.enum.value) {
          assert.equal(e.validation.hasError, false);
          assert.equal(e.validation.hasWarning, false);
          assert.equal(e.validation.warning, null);
          assert.equal(e.validation.error, null);
        }
      });
    });
  });
});
