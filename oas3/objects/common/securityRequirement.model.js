import { CONSTANTS } from '../../config/constants';
import { OasMapNode } from '../../base/oasMapNode';
import { SecurityRequirementScopes } from './securityRequirementScopes.model';

export class SecurityRequirement extends OasMapNode {
  static get nodeObjectType() {
    return 'SecurityRequirement';
  }

  constructor(params = {}, parent = null, nodeName = CONSTANTS.emptyStr) {
    super(parent, nodeName);
    for (let key of Object.keys(params)) {
      this.addItem(key, new SecurityRequirementScopes(params[key], this, key));
    }
  }

  updateSecurityRequirementItem(key, params) {
    this.updateItem(key, new SecurityRequirementScopes(params, this, key));
  }

  removeSecurityRequirementItem(key) {
    this.removeItem(key);
  }
}
