import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Media } from './media.model';
import { RequestBody } from '../components/requestBody.model';
import { Parameter } from '../components/parameter.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Media', () => {
  let mediaData01 = {
    schema: {
      example: '',
      type: 'string',
      format: 'password',
      description: '',
      pattern: '',
      default: '',
      enum: [],
      title: 'title001',
      nullable: false,
      readOnly: false,
      writeOnly: false,
      deprecated: false,
    },
    example: 'example75',
    encoding: {},
    examples: {},
  };

  let mediaData02 = {
    schema: {
      example: '',
      type: 'integer',
      format: 'int32',
      description: '',
      pattern: '',
      default: '',
      enum: [],
      title: 'title001',
    },
    example: 'example75',
    encoding: {},
    examples: {},
  };

  let mediaData03 = {
    schema: {
      example: '',
      type: 'integer',
      format: 'int32',
      description: '',
      pattern: '',
      default: '',
      enum: [],
      title: 'title001',
    },
    example: 'example9',
    examples: {
      ex1: {
        externalValue: 'ssss',
        value: 111,
      },
    },
    encoding: {},
  };

  let requestBodyData01 = {
    required: false,
    description: 'description97',
    content: {
      'application/json': {
        schema: {
          oneOf: [
            {
              $ref: '#/components/schemas/Pet',
            },
          ],
        },
        encoding: {
          petImage: {
            style: 'form',
            contentType: 'application/xml; charset=utf-8',
            explode: true,
            allowReserved: true,
            headers: {},
          },
        },
      },
    },
  };
  let requestBodyData02 = {
    required: false,
    description: 'description97',
    content: {
      'application/x-www-form-urlencoded': {
        schema: {
          oneOf: [
            {
              $ref: '#/components/schemas/Pet',
            },
          ],
        },
        encoding: {
          petImage: {
            style: 'form',
            contentType: 'application/xml; charset=utf-8',
            explode: true,
            allowReserved: true,
            headers: {},
          },
        },
      },
    },
  };

  let parameterData01 = {
    name: 'userId',
    in: 'path',
    description: 'description86',
    required: 'N/A',
    deprecated: false,
    allowEmptyValue: false,
    style: '',
    explode: false,
    allowReserved: false,
    examples: {
      ex1: {
        externalValue: 'ssss',
        value: 111,
      },
    },
    content: {
      'application/json': {
        schema: {
          oneOf: [
            {
              $ref: '#/components/schemas/Pet',
            },
          ],
        },
        encoding: {
          petImage: {
            style: 'form',
            contentType: 'application/xml; charset=utf-8',
            explode: true,
            allowReserved: true,
            headers: {},
          },
        },
      },
    },
  };

  let parameterData02 = {
    name: 'userId',
    in: 'path',
    description: 'description86',
    required: 'N/A',
    deprecated: false,
    allowEmptyValue: false,
    style: '',
    explode: false,
    allowReserved: false,
    examples: {
      ex1: {
        externalValue: 'ssss',
        value: 111,
      },
    },
    content: {
      'application/x-www-form-urlencoded': {
        schema: {
          oneOf: [
            {
              $ref: '#/components/schemas/Pet',
            },
          ],
        },
        encoding: {
          petImage: {
            style: 'form',
            contentType: 'application/xml; charset=utf-8',
            explode: true,
            allowReserved: true,
            headers: {},
          },
        },
      },
    },
  };

  let media;

  let requestBody;

  let parameter;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        media = new Media(mediaData01);
        assert.equal(Utils.deepEqual(mediaData01, media.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate media data', () => {
      it('can get error named EXCLUSIVE_EXAMPLE_AND_EXAMPLES', () => {
        media = new Media(mediaData03);
        media.validate();
        assert.equal(media.validation.hasError, true);
        assert.equal(media.validation.error.summary, OAS_ERRORS.EXCLUSIVE_EXAMPLE_AND_EXAMPLES);
      });

      it('can get error named INEFFECTIVE_ENCODING (RequestBody MediaType:application/json)', () => {
        requestBody = new RequestBody(requestBodyData01);
        let encoding = requestBody.content['application/json'].encoding;
        requestBody.validate();
        assert.equal(encoding.validation.hasWarning, true);
        assert.equal(encoding.validation.warning.summary, OAS_ERRORS.INEFFECTIVE_ENCODING);
      });

      it(
        'should not get error named INEFFECTIVE_ENCODING (RequestBody' +
          ' MediaType:application/x-www-form-urlencoded)',
        () => {
          requestBody = new RequestBody(requestBodyData02);
          let encoding = requestBody.content['application/x-www-form-urlencoded'].encoding;
          requestBody.validate();
          assert.equal(encoding.validation.hasWarning, false);
          assert.equal(encoding.validation.warning, null);
        }
      );

      it('can get error named INEFFECTIVE_ENCODING (Parameter MediaType:application/json)', () => {
        parameter = new Parameter(parameterData01);
        let encoding = parameter.content['application/json'].encoding;
        parameter.validate();
        assert.equal(encoding.validation.hasWarning, true);
        assert.equal(encoding.validation.warning.summary, OAS_ERRORS.INEFFECTIVE_ENCODING);
      });

      it('can get error named INEFFECTIVE_ENCODING (Parameter MediaType:application/x-www-form-urlencoded)', () => {
        parameter = new Parameter(parameterData02);
        let encoding = parameter.content['application/x-www-form-urlencoded'].encoding;
        parameter.validate();
        assert.equal(encoding.validation.hasWarning, true);
        assert.equal(encoding.validation.warning.summary, OAS_ERRORS.INEFFECTIVE_ENCODING);
      });

      it('can pass validation when given valid data', () => {
        media = new Media(mediaData01);
        media.validate();
        assert.equal(media.validation.hasError, false);
        assert.equal(media.validation.hasWarning, false);
      });
    });
  });
});
