import { OasListNode } from '../../base/oasListNode';
import { NumberField } from '../../fields/number.field';
import { OAS_WARNINGS } from '../../config/oasErrors';

export class NumberEnumValues extends OasListNode {
  static get nodeObjectType() {
    return 'NumberEnumValues';
  }

  constructor(params = [], parent = null, nodeName = 'enum') {
    super(parent, nodeName);
    this.value = [];
    if (params instanceof Array) {
      params.forEach(p => this.addEnum(p));
    }
  }

  get isEmpty() {
    return this.value.length === 0;
  }

  get values() {
    return this.value.map(v => {
      return v.value;
    });
  }

  addEnum(e) {
    if (e instanceof NumberField) {
      e.parent = this;
      this.value.push(e);
      return;
    }
    this.value.push(new NumberField(e, this, 0));
  }

  removeEnum(index) {
    return this.removeItem(index);
  }

  validateNode() {
    for (let e of this.value) {
      this.value.forEach(v => {
        if (v === e) {
          return;
        }
        if (v.validation.hasError) {
          return;
        }
        if (v.value === e.value) {
          v.validation.update({
            summary: OAS_WARNINGS.DUPLICATED_ENUMS,
          });
          e.validation.update({
            summary: OAS_WARNINGS.DUPLICATED_ENUMS,
          });
        }
      });
    }
  }
}
