import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { SecurityRequirementScopes } from './securityRequirementScopes.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('SecurityRequirementScopes', () => {
  let securityRequirementScopesData01 = [];

  let securityRequirementScopesData02 = [];

  let securityRequirementScopes;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        securityRequirementScopes = new SecurityRequirementScopes(securityRequirementScopesData01);
        assert.equal(
          Utils.deepEqual(securityRequirementScopesData01, securityRequirementScopes.extract()),
          true
        );
      });
    });
  });

  describe('#validation', () => {
    describe('validate securityRequirementScopes data', () => {
      it('can pass validation when given valid data', () => {
        securityRequirementScopes = new SecurityRequirementScopes(securityRequirementScopesData01);
        securityRequirementScopes.validate();
        assert.equal(securityRequirementScopes.validation.hasError, false);
        assert.equal(securityRequirementScopes.validation.error, null);
      });
    });
  });
});
