import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Encoding } from './encoding.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Encoding', () => {
  let encodingData01 = {
    contentType: 'contentType8',
    headers: {},
    style: 'style63',
    explode: true,
    allowReserved: true,
  };

  let encodingData02 = {
    contentType: 'contentType8',
    headers: {},
    style: 'style63',
    explode: true,
    allowReserved: true,
  };

  let encoding;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        encoding = new Encoding(encodingData01);
        assert.equal(Utils.deepEqual(encodingData01, encoding.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate encoding data', () => {
      it('can pass validation when given valid data', () => {
        encoding = new Encoding(encodingData01);
        encoding.validate();
        assert.equal(encoding.validation.hasError, false);
        assert.equal(encoding.validation.error, null);
      });
    });
  });
});
