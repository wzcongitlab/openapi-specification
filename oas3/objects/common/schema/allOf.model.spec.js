import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { AllOf } from './allOf.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('AllOf', () => {
  let allOfData01 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example59',
    allOf: [],
    description: 'description001',
  };

  let allOfData02 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example59',
    allOf: [],
    description: 'description001',
  };

  let allOf;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        allOf = new AllOf(allOfData01);
        assert.equal(Utils.deepEqual(allOfData01, allOf.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate allOf data', () => {
      it('can pass validation when given valid data', () => {
        allOf = new AllOf(allOfData01);
        allOf.validate();
        assert.equal(allOf.validation.hasError, false);
        assert.equal(allOf.validation.hasWarning, false);
      });
    });
  });
});
