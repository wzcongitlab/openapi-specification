import { CONSTANTS } from '../../../config/constants';
import { StringField } from '../../../fields/string.field';
import { OasNode } from '../../../base/oasNode';
import { OAS_WARNINGS } from '../../../config/oasErrors';

export class EmptyJsonSchema extends OasNode {
  static get nodeObjectType() {
    return 'EmptyJsonSchema';
  }

  constructor(params = {}, parent = null, nodeName = CONSTANTS.emptyStr) {
    super(parent, nodeName);
    this.type = new StringField(CONSTANTS.emptyStr, this, 'type');
    this.format = new StringField(params.format, this, 'format');
  }

  validateNode() {
    if (this.type.isEmpty) {
      this.type.validation.update({
        hasError: true,
        summary: OAS_WARNINGS.SCHEMA_TYPE_REQUIRED,
      });
    }
  }
}
