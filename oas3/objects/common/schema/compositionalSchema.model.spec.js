import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { CompositionalSchema } from './compositionalSchema.model';
import { JSON_SCHEMA_TYPES } from '../../../config/oasConfig';
import { OAS_WARNINGS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('CompositionalSchema', () => {
  let compositionalSchemaData01 = [];

  let compositionalSchemaData02 = [
    {
      $ref: 'N/V',
    },
  ];

  let compositionalSchema;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        compositionalSchema = new CompositionalSchema(compositionalSchemaData01);
        assert.equal(
          Utils.deepEqual(compositionalSchemaData01, compositionalSchema.extract()),
          true
        );
      });
    });
    describe('functions', () => {
      it('can add Schema', () => {
        compositionalSchema = new CompositionalSchema(compositionalSchemaData01);
        let schemaType = JSON_SCHEMA_TYPES.integer;
        compositionalSchema.addSchema({ type: schemaType });
        assert.equal(compositionalSchema.length > 0, true);
        assert.equal(compositionalSchema.value[0].type.value === schemaType, true);
      });
      it('can remove Schema', () => {
        compositionalSchema = new CompositionalSchema(compositionalSchemaData01);
        let schemaType1 = JSON_SCHEMA_TYPES.integer;
        let schemaType2 = JSON_SCHEMA_TYPES.string;
        compositionalSchema.addSchema({ type: schemaType1 });
        compositionalSchema.addSchema({ type: schemaType2 });
        compositionalSchema.removeSchema(1);
        assert.equal(compositionalSchema.length === 1, true);
        assert.equal(compositionalSchema.value[0].type.value === schemaType1, true);
        compositionalSchema.removeSchema(0);
        assert.equal(compositionalSchema.isEmpty, true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate compositionalSchema data', () => {
      it('can get warning named COMPOSITIONAL_SCHEMA_ITEMS_REQUIRED', () => {
        compositionalSchema = new CompositionalSchema(compositionalSchemaData01);
        compositionalSchema.validate();
        assert.equal(compositionalSchema.validation.hasWarning, true);
        assert.equal(
          compositionalSchema.validation.warning.summary,
          OAS_WARNINGS.COMPOSITIONAL_SCHEMA_ITEMS_REQUIRED
        );
      });
      it('can pass validation when given valid data', () => {
        compositionalSchema = new CompositionalSchema(compositionalSchemaData02);
        compositionalSchema.validate();
        assert.equal(compositionalSchema.validation.hasError, false);
        assert.equal(compositionalSchema.validation.hasWarning, false);
      });
    });
  });
});
