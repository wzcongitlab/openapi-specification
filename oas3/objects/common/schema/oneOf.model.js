import { CONSTANTS } from '../../../config/constants';
import { OasSchema } from './oasSchema.model';
import { Discriminator } from './discriminator.model';
import { CompositionalSchema } from './compositionalSchema.model';
import { AnyField } from '../../../fields/any.field';

export class OneOf extends OasSchema {
  static get nodeObjectType() {
    return 'OneOf';
  }

  constructor(params = {}, parent, nodeName = CONSTANTS.emptyStr) {
    super(params, parent, nodeName);
    this.oneOf = new CompositionalSchema(params.oneOf, this, 'oneOf');
    this.discriminator = params.discriminator
      ? new Discriminator(params.discriminator, this)
      : null;
    this.example = new AnyField(params.example, this, 'example');
  }

  addDiscriminator(discriminator = {}) {
    this.discriminator = new Discriminator(discriminator, this);
  }

  removeDiscriminator() {
    this.discriminator = null;
  }
}
