import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { NumberSchema } from './numberSchema.model';
import { OAS_ERRORS, OAS_WARNINGS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('NumberSchema', () => {
  let schemaData01 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 1.88,
    type: 'number',
    description: 'description19',
    format: 'float',
    enum: [],
    title: 'title001',
    exclusiveMaximum: false,
    exclusiveMinimum: false,
  };

  let schemaData02 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 1.88,
    type: 'number',
    description: 'description19',
    format: 'float',
    enum: [],
    title: 'title001',
    exclusiveMaximum: false,
    exclusiveMinimum: false,
  };

  let schemaData03 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: -3,
    maximum: 10000,
    exclusiveMaximum: false,
    minimum: 4,
    exclusiveMinimum: true,
    default: 1,
    example: 1.77,
    enum: [],
    title: 'title001',
  };

  let schemaData04 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: 3,
    maximum: 10000,
    exclusiveMaximum: false,
    minimum: 4,
    exclusiveMinimum: true,
    default: 5,
    example: 1.77,
    enum: [],
    title: 'title001',
  };

  let schemaData05 = {
    type: 'number',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: 3,
    maximum: 12,
    exclusiveMaximum: false,
    minimum: 33,
    exclusiveMinimum: true,
    default: 5,
    example: 1.77,
    enum: [],
    title: 'title001',
  };

  let schemaData06 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: 3,
    maximum: 45,
    exclusiveMaximum: true,
    minimum: 33,
    exclusiveMinimum: true,
    default: 45,
    example: 1.77,
    enum: [],
    title: 'title001',
  };

  let schemaData07 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: 3,
    maximum: 45,
    exclusiveMaximum: false,
    minimum: 33,
    exclusiveMinimum: false,
    default: 46,
    example: 1.77,
    enum: [],
    title: 'title001',
  };

  let schemaData08 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: 3,
    maximum: 45,
    exclusiveMaximum: true,
    minimum: 33,
    exclusiveMinimum: true,
    default: 33,
    example: 1.77,
    enum: [],
    title: 'title001',
  };

  let schemaData09 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: 3,
    maximum: 45,
    exclusiveMaximum: true,
    minimum: 33,
    exclusiveMinimum: false,
    default: 32,
    example: 1.77,
    enum: [],
    title: 'title001',
  };

  let schemaData10 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: 3,
    maximum: 45,
    exclusiveMaximum: false,
    minimum: 33,
    exclusiveMinimum: false,
    default: 33,
    example: 1.77,
    enum: [],
    title: 'title001',
  };

  let schemaData11 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: 10,
    maximum: 40,
    exclusiveMaximum: false,
    minimum: 10,
    exclusiveMinimum: false,
    default: 10,
    example: 1.77,
    enum: [50, 9880],
    title: 'title001',
  };

  let schemaData12 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: 10,
    maximum: 50,
    exclusiveMaximum: true,
    minimum: 10,
    exclusiveMinimum: false,
    default: 10,
    example: 1.77,
    enum: [50],
    title: 'title001',
  };

  let schemaData13 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: 10,
    maximum: 50,
    exclusiveMaximum: false,
    minimum: 51,
    exclusiveMinimum: false,
    default: 10,
    example: 1.77,
    enum: [20],
    title: 'title001',
  };

  let schemaData14 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: 10,
    maximum: 50,
    exclusiveMaximum: false,
    minimum: 40,
    exclusiveMinimum: true,
    default: 10,
    example: 1.77,
    enum: [40],
    title: 'title001',
  };

  let schemaData15 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: 10,
    maximum: 50,
    exclusiveMaximum: false,
    minimum: 40,
    exclusiveMinimum: true,
    default: 10,
    example: 1.77,
    enum: [44],
    title: 'title001',
  };

  let numberSchema;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        numberSchema = new NumberSchema(schemaData01);
        assert.equal(Utils.deepEqual(schemaData01, numberSchema.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate numberSchema data', () => {
      it('can get error named SCHEMA_MULTIPLEOF_RESTRICTED', () => {
        numberSchema = new NumberSchema(schemaData03);
        numberSchema.validate();
        assert.equal(numberSchema.multipleOf.validation.hasWarning, true);
        assert.equal(
          numberSchema.multipleOf.validation.warning.summary,
          OAS_ERRORS.SCHEMA_MULTIPLEOF_RESTRICTED
        );
      });

      it('can get error named INTEGER_SCHEMA_MINIMUM_MATCH_MULTIPLEOF', () => {
        numberSchema = new NumberSchema(schemaData03);
        numberSchema.validate();
        assert.equal(numberSchema.minimum.validation.hasWarning, true);
        assert.equal(
          numberSchema.minimum.validation.warning.summary,
          OAS_ERRORS.INTEGER_SCHEMA_MINIMUM_MATCH_MULTIPLEOF
        );
      });

      it('can get error named INTEGER_SCHEMA_MAXIMUM_MATCH_MULTIPLEOF', () => {
        numberSchema = new NumberSchema(schemaData03);
        numberSchema.validate();
        assert.equal(numberSchema.maximum.validation.hasWarning, true);
        assert.equal(
          numberSchema.maximum.validation.warning.summary,
          OAS_ERRORS.INTEGER_SCHEMA_MAXIMUM_MATCH_MULTIPLEOF
        );
      });

      it('can get error named INTEGER_SCHEMA_DEFAULT_MATCH_MULTIPLEOF', () => {
        numberSchema = new NumberSchema(schemaData04);
        numberSchema.validate();
        assert.equal(numberSchema.default.validation.hasWarning, true);
        assert.equal(
          numberSchema.default.validation.warning.summary,
          OAS_ERRORS.INTEGER_SCHEMA_DEFAULT_MATCH_MULTIPLEOF
        );
      });

      it('can get error named SCHEMA_MAXIMUM_MUST_GREATER_THAN_MINIMUM', () => {
        numberSchema = new NumberSchema(schemaData05);
        numberSchema.validate();
        assert.equal(numberSchema.maximum.validation.hasWarning, true);
        assert.equal(
          numberSchema.maximum.validation.warning.summary,
          OAS_ERRORS.SCHEMA_MAXIMUM_MUST_GREATER_THAN_MINIMUM
        );
      });

      it('can get error named INTEGER_DEFAULT_SHOULD_LESSTHAN_MAXIMUM', () => {
        numberSchema = new NumberSchema(schemaData06);
        numberSchema.validate();
        assert.equal(numberSchema.default.validation.hasWarning, true);
        assert.equal(
          numberSchema.default.validation.warning.summary,
          OAS_ERRORS.INTEGER_DEFAULT_SHOULD_LESSTHAN_MAXIMUM
        );
      });

      it('can get error named INTEGER_DEFAULT_SHOULD_LESSTHAN_OR_EQUAL_MAXIMUM', () => {
        numberSchema = new NumberSchema(schemaData07);
        numberSchema.validate();
        assert.equal(numberSchema.default.validation.hasWarning, true);
        assert.equal(
          numberSchema.default.validation.warning.summary,
          OAS_ERRORS.INTEGER_DEFAULT_SHOULD_LESSTHAN_OR_EQUAL_MAXIMUM
        );
      });

      it('can get error named INTEGER_DEFAULT_SHOULD_GREATERTHAN_MINIMUM', () => {
        numberSchema = new NumberSchema(schemaData08);
        numberSchema.validate();
        assert.equal(numberSchema.default.validation.hasWarning, true);
        assert.equal(
          numberSchema.default.validation.warning.summary,
          OAS_ERRORS.INTEGER_DEFAULT_SHOULD_GREATERTHAN_MINIMUM
        );
      });

      it('can get error named INTEGER_DEFAULT_SHOULD_GREATERTHAN_OR_EQUAL_MINIMUM', () => {
        numberSchema = new NumberSchema(schemaData09);
        numberSchema.validate();
        assert.equal(numberSchema.default.validation.hasWarning, true);
        assert.equal(
          numberSchema.default.validation.warning.summary,
          OAS_ERRORS.INTEGER_DEFAULT_SHOULD_GREATERTHAN_OR_EQUAL_MINIMUM
        );
      });

      it('can get error named NUMERIC_SCHEMA_ENUM_SHOULD_LESSTHAN_OR_EQUAL_MAXIMUM', () => {
        numberSchema = new NumberSchema(schemaData11);
        numberSchema.validate();
        for (let enumField of numberSchema.enum.value) {
          assert.equal(enumField.validation.hasWarning, true);
          assert.equal(
            enumField.validation.warning.summary,
            OAS_WARNINGS.NUMERIC_SCHEMA_ENUM_SHOULD_LESSTHAN_OR_EQUAL_MAXIMUM
          );
        }
      });

      it('can get error named NUMERIC_SCHEMA_ENUM_SHOULD_LESSTHAN_MAXIMUM', () => {
        numberSchema = new NumberSchema(schemaData12);
        numberSchema.validate();
        for (let enumField of numberSchema.enum.value) {
          assert.equal(enumField.validation.hasWarning, true);
          assert.equal(
            enumField.validation.warning.summary,
            OAS_WARNINGS.NUMERIC_SCHEMA_ENUM_SHOULD_LESSTHAN_MAXIMUM
          );
        }
      });

      it('can get error named NUMERIC_SCHEMA_ENUM_SHOULD_GREATERTHAN_OR_EQUAL_MINIMUM', () => {
        numberSchema = new NumberSchema(schemaData13);
        numberSchema.validate();
        for (let enumField of numberSchema.enum.value) {
          assert.equal(enumField.validation.hasWarning, true);
          assert.equal(
            enumField.validation.warning.summary,
            OAS_WARNINGS.NUMERIC_SCHEMA_ENUM_SHOULD_GREATERTHAN_OR_EQUAL_MINIMUM
          );
        }
      });

      it('can get error named NUMERIC_SCHEMA_ENUM_SHOULD_GREATERTHAN_MINIMUM', () => {
        numberSchema = new NumberSchema(schemaData14);
        numberSchema.validate();
        for (let enumField of numberSchema.enum.value) {
          assert.equal(enumField.validation.hasWarning, true);
          assert.equal(
            enumField.validation.warning.summary,
            OAS_WARNINGS.NUMERIC_SCHEMA_ENUM_SHOULD_GREATERTHAN_MINIMUM
          );
        }
      });

      it('can get error named NUMERIC_SCHEMA_ENUM_MATCH_MULTIPLEOF', () => {
        numberSchema = new NumberSchema(schemaData15);
        numberSchema.validate();
        for (let enumField of numberSchema.enum.value) {
          assert.equal(enumField.validation.hasWarning, true);
          assert.equal(
            enumField.validation.warning.summary,
            OAS_WARNINGS.NUMERIC_SCHEMA_ENUM_MATCH_MULTIPLEOF
          );
        }
      });

      it('can pass validation when given valid data', () => {
        numberSchema = new NumberSchema(schemaData10);
        numberSchema.validate();
        assert.equal(numberSchema.maximum.validation.hasWarning, false);
        assert.equal(numberSchema.minimum.validation.hasWarning, false);
        assert.equal(numberSchema.default.validation.hasWarning, false);
      });
    });
  });
});
