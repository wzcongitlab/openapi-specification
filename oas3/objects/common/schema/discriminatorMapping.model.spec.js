import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { DiscriminatorMapping } from './discriminatorMapping.model';
import { StringField } from '../../../fields/string.field';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('DiscriminatorMapping', () => {
  let discriminatorMappingsData01 = {
    userType: 'type',
  };

  let discriminatorMappingsData02 = {
    dog: '',
    cat: '',
  };

  let discriminatorMappingsData03 = {
    dog: '#/components/schemas/Dog',
    cat: '#/components/schemas/Dog',
  };

  let discriminatorMappings;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        discriminatorMappings = new DiscriminatorMapping(discriminatorMappingsData01);
        assert.equal(
          Utils.deepEqual(discriminatorMappingsData01, discriminatorMappings.extract()),
          true
        );
      });
    });
    describe('functions', () => {
      it('can add DiscriminatorMapping', () => {
        discriminatorMappings = new DiscriminatorMapping(discriminatorMappingsData01);
        let discriminatorMappingName = 'discriminatorMapping1';
        discriminatorMappings.addMapping(discriminatorMappingName);
        assert.equal(discriminatorMappings[discriminatorMappingName] instanceof StringField, true);
      });
      it('can remove DiscriminatorMapping', () => {
        discriminatorMappings = new DiscriminatorMapping(discriminatorMappingsData01);
        let discriminatorMappingName = 'discriminatorMapping1';
        discriminatorMappings.addMapping(discriminatorMappingName, {});
        discriminatorMappings.removeMapping(discriminatorMappingName);
        assert.equal(discriminatorMappings.itemKeys.includes(discriminatorMappingName), false);
        assert.equal(discriminatorMappings[discriminatorMappingName] === undefined, true);
      });
    });
  });

  describe('#validation', () => {
    it('can get error named DISCRIMINATOR_MAPPING_VALUE_REQUIRED', () => {
      discriminatorMappings = new DiscriminatorMapping(discriminatorMappingsData02);
      discriminatorMappings.validate();
      for (let m of discriminatorMappings.items) {
        assert.equal(m.validation.hasWarning, true);
        assert.equal(m.validation.warning.summary, OAS_ERRORS.DISCRIMINATOR_MAPPING_VALUE_REQUIRED);
      }
    });
    it('can get error named DUPLICATED_DISCRIMINATOR_MAPPING_VALUES', () => {
      discriminatorMappings = new DiscriminatorMapping(discriminatorMappingsData03);
      discriminatorMappings.validate();
      for (let m of discriminatorMappings.items) {
        assert.equal(m.validation.hasWarning, true);
        assert.equal(
          m.validation.warning.summary,
          OAS_ERRORS.DUPLICATED_DISCRIMINATOR_MAPPING_VALUES
        );
      }
    });
  });
});
