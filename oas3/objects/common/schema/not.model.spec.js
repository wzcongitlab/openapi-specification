import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { Not } from './not.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('Not', () => {
  let notData01 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example7',
    not: {
      example: '',
      type: 'string',
      format: 'password',
      description: '',
      pattern: '',
      default: '',
      enum: [],
      title: 'title001',
      nullable: false,
      readOnly: false,
      writeOnly: false,
      deprecated: false,
    },
    description: 'description001',
  };

  let notData02 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example7',
    not: {
      example: '',
      type: '',
      format: '',
      description: '',
      pattern: '',
      default: '',
      enum: [],
      title: 'title001',
    },
    description: 'description001',
  };

  let not;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        not = new Not(notData01);
        assert.equal(Utils.deepEqual(notData01, not.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate not data', () => {
      it('can pass validation when given valid data', () => {
        not = new Not(notData01);
        not.validate();
        assert.equal(not.validation.hasError, false);
        assert.equal(not.validation.hasWarning, false);
      });
    });
  });
});
