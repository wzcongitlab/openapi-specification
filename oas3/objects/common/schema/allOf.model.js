import { CONSTANTS } from '../../../config/constants';
import { CompositionalSchema } from './compositionalSchema.model';
import { OasSchema } from './oasSchema.model';
import { Discriminator } from './discriminator.model';
import { AnyField } from '../../../fields/any.field';

export class AllOf extends OasSchema {
  static get nodeObjectType() {
    return 'AllOf';
  }

  constructor(params = {}, parent, nodeName = CONSTANTS.emptyStr) {
    super(params, parent, nodeName);
    this.allOf = new CompositionalSchema(params.allOf, this, 'allOf');
    this.discriminator = params.discriminator
      ? new Discriminator(params.discriminator, this)
      : null;
    this.example = new AnyField(params.example, this, 'example');
  }

  addDiscriminator(discriminator = {}) {
    this.discriminator = new Discriminator(discriminator, this);
  }

  removeDiscriminator() {
    this.discriminator = null;
  }
}
