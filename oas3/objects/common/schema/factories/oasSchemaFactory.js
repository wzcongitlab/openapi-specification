import { Reference } from '../../reference.model';
import { OneOf } from '../oneOf.model';
import { AnyOf } from '../anyOf.model';
import { AllOf } from '../allOf.model';
import { Not } from '../not.model';
import { JsonSchemaFactory } from './jsonSchemaFactory';

export class OasSchemaFactory {
  // 根据所给schema是否为$ref, 或组合字段, 或jsonSchema创建不同的schema对象;
  static createSchema(params = {}, parent = null, nodeName = 'schema') {
    /*
    传入参数
    params = {
      '$ref'/'oneOf'/'not': ...
    }
    得到'$ref'/'oneOf'/'not'类型的schema,
    其他情况创建json schema.
     */
    if (params.hasOwnProperty('$ref')) {
      // OasSchemaFactory.createSchema({$ref: ''});
      return new Reference(params, parent, nodeName);
    }
    if (params.hasOwnProperty('oneOf')) {
      // OasSchemaFactory.createSchema({oneOf: []});
      return new OneOf(params, parent, nodeName);
    }
    if (params.hasOwnProperty('anyOf')) {
      // OasSchemaFactory.createSchema({anyOf: []});
      return new AnyOf(params, parent, nodeName);
    }
    if (params.hasOwnProperty('allOf')) {
      // OasSchemaFactory.createSchema({allOf: []});
      return new AllOf(params, parent, nodeName);
    }
    if (params.hasOwnProperty('not')) {
      // 创建值为引用对象的not
      // OasSchemaFactory.createSchema({not: {$ref:''}});
      return new Not(params, parent, nodeName);
    }
    // OasSchemaFactory.createSchema({type: 'string'});
    return JsonSchemaFactory.createSchema(params, parent, nodeName);
  }
}
