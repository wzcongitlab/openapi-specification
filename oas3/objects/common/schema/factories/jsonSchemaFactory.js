import { NumberSchema } from '../numberSchema.model';
import { StringSchema } from '../stringSchema.model';
import { IntegerSchema } from '../integerSchema.model';
import { BooleanSchema } from '../booleanSchema.model';
import { ObjectSchema } from '../objectSchema.model';
import { ArraySchema } from '../arraySchema.model';
import { EmptyJsonSchema } from '../emptyJsonSchema.model.js';

export class JsonSchemaFactory {
  // 根据所给的json schema的数据中的type字段创建不同类型的schema对象;
  // 这一类工厂方法应保持简单, 仅根据类型创建不同的对象,
  // 具体新建对象的方式应与event的回退前进中新建对象的方式一致
  // 即 new Class(params, parent, nodeName);
  static createSchema(params = {}, parent = null, nodeName = 'schema') {
    let schemaType = params.type;
    if (JsonSchemaFactory.jsonSchemaConstructors.hasOwnProperty(schemaType)) {
      return new JsonSchemaFactory.jsonSchemaConstructors[schemaType](params, parent, nodeName);
    }
    return new EmptyJsonSchema({}, parent, nodeName);
  }

  static get jsonSchemaConstructors() {
    return {
      integer: IntegerSchema,
      string: StringSchema,
      number: NumberSchema,
      boolean: BooleanSchema,
      object: ObjectSchema,
      array: ArraySchema,
      empty: EmptyJsonSchema,
    };
  }
}
