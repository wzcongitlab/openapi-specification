import { CONSTANTS } from '../../../config/constants';
import { NumberField } from '../../../fields/number.field';
import { Properties } from './properties.model';
import { RequiredFields } from '../requiredFields.model';
import { JsonSchema } from './jsonSchema.model';
import { Discriminator } from './discriminator.model';
import { OasSchemaFactory } from './factories/oasSchemaFactory';
import { OasSchema } from './oasSchema.model';
import { OAS_ERRORS, OAS_WARNINGS } from '../../../config/oasErrors';

export class ObjectSchema extends JsonSchema {
  static get nodeObjectType() {
    return 'ObjectSchema';
  }

  constructor(params = {}, parent = null, nodeName = CONSTANTS.emptyStr) {
    super(params, parent, nodeName);
    this.maxProperties = new NumberField(params.maxProperties, this, 'maxProperties');
    this.minProperties = new NumberField(params.minProperties, this, 'minProperties');
    this.required = new RequiredFields(params.required, this);
    this.properties = new Properties(params.properties, this);
    this.discriminator = params.discriminator
      ? new Discriminator(params.discriminator, this)
      : null;
    // additionalProperties 要么为不存在(代表false), 要么是一个schema
    this.additionalProperties = params.additionalProperties
      ? OasSchemaFactory.createSchema(params.additionalProperties, this)
      : null;
  }

  addDiscriminator(discriminator = {}) {
    this.discriminator = new Discriminator(discriminator, this);
  }

  removeDiscriminator() {
    this.discriminator = null;
  }

  addAdditionalProperties(properties = {}) {
    this.additionalProperties = OasSchemaFactory.createSchema(properties, this);
  }

  removeAdditionalProperties() {
    this.additionalProperties = null;
  }

  validateNode() {
    // 校验最大长度与最小长度的值关系
    if (this.maxProperties.hasValue && this.minProperties.hasValue) {
      if (!this.maxProperties.greaterThanOrEqual(this.minProperties.value)) {
        this.maxProperties.validation.update({
          hasWarning: true,
          summary: OAS_ERRORS.SCHEMA_OBJECT_MAXPROPS_MUST_GREATERTHAN_MINPROPS,
        });
      }
    }
    // 校验properties个数
    if (
      this.maxProperties.hasValue &&
      !this.maxProperties.greaterThanOrEqual(this.properties.size)
    ) {
      this.properties.validation.update({
        hasWarning: true,
        summary: OAS_ERRORS.SCHEMA_OBJECT_PROPS_LENGTH_MUST_LESSTHAN_OR_EQUAL_MAXPROPS,
      });
    }
    if (this.minProperties.hasValue && !this.minProperties.lessThanOrEqual(this.properties.size)) {
      this.properties.validation.update({
        hasWarning: true,
        summary: OAS_ERRORS.SCHEMA_OBJECT_PROPS_LENGTH_MUST_GREATERTHAN_OR_EQUAL_MINPROPS,
      });
    }
    // 校验指定property的writeOnly和readOnly字段;
    // 校验properties > schema(object) 的层次;
    for (let property of this.properties.items) {
      if (property instanceof OasSchema) {
        property.validateWriteOnlyAndReadOnly();
      }
      if (property instanceof ObjectSchema) {
        this.properties.validation.update({
          hasWarning: true,
          summary: OAS_WARNINGS.SCHEMA_OBJECT_PROPERTIES_HIERARCHY_RESTRICTED,
        });
      }
    }
  }
}
