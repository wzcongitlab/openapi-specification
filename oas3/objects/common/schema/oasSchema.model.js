import { CONSTANTS } from '../../../config/constants';
import { OasNode } from '../../../base/oasNode';
import { OasExtensibleNode } from '../../../base/oasExtensibleNode';
import { BooleanField } from '../../../fields/boolean.field';
import { RichTextField } from '../../../fields/richText.field';
import { ExternalDocs } from '../externalDocs.model';
import { Xml } from './xml.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

export class OasSchema extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'OasSchema';
  }

  constructor(params = {}, parent = null, nodeName = CONSTANTS.emptyStr) {
    super(params, parent, nodeName);
    this.description = new RichTextField(params.description, this, 'description');
    this.nullable = new BooleanField(params.nullable, this, 'nullable');
    this.readOnly = new BooleanField(params.readOnly, this, 'readOnly');
    this.writeOnly = new BooleanField(params.writeOnly, this, 'writeOnly');
    this.deprecated = new BooleanField(params.deprecated, this, 'deprecated');
    this.xml = params.xml ? new Xml(params.xml, this, 'xml') : null;
    this.externalDocs = params.externalDocs ? new ExternalDocs(params.externalDocs, this) : null;
  }

  addXml(xml) {
    this.xml = new Xml(xml, this);
  }

  removeXml() {
    this.xml = null;
  }

  addExternalDocs(externalDocs) {
    this.externalDocs = new ExternalDocs(externalDocs, this);
  }

  removeExternalDocs() {
    this.externalDocs = null;
  }

  validateWriteOnlyAndReadOnly() {
    if (this.readOnly.equal(true) && this.writeOnly.equal(true)) {
      this.readOnly.validation.update({
        hasError: true,
        summary: OAS_ERRORS.INVALID_WRITEONLY_AND_READONLY,
      });
      this.writeOnly.validation.update({
        hasError: true,
        summary: OAS_ERRORS.INVALID_WRITEONLY_AND_READONLY,
      });
    }
  }

  validateNode() {
    // 校验WriteOnlyA和ReadOnly;
    this.validateWriteOnlyAndReadOnly();
  }
}
