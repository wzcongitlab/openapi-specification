import { CONSTANTS } from '../../../config/constants';
import { OasListNode } from '../../../base/oasListNode';
import { OasSchema } from './oasSchema.model';
import { Reference } from '../reference.model';
import { OasSchemaFactory } from './factories/oasSchemaFactory';
import { OAS_WARNINGS } from '../../../config/oasErrors';

export class CompositionalSchema extends OasListNode {
  static get nodeObjectType() {
    return 'CompositionalSchema';
  }

  constructor(params = [], parent, nodeName = CONSTANTS.emptyStr) {
    super(parent, nodeName);
    if (params instanceof Array) {
      params.forEach(p => this.addSchema(p));
    }
  }

  addSchema(schema) {
    if (schema instanceof OasSchema || schema instanceof Reference) {
      schema.parent = this;
      return this.addItem(schema);
    }
    let newSchema = OasSchemaFactory.createSchema(schema, this);
    return this.addItem(newSchema);
  }

  removeSchema(index) {
    return this.removeItem(index);
  }

  validateNode() {
    if (this.isEmpty) {
      this.validation.update({
        hasWarning: true,
        summary: OAS_WARNINGS.COMPOSITIONAL_SCHEMA_ITEMS_REQUIRED,
      });
    }
  }
}
