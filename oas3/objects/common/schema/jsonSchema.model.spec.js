import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { JsonSchema } from './jsonSchema.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('JsonSchema', () => {
  let jsonSchemaData01 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example57',
    type: 'string',
    description: '',
    title: 'title001',
  };

  let jsonSchemaData02 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example57',
    type: 'string',
    description: '',
    title: 'title001',
  };

  let jsonSchema;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        jsonSchema = new JsonSchema(jsonSchemaData01);
        assert.equal(Utils.deepEqual(jsonSchemaData01, jsonSchema.extract()), true);
      });
    });
  });

  describe('#validation', () => {});
});
