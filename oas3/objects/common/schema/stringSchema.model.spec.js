import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { StringSchema } from './stringSchema.model';
import { OAS_ERRORS, OAS_WARNINGS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('StringSchema', () => {
  let stringSchemaData01 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example19',
    type: 'type92',
    format: 'password',
    description: 'description10',
    pattern: 'pattern88',
    default: 'default61',
    enum: [],
    title: 'title001',
  };

  let stringSchemaData02 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example19',
    type: 'type92',
    format: 'password',
    description: 'description10',
    pattern: 'pattern88',
    default: 'default61',
    enum: [],
    title: 'title001',
  };

  let stringSchemaData03 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example19',
    type: 'type92',
    format: 'password',
    description: 'description10',
    pattern: 'default61',
    default: 'default61',
    enum: [],
    maxLength: 10,
    minLength: 11,
    title: 'title001',
  };

  let stringSchemaData04 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example19',
    type: 'type92',
    format: 'password',
    description: 'description10',
    pattern: 'default61',
    default: 'default61',
    enum: [],
    maxLength: 6,
    minLength: 11,
    title: 'title001',
  };

  let stringSchemaData05 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example19',
    type: 'type92',
    format: 'password',
    description: 'description10',
    pattern: '^\\d+$',
    default: '6ddd1',
    enum: [],
    maxLength: 6,
    minLength: 11,
    title: 'title001',
  };

  let stringSchemaData09 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example19',
    type: 'type92',
    format: 'password',
    description: 'description10',
    pattern: '^\\d+$',
    default: '6ddd1',
    enum: [],
    maxLength: 6,
    minLength: 11,
    title: 'title001',
  };

  let stringSchemaData06 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example19',
    type: 'type92',
    format: 'password',
    description: 'description10',
    pattern: '\\d+',
    default: '61',
    enum: ['123', '345'],
    maxLength: 6,
    minLength: 11,
    title: 'title001',
  };

  let stringSchemaData07 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example19',
    type: 'type92',
    format: 'password',
    description: 'description10',
    pattern: '\\d+',
    default: '1222223',
    enum: ['1222223', '3422225'],
    maxLength: 10,
    minLength: 5,
    title: 'title001',
  };

  let stringSchemaData08 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example19',
    type: 'type92',
    format: 'password',
    description: 'description10',
    pattern: '\\d+',
    default: '61',
    enum: ['ddsf', 'fff'],
    maxLength: 6,
    minLength: 3,
    title: 'title001',
  };

  let stringSchemaData10 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example19',
    type: 'type92',
    format: 'password',
    description: 'description10',
    pattern: '\\d+',
    default: '61',
    enum: ['d', 'f'],
    maxLength: 6,
    minLength: 3,
    title: 'title001',
  };

  let stringSchemaData11 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example19',
    type: 'type92',
    format: 'password',
    description: 'description10',
    pattern: '\\d+',
    default: '61',
    enum: ['ddddddd', 'fddddddd'],
    maxLength: 6,
    minLength: 3,
    title: 'title001',
  };

  let stringSchema;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        stringSchema = new StringSchema(stringSchemaData01);
        assert.equal(Utils.deepEqual(stringSchemaData01, stringSchema.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate stringSchema data', () => {
      it('can get error named SCHEMA_STRING_MAXLENGTH_MUST_GREATERTHAN_MINLENGTH', () => {
        stringSchema = new StringSchema(stringSchemaData03);
        stringSchema.validate();
        assert.equal(stringSchema.maxLength.validation.hasWarning, true);
        assert.equal(
          stringSchema.maxLength.validation.warning.summary,
          OAS_ERRORS.SCHEMA_STRING_MAXLENGTH_MUST_GREATERTHAN_MINLENGTH
        );
      });

      it('can get error named SCHEMA_STRING_DEFAULT_MUST_MATCH_MINLENGTH', () => {
        stringSchema = new StringSchema(stringSchemaData03);
        stringSchema.validate();
        assert.equal(stringSchema.default.validation.hasWarning, true);
        assert.equal(
          stringSchema.default.validation.warning.summary,
          OAS_ERRORS.SCHEMA_STRING_DEFAULT_MUST_MATCH_MINLENGTH
        );
      });

      it('can get error named SCHEMA_STRING_DEFAULT_MUST_MATCH_MAXLENGTH', () => {
        stringSchema = new StringSchema(stringSchemaData04);
        stringSchema.validate();
        assert.equal(stringSchema.default.validation.hasWarning, true);
        assert.equal(
          stringSchema.default.validation.warning.summary,
          OAS_ERRORS.SCHEMA_STRING_DEFAULT_MUST_MATCH_MAXLENGTH
        );
      });

      it('can get error named SCHEMA_STRING_DEFAULT_MUST_MATCH_PATTERN', () => {
        stringSchema = new StringSchema(stringSchemaData05);
        stringSchema.validate();
        assert.equal(stringSchema.default.validation.hasWarning, true);
        assert.equal(
          stringSchema.default.validation.warning.summary,
          OAS_WARNINGS.SCHEMA_STRING_DEFAULT_MUST_MATCH_PATTERN
        );
      });

      it('can get error named SCHEMA_STRING_EXAMPLE_MUST_MATCH_PATTERN', () => {
        stringSchema = new StringSchema(stringSchemaData09);
        stringSchema.validate();
        assert.equal(stringSchema.example.validation.hasWarning, true);
        assert.equal(
          stringSchema.example.validation.warning.summary,
          OAS_WARNINGS.SCHEMA_STRING_EXAMPLE_MUST_MATCH_PATTERN
        );
      });

      it('can get error named SCHEMA_STRING_DEFAULT_RESTRICTED_BY_ENUM', () => {
        stringSchema = new StringSchema(stringSchemaData06);
        stringSchema.validate();
        assert.equal(stringSchema.default.validation.hasWarning, true);
        assert.equal(
          stringSchema.default.validation.warning.summary,
          OAS_ERRORS.SCHEMA_STRING_DEFAULT_RESTRICTED_BY_ENUM
        );
      });

      it('can get error named SCHEMA_STRING_ENUM_MUST_MATCH_PATTERN', () => {
        stringSchema = new StringSchema(stringSchemaData08);
        stringSchema.validate();
        for (let enumField of stringSchema.enum.value) {
          assert.equal(enumField.validation.hasWarning, true);
          assert.equal(
            enumField.validation.warning.summary,
            OAS_WARNINGS.SCHEMA_STRING_ENUM_MUST_MATCH_PATTERN
          );
        }
      });

      it('can get error named SCHEMA_STRING_ENUM_MUST_MATCH_MINLENGTH', () => {
        stringSchema = new StringSchema(stringSchemaData10);
        stringSchema.validate();
        for (let enumField of stringSchema.enum.value) {
          assert.equal(enumField.validation.hasWarning, true);
          assert.equal(
            enumField.validation.warning.summary,
            OAS_WARNINGS.SCHEMA_STRING_ENUM_MUST_MATCH_MINLENGTH
          );
        }
      });

      it('can get error named SCHEMA_STRING_ENUM_MUST_MATCH_MAXLENGTH', () => {
        stringSchema = new StringSchema(stringSchemaData11);
        stringSchema.validate();
        for (let enumField of stringSchema.enum.value) {
          assert.equal(enumField.validation.hasWarning, true);
          assert.equal(
            enumField.validation.warning.summary,
            OAS_WARNINGS.SCHEMA_STRING_ENUM_MUST_MATCH_MAXLENGTH
          );
        }
      });

      it('can pass validation when given valid data', () => {
        stringSchema = new StringSchema(stringSchemaData07);
        stringSchema.validate();
        assert.equal(stringSchema.maxLength.validation.hasWarning, false);
        assert.equal(stringSchema.minLength.validation.hasWarning, false);
        assert.equal(stringSchema.default.validation.hasWarning, false);
      });
    });
  });
});
