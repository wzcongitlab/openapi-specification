import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { IntegerSchema } from './integerSchema.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('IntegerSchema', () => {
  let integerSchema;

  let schemaData01 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: 1,
    maximum: 10000,
    exclusiveMaximum: false,
    minimum: 1,
    exclusiveMinimum: true,
    default: 1,
    enum: [],
    example: 22,
    title: 'title001',
    readOnly: false,
    writeOnly: false,
    deprecated: false,
  };

  let schemaData02 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: 3,
    maximum: 10000,
    exclusiveMaximum: false,
    minimum: 4,
    exclusiveMinimum: true,
    default: 1,
    example: 11,
    enum: [],
    title: 'title001',
  };

  let schemaData03 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: -3,
    maximum: 10000,
    exclusiveMaximum: false,
    minimum: 4,
    exclusiveMinimum: true,
    default: 1,
    example: 22,
    enum: [],
    title: 'title001',
  };

  let schemaData04 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: 3,
    maximum: 10000,
    exclusiveMaximum: false,
    minimum: 4,
    exclusiveMinimum: true,
    default: 5,
    example: 44,
    enum: [],
    title: 'title001',
  };

  let schemaData05 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: 3,
    maximum: 12,
    exclusiveMaximum: false,
    minimum: 33,
    exclusiveMinimum: true,
    default: 5,
    example: 5,
    enum: [],
    title: 'title001',
  };

  let schemaData06 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: 3,
    maximum: 45,
    exclusiveMaximum: true,
    minimum: 33,
    exclusiveMinimum: true,
    default: 45,
    example: 5,
    enum: [],
    title: 'title001',
  };

  let schemaData07 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: 3,
    maximum: 45,
    exclusiveMaximum: false,
    minimum: 33,
    exclusiveMinimum: false,
    default: 46,
    example: 5,
    enum: [],
    title: 'title001',
  };

  let schemaData08 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: 3,
    maximum: 45,
    exclusiveMaximum: true,
    minimum: 33,
    exclusiveMinimum: true,
    default: 33,
    example: 5,
    enum: [],
    title: 'title001',
  };

  let schemaData09 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: 3,
    maximum: 45,
    exclusiveMaximum: true,
    minimum: 33,
    exclusiveMinimum: false,
    default: 32,
    example: 5,
    enum: [],
    title: 'title001',
  };

  let schemaData10 = {
    type: 'integer',
    format: 'int32',
    nullable: true,
    description: 'quantity of order',
    multipleOf: 3,
    maximum: 45,
    exclusiveMaximum: false,
    minimum: 33,
    exclusiveMinimum: false,
    default: 33,
    example: 5,
    enum: [],
    title: 'title001',
  };

  describe('#fields', () => {
    beforeEach(() => {
      integerSchema = new IntegerSchema(schemaData01);
    });

    describe('#data extraction', () => {
      it('can extract correct data.', () => {
        assert.equal(Utils.deepEqual(integerSchema.extract(), schemaData01), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate integerSchema data', () => {
      it('can get error named SCHEMA_MULTIPLEOF_RESTRICTED', () => {
        integerSchema = new IntegerSchema(schemaData03);
        integerSchema.validate();
        assert.equal(integerSchema.multipleOf.validation.hasWarning, true);
        assert.equal(
          integerSchema.multipleOf.validation.warning.summary,
          OAS_ERRORS.SCHEMA_MULTIPLEOF_RESTRICTED
        );
      });

      it('can get error named INTEGER_SCHEMA_MINIMUM_MATCH_MULTIPLEOF', () => {
        integerSchema = new IntegerSchema(schemaData03);
        integerSchema.validate();
        assert.equal(integerSchema.minimum.validation.hasWarning, true);
        assert.equal(
          integerSchema.minimum.validation.warning.summary,
          OAS_ERRORS.INTEGER_SCHEMA_MINIMUM_MATCH_MULTIPLEOF
        );
      });

      it('can get error named INTEGER_SCHEMA_MAXIMUM_MATCH_MULTIPLEOF', () => {
        integerSchema = new IntegerSchema(schemaData03);
        integerSchema.validate();
        assert.equal(integerSchema.maximum.validation.hasWarning, true);
        assert.equal(
          integerSchema.maximum.validation.warning.summary,
          OAS_ERRORS.INTEGER_SCHEMA_MAXIMUM_MATCH_MULTIPLEOF
        );
      });

      it('can get error named INTEGER_SCHEMA_DEFAULT_MATCH_MULTIPLEOF', () => {
        integerSchema = new IntegerSchema(schemaData04);
        integerSchema.validate();
        assert.equal(integerSchema.default.validation.hasWarning, true);
        assert.equal(
          integerSchema.default.validation.warning.summary,
          OAS_ERRORS.INTEGER_SCHEMA_DEFAULT_MATCH_MULTIPLEOF
        );
      });

      it('can get error named SCHEMA_MAXIMUM_MUST_GREATER_THAN_MINIMUM', () => {
        integerSchema = new IntegerSchema(schemaData05);
        integerSchema.validate();
        assert.equal(integerSchema.maximum.validation.hasWarning, true);
        assert.equal(
          integerSchema.maximum.validation.warning.summary,
          OAS_ERRORS.SCHEMA_MAXIMUM_MUST_GREATER_THAN_MINIMUM
        );
      });

      it('can get error named INTEGER_DEFAULT_SHOULD_LESSTHAN_MAXIMUM', () => {
        integerSchema = new IntegerSchema(schemaData06);
        integerSchema.validate();
        assert.equal(integerSchema.default.validation.hasWarning, true);
        assert.equal(
          integerSchema.default.validation.warning.summary,
          OAS_ERRORS.INTEGER_DEFAULT_SHOULD_LESSTHAN_MAXIMUM
        );
      });

      it('can get error named INTEGER_DEFAULT_SHOULD_LESSTHAN_OR_EQUAL_MAXIMUM', () => {
        integerSchema = new IntegerSchema(schemaData07);
        integerSchema.validate();
        assert.equal(integerSchema.default.validation.hasWarning, true);
        assert.equal(
          integerSchema.default.validation.warning.summary,
          OAS_ERRORS.INTEGER_DEFAULT_SHOULD_LESSTHAN_OR_EQUAL_MAXIMUM
        );
      });

      it('can get error named INTEGER_DEFAULT_SHOULD_GREATERTHAN_MINIMUM', () => {
        integerSchema = new IntegerSchema(schemaData08);
        integerSchema.validate();
        assert.equal(integerSchema.default.validation.hasWarning, true);
        assert.equal(
          integerSchema.default.validation.warning.summary,
          OAS_ERRORS.INTEGER_DEFAULT_SHOULD_GREATERTHAN_MINIMUM
        );
      });

      it('can get error named INTEGER_DEFAULT_SHOULD_GREATERTHAN_OR_EQUAL_MINIMUM', () => {
        integerSchema = new IntegerSchema(schemaData09);
        integerSchema.validate();
        assert.equal(integerSchema.default.validation.hasWarning, true);
        assert.equal(
          integerSchema.default.validation.warning.summary,
          OAS_ERRORS.INTEGER_DEFAULT_SHOULD_GREATERTHAN_OR_EQUAL_MINIMUM
        );
      });

      it('can pass validation when given valid data', () => {
        integerSchema = new IntegerSchema(schemaData10);
        integerSchema.validate();
        assert.equal(integerSchema.maximum.validation.hasWarning, false);
        assert.equal(integerSchema.minimum.validation.hasWarning, false);
        assert.equal(integerSchema.default.validation.hasWarning, false);
      });
    });
  });
});
