import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { OasSchema } from './oasSchema.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('OasSchema', () => {
  let oasSchemaData01 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    description: 'description001',
  };

  let oasSchemaData02 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    description: 'description001',
  };

  let oasSchemaData03 = {
    nullable: false,
    readOnly: true,
    writeOnly: true,
    deprecated: false,
    description: 'description001',
  };

  let oasSchema;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        oasSchema = new OasSchema(oasSchemaData01);
        assert.equal(Utils.deepEqual(oasSchemaData01, oasSchema.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate oasSchema data', () => {
      it('can get error named INVALID_WRITEONLY_AND_READONLY', () => {
        oasSchema = new OasSchema(oasSchemaData03);
        oasSchema.validate();
        assert.equal(oasSchema.writeOnly.validation.hasError, true);
        assert.equal(
          oasSchema.writeOnly.validation.error.summary,
          OAS_ERRORS.INVALID_WRITEONLY_AND_READONLY
        );
        assert.equal(oasSchema.readOnly.validation.hasError, true);
        assert.equal(
          oasSchema.readOnly.validation.error.summary,
          OAS_ERRORS.INVALID_WRITEONLY_AND_READONLY
        );
      });

      it('can pass validation when given valid data', () => {
        oasSchema = new OasSchema(oasSchemaData01);
        oasSchema.validate();
        assert.equal(oasSchema.validation.hasError, false);
        assert.equal(oasSchema.validation.hasWarning, false);
      });
    });
  });
});
