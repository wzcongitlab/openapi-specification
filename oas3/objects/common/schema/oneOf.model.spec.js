import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { OneOf } from './oneOf.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('OneOf', () => {
  let oneOfData01 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example51',
    oneOf: [],
    description: 'description001',
  };

  let oneOfData02 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example51',
    oneOf: [],
  };

  let oneOf;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        oneOf = new OneOf(oneOfData01);
        assert.equal(Utils.deepEqual(oneOfData01, oneOf.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate oneOf data', () => {
      it('can pass validation when given valid data', () => {
        oneOf = new OneOf(oneOfData01);
        oneOf.validate();
        assert.equal(oneOf.validation.hasError, false);
        assert.equal(oneOf.validation.hasWarning, false);
      });
    });
  });
});
