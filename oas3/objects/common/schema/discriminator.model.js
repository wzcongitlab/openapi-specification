import { OasNode } from '../../../base/oasNode';
import { StringField } from '../../../fields/string.field';
import { DiscriminatorMapping } from './discriminatorMapping.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

export class Discriminator extends OasNode {
  static get nodeObjectType() {
    return 'Discriminator';
  }

  constructor(params = {}, parent = null, nodeName = 'discriminator') {
    super(parent, nodeName);
    this.propertyName = new StringField(params.propertyName, this, 'propertyName');
    this.mapping = new DiscriminatorMapping(params.mapping, this);
  }

  validateNode() {
    if (this.propertyName.isEmpty) {
      this.propertyName.validation.update({
        hasError: true,
        summary: OAS_ERRORS.DESCRIMINATOR_PROPNAME_REQUIRED,
      });
    }
  }
}
