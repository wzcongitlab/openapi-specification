import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { Xml } from './xml.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('Xml', () => {
  let xmlData01 = {
    name: 'name98',
    namespace: 'namespace7',
    prefix: 'prefix98',
    attribute: false,
    wrapped: false,
  };

  let xmlData02 = {
    name: 'name98',
    namespace: 'namespace7',
    prefix: 'prefix98',
    attribute: false,
    wrapped: false,
  };

  let xml;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        xml = new Xml(xmlData01);
        assert.equal(Utils.deepEqual(xmlData01, xml.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate xml data', () => {
      it('can pass validation when given valid data', () => {
        xml = new Xml(xmlData01);
        xml.validate();
        assert.equal(xml.validation.hasError, false);
        assert.equal(xml.validation.hasWarning, false);
      });
    });
  });
});
