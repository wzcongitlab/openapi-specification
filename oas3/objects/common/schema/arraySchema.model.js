import { CONSTANTS } from '../../../config/constants';
import { JsonSchema } from './jsonSchema.model';
import { OasSchemaFactory } from './factories/oasSchemaFactory';
import { IntegerField } from '../../../fields/integer.field';
import { BooleanField } from '../../../fields/boolean.field';
import { OAS_ERRORS } from '../../../config/oasErrors';

export class ArraySchema extends JsonSchema {
  static get nodeObjectType() {
    return 'ArraySchema';
  }

  constructor(params = {}, parent = null, nodeName = CONSTANTS.emptyStr) {
    super(params, parent, nodeName);
    this.items = OasSchemaFactory.createSchema(params.items, this, 'items');
    this.maxItems = new IntegerField(params.maxItems, this, 'maxItems');
    this.minItems = new IntegerField(params.minItems, this, 'minItems');
    this.uniqueItems = new BooleanField(params.uniqueItems, this, 'uniqueItems');
  }

  validateNode() {
    // 校验最大长度与最小程度的值关系
    if (this.maxItems.hasValue && this.minItems.hasValue) {
      if (!this.maxItems.greaterThanOrEqual(this.minItems.value)) {
        this.maxItems.validation.update({
          hasError: true,
          summary: OAS_ERRORS.SCHEMA_ARRAY_MAXITEMS_MUST_GREATERTHAN_MINITEMS,
        });
      }
    }
  }
}
