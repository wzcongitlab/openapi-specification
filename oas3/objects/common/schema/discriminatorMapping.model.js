import { OasMapNode } from '../../../base/oasMapNode';
import { StringField } from '../../../fields/string.field';
import { OAS_ERRORS } from '../../../config/oasErrors';

export class DiscriminatorMapping extends OasMapNode {
  static get nodeObjectType() {
    return 'DiscriminatorMapping';
  }

  constructor(params = {}, parent = null, nodeName = 'mapping') {
    super(parent, nodeName);
    for (let key of Object.keys(params)) {
      this.addMapping(key, params[key]);
    }
  }

  addMapping(key, value) {
    this.addItem(key, new StringField(value, this, key));
  }

  removeMapping(key) {
    // 删除mapping时参考scopes的项的删除, event为delete类型;
    this.removeItem(key);
  }

  validateNode() {
    for (let m of this.items) {
      if (m.isEmpty) {
        m.validation.update({
          hasWarning: true,
          summary: OAS_ERRORS.DISCRIMINATOR_MAPPING_VALUE_REQUIRED,
        });
      }
      if (!m.isEmpty) {
        this.items.forEach(mapping => {
          if (mapping === m) {
            return;
          }
          if (mapping.validation.hasWarning) {
            return;
          }
          if (mapping.equal(m.value)) {
            m.validation.update({
              hasWarning: true,
              summary: OAS_ERRORS.DUPLICATED_DISCRIMINATOR_MAPPING_VALUES,
            });
            mapping.validation.update({
              hasWarning: true,
              summary: OAS_ERRORS.DUPLICATED_DISCRIMINATOR_MAPPING_VALUES,
            });
          }
        });
      }
    }
  }
}
