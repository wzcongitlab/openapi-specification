import { CONSTANTS } from '../../../config/constants';
import { Utils } from '../../../../utils/utils';
import { StringField } from '../../../fields/string.field';
import { IntegerField } from '../../../fields/integer.field';
import { BooleanField } from '../../../fields/boolean.field';
import { JsonSchema } from './jsonSchema.model';
import { NumberEnumValues } from '../numberEnumValues.model';
import { OAS_ERRORS, OAS_WARNINGS } from '../../../config/oasErrors';

export class IntegerSchema extends JsonSchema {
  static get nodeObjectType() {
    return 'IntegerSchema';
  }

  constructor(params = {}, parent = null, nodeName = CONSTANTS.emptyStr) {
    super(params, parent, nodeName);
    this.format = new StringField(
      params.format ? params.format : CONSTANTS.unavailable,
      this,
      'format'
    );
    this.multipleOf = new IntegerField(params.multipleOf, this, 'multipleOf');
    this.maximum = new IntegerField(params.maximum, this, 'maximum');
    this.exclusiveMaximum = new BooleanField(params.exclusiveMaximum, this, 'exclusiveMaximum');
    this.minimum = new IntegerField(params.minimum, this, 'minimum');
    this.exclusiveMinimum = new BooleanField(params.exclusiveMinimum, this, 'exclusiveMinimum');
    this.default = new IntegerField(params.default, this, 'default');
    this.enum = new NumberEnumValues(params.enum, this);
  }

  validateNode() {
    // 校验multipleOf
    if (this.multipleOf.hasValue && this.multipleOf.lessThan(0)) {
      this.multipleOf.validation.update({
        hasWarning: true,
        summary: OAS_ERRORS.SCHEMA_MULTIPLEOF_RESTRICTED,
      });
    }
    // 校验multipleOf相关
    if (this.multipleOf.hasValue && this.multipleOf.lessThan(0)) {
      this.multipleOf.validation.update({
        hasWarning: true,
        summary: OAS_ERRORS.SCHEMA_MULTIPLEOF_RESTRICTED,
      });
    }
    // 校验multipleOf相关
    if (this.multipleOf.hasValue) {
      if (this.maximum.hasValue && !Utils.isInteger(this.maximum.value / this.multipleOf.value)) {
        this.maximum.validation.update({
          hasWarning: true,
          summary: OAS_ERRORS.INTEGER_SCHEMA_MAXIMUM_MATCH_MULTIPLEOF,
        });
      }
      if (this.minimum.hasValue && !Utils.isInteger(this.minimum.value / this.multipleOf.value)) {
        this.minimum.validation.update({
          hasWarning: true,
          summary: OAS_ERRORS.INTEGER_SCHEMA_MINIMUM_MATCH_MULTIPLEOF,
        });
      }
      if (this.default.hasValue && !Utils.isInteger(this.default.value / this.multipleOf.value)) {
        this.default.validation.update({
          hasWarning: true,
          summary: OAS_ERRORS.INTEGER_SCHEMA_DEFAULT_MATCH_MULTIPLEOF,
        });
      }
      if (
        !this.example.isEmpty &&
        !Utils.isInteger(parseFloat(this.example.value) / this.multipleOf.value)
      ) {
        this.example.validation.update({
          hasWarning: true,
          summary: OAS_ERRORS.INTEGER_SCHEMA_EXAMPLE_MATCH_MULTIPLEOF,
        });
      }
    }
    // 校验minimum与maximum的大小
    if (this.maximum.hasValue && this.minimum.hasValue) {
      if (this.maximum.lessThan(this.minimum.value)) {
        this.maximum.validation.update({
          hasWarning: true,
          summary: OAS_ERRORS.SCHEMA_MAXIMUM_MUST_GREATER_THAN_MINIMUM,
        });
      }
    }
    // 校验default与exclusiveMaximum的关系
    if (this.maximum.hasValue && this.default.hasValue) {
      if (this.exclusiveMaximum.equal(true)) {
        if (!this.default.lessThan(this.maximum.value)) {
          this.default.validation.update({
            hasWarning: true,
            summary: OAS_ERRORS.INTEGER_DEFAULT_SHOULD_LESSTHAN_MAXIMUM,
          });
        }
      } else if (!this.default.lessThanOrEqual(this.maximum.value)) {
        this.default.validation.update({
          hasWarning: true,
          summary: OAS_ERRORS.INTEGER_DEFAULT_SHOULD_LESSTHAN_OR_EQUAL_MAXIMUM,
        });
      }
    }
    // 校验example与exclusiveMaximum的关系
    if (this.maximum.hasValue && !this.example.isEmpty) {
      if (this.exclusiveMaximum.equal(true)) {
        if (!this.maximum.greaterThan(parseFloat(this.example.value))) {
          this.example.validation.update({
            hasWarning: true,
            summary: OAS_ERRORS.INTEGER_EXAMPLE_SHOULD_LESSTHAN_MAXIMUM,
          });
        }
      } else if (!this.maximum.greaterThanOrEqual(parseFloat(this.example.value))) {
        this.example.validation.update({
          hasWarning: true,
          summary: OAS_ERRORS.INTEGER_EXAMPLE_SHOULD_LESSTHAN_OR_EQUAL_MAXIMUM,
        });
      }
    }
    // 校验default与exclusiveMinimum的关系
    if (this.minimum.hasValue && this.default.hasValue) {
      if (this.exclusiveMinimum.equal(true)) {
        if (!this.default.greaterThan(this.minimum.value)) {
          this.default.validation.update({
            hasWarning: true,
            summary: OAS_ERRORS.INTEGER_DEFAULT_SHOULD_GREATERTHAN_MINIMUM,
          });
        }
      } else if (!this.default.greaterThanOrEqual(this.minimum.value)) {
        this.default.validation.update({
          hasWarning: true,
          summary: OAS_ERRORS.INTEGER_DEFAULT_SHOULD_GREATERTHAN_OR_EQUAL_MINIMUM,
        });
      }
    }
    // 校验example与exclusiveMinimum的关系
    if (this.minimum.hasValue && !this.example.isEmpty) {
      if (this.exclusiveMinimum.equal(true)) {
        if (!this.minimum.lessThan(parseFloat(this.example.value))) {
          this.example.validation.update({
            hasWarning: true,
            summary: OAS_ERRORS.INTEGER_EXAMPLE_SHOULD_GREATERTHAN_MINIMUM,
          });
        }
      } else if (!this.minimum.lessThanOrEqual(parseFloat(this.example.value))) {
        this.example.validation.update({
          hasWarning: true,
          summary: OAS_ERRORS.INTEGER_EXAMPLE_SHOULD_GREATERTHAN_OR_EQUAL_MINIMUM,
        });
      }
    }
    // 校验枚举值与Multiple Of的关系; 与Maximum, Minimum的的关系;
    // !!!!! 该部分校验逻辑同NumberSchema, 所以没有必要编写单元测试;
    for (let enumField of this.enum.value) {
      if (this.maximum.hasValue) {
        if (this.exclusiveMaximum.equal(true)) {
          if (!enumField.lessThan(this.maximum.value)) {
            enumField.validation.update({
              hasWarning: true,
              summary: OAS_WARNINGS.NUMERIC_SCHEMA_ENUM_SHOULD_LESSTHAN_MAXIMUM,
            });
          }
        } else if (!enumField.lessThanOrEqual(this.maximum.value)) {
          enumField.validation.update({
            hasWarning: true,
            summary: OAS_WARNINGS.NUMERIC_SCHEMA_ENUM_SHOULD_LESSTHAN_OR_EQUAL_MAXIMUM,
          });
        }
      }
      if (this.minimum.hasValue) {
        if (this.exclusiveMinimum.equal(true)) {
          if (!enumField.greaterThan(this.minimum.value)) {
            enumField.validation.update({
              hasWarning: true,
              summary: OAS_WARNINGS.NUMERIC_SCHEMA_ENUM_SHOULD_GREATERTHAN_MINIMUM,
            });
          }
        } else if (!enumField.greaterThanOrEqual(this.minimum.value)) {
          enumField.validation.update({
            hasWarning: true,
            summary: OAS_WARNINGS.NUMERIC_SCHEMA_ENUM_SHOULD_GREATERTHAN_OR_EQUAL_MINIMUM,
          });
        }
      }
      if (this.multipleOf.hasValue && !Utils.isInteger(enumField.value / this.multipleOf.value)) {
        enumField.validation.update({
          hasWarning: true,
          summary: OAS_WARNINGS.NUMERIC_SCHEMA_ENUM_MATCH_MULTIPLEOF,
        });
      }
    }
    // 校验WriteOnlyA和ReadOnly;
    this.validateWriteOnlyAndReadOnly();
  }
}
