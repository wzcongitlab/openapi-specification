import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { JSON_SCHEMA_TYPES } from '../../../config/oasConfig';
import { Properties } from './properties.model';
import { OasSchema } from './oasSchema.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('Properties', () => {
  let propertiesData01 = {};

  let propertiesData02 = {};

  let properties;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        properties = new Properties(propertiesData01);
        assert.equal(Utils.deepEqual(propertiesData01, properties.extract()), true);
      });
    });
    describe('functions', () => {
      it('can add Property', () => {
        properties = new Properties(propertiesData01);
        let propKey = 'userId';
        properties.addProperty(propKey, { type: JSON_SCHEMA_TYPES.integer });
        assert.equal(properties[propKey] instanceof OasSchema, true);
      });
      it('can remove Property', () => {
        properties = new Properties(propertiesData01);
        let propKey = 'userId';
        properties.addProperty(propKey, { type: JSON_SCHEMA_TYPES.integer });
        properties.removeProperty(propKey);
        assert.equal(properties.itemKeys.includes(propKey), false);
        assert.equal(properties[propKey] === undefined, true);
      });
    });
  });

  describe('#validation', () => {});
});
