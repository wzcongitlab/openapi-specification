import { CONSTANTS } from '../../../config/constants';
import { OasSchemaFactory } from './factories/oasSchemaFactory';
import { OasSchema } from './oasSchema.model';
import { AnyField } from '../../../fields/any.field';

export class Not extends OasSchema {
  static get nodeObjectType() {
    return 'Not';
  }

  constructor(params = {}, parent, nodeName = CONSTANTS.emptyStr) {
    super(params, parent, nodeName);
    this.not = OasSchemaFactory.createSchema(params.not, this, 'not');
    this.example = new AnyField(params.example, this, 'example');
  }
}
