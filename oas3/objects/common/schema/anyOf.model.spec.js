import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { AnyOf } from './anyOf.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('AnyOf', () => {
  let anyOfData01 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example77',
    anyOf: [],
    description: 'description001',
  };

  let anyOfData02 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example77',
    anyOf: [],
    description: 'description001',
  };

  let anyOf;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        anyOf = new AnyOf(anyOfData01);
        assert.equal(Utils.deepEqual(anyOfData01, anyOf.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate anyOf data', () => {
      it('can pass validation when given valid data', () => {
        anyOf = new AnyOf(anyOfData01);
        anyOf.validate();
        assert.equal(anyOf.validation.hasError, false);
        assert.equal(anyOf.validation.hasWarning, false);
      });
    });
  });
});
