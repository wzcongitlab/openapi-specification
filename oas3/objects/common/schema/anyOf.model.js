import { CONSTANTS } from '../../../config/constants';
import { OasSchema } from './oasSchema.model';
import { Discriminator } from './discriminator.model';
import { CompositionalSchema } from './compositionalSchema.model';
import { AnyField } from '../../../fields/any.field';

export class AnyOf extends OasSchema {
  static get nodeObjectType() {
    return 'AnyOf';
  }

  constructor(params = {}, parent, nodeName = CONSTANTS.emptyStr) {
    super(params, parent, nodeName);
    this.anyOf = new CompositionalSchema(params.anyOf, this, 'anyOf');
    this.discriminator = params.discriminator
      ? new Discriminator(params.discriminator, this)
      : null;
    this.example = new AnyField(params.example, this, 'example');
  }

  addDiscriminator(discriminator = {}) {
    this.discriminator = new Discriminator(discriminator, this);
  }

  removeDiscriminator() {
    this.discriminator = null;
  }
}
