import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { ArraySchema } from './arraySchema.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('ArraySchema', () => {
  let arraySchemaData01 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example62',
    type: 'type80',
    description: '',
    maxItems: 10,
    minItems: 12,
    title: 'title001',
    items: {
      example: '',
      type: 'string',
      format: 'password',
      description: '',
      pattern: '',
      default: '',
      enum: [],
      title: 'title001',
      nullable: false,
      readOnly: false,
      writeOnly: false,
      deprecated: false,
    },
    uniqueItems: false,
  };

  let arraySchemaData02 = {
    nullable: false,
    deprecated: false,
    example: 'example62',
    type: 'type80',
    description: '',
    maxItems: 10,
    minItems: 9,
    title: 'title001',
    items: {
      example: '',
      type: 'string',
      format: 'password',
      description: '',
      pattern: '',
      default: '',
      enum: [],
      title: 'title001',
    },
    uniqueItems: false,
  };

  let arraySchema;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        arraySchema = new ArraySchema(arraySchemaData01);
        assert.equal(Utils.deepEqual(arraySchemaData01, arraySchema.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate arraySchema data', () => {
      it('can get error named SCHEMA_ARRAY_MAXITEMS_MUST_GREATERTHAN_MINITEMS', () => {
        arraySchema = new ArraySchema(arraySchemaData01);
        arraySchema.validate();
        assert.equal(arraySchema.maxItems.validation.hasError, true);
        assert.equal(
          arraySchema.maxItems.validation.error.summary,
          OAS_ERRORS.SCHEMA_ARRAY_MAXITEMS_MUST_GREATERTHAN_MINITEMS
        );
      });

      it('can pass validation when given valid data', () => {
        arraySchema = new ArraySchema(arraySchemaData02);
        arraySchema.validate();
        assert.equal(arraySchema.maxItems.validation.hasError, false);
      });
    });
  });
});
