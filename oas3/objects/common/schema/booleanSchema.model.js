import { CONSTANTS } from '../../../config/constants';
import { BooleanField } from '../../../fields/boolean.field';
import { JsonSchema } from './jsonSchema.model';

export class BooleanSchema extends JsonSchema {
  static get nodeObjectType() {
    return 'BooleanSchema';
  }

  constructor(
    params = {
      default: CONSTANTS.unavailable,
    },
    parent = null,
    nodeName = CONSTANTS.emptyStr
  ) {
    super(params, parent, nodeName);
    this.default = new BooleanField(params.default, this, 'default');
  }

  validateNode() {
    // 校验WriteOnlyA和ReadOnly;
    this.validateWriteOnlyAndReadOnly();
  }
}
