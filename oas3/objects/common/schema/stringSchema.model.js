import { CONSTANTS } from '../../../config/constants';
import { StringField } from '../../../fields/string.field';
import { IntegerField } from '../../../fields/integer.field';
import { EnumValues } from '../enumValues.model';
import { JsonSchema } from './jsonSchema.model';
import { OAS_ERRORS, OAS_WARNINGS } from '../../../config/oasErrors';

export class StringSchema extends JsonSchema {
  static get nodeObjectType() {
    return 'StringSchema';
  }

  constructor(params = {}, parent = null, nodeName = CONSTANTS.emptyStr) {
    super(params, parent, nodeName);
    this.format = new StringField(
      params.format ? params.format : CONSTANTS.unavailable,
      this,
      'format'
    );
    this.maxLength = new IntegerField(params.maxLength, this, 'maxLength');
    this.minLength = new IntegerField(params.minLength, this, 'minLength');
    this.pattern = new StringField(params.pattern, this, 'pattern');
    this.default = new StringField(params.default, this, 'default');
    this.enum = new EnumValues(params.enum, this);
  }

  validateNode() {
    // 校验最大长度与最小程度的值关系
    if (this.maxLength.hasValue && this.minLength.hasValue) {
      if (!this.maxLength.greaterThanOrEqual(this.minLength.value)) {
        this.maxLength.validation.update({
          hasWarning: true,
          summary: OAS_ERRORS.SCHEMA_STRING_MAXLENGTH_MUST_GREATERTHAN_MINLENGTH,
        });
      }
    }
    // 校验默认值是否匹配最大最小长度
    if (!this.default.isEmpty) {
      if (this.minLength.hasValue && !this.minLength.lessThanOrEqual(this.default.length)) {
        this.default.validation.update({
          hasWarning: true,
          summary: OAS_ERRORS.SCHEMA_STRING_DEFAULT_MUST_MATCH_MINLENGTH,
        });
      }
      if (this.maxLength.hasValue && !this.maxLength.greaterThanOrEqual(this.default.length)) {
        this.default.validation.update({
          hasWarning: true,
          summary: OAS_ERRORS.SCHEMA_STRING_DEFAULT_MUST_MATCH_MAXLENGTH,
        });
      }
    }
    // 校验example是否匹配最大最小长度
    if (!this.example.isEmpty) {
      if (this.minLength.hasValue && !this.minLength.lessThanOrEqual(this.example.length)) {
        this.example.validation.update({
          hasWarning: true,
          summary: OAS_ERRORS.SCHEMA_STRING_EXAMPLE_MUST_MATCH_MINLENGTH,
        });
      }
      if (this.maxLength.hasValue && !this.maxLength.greaterThanOrEqual(this.example.length)) {
        this.example.validation.update({
          hasWarning: true,
          summary: OAS_ERRORS.SCHEMA_STRING_EXAMPLE_MUST_MATCH_MAXLENGTH,
        });
      }
    }
    // 校验默认值是否匹配pattern
    if (!this.default.isEmpty && !this.pattern.isEmpty) {
      // 优化regex的编辑 添加g, i等选项
      let patternRegex = new RegExp(`${unescape(this.pattern.value)}`);
      if (!patternRegex.exec(this.default.value)) {
        this.default.validation.update({
          hasWarning: true,
          summary: OAS_WARNINGS.SCHEMA_STRING_DEFAULT_MUST_MATCH_PATTERN,
        });
      }
    }
    // 校验example是否匹配pattern
    if (!this.example.isEmpty && !this.pattern.isEmpty) {
      let patternRegex = new RegExp(`${unescape(this.pattern.value)}`);
      if (!patternRegex.test(this.example.value)) {
        this.example.validation.update({
          hasWarning: true,
          summary: OAS_WARNINGS.SCHEMA_STRING_EXAMPLE_MUST_MATCH_PATTERN,
        });
      }
    }
    // 校验enum中的项是否匹配pattern; 是否匹配最大长度; 是否匹配最小长度;
    for (let enumField of this.enum.value) {
      if (!this.pattern.isEmpty) {
        let patternRegex = new RegExp(`${unescape(this.pattern.value)}`);
        if (!patternRegex.test(enumField.value)) {
          enumField.validation.update({
            hasWarning: true,
            summary: OAS_WARNINGS.SCHEMA_STRING_ENUM_MUST_MATCH_PATTERN,
          });
        }
      }
      if (this.minLength.hasValue && !this.minLength.lessThanOrEqual(enumField.length)) {
        enumField.validation.update({
          hasWarning: true,
          summary: OAS_WARNINGS.SCHEMA_STRING_ENUM_MUST_MATCH_MINLENGTH,
        });
      }
      if (this.maxLength.hasValue && !this.maxLength.greaterThanOrEqual(enumField.length)) {
        enumField.validation.update({
          hasWarning: true,
          summary: OAS_WARNINGS.SCHEMA_STRING_ENUM_MUST_MATCH_MAXLENGTH,
        });
      }
    }
    // 校验默认值是否匹配enum
    if (!this.default.isEmpty && !this.enum.isEmpty) {
      if (!this.enum.values.includes(this.default.value)) {
        this.default.validation.update({
          hasWarning: true,
          summary: OAS_ERRORS.SCHEMA_STRING_DEFAULT_RESTRICTED_BY_ENUM,
        });
      }
    }
    // 校验example是否匹配enum
    if (!this.example.isEmpty && !this.enum.isEmpty) {
      if (!this.enum.values.includes(this.example.value)) {
        this.example.validation.update({
          hasWarning: true,
          summary: OAS_ERRORS.SCHEMA_STRING_EXAMPLE_RESTRICTED_BY_ENUM,
        });
      }
    }
    // 校验WriteOnlyA和ReadOnly;
    this.validateWriteOnlyAndReadOnly();
  }
}
