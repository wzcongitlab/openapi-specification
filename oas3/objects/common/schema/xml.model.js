import { CONSTANTS } from '../../../config/constants';
import { OasNode } from '../../../base/oasNode';
import { OasExtensibleNode } from '../../../base/oasExtensibleNode';
import { StringField } from '../../../fields/string.field';
import { UrlField } from '../../../fields/url.field';
import { BooleanField } from '../../../fields/boolean.field';

export class Xml extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'Xml';
  }

  constructor(params = {}, parent = null, nodeName = CONSTANTS.emptyStr) {
    super(params, parent, nodeName);
    this.name = new StringField(params.name, this, 'name');
    this.namespace = new UrlField(params.namespace, this, 'namespace');
    this.prefix = new StringField(params.prefix, this, 'prefix');
    this.attribute = new BooleanField(
      typeof params.attribute === 'boolean' ? params.attribute : false,
      this,
      'attribute'
    );
    this.wrapped = new BooleanField(
      typeof params.wrapped === 'boolean' ? params.wrapped : false,
      this,
      'wrapped'
    );
  }
}
