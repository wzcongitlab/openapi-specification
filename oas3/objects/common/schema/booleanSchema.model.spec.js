import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { BooleanSchema } from './booleanSchema.model';
import { OAS_WARNINGS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('BooleanSchema', () => {
  let booleanSchema;

  let schemaData01 = {
    description: 'if the order is completed',
    type: 'boolean',
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'false',
    default: false,
    title: 'title001',
  };
  let schemaData02 = {
    description: 'if the order is completed',
    type: 'boolean',
    nullable: true,
    default: false,
    example: '',
    title: 'title001',
  };

  describe('#fields', () => {
    beforeEach(() => {
      booleanSchema = new BooleanSchema(schemaData01);
    });

    describe('#data extraction', () => {
      it('can extract correct data.', () => {
        assert.equal(Utils.deepEqual(booleanSchema.extract(), schemaData01), true);
      });
    });
  });
});
