import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { ObjectSchema } from './objectSchema.model';
import { OAS_ERRORS, OAS_WARNINGS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('ObjectSchema', () => {
  let objectSchemaData01 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example86',
    type: 'type99',
    description: 'description21',
    required: [],
    properties: {},
    maxProperties: 10,
    minProperties: 5,
    title: 'title001',
  };

  let objectSchemaData02 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example86',
    type: 'type99',
    description: 'description21',
    required: [],
    properties: {
      userId: {
        nullable: false,
        readOnly: false,
        writeOnly: false,
        deprecated: false,
        example: 'example1',
        type: 'type2',
        description: 'description19',
        format: 'float',
        enum: [],
        title: 'title001',
      },
    },
    maxProperties: 10,
    minProperties: 11,
    title: 'title001',
  };

  let objectSchemaData03 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example86',
    type: 'type99',
    description: 'description21',
    required: [],
    properties: {
      userId: {
        nullable: false,
        readOnly: false,
        writeOnly: false,
        deprecated: false,
        example: 'example1',
        type: 'type2',
        description: 'description19',
        format: 'float',
        enum: [],
        title: 'title001',
      },
      money: {
        nullable: false,
        readOnly: false,
        writeOnly: false,
        deprecated: false,
        example: 'example1',
        type: 'type2',
        description: 'description19',
        format: 'float',
        enum: [],
        title: 'title001',
      },
      userName: {
        nullable: false,
        readOnly: false,
        writeOnly: false,
        deprecated: false,
        example: 'example19',
        type: 'type92',
        format: 'password',
        description: 'description10',
        pattern: 'pattern88',
        default: 'default61',
        enum: [],
        title: 'title001',
      },
      userLoginName: {
        nullable: false,
        readOnly: false,
        writeOnly: false,
        deprecated: false,
        example: 'example19',
        type: 'type92',
        format: 'password',
        description: 'description10',
        pattern: 'pattern88',
        default: 'default61',
        enum: [],
        title: 'title001',
      },
    },
    maxProperties: 3,
    minProperties: 2,
    title: 'title001',
  };

  let objectSchemaData04 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example86',
    type: 'type99',
    description: 'description21',
    required: [],
    properties: {
      userName: {
        nullable: false,
        readOnly: false,
        writeOnly: false,
        deprecated: false,
        example: 'example19',
        type: 'type92',
        format: 'password',
        description: 'description10',
        pattern: 'pattern88',
        default: 'default61',
        enum: [],
        title: 'title001',
      },
    },
    maxProperties: 3,
    minProperties: 2,
    title: 'title001',
  };

  let objectSchemaData05 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example86',
    type: 'type99',
    description: 'description21',
    required: [],
    properties: {
      userId: {
        nullable: false,
        readOnly: false,
        writeOnly: false,
        deprecated: false,
        example: 'example1',
        type: 'type2',
        description: 'description19',
        format: 'float',
        enum: [],
      },
      userName: {
        nullable: false,
        readOnly: false,
        writeOnly: false,
        deprecated: false,
        example: 'example19',
        type: 'type92',
        format: 'password',
        description: 'description10',
        pattern: 'pattern88',
        default: 'default61',
        enum: [],
        title: 'title001',
      },
      userLoginName: {
        nullable: false,
        readOnly: false,
        writeOnly: false,
        deprecated: false,
        example: 'example19',
        type: 'type92',
        format: 'password',
        description: 'description10',
        pattern: 'pattern88',
        default: 'default61',
        enum: [],
        title: 'title001',
      },
    },
    maxProperties: 4,
    minProperties: 2,
    title: 'title001',
  };

  let objectSchemaData06 = {
    nullable: false,
    readOnly: false,
    writeOnly: false,
    deprecated: false,
    example: 'example86',
    type: 'object',
    description: 'description21',
    required: [],
    properties: {
      id: {
        nullable: false,
        readOnly: true,
        writeOnly: true,
        deprecated: false,
        example: 'example1',
        type: 'string',
        description: 'description19',
        format: 'byte',
        enum: [],
      },
      userInfo: {
        type: 'object',
        description: 'description21',
        required: [],
        properties: {
          userName: {
            nullable: false,
            readOnly: true,
            writeOnly: true,
            deprecated: false,
            example: 'example1',
            type: 'string',
            description: 'description19',
            format: 'byte',
            enum: [],
          },
          otherInfo: {
            type: 'object',
            description: 'other user info',
            required: [],
            properties: {
              address: {
                nullable: false,
                readOnly: true,
                writeOnly: true,
                deprecated: false,
                example: 'example1',
                type: 'string',
                description: 'description19',
                format: 'byte',
                enum: [],
              },
            },
            maxProperties: 4,
            minProperties: 2,
            title: 'title001',
            nullable: false,
            readOnly: false,
            writeOnly: false,
            deprecated: false,
            example: 'example86',
          },
        },
        maxProperties: 4,
        minProperties: 2,
        title: 'title001',
        nullable: false,
        readOnly: false,
        writeOnly: false,
        deprecated: false,
        example: 'example86',
      },
    },
    maxProperties: 4,
    minProperties: 2,
    title: 'title001',
  };

  let objectSchema;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        objectSchema = new ObjectSchema(objectSchemaData01);
        assert.equal(Utils.deepEqual(objectSchemaData01, objectSchema.extract()), true);
      });
    });

    describe('functions', () => {
      it('can add Discriminator', () => {
        objectSchema = new ObjectSchema(objectSchemaData05);
        objectSchema.addDiscriminator({});
        assert.equal(objectSchema.discriminator !== null, true);
      });
      it('can remove Discriminator', () => {
        objectSchema = new ObjectSchema(objectSchemaData05);
        objectSchema.addDiscriminator({});
        objectSchema.removeDiscriminator();
        assert.equal(objectSchema.discriminator === null, true);
      });
      it('can add AdditionalProperties', () => {
        objectSchema = new ObjectSchema(objectSchemaData05);
        objectSchema.addAdditionalProperties({});
        assert.equal(objectSchema.addAdditionalProperties !== null, true);
      });
      it('can remove AdditionalProperties', () => {
        objectSchema = new ObjectSchema(objectSchemaData05);
        objectSchema.addAdditionalProperties({});
        objectSchema.removeAdditionalProperties();
        assert.equal(objectSchema.additionalProperties === null, true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate objectSchema data', () => {
      it('can get error named SCHEMA_OBJECT_MAXPROPS_MUST_GREATERTHAN_MINPROPS', () => {
        objectSchema = new ObjectSchema(objectSchemaData02);
        objectSchema.validate();
        assert.equal(objectSchema.maxProperties.validation.hasWarning, true);
        assert.equal(
          objectSchema.maxProperties.validation.warning.summary,
          OAS_ERRORS.SCHEMA_OBJECT_MAXPROPS_MUST_GREATERTHAN_MINPROPS
        );
      });
      it('can get error named SCHEMA_OBJECT_PROPS_LENGTH_MUST_LESSTHAN_OR_EQUAL_MAXPROPS', () => {
        objectSchema = new ObjectSchema(objectSchemaData03);
        objectSchema.validate();
        assert.equal(objectSchema.properties.validation.hasWarning, true);
        assert.equal(
          objectSchema.properties.validation.warning.summary,
          OAS_ERRORS.SCHEMA_OBJECT_PROPS_LENGTH_MUST_LESSTHAN_OR_EQUAL_MAXPROPS
        );
      });
      it('can get error named SCHEMA_OBJECT_PROPS_LENGTH_MUST_GREATERTHAN_OR_EQUAL_MINPROPS', () => {
        objectSchema = new ObjectSchema(objectSchemaData04);
        objectSchema.validate();
        assert.equal(objectSchema.properties.validation.hasWarning, true);
        assert.equal(
          objectSchema.properties.validation.warning.summary,
          OAS_ERRORS.SCHEMA_OBJECT_PROPS_LENGTH_MUST_GREATERTHAN_OR_EQUAL_MINPROPS
        );
      });

      it('can get error named SCHEMA_OBJECT_PROPERTIES_HIERARCHY_RESTRICTED', () => {
        objectSchema = new ObjectSchema(objectSchemaData06);
        objectSchema.validate();
        assert.equal(objectSchema.properties.validation.hasWarning, true);
        assert.equal(
          objectSchema.properties.validation.warning.summary,
          OAS_WARNINGS.SCHEMA_OBJECT_PROPERTIES_HIERARCHY_RESTRICTED
        );
        assert.equal(objectSchema.properties.userInfo.properties.validation.hasWarning, true);
        assert.equal(
          objectSchema.properties.userInfo.properties.validation.warning.summary,
          OAS_WARNINGS.SCHEMA_OBJECT_PROPERTIES_HIERARCHY_RESTRICTED
        );
      });

      it('can pass validation when given valid data', () => {
        objectSchema = new ObjectSchema(objectSchemaData05);
        objectSchema.validate();
        assert.equal(objectSchema.maxProperties.validation.hasWarning, false);
        assert.equal(objectSchema.properties.validation.hasWarning, false);
      });
    });
  });
});
