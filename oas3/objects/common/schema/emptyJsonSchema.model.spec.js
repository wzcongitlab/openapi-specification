import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { EmptyJsonSchema } from './emptyJsonSchema.model';
import { OAS_WARNINGS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('JsonSchema', () => {
  let emptyJsonSchemaData01 = {
    format: '',
    type: '',
  };

  let emptyJsonSchema;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        emptyJsonSchema = new EmptyJsonSchema(emptyJsonSchemaData01);
        assert.equal(Utils.deepEqual(emptyJsonSchemaData01, emptyJsonSchema.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate emptyJsonSchema data', () => {
      it('can get error named SCHEMA_TYPE_REQUIRED', () => {
        emptyJsonSchema = new EmptyJsonSchema(emptyJsonSchemaData01);
        emptyJsonSchema.validate();
        assert.equal(emptyJsonSchema.type.validation.hasError, true);
        assert.equal(
          emptyJsonSchema.type.validation.error.summary,
          OAS_WARNINGS.SCHEMA_TYPE_REQUIRED
        );
      });
    });
  });
});
