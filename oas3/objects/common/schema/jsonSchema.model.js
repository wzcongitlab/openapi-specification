import { CONSTANTS } from '../../../config/constants';
import { StringField } from '../../../fields/string.field';
import { OasSchema } from './oasSchema.model';
import { AnyField } from '../../../fields/any.field';
import { JSON_SCHEMA_TYPES } from '../../../config/oasConfig';

export class JsonSchema extends OasSchema {
  static get nodeObjectType() {
    return 'JsonSchema';
  }

  constructor(params = {}, parent = null, nodeName = CONSTANTS.emptyStr) {
    super(params, parent, nodeName);
    this.type = new StringField(params.type, this, 'type');
    this.title = new StringField(params.title, this, 'title');
    let exampleValueType;
    /* eslint-disable */
    switch (params.type) {
      case JSON_SCHEMA_TYPES.array:
        exampleValueType = CONSTANTS.jsonFieldTypes.object;
        break;
      case JSON_SCHEMA_TYPES.object:
        if (typeof params.example === JSON_SCHEMA_TYPES.object) {
          exampleValueType = CONSTANTS.jsonFieldTypes.object;
        } else {
          // example不为对象类型则默认使用string;
          exampleValueType = JSON_SCHEMA_TYPES.string;
        }
        break;
      case JSON_SCHEMA_TYPES.string:
        exampleValueType = CONSTANTS.jsonFieldTypes.string;
        break;
      case JSON_SCHEMA_TYPES.integer:
        exampleValueType = CONSTANTS.jsonFieldTypes.number;
        break;
      case JSON_SCHEMA_TYPES.number:
        exampleValueType = CONSTANTS.jsonFieldTypes.number;
        break;
      default:
        break;
    }
    /* eslint-disable */
    this.example = new AnyField(params.example, this, 'example', exampleValueType);
  }
}
