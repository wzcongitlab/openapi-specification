import { OasSchemaFactory } from './factories/oasSchemaFactory';
import { OasMapNode } from '../../../base/oasMapNode';
import { JsonSchema } from './jsonSchema.model';
import { Reference } from '../reference.model';

export class Properties extends OasMapNode {
  static get nodeObjectType() {
    return 'Properties';
  }

  constructor(params = {}, parent = null, nodeName = 'properties') {
    super(parent, nodeName);
    for (let key of Object.keys(params)) {
      this.addProperty(key, params[key]);
    }
  }

  addProperty(key, schema) {
    if (schema instanceof JsonSchema || schema instanceof Reference) {
      schema.parent = this;
      this.addItem(key, schema);
      return;
    }
    this.addItem(key, OasSchemaFactory.createSchema(schema, this, key));
  }

  updateProperty(key, schema) {
    // schema 是一个指定schema的oas json数据或schema对象;
    if (schema instanceof JsonSchema || schema instanceof Reference) {
      schema.parent = this;
      this.updateItem(key, schema);
      return;
    }
    this.updateItem(key, OasSchemaFactory.createSchema(schema, this, key));
  }

  removeProperty(key) {
    this.removeItem(key);
  }
}
