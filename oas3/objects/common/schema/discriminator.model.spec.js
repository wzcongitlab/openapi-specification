import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { Discriminator } from './discriminator.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('Discriminator', () => {
  let discriminatorData01 = {
    propertyName: 'propertyName32',
    mapping: {},
  };

  let discriminatorData02 = {
    propertyName: '',
    mapping: {},
  };

  let discriminator;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        discriminator = new Discriminator(discriminatorData01);
        assert.equal(Utils.deepEqual(discriminatorData01, discriminator.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate discriminator data', () => {
      it('can get error named DESCRIMINATOR_PROPNAME_REQUIRED', () => {
        discriminator = new Discriminator(discriminatorData02);
        discriminator.validate();
        assert.equal(discriminator.propertyName.validation.hasError, true);
        assert.equal(
          discriminator.propertyName.validation.error.summary,
          OAS_ERRORS.DESCRIMINATOR_PROPNAME_REQUIRED
        );
      });

      it('can pass validation when given valid data', () => {
        discriminator = new Discriminator(discriminatorData01);
        discriminator.validate();
        assert.equal(discriminator.propertyName.validation.hasError, false);
      });
    });
  });
});
