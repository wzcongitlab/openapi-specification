import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { ExternalDocs } from './externalDocs.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('ExternalDocs', () => {
  let externalDocsData01 = {
    url: 'http://www.kato.com/docs/user-guide',
    description: 'some description about user-guide',
  };

  let externalDocsData02 = {
    url: '',
    description: 'some description about user-guide',
  };

  let externalDocs;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        externalDocs = new ExternalDocs(externalDocsData01);
        assert.equal(Utils.deepEqual(externalDocsData01, externalDocs.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate externalDocs data', () => {
      it('can get error named EXTERNAL_DOC_URL_REQUIRED', () => {
        externalDocs = new ExternalDocs(externalDocsData02);
        externalDocs.validate();
        assert.equal(externalDocs.url.validation.hasError, true);
        assert.equal(
          externalDocs.url.validation.error.summary,
          OAS_ERRORS.EXTERNAL_DOC_URL_REQUIRED
        );
      });

      it('can pass validation when given valid url', () => {
        externalDocs = new ExternalDocs(externalDocsData01);
        externalDocs.validate();
        assert.equal(externalDocs.url.validation.hasError, false);
        assert.equal(externalDocs.url.validation.hasWarning, false);
        assert.equal(externalDocs.description.validation.hasError, false);
        assert.equal(externalDocs.description.validation.hasWarning, false);
      });
    });
  });
});
