import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { ServerVariables } from './serverVariables.model';
import { ServerVariable } from './serverVariable.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('ServerVariables', () => {
  let serverVariablesData01 = {};

  let serverVariablesData02 = {};

  let serverVariables;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        serverVariables = new ServerVariables(serverVariablesData01);
        assert.equal(Utils.deepEqual(serverVariablesData01, serverVariables.extract()), true);
      });
    });

    describe('functions', () => {
      it('can add ServerVariable', () => {
        serverVariables = new ServerVariables(serverVariablesData01);
        let serverVariableName = 'serverVariable1';
        serverVariables.addVariable(serverVariableName);
        assert.equal(serverVariables[serverVariableName] instanceof ServerVariable, true);
      });
      it('can remove ServerVariable', () => {
        serverVariables = new ServerVariables(serverVariablesData01);
        let serverVariableName = 'serverVariable1';
        serverVariables.addVariable(serverVariableName, {});
        serverVariables.removeVariable(serverVariableName);
        assert.equal(serverVariables.itemKeys.includes(serverVariableName), false);
        assert.equal(serverVariables[serverVariableName] === undefined, true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate serverVariables data', () => {
      it('can pass validation when given valid data', () => {
        serverVariables = new ServerVariables(serverVariablesData01);
        serverVariables.validate();
        assert.equal(serverVariables.validation.hasError, false);
        assert.equal(serverVariables.validation.hasWarning, false);
      });
    });
  });
});
