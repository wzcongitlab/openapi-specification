import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { PayloadContent } from './payloadContent.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('PayloadContent', () => {
  let payloadContentData01 = {};

  let payloadContentData02 = {};

  let payloadContent;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        payloadContent = new PayloadContent(payloadContentData01);
        assert.equal(Utils.deepEqual(payloadContentData01, payloadContent.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate payloadContent data', () => {
      it('can pass validation when given valid data', () => {
        payloadContent = new PayloadContent(payloadContentData01);
        payloadContent.validate();
        assert.equal(payloadContent.validation.hasError, false);
        assert.equal(payloadContent.validation.error, null);
      });
    });
  });
});
