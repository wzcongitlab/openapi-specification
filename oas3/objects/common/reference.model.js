import { CONSTANTS } from '../../config/constants';
import { OasNode } from '../../base/oasNode';
import { RefField } from '../../fields/ref.field';
import { OAS_ERRORS } from '../../config/oasErrors';

export class Reference extends OasNode {
  static get nodeObjectType() {
    return 'Reference';
  }

  constructor(params = {}, parent = null, nodeName = CONSTANTS.emptyStr) {
    super(parent, nodeName);
    this.$ref = new RefField(params.$ref, this, '$ref');
  }

  validateNode() {
    if (this.$ref.isEmpty || this.$ref.equal(CONSTANTS.notValid)) {
      this.$ref.validation.update({
        hasError: true,
        summary: OAS_ERRORS.REFERENCE_REQUIRED,
      });
    }
  }
}
