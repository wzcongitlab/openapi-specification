import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { SecurityRequirement } from './securityRequirement.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('SecurityRequirement', () => {
  let securityRequirementData01 = {};

  let securityRequirementData02 = {};

  let securityRequirement;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        securityRequirement = new SecurityRequirement(securityRequirementData01);
        assert.equal(
          Utils.deepEqual(securityRequirementData01, securityRequirement.extract()),
          true
        );
      });
    });
  });

  describe('#validation', () => {
    describe('validate securityRequirement data', () => {
      it('can pass validation when given valid data', () => {
        securityRequirement = new SecurityRequirement(securityRequirementData01);
        securityRequirement.validate();
        assert.equal(securityRequirement.validation.hasError, false);
        assert.equal(securityRequirement.validation.hasWarning, false);
      });
    });
  });
});
