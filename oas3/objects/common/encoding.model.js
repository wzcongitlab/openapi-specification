import { CONSTANTS } from '../../config/constants';
import { OasNode } from '../../base/oasNode';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';
import { StringField } from '../../fields/string.field';
import { BooleanField } from '../../fields/boolean.field';
import { Headers } from '../components/headers.model';

export class Encoding extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'Encoding';
  }

  constructor(params = {}, parent = null, nodeName = CONSTANTS.emptyStr) {
    super(params, parent, nodeName);
    this.contentType = new StringField(params.contentType, this, 'contentType');
    this.headers = new Headers(params.headers, this);
    this.style = new StringField(params.style, this, 'style');
    this.explode = new BooleanField(params.explode, this, 'explode');
    this.allowReserved = new BooleanField(params.allowReserved, this, 'allowReserved');
  }
}
