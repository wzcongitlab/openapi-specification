import { CONSTANTS } from '../../config/constants';
import { OasNode } from '../../base/oasNode';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';
import { OasSchemaFactory } from './schema/factories/oasSchemaFactory';
import { AnyField } from '../../fields/any.field';
import { Examples } from '../components/examples.model';
import { EncodingMap } from './encodingMap.model';
import { RequestBody } from '../components/requestBody.model';
import { OAS_ERRORS } from '../../config/oasErrors';

export class Media extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'Media';
  }

  constructor(params = {}, parent, nodeName = CONSTANTS.emptyStr) {
    super(params, parent, nodeName);
    this.schema = OasSchemaFactory.createSchema(params.schema, this, 'schema');
    this.example = new AnyField(params.example, this, 'example');
    this.examples = new Examples(params.examples, this);
    this.encoding = new EncodingMap(params.encoding, this);
  }

  validateNode() {
    // 校验example/examples的互斥性;
    if (!this.example.isEmpty && !this.examples.isEmpty) {
      this.validation.update({
        hasError: true,
        summary: OAS_ERRORS.EXCLUSIVE_EXAMPLE_AND_EXAMPLES,
      });
    }
    // The encoding object SHALL only apply to requestBody objects when the media type is multipart or application/x-www-form-urlencoded
    if (
      !this.encoding.isEmpty &&
      !(
        this.parent.parent instanceof RequestBody &&
        (this.nodeName.includes('multipart') ||
          this.nodeName === 'application/x-www-form-urlencoded')
      )
    ) {
      this.encoding.validation.update({
        hasWarning: true,
        summary: OAS_ERRORS.INEFFECTIVE_ENCODING,
      });
    }
  }
}
