import { CONSTANTS } from '../../config/constants';
import { OasNode } from '../../base/oasNode';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';
import { EnumValues } from './enumValues.model';
import { StringField } from '../../fields/string.field';
import { RichTextField } from '../../fields/richText.field';
import { OAS_ERRORS } from '../../config/oasErrors';

export class ServerVariable extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'ServerVariable';
  }

  constructor(params = {}, parent = null, nodeName = CONSTANTS.emptyStr) {
    super(params, parent, nodeName);
    this.enum = new EnumValues(params.enum, this);
    this.default = new StringField(params.default, this, 'default');
    this.description = new RichTextField(params.description, this, 'description');
  }

  validateNode() {
    if (this.default.isEmpty) {
      this.default.validation.update({
        hasError: true,
        summary: OAS_ERRORS.SERVER_VARIABLE_DEFAULT_REQUIRED,
      });
      return;
    }
    if (this.enum.length > 0) {
      if (!this.enum.value.some(e => e.value === this.default.value)) {
        this.default.validation.update({
          summary: OAS_ERRORS.SERVER_VARIABLE_DEFAULT_NOT_IN_ENUM,
        });
      }
    }
  }
}
