import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { RequiredFields } from './requiredFields.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('RequiredFields', () => {
  let requiredFieldsData01 = [];

  let requiredFieldsData02 = [];

  let requiredFields;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        requiredFields = new RequiredFields(requiredFieldsData01);
        assert.equal(Utils.deepEqual(requiredFieldsData01, requiredFields.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate requiredFields data', () => {
      it('can pass validation when given valid data', () => {
        requiredFields = new RequiredFields(requiredFieldsData01);
        requiredFields.validate();
        assert.equal(requiredFields.validation.hasError, false);
        assert.equal(requiredFields.validation.hasWarning, false);
      });
    });
  });
});
