import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Reference } from './reference.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Reference', () => {
  let referenceData01 = {
    $ref: '$ref71',
  };

  let referenceData02 = {
    $ref: '',
  };

  let reference;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        reference = new Reference(referenceData01);
        assert.equal(Utils.deepEqual(referenceData01, reference.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate reference data', () => {
      it('can get error named REFERENCE_REQUIRED', () => {
        reference = new Reference(referenceData02);
        reference.validate();
        assert.equal(reference.$ref.validation.hasError, true);
        assert.equal(reference.$ref.validation.error.summary, OAS_ERRORS.REFERENCE_REQUIRED);
      });

      it('can pass validation when given valid data', () => {
        reference = new Reference(referenceData01);
        reference.validate();
        assert.equal(reference.$ref.validation.hasError, false);
        assert.equal(reference.$ref.validation.error, null);
      });
    });
  });
});
