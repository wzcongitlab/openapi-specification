import { CONSTANTS } from '../../config/constants';
import { StringField } from '../../fields/string.field';
import { OasListNode } from '../../base/oasListNode';
import { OAS_WARNINGS } from '../../config/oasErrors';

export class SecurityRequirementScopes extends OasListNode {
  static get nodeObjectType() {
    return 'SecurityRequirementScopes';
  }

  constructor(params = [], parent, nodeName = CONSTANTS.emptyStr) {
    super(parent, nodeName);
    if (params instanceof Array) {
      params.forEach(p => this.addScope(p));
    }
  }

  addScope(scope) {
    if (scope instanceof StringField) {
      scope.parent = this;
      return this.addItem(scope);
    }
    let newScope = new StringField(scope, this);
    return this.addItem(newScope);
  }

  removeScope(index) {
    return this.removeItem(index);
  }

  validateNode() {
    for (let scope of this.items) {
      if (scope.value === CONSTANTS.notValid) {
        scope.validation.update({
          hasWarning: true,
          summary: OAS_WARNINGS.INVALID_SECURITY_SCOPE,
        });
      }
    }
  }
}
