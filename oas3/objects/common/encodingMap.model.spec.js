import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { EncodingMap } from './encodingMap.model';
import { Encoding } from './encoding.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('EncodingMap', () => {
  let encodingMapData01 = {};

  let encodingMapData02 = {};

  let encodingMap;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        encodingMap = new EncodingMap(encodingMapData01);
        assert.equal(Utils.deepEqual(encodingMapData01, encodingMap.extract()), true);
      });
    });
    describe('functions', () => {
      it('can add Encoding', () => {
        encodingMap = new EncodingMap(encodingMapData01);
        let encodingName = 'encoding1';
        encodingMap.addEncoding(encodingName);
        assert.equal(encodingMap[encodingName] instanceof Encoding, true);
      });
      it('can remove Encoding', () => {
        encodingMap = new EncodingMap(encodingMapData01);
        let encodingName = 'encoding1';
        encodingMap.addEncoding(encodingName, {});
        encodingMap.removeEncoding(encodingName);
        assert.equal(encodingMap.itemKeys.includes(encodingName), false);
        assert.equal(encodingMap[encodingName] === undefined, true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate encodingMap data', () => {
      it('can pass validation when given valid data', () => {
        encodingMap = new EncodingMap(encodingMapData01);
        encodingMap.validate();
        assert.equal(encodingMap.validation.hasError, false);
        assert.equal(encodingMap.validation.hasWarning, false);
      });
    });
  });
});
