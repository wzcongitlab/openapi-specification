import { StringField } from '../../fields/string.field';
import { OasNode } from '../../base/oasNode';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';
import { ServerVariables } from './serverVariables.model';
import { RichTextField } from '../../fields/richText.field';
import { OAS_ERRORS, OAS_WARNINGS } from '../../config/oasErrors';

export class Server extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'Server';
  }

  constructor(params, parent = null, nodeName = 'server') {
    super(params, parent, nodeName);
    // url字段可能包含variable的占位, 用普通字符串类型
    this.url = new StringField(params.url, this, 'url');
    this.description = new RichTextField(params.description, this, 'description');
    this.variables = new ServerVariables(params.variables, this);
  }

  validateNode() {
    if (this.url.isEmpty) {
      this.url.validation.update({
        hasError: true,
        summary: OAS_ERRORS.SERVER_URL_REQUIRED,
      });
    }
    for (let key of this.variables.itemKeys) {
      if (!this.url.value.includes(`\{${key}\}`)) {
        this.variables[key].validation.update({
          summary: OAS_WARNINGS.SERVER_VARIABLE_NOT_USED,
        });
      }
    }
  }
}
