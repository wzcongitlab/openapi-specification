import { OasNode } from '../../base/oasNode';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';
import { RichTextField } from '../../fields/richText.field';
import { UrlField } from '../../fields/url.field';
import { OAS_ERRORS } from '../../config/oasErrors';

export class ExternalDocs extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'ExternalDocs';
  }

  constructor(params = {}, parent = null, nodeName = 'externalDocs') {
    super(params, parent, nodeName);
    this.url = new UrlField(params.url, this, 'url');
    this.description = new RichTextField(params.description, this, 'description');
  }

  validateNode() {
    if (this.url.isEmpty) {
      this.url.validation.update({
        hasError: true,
        summary: OAS_ERRORS.EXTERNAL_DOC_URL_REQUIRED,
      });
    }
  }
}
