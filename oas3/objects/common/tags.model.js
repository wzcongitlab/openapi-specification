import { OAS_ERRORS } from '../../config/oasErrors';
import { OasListNode } from '../../base/oasListNode';
import { Tag } from './tag.model';

export class Tags extends OasListNode {
  static get nodeObjectType() {
    return 'Tags';
  }

  constructor(params = [], parent, nodeName = 'tags') {
    super(parent, nodeName);
    if (params instanceof Array) {
      params.forEach(p => this.addTag(p));
    }
  }

  addTag(tag) {
    if (tag instanceof Tag) {
      tag.parent = this;
      return this.addItem(tag);
    }
    let newTag = new Tag(tag, this);
    return this.addItem(newTag);
  }

  removeTag(index) {
    return this.removeItem(index);
  }

  validateNode() {
    for (let t of this.value) {
      this.value.forEach(v => {
        if (v === t) {
          return;
        }
        if (v.name.validation.hasError) {
          return;
        }
        if (v.name.value === t.name.value) {
          t.name.validation.update({
            hasError: true,
            summary: OAS_ERRORS.DOCUMENT_TAGS_NAME_NOT_UNIQUE,
          });
          v.name.validation.update({
            hasError: true,
            summary: OAS_ERRORS.DOCUMENT_TAGS_NAME_NOT_UNIQUE,
          });
        }
      });
    }
  }
}
