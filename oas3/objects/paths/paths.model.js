import { OasMapNode } from '../../base/oasMapNode';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';
import { PathFactory } from './pathFactory';

export class Paths extends OasExtensibleNode(OasMapNode) {
  static get nodeObjectType() {
    return 'Paths';
  }

  constructor(params = {}, parent = null, nodeName = 'paths') {
    super(params, parent, nodeName);
    for (let key of Object.keys(params)) {
      this.addPathItem(key, params[key]);
    }
  }

  addPathItem(key, params) {
    this.addItem(key, PathFactory.createPathItem(params, this, key));
  }

  updatePathItem(key, params) {
    // params 是一个指定path的oas json数据;
    this.updateItem(key, PathFactory.createPathItem(params, this, key));
  }

  removePathItem(key) {
    this.removeItem(key);
  }
}
