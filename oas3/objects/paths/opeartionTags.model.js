import { OasListNode } from '../../base/oasListNode';
import { StringField } from '../../fields/string.field';

export class OperationTags extends OasListNode {
  static get nodeObjectType() {
    return 'OperationTags';
  }

  constructor(params = [], parent, nodeName = 'tags') {
    super(parent, nodeName);
    this.value = [];
    if (params instanceof Array) {
      params.forEach(p => this.addTag(p));
    }
  }

  addTag(tag) {
    if (tag instanceof StringField) {
      tag.parent = this;
      this.addItem(tag);
      return;
    }
    this.addItem(new StringField(tag, this));
  }

  removeTag(index) {
    this.removeItem(index);
  }
}
