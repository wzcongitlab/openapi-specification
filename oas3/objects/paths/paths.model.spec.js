import chai from 'chai';
import { Paths } from './paths.model';
import { petStoreOas } from '../../../mock/petStore.oas';

const assert = chai.assert;

describe('Paths', () => {
  let paths = petStoreOas.paths;
  let pathsObject;

  describe('functions', () => {
    beforeEach(() => {
      pathsObject = new Paths(paths);
    });

    it('can initialize all path item keys from oas paths object', () => {
      for (let key of Object.keys(paths)) {
        assert.equal(true, pathsObject.hasOwnProperty(key));
      }
    });
    it('can add path item to paths', () => {
      let testPathKey = '/testpath';
      pathsObject.addPathItem(testPathKey, {});
      assert.equal(true, pathsObject.hasOwnProperty(testPathKey));
    });
    it('can remove path item from paths', () => {
      let testPathKey = '/testpath';
      pathsObject.addPathItem(testPathKey, {});
      assert.equal(true, pathsObject.hasOwnProperty(testPathKey));
      pathsObject.removePathItem(testPathKey);
      assert.equal(false, pathsObject.hasOwnProperty(testPathKey));
    });
  });
});
