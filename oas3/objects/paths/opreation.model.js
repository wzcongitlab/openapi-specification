import { CONSTANTS } from '../../config/constants';
import { OasNode } from '../../base/oasNode';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';
import { Servers } from '../common/servers.model';
import { ExternalDocs } from '../common/externalDocs.model';
import { StringField } from '../../fields/string.field';
import { BooleanField } from '../../fields/boolean.field';
import { OperationTags } from './opeartionTags.model';
import { RichTextField } from '../../fields/richText.field';
import { ParameterList } from './parameterList.model';
import { Responses } from '../components/responses.model';
import { Callbacks } from '../components/callbacks.model';
import { Security } from '../common/security.model';
import { ComponentFactory } from '../components/componentFactory';
import { OAS_COMPONENT_TYPES } from '../../config/oasConfig';
import { OAS_ERRORS, OAS_WARNINGS } from '../../config/oasErrors';

export class Operation extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'Operation';
  }

  constructor(params = {}, parent = null, nodeName = CONSTANTS.emptyStr) {
    super(params, parent, nodeName);
    this.tags = new OperationTags(params.tags, this);
    this.summary = new StringField(params.summary, this, 'summary');
    this.description = new RichTextField(params.description, this, 'description');
    this.externalDocs = params.externalDocs ? new ExternalDocs(params.externalDocs, this) : null;
    this.operationId = new StringField(params.operationId, this, 'operationId');
    this.parameters = new ParameterList(params.parameters, this);
    if (params.requestBody) {
      this.requestBody = ComponentFactory.createComponent({
        params: params.requestBody,
        parent: this,
        type: OAS_COMPONENT_TYPES.requestBody.name,
        nodeName: 'requestBody',
      });
    } else {
      this.requestBody = null;
    }
    this.responses = new Responses(params.responses, this);
    this.callbacks = new Callbacks(params.callbacks, this);
    this.deprecated = new BooleanField(params.deprecated, this, 'deprecated');
    this.security = new Security(params.security, this);
    this.servers = new Servers(params.servers, this);
  }

  addExternalDocs(externalDocs) {
    this.externalDocs = new ExternalDocs(externalDocs, this);
  }

  removeExternalDocs() {
    this.externalDocs = null;
  }

  addRequestBody(requestBody = {}) {
    this.requestBody = ComponentFactory.createComponent({
      params: requestBody,
      parent: this,
      type: OAS_COMPONENT_TYPES.requestBody.name,
      nodeName: 'requestBody',
    });
  }

  removeRequestBody() {
    this.requestBody = null;
  }

  validateNode() {
    if (this.responses.isEmpty) {
      this.responses.validation.update({
        hasError: true,
        summary: OAS_ERRORS.OPERATION_RESPONSES_REQUIRED,
      });
    }

    if (this.operationId.isEmpty) {
      this.operationId.validation.update({
        hasError: true,
        summary: OAS_ERRORS.OPERATION_ID_REQUIRED,
      });
    }
  }
}
