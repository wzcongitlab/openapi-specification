import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { OperationTags } from './opeartionTags.model';
import { StringField } from '../../fields/string.field';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('OperationTags', () => {
  let operationTagsData01 = [];

  let operationTagsData02 = [];

  let operationTags;

  let operationTag = 'pet';

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        operationTags = new OperationTags(operationTagsData01);
        assert.equal(Utils.deepEqual(operationTagsData01, operationTags.extract()), true);
      });
    });
    describe('#functions', () => {
      it('can add tag item to operationTags', () => {
        operationTags = new OperationTags([]);
        operationTags.addTag(operationTag);
        assert.equal(operationTags.length === 1, true);
        assert.equal(operationTags.value[0] instanceof StringField, true);
        assert.equal(Utils.deepEqual(operationTags.value[0].extract(), operationTag), true);
      });
      it('can remove tag item from operationTags', () => {
        operationTags = new OperationTags([]);
        operationTags.addTag(operationTag);
        assert.equal(operationTags.length === 1, true);
        assert.equal(operationTags.value[0] instanceof StringField, true);
        assert.equal(Utils.deepEqual(operationTags.value[0].extract(), operationTag), true);
        operationTags.removeTag(0);
        assert.equal(operationTags.isEmpty, true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate operationTags data', () => {
      it('can pass validation when given valid data', () => {
        operationTags = new OperationTags(operationTagsData01);
        operationTags.validate();
        assert.equal(operationTags.validation.hasError, false);
        assert.equal(operationTags.validation.hasWarning, false);
      });
    });
  });
});
