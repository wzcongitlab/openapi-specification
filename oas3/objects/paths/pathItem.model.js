import { CONSTANTS } from '../../config/constants';
import { OasNode } from '../../base/oasNode';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';
import { Parameter } from '../components/parameter.model';
import { StringField } from '../../fields/string.field';
import { RichTextField } from '../../fields/richText.field';
import { Operation } from './opreation.model';
import { ParameterList } from './parameterList.model';
import { Servers } from '../common/servers.model';
import { OPERATION_TYPE_LIST, PARAMETER_LOCATIONS } from '../../config/oasConfig';
import { OAS_ERRORS } from '../../config/oasErrors';

export class PathItem extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'PathItem';
  }

  constructor(params = {}, parent = null, nodeName = CONSTANTS.emptyStr) {
    super(params, parent, nodeName);
    this.summary = new StringField(params.summary, this, 'summary');
    this.description = new RichTextField(params.description, this, 'description');
    this.parameters = new ParameterList(params.parameters, this);
    this.servers = new Servers(params.servers, this);
    for (let operationKey of OPERATION_TYPE_LIST) {
      if (params.hasOwnProperty(operationKey)) {
        this[operationKey] = new Operation(params[operationKey], this, operationKey);
      }
    }
  }

  addOperation(operationKey, operation) {
    if (OPERATION_TYPE_LIST.includes(operationKey)) {
      if (operation instanceof Operation) {
        operation.parent = this;
        this[operationKey] = operation;
        return;
      }
      this[operationKey] = new Operation(operation, this, operationKey);
    }
  }

  hasOperation(operationKey) {
    return this[operationKey] instanceof Operation;
  }

  removeOperation(operationKey) {
    if (this[operationKey] instanceof Operation) {
      delete this[operationKey];
    }
  }

  validateNode() {
    for (let p of this.parameters.value) {
      if (p instanceof Parameter) {
        if (!p.name.isEmpty && p.in.equal(PARAMETER_LOCATIONS.path)) {
          if (!this.nodeName.includes(`/{${p.name.value}}`)) {
            p.name.validation.update({
              hasError: true,
              summary: OAS_ERRORS.UNDECLARED_PATH_PARAMETER_NAME,
            });
          }
        }
      }
    }
  }
}
