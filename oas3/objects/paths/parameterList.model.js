import { OasListNode } from '../../base/oasListNode';
import { Reference } from '../common/reference.model';
import { Parameter } from '../components/parameter.model';
import { ComponentFactory } from '../components/componentFactory';
import { OAS_COMPONENT_TYPES } from '../../config/oasConfig';
import { OAS_ERRORS } from '../../config/oasErrors';

export class ParameterList extends OasListNode {
  static get nodeObjectType() {
    return 'ParameterList';
  }

  constructor(params = [], parent, nodeName = 'parameters') {
    super(parent, nodeName);
    if (params instanceof Array) {
      params.forEach(p => this.addParameter(p));
    }
  }

  addParameter(parameter) {
    if (parameter instanceof Parameter || parameter instanceof Reference) {
      parameter.parent = this;
      return this.addItem(parameter);
    }
    let newParameter = ComponentFactory.createComponent({
      params: parameter,
      parent: this,
      type: OAS_COMPONENT_TYPES.parameter.name,
      nodeName: this.length,
    });
    return this.addItem(newParameter);
  }

  removeParameter(index) {
    return this.removeItem(index);
  }

  validateNode() {
    for (let t of this.value) {
      if (!(t instanceof Parameter) || t.name.isEmpty) {
        return;
      }
      this.value.forEach(v => {
        if (v === t) {
          return;
        }
        if (!(v instanceof Parameter) || v.name.isEmpty) {
          return;
        }
        if (v.name.equal(t.name.value) && v.in.equal(t.in.value)) {
          t.validation.update({
            hasError: true,
            summary: OAS_ERRORS.DUPLICATED_PARAMETERS,
          });
          v.validation.update({
            hasError: true,
            summary: OAS_ERRORS.DUPLICATED_PARAMETERS,
          });
        }
      });
    }
  }
}
