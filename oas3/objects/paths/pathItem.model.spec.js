import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { OPERATION_TYPES } from '../../config/oasConfig';
import { PathItem } from './pathItem.model';
import { Paths } from './paths.model';
import { Operation } from './opreation.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('PathItem', () => {
  let pathItemData01 = {
    summary: 'summary1',
    description: 'description96',
    parameters: [],
    servers: [],
  };

  let pathItemData02 = {
    summary: 'summary1',
    description: 'description96',
    parameters: [],
    servers: [],
  };

  let pathItem;

  let operationData01 = {
    tags: [],
    summary: 'summary62',
    description: 'description53',
    operationId: 'operationId7',
    parameters: [],
    requestBody: {
      required: false,
      description: 'description54',
      content: {},
    },
    responses: {},
    callbacks: {},
    deprecated: false,
    security: [],
    servers: [],
  };

  let parameterData01 = {
    name: 'name83',
    in: 'path',
    description: 'description86',
    required: false,
    deprecated: false,
    allowEmptyValue: false,
    style: 'query',
    explode: false,
    allowReserved: false,
    example: 'example9',
    schema: {
      type: 'integer',
      format: 'int32',
      nullable: true,
      description: 'quantity of order',
      multipleOf: 1,
      maximum: 10000,
      exclusiveMaximum: false,
      minimum: 1,
      exclusiveMinimum: true,
      default: 1,
      enum: [],
      example: '',
      title: 'title001',
    },
    examples: {},
  };

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        pathItem = new PathItem(pathItemData01);
        assert.equal(Utils.deepEqual(pathItemData01, pathItem.extract()), true);
      });
    });
    describe('functions', () => {
      it('can add Operation', () => {
        let operation = new Operation(operationData01);
        let operationName = OPERATION_TYPES.post.name;
        pathItem.addOperation(operationName, operation);
        assert.equal(pathItem[operationName] instanceof Operation, true);
        assert.equal(Utils.deepEqual(pathItem[operationName].extract(), operationData01), true);
      });
      it('can remove Operation', () => {
        let operationName = OPERATION_TYPES.post.name;
        pathItem.addOperation(operationName, operationData01);
        assert.equal(pathItem[operationName] instanceof Operation, true);
        assert.equal(Utils.deepEqual(pathItem[operationName].extract(), operationData01), true);
        pathItem.removeOperation(operationName);
        assert.equal(pathItem[operationName] === undefined, true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate pathItem data', () => {
      it('can pass validation when given valid data', () => {
        pathItem = new PathItem(pathItemData01);
        pathItem.validate();
        assert.equal(pathItem.validation.hasError, false);
        assert.equal(pathItem.validation.error, null);
      });
    });
    it('can get error named UNDECLARED_PATH_PARAMETER_NAME', () => {
      let paths = new Paths();
      let pathItemKey = '/pet/{petType}/{petId}';
      paths.addPathItem(pathItemKey, pathItemData01);
      let path = paths[pathItemKey];
      path.parameters.addParameter(parameterData01);
      path.validate();
      assert.equal(path.parameters.value[0].name.validation.hasError, true);
      assert.equal(
        path.parameters.value[0].name.validation.error.summary,
        OAS_ERRORS.UNDECLARED_PATH_PARAMETER_NAME
      );
    });
  });
});
