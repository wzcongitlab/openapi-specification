import { PathItem } from './pathItem.model';

export class PathFactory {
  // 根据所给path item是否为$ref, 创建引用对象或path item对象;
  static createPathItem(params = {}, parent = null, nodeName = '') {
    // TODO 添加外部path对象引用时的支持
    // if (params.hasOwnProperty('$ref')) {
    //   return new Reference(params, parent, nodeName);
    // }
    // $ref
    // Allows for an external definition of this path item. The referenced structure MUST be in the format of a Path Item Object. If there are conflicts between the referenced definition and this Path Item's definition, the behavior is undefined.
    // https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md#pathItemObject
    return new PathItem(params, parent, nodeName);
  }
}
