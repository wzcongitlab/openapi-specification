import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { ParameterList } from './parameterList.model';
import { Parameter } from '../components/parameter.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('ParameterList', () => {
  let parameterListData01 = [];

  let parameterListData02 = [];

  let parameterList;

  let parameterData01 = {
    name: 'name83',
    in: 'path',
    description: 'description86',
    required: false,
    deprecated: false,
    allowEmptyValue: false,
    style: 'query',
    explode: false,
    allowReserved: false,
    example: 'example9',
    schema: {
      type: 'integer',
      format: 'int32',
      nullable: false,
      readOnly: false,
      writeOnly: false,
      deprecated: false,
      description: 'quantity of order',
      multipleOf: 1,
      maximum: 10000,
      exclusiveMaximum: false,
      minimum: 1,
      exclusiveMinimum: true,
      default: 1,
      enum: [],
      example: 11,
      title: 'title001',
    },
    examples: {},
  };

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        parameterList = new ParameterList(parameterListData01);
        assert.equal(Utils.deepEqual(parameterListData01, parameterList.extract()), true);
      });
    });
    describe('#functions', () => {
      it('can add tag item to parameterList', () => {
        parameterList = new ParameterList([]);
        parameterList.addParameter(parameterData01);
        assert.equal(parameterList.length === 1, true);
        assert.equal(parameterList.value[0] instanceof Parameter, true);
        assert.equal(Utils.deepEqual(parameterList.value[0].extract(), parameterData01), true);
      });
      it('can remove tag item from parameterList', () => {
        parameterList = new ParameterList([]);
        parameterList.addParameter(parameterData01);
        assert.equal(parameterList.length === 1, true);
        assert.equal(parameterList.value[0] instanceof Parameter, true);
        assert.equal(Utils.deepEqual(parameterList.value[0].extract(), parameterData01), true);
        parameterList.removeParameter(0);
        assert.equal(parameterList.isEmpty, true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate parameterList data', () => {
      it('can pass validation when given valid data', () => {
        parameterList = new ParameterList(parameterListData01);
        parameterList.addParameter(parameterData01);
        parameterList.validate();
        assert.equal(parameterList.validation.hasError, false);
        assert.equal(parameterList.validation.hasWarning, false);
      });
    });
    it('can get error named DUPLICATED_PARAMETERS', () => {
      parameterList = new ParameterList(parameterListData01);
      parameterList.addParameter(parameterData01);
      parameterList.addParameter(parameterData01);
      parameterList.validate();
      assert.equal(parameterList.value[0].validation.hasError, true);
      assert.equal(parameterList.value[1].validation.hasError, true);
      assert.equal(
        parameterList.value[0].validation.error.summary,
        OAS_ERRORS.DUPLICATED_PARAMETERS
      );
      assert.equal(
        parameterList.value[1].validation.error.summary,
        OAS_ERRORS.DUPLICATED_PARAMETERS
      );
    });
  });
});
