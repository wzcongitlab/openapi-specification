import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Operation } from './opreation.model';
import { OAS_ERRORS, OAS_WARNINGS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Operation', () => {
  let operationData01 = {
    tags: [],
    summary: 'summary62',
    description: 'description53',
    operationId: 'operationId7',
    parameters: [],
    requestBody: {
      required: false,
      description: 'description54',
      content: {},
    },
    responses: {},
    callbacks: {},
    deprecated: false,
    security: [],
    servers: [],
  };

  let operationData02 = {
    tags: [],
    summary: 'summary62',
    description: 'description53',
    operationId: 'operationId7',
    parameters: [],
    requestBody: {
      required: false,
      description: '',
      content: {},
    },
    responses: {
      '405': {
        content: {},
        description: 'Invalid input',
        headers: {},
        links: {},
      },
    },
    callbacks: {},
    deprecated: false,
    security: [],
    servers: [],
  };

  let operationData03 = {
    tags: [],
    summary: 'summary62',
    description: 'description53',
  };

  let operation;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        operation = new Operation(operationData02);
        assert.equal(Utils.deepEqual(operationData02, operation.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate operation data', () => {
      it('can get error named OPERATION_ID_REQUIRED', () => {
        operation = new Operation(operationData03);
        operation.validate();
        assert.equal(operation.operationId.validation.hasError, false);
        assert.equal(operation.operationId.validation.hasWarning, true);
        assert.equal(
          operation.operationId.validation.warning.summary,
          OAS_WARNINGS.OPERATION_ID_REQUIRED
        );
      });

      it('can get error named OPERATION_RESPONSES_REQUIRED', () => {
        operation = new Operation(operationData01);
        operation.validate();
        assert.equal(operation.responses.validation.hasError, true);
        assert.equal(operation.responses.validation.hasWarning, false);
        assert.equal(
          operation.responses.validation.error.summary,
          OAS_ERRORS.OPERATION_RESPONSES_REQUIRED
        );
      });

      it('can pass validation when given valid data', () => {
        operation = new Operation(operationData02);
        operation.validate();
        assert.equal(operation.responses.validation.hasError, false);
        assert.equal(operation.responses.validation.hasWarning, false);
      });
    });
  });
});
