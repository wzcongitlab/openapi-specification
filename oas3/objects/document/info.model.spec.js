import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Info } from './info.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Info', () => {
  let infoData01 = {
    description:
      'This is a sample server Petstore server. For this sample, you can use the api key `special-key` to test the authorization filters.',
    license: {
      name: 'Apache-2.0',
      url: 'http://www.apache.org/licenses/LICENSE-2.0.html',
    },
    title: 'OpenAPI Petstore',
    version: '1.0.0',
    contact: {
      name: 'Wangzc',
      email: '',
      url: 'http://www.apache.org/licenses',
    },
    termsOfService: 'http://www.apache.org/licenses/LICENSE-2.0.html',
  };

  let infoData02 = {
    description:
      'This is a sample server Petstore server. For this sample, you can use the api key `special-key` to test the authorization filters.',
    license: {
      name: 'Apache-2.0',
      url: 'http://www.apache.org/licenses/LICENSE-2.0.html',
    },
    title: '',
    version: '',
    contact: {
      name: 'Wangzc',
      email: '',
      url: 'http://www.apache.org/licenses',
    },
    termsOfService: 'http://www.apache.org/licenses/LICENSE-2.0.html',
  };

  let infoData03 = {
    description:
      'This is a sample server Petstore server. For this sample, you can use the api key `special-key` to test the authorization filters.',
    license: {
      name: 'Apache-2.0',
      url: 'http://www.apache.org/licenses/LICENSE-2.0.html',
    },
    title: '',
    version: '',
    contact: {
      name: 'Wangzc',
      email: '',
      url: 'http://www.apache.org/licenses',
    },
    termsOfService: 'http://www.apache.org/licenses/LICENSE-2.0.html',
    'x-1': 'x-1',
    'x-2': false,
    'x-3': [1, 3, 4],
    'x-4': {
      a: '11',
      b: '123',
    },
  };

  let info;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        info = new Info(infoData01);
        assert.equal(Utils.deepEqual(infoData01, info.extract()), true);
      });

      it('can extract correct data with extensions', () => {
        info = new Info(infoData03);
        assert.equal(Utils.deepEqual(infoData03, info.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate info data', () => {
      it('can get error named INFO_TITLE_REQUIRED', () => {
        info = new Info(infoData02);
        info.validate();
        assert.equal(info.title.validation.hasError, true);
        assert.equal(info.title.validation.error.summary, OAS_ERRORS.INFO_TITLE_REQUIRED);
      });

      it('can get error named INFO_VERSION_REQUIRED', () => {
        info = new Info(infoData02);
        info.validate();
        assert.equal(info.version.validation.hasError, true);
        assert.equal(info.version.validation.error.summary, OAS_ERRORS.INFO_VERSION_REQUIRED);
      });

      it('can pass validation when given valid info title and version', () => {
        info = new Info(infoData01);
        info.validate();
        assert.equal(info.title.validation.hasError, false);
        assert.equal(info.title.validation.error, null);
        assert.equal(info.version.validation.hasError, false);
        assert.equal(info.version.validation.error, null);
      });
    });
  });
});
