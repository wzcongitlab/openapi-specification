import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Contact } from './contact.model';

const assert = chai.assert;

describe('Contact', () => {
  let contact;

  let params = {
    name: 'Wangzc',
    email: 'Wangzc@fiberhome.com',
    url: 'http://www.apache.org/licenses',
  };

  describe('#fields', () => {
    beforeEach(() => {
      contact = new Contact(params);
    });
    describe('data extracting', () => {
      it('extract oas node and get oas data that has the same key-values as input params', () => {
        Utils.deepEqual(contact.extract(), params).should.equal(true);
      });
    });
  });
});
