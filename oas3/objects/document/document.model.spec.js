import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Document } from './document.model';
import { petStoreOas } from '../../../mock/petStore.oas';
import { OasNode } from '../../base/oasNode';

const assert = chai.assert;

describe('Document', () => {
  let document;

  let params = petStoreOas;

  describe('#funcs', () => {
    // [TODO] 添加整体document的数据导入导出的单元测试;
    // describe('#data extraction', () => {
    //   it('extract oas node and get oas data that has same key-values with input params', () => {
    //     assert.equal(Utils.deepEqual(document.extract(), params), true);
    //   });
    // });

    describe('#data extraction', () => {
      it('document can be extend with specification extensions(anyField)', () => {
        document = new Document(params);
        let docData = document.extract();
        assert.equal(Utils.deepEqual(docData['x-1'], params['x-1']), true);
        assert.equal(Utils.deepEqual(docData['x-2'], params['x-2']), true);
        assert.equal(Utils.deepEqual(docData['x-3'], params['x-3']), true);
        assert.equal(Utils.deepEqual(docData['x-4'], params['x-4']), true);
        assert.equal(Utils.deepEqual(docData['x-5'], params['x-5']), true);
      });
    });

    describe('#object functions', () => {
      it('can get component by reference path', () => {
        document = new Document(params);
        let refString = '#/components/schemas/Pet';
        assert.equal(document.getComponentByRefPath(refString) instanceof OasNode, true);
      });
    });
  });
});
