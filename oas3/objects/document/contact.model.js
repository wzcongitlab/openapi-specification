import { OasNode } from '../../base/oasNode';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';
import { StringField } from '../../fields/string.field';
import { EmailField } from '../../fields/email.field';
import { UrlField } from '../../fields/url.field';

export class Contact extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'Contact';
  }

  constructor(params = {}, parent = null, nodeName = 'contact') {
    super(params, parent, nodeName);
    this.name = new StringField(params.name, this, 'name');
    this.url = new UrlField(params.url, this, 'url');
    this.email = new EmailField(params.email, this, 'email');
  }
}
