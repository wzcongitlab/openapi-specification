import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { License } from './license.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('License', () => {
  let licenseData01 = {
    name: 'MIT',
    url: 'http://www.kato.com/docs/user-guide',
  };

  let licenseData02 = {
    name: '',
    url: 'http://www.kato.com/docs/user-guide',
  };

  let licenseData03 = {
    name: '',
    url: 'http://www.kato.com/docs/user-guide',
    'x-1': 'x-1',
    'x-2': false,
    'x-3': [1, 3, 4],
    'x-4': {
      a: '11',
      b: '123',
    },
  };

  let license;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        license = new License(licenseData01);
        assert.equal(Utils.deepEqual(licenseData01, license.extract()), true);
      });

      it('can extract correct data with extensions', () => {
        license = new License(licenseData03);
        assert.equal(Utils.deepEqual(licenseData03, license.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate license data', () => {
      it('can get error named LICENSE_NAME_REQUIRED', () => {
        license = new License(licenseData02);
        license.validate();
        assert.equal(license.name.validation.hasError, true);
        assert.equal(license.name.validation.error.summary, OAS_ERRORS.LICENSE_NAME_REQUIRED);
      });

      it('can pass validation when given valid license name', () => {
        license = new License(licenseData01);
        license.validate();
        assert.equal(license.validation.hasError, false);
        assert.equal(license.validation.error, null);
      });
    });
  });
});
