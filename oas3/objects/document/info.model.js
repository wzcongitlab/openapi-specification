import { OasNode } from '../../base/oasNode';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';
import { Contact } from './contact.model';
import { License } from './license.model';
import { StringField } from '../../fields/string.field';
import { UrlField } from '../../fields/url.field';
import { RichTextField } from '../../fields/richText.field';
import { OAS_ERRORS } from '../../config/oasErrors';

export class Info extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'Info';
  }

  constructor(params = {}, parent = null, nodeName = 'info') {
    super(params, parent, nodeName);
    this.title = new StringField(params.title, this, 'title');
    this.description = new RichTextField(params.description, this, 'description');
    this.termsOfService = new UrlField(params.termsOfService, this, 'termsOfService');
    this.version = new StringField(params.version, this, 'version');
    this.contact = params.contact ? new Contact(params.contact, this) : null;
    this.license = params.license ? new License(params.license, this) : null;
  }

  addContact(contact) {
    this.contact = new Contact(contact, this);
  }

  removeContact() {
    this.contact = null;
  }

  addLicense(license) {
    this.license = new License(license, this);
  }

  removeLicense() {
    this.license = null;
  }

  validateNode() {
    if (this.title.isEmpty) {
      this.title.validation.update({
        hasError: true,
        summary: OAS_ERRORS.INFO_TITLE_REQUIRED,
      });
    }
    if (this.version.isEmpty) {
      this.version.validation.update({
        hasError: true,
        summary: OAS_ERRORS.INFO_VERSION_REQUIRED,
      });
    }
  }
}
