import { OasNode } from '../../base/oasNode';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';
import { StringField } from '../../fields/string.field';
import { UrlField } from '../../fields/url.field';
import { OAS_ERRORS } from '../../config/oasErrors';

export class License extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'License';
  }

  constructor(params = {}, parent = null, nodeName = 'license') {
    super(params, parent, nodeName);
    this.name = new StringField(params.name, this, 'name');
    this.url = new UrlField(params.url, this, 'url');
  }

  validateNode() {
    if (this.name.isEmpty) {
      this.name.validation.update({
        hasError: true,
        summary: OAS_ERRORS.LICENSE_NAME_REQUIRED,
      });
    }
  }
}
