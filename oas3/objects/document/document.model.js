import { OasNode } from '../../base/oasNode';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';
import { CONSTANTS } from '../../config/constants';
import { OAS_OBJECT_TYPES, OAS_COMPONENT_TYPES } from '../../config/oasConfig';
import { OasListNode } from '../../base/oasListNode';
import { StringField } from '../../fields/string.field';
import { Info } from './info.model';
import { Servers } from '../common/servers.model';
import { Tags } from '../common/tags.model';
import { ExternalDocs } from '../common/externalDocs.model';
import { Paths } from '../paths/paths.model';
import { Components } from '../components/components.model';
import { Security } from '../common/security.model';
import { AnyField } from '../../fields/any.field';

export class Document extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'Document';
  }

  constructor(params, parent = null, nodeName = CONSTANTS.oas.rootNodeName) {
    super(params, parent, nodeName);
    this.updateOasBasics(params);
    this.paths = new Paths(params.paths, this);
    this.components = new Components(params.components, this);
  }

  deepCopy() {
    let oasDataWithIds = JSON.parse(
      JSON.stringify(this, (key, value) => {
        if (key !== CONSTANTS.oas.objectParentProp) {
          return value;
        }
      })
    );
    let a = this.extract();
    let oasDoc = new Document(this.extract());
    copyOasId(oasDataWithIds, oasDoc);
    return oasDoc;
  }

  extractOasBasicsData() {
    let finalData = this.extract();
    delete finalData.paths;
    delete finalData.components;
    return finalData;
  }

  addExternalDocs(externalDocs) {
    this.externalDocs = new ExternalDocs(externalDocs, this);
  }

  removeExternalDocs() {
    this.externalDocs = null;
  }

  updateOasBasics({
    openapi = CONSTANTS.oas.version,
    info = {},
    servers = [],
    tags = [],
    security = [],
    externalDocs = null,
    ...extensions
  } = {}) {
    this.openapi = new StringField(CONSTANTS.oas.version, this, 'openapi');
    this.info = new Info(info, this);
    this.servers = new Servers(servers, this);
    this.tags = new Tags(tags, this);
    this.externalDocs = externalDocs ? new ExternalDocs(externalDocs, this) : null;
    this.security = new Security(security, this);
    this.clearExtensiveProps();
    for (let extensionProp of Object.keys(extensions)) {
      this.addExtension(extensionProp, extensions[extensionProp]);
    }
  }

  extractApiBasics() {
    let apiBasicsData = {};
    let apiExtensionProps = Object.keys(this).filter(key => {
      return CONSTANTS.oas.extensivePropRegex.test(key) && this[key] instanceof AnyField;
    });
    let apiBasicsProps = OAS_OBJECT_TYPES.other.nodeNames.concat(apiExtensionProps);
    for (let prop of apiBasicsProps) {
      if (this[prop] instanceof OasNode) {
        apiBasicsData[prop] = this[prop].extract();
      }
    }
    return apiBasicsData;
  }

  getPathKeyByErrorPath(oasErrorPath) {
    let targetNode = oasErrorPath.split(CONSTANTS.oas.pathSep).reduce((oasNode, next) => {
      let nextObject = oasNode instanceof OasListNode ? oasNode.value[next] : oasNode[next];
      if (!nextObject) {
        return oasNode;
      }
      return nextObject;
    }, this);
    return targetNode.pathKey;
  }

  getComponentByRefPath(refString) {
    let oasNodePath = refString
      .replace(CONSTANTS.oas.refStringPrefix, CONSTANTS.emptyStr)
      .replace(CONSTANTS.oas.refPathSepRegex, CONSTANTS.oas.pathSep);
    return this.getNodeByRelPath(oasNodePath);
  }

  validateOasObject({ objectType, objectName, componentType } = {}) {
    if (objectType === OAS_OBJECT_TYPES.other.name) {
      this.validateApiBasics();
      return;
    }
    if (objectType === OAS_OBJECT_TYPES.path.name) {
      this.validatePathObject(objectName);
      return;
    }
    if (objectType === OAS_OBJECT_TYPES.component.name) {
      this.validateComponentObject(componentType, objectName);
    }
  }

  validateApiBasics() {
    let apiExtensionProps = Object.keys(this).filter(key => {
      return CONSTANTS.oas.extensivePropRegex.test(key) && this[key] instanceof AnyField;
    });
    let apiBasicsProps = OAS_OBJECT_TYPES.other.nodeNames.concat(apiExtensionProps);
    for (let prop of apiBasicsProps) {
      if (this[prop] instanceof OasNode) {
        this[prop].validate();
      }
    }
  }

  validatePathObject(pathName) {
    if (this.paths[pathName] instanceof OasNode) {
      this.paths[pathName].validate();
    }
  }

  validateComponentObject(componentType, componentName) {
    let componentObject = this.components[OAS_COMPONENT_TYPES[componentType].series][componentName];
    if (componentObject instanceof OasNode) {
      componentObject.validate();
    }
  }
}

export const copyOasId = (oasNodeDataWithIds, oasNode) => {
  if (!(oasNode instanceof OasNode)) {
    return;
  }
  if (oasNode.hasOwnProperty(CONSTANTS.oas.oasNodeIdKey)) {
    oasNode.oasNodeId = oasNodeDataWithIds[CONSTANTS.oas.oasNodeIdKey];
  }
  if (oasNode instanceof OasListNode) {
    oasNode.value.forEach((node, index) => {
      copyOasId(oasNodeDataWithIds.value[index], node);
    });
  } else {
    for (let nodeKey of Object.keys(oasNode)) {
      if (!nodeKey.includes(CONSTANTS.oas.reservedPropPrefix)) {
        copyOasId(oasNodeDataWithIds[nodeKey], oasNode[nodeKey]);
      }
    }
  }
};
