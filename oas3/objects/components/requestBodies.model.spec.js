import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { RequestBodies } from './requestBodies.model';
import { RequestBody } from './requestBody.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('RequestBodies', () => {
  let requestBodiesData01 = {};

  let requestBodiesData02 = {};

  let requestBodies;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        requestBodies = new RequestBodies(requestBodiesData01);
        assert.equal(Utils.deepEqual(requestBodiesData01, requestBodies.extract()), true);
      });
    });
    describe('functions', () => {
      it('can add RequestBody', () => {
        requestBodies = new RequestBodies(requestBodiesData01);
        let requestBodyName = 'requestBody1';
        requestBodies.addRequestBody(requestBodyName);
        assert.equal(requestBodies[requestBodyName] instanceof RequestBody, true);
      });
      it('can remove RequestBody', () => {
        requestBodies = new RequestBodies(requestBodiesData01);
        let requestBodyName = 'requestBody1';
        requestBodies.addRequestBody(requestBodyName, {});
        requestBodies.removeRequestBody(requestBodyName);
        assert.equal(requestBodies.itemKeys.includes(requestBodyName), false);
        assert.equal(requestBodies[requestBodyName] === undefined, true);
      });
    });
  });

  describe('#validation', () => {});
});
