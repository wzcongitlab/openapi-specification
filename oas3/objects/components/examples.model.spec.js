import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Examples } from './examples.model';
import { Example } from './example.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Examples', () => {
  let examplesData01 = {};

  let examplesData02 = {};

  let examples;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        examples = new Examples(examplesData01);
        assert.equal(Utils.deepEqual(examplesData01, examples.extract()), true);
      });
    });

    describe('functions', () => {
      it('can add Example', () => {
        examples = new Examples(examplesData01);
        let exampleName = 'example1';
        examples.addExample(exampleName);
        assert.equal(examples[exampleName] instanceof Example, true);
      });
      it('can remove Example', () => {
        examples = new Examples(examplesData01);
        let exampleName = 'example1';
        examples.addExample(exampleName, {});
        examples.removeExample(exampleName);
        assert.equal(examples.itemKeys.includes(exampleName), false);
        assert.equal(examples[exampleName] === undefined, true);
      });
    });
  });

  describe('#validation', () => {});
});
