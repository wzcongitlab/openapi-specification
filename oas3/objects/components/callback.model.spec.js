import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { PathItem } from '../paths/pathItem.model';
import { Callback } from './callback.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Callback', () => {
  let callbackData01 = {};

  let callbackData02 = {};

  let callback;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        callback = new Callback(callbackData01);
        assert.equal(Utils.deepEqual(callbackData01, callback.extract()), true);
      });
    });
    describe('functions', () => {
      it('can add path item to paths', () => {
        let testPathKey = '/testcallback';
        callback = new Callback(callbackData01);
        callback.addPathItem(testPathKey, {});
        assert.equal(true, callback[testPathKey] instanceof PathItem);
      });
      it('can remove path item from paths', () => {
        let testPathKey = '/testcallback';
        callback = new Callback(callbackData01);
        callback.addPathItem(testPathKey, {});
        assert.equal(true, callback.hasOwnProperty(testPathKey));
        callback.removePathItem(testPathKey);
        assert.equal(false, callback.hasOwnProperty(testPathKey));
      });
    });
  });

  describe('#validation', () => {
    describe('validate callback data', () => {
      it('can pass validation when given valid data', () => {
        callback = new Callback(callbackData01);
        callback.validate();
        assert.equal(callback.validation.hasError, false);
        assert.equal(callback.validation.hasWarning, false);
      });
    });
  });
});
