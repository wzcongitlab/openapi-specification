import { OasNode } from '../../base/oasNode';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';
import { StringField } from '../../fields/string.field';
import { AnyField } from '../../fields/any.field';
import { LinkParameters } from './linkParameters.model';
import { Server } from '../common/server.model';
import { OAS_ERRORS } from '../../config/oasErrors';

export class Link extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'Link';
  }

  constructor(params = {}, parent, nodeName = 'header') {
    super(params, parent, nodeName);
    this.operationRef = new StringField(params.operationRef, this, 'operationRef');
    this.operationId = new StringField(params.operationId, this, 'operationId');
    this.parameters = new LinkParameters(params.parameters, this);
    this.requestBody = new AnyField(params.requestBody, this, 'requestBody');
    this.description = new StringField(params.description, this, 'description');
    this.server = params.server ? new Server(params.server, this) : null;
  }

  get enableOperationId() {
    return this.operationRef.isEmpty || !this.operationId.isEmpty;
  }

  get enableOperationRef() {
    return this.operationId.isEmpty || !this.operationRef.isEmpty;
  }

  addServer(server) {
    this.server = new Server(server, this);
  }

  removeServer() {
    this.server = null;
  }

  validateNode() {
    // 校验allowEmptyValue;
    if (!this.operationRef.isEmpty && !this.operationId.isEmpty) {
      this.operationRef.validation.update({
        hasError: true,
        summary: OAS_ERRORS.LINK_OPERATIONREF_OPERATIONID_CONFLICTED,
      });
      this.operationId.validation.update({
        hasError: true,
        summary: OAS_ERRORS.LINK_OPERATIONREF_OPERATIONID_CONFLICTED,
      });
    }
  }
}
