import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { RequestBody } from './requestBody.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('RequestBody', () => {
  let requestBodyData01 = {
    required: false,
    description: 'description97',
    content: {},
  };

  let requestBodyData02 = {
    required: false,
    description: 'description97',
    content: {
      'application/json': {
        schema: {
          oneOf: [
            {
              $ref: '#/components/schemas/Pet',
            },
          ],
        },
      },
    },
  };

  let requestBody;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        requestBody = new RequestBody(requestBodyData01);
        assert.equal(Utils.deepEqual(requestBodyData01, requestBody.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate requestBody data', () => {
      it('can get error named REQUEST_BODY_CONTENT_REQUIRED', () => {
        requestBody = new RequestBody(requestBodyData01);
        requestBody.validate();
        assert.equal(requestBody.content.validation.hasError, true);
        assert.equal(requestBody.content.validation.hasWarning, false);
        assert.equal(
          requestBody.content.validation.error.summary,
          OAS_ERRORS.REQUEST_BODY_CONTENT_REQUIRED
        );
      });

      it('can pass validation when given valid data', () => {
        requestBody = new RequestBody(requestBodyData02);
        requestBody.validate();
        assert.equal(requestBody.content.validation.hasError, false);
        assert.equal(requestBody.content.validation.hasWarning, false);
      });
    });
  });
});
