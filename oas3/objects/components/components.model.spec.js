import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Components } from './components.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Components', () => {
  let componentsData01 = {
    schemas: {},
    responses: {},
    parameters: {},
    examples: {},
    requestBodies: {},
    headers: {},
    securitySchemes: {},
    links: {},
    callbacks: {},
  };

  let componentsData02 = {
    schemas: {},
    responses: {},
    parameters: {},
    examples: {},
    requestBodies: {},
    headers: {},
    securitySchemes: {},
    links: {},
    callbacks: {},
  };

  let components;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        components = new Components(componentsData01);
        assert.equal(Utils.deepEqual(componentsData01, components.extract()), true);
      });
    });
  });

  describe('#validation', () => {});
});
