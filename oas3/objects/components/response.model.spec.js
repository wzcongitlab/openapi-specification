import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Response } from './response.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Response', () => {
  let responseData01 = {
    description: '',
    headers: {},
    content: {},
    links: {},
  };

  let responseData02 = {
    description: 'description99',
    headers: {},
    content: {},
    links: {},
  };

  let response;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        response = new Response(responseData01);
        assert.equal(Utils.deepEqual(responseData01, response.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate response data', () => {
      it('can get error named RESPONSE_DESCRIPTION_REQUIRED', () => {
        response = new Response(responseData01);
        response.validate();
        assert.equal(response.description.validation.hasError, true);
        assert.equal(
          response.description.validation.error.summary,
          OAS_ERRORS.RESPONSE_DESCRIPTION_REQUIRED
        );
      });

      it('can pass validation when given valid data', () => {
        response = new Response(responseData02);
        response.validate();
        assert.equal(response.description.validation.hasError, false);
        assert.equal(response.description.validation.hasWarning, false);
      });
    });
  });
});
