import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Parameter } from './parameter.model';
import { OAS_ERRORS, OAS_WARNINGS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Parameter', () => {
  let parameterData01 = {
    name: 'name83',
    in: 'path',
    description: 'description86',
    required: false,
    deprecated: false,
    allowEmptyValue: false,
    style: 'query',
    explode: false,
    allowReserved: false,
    example: 'example9',
    examples: {},
    schema: {
      type: 'integer',
      format: 'int32',
      nullable: false,
      readOnly: false,
      writeOnly: false,
      deprecated: false,
      description: 'quantity of order',
      multipleOf: 1,
      maximum: 10000,
      exclusiveMaximum: false,
      minimum: 1,
      exclusiveMinimum: true,
      default: 1,
      enum: [],
      example: 44,
      title: 'title001',
    },
  };

  let parameterData03 = {
    name: '',
    in: 'dfdsf',
    description: 'description86',
    required: false,
    deprecated: false,
    allowEmptyValue: false,
    style: 'query',
    explode: false,
    allowReserved: false,
    example: 'example9',
    examples: {},
    schema: {
      type: 'integer',
      format: 'int32',
      nullable: true,
      description: 'quantity of order',
      multipleOf: 1,
      maximum: 10000,
      exclusiveMaximum: false,
      minimum: 1,
      exclusiveMinimum: true,
      default: 1,
      enum: [],
      example: '',
      title: 'title001',
    },
  };

  let parameterData04 = {
    name: 'userId',
    in: 'path',
    description: 'description86',
    required: 'N/A',
    deprecated: false,
    allowEmptyValue: false,
    style: 'query',
    explode: false,
    allowReserved: false,
    example: 'example9',
    examples: {},
    schema: {
      type: 'integer',
      format: 'int32',
      nullable: false,
      readOnly: false,
      writeOnly: false,
      deprecated: false,
      description: 'quantity of order',
      multipleOf: 1,
      maximum: 10000,
      exclusiveMaximum: false,
      minimum: 1,
      exclusiveMinimum: true,
      default: 1,
      enum: [],
      example: '',
      title: 'title001',
    },
  };

  let parameterData05 = {
    name: 'userId',
    in: 'path',
    description: 'description86',
    required: 'N/A',
    deprecated: false,
    allowEmptyValue: true,
    style: 'form1',
    explode: false,
    allowReserved: false,
    example: 'example9',
    examples: {},
    schema: {
      type: 'integer',
      format: 'int32',
      nullable: true,
      description: 'quantity of order',
      multipleOf: 1,
      maximum: 10000,
      exclusiveMaximum: false,
      minimum: 1,
      exclusiveMinimum: true,
      default: 1,
      enum: [],
      example: '',
      title: 'title001',
    },
  };

  let parameterData051 = {
    name: 'userId',
    in: 'query',
    description: 'description86',
    required: 'N/A',
    deprecated: false,
    allowEmptyValue: true,
    style: 'form1',
    explode: false,
    allowReserved: false,
    example: 'example9',
    examples: {},
    schema: {
      type: 'integer',
      format: 'int32',
      nullable: true,
      description: 'quantity of order',
      multipleOf: 1,
      maximum: 10000,
      exclusiveMaximum: false,
      minimum: 1,
      exclusiveMinimum: true,
      default: 1,
      enum: [],
      example: '',
      title: 'title001',
    },
  };

  let parameterData06 = {
    name: 'userId',
    in: 'path',
    description: 'description86',
    required: 'N/A',
    deprecated: false,
    allowEmptyValue: false,
    style: '',
    explode: false,
    allowReserved: false,
    example: 'example9',
    examples: {},
    schema: {
      type: 'integer',
      format: 'int32',
      nullable: true,
      description: 'quantity of order',
      multipleOf: 1,
      maximum: 10000,
      exclusiveMaximum: false,
      minimum: 1,
      exclusiveMinimum: true,
      default: 1,
      enum: [],
      example: '',
      title: 'title001',
    },
    content: {
      'application/json': {},
    },
  };

  let parameterData07 = {
    name: 'userId',
    in: 'path',
    description: 'description86',
    required: 'N/A',
    deprecated: false,
    allowEmptyValue: false,
    style: '',
    explode: false,
    allowReserved: false,
    example: 'example9',
    examples: {
      ex1: {
        externalValue: 'ssss',
        value: 111,
      },
    },
    schema: {
      type: 'integer',
      format: 'int32',
      nullable: true,
      description: 'quantity of order',
      multipleOf: 1,
      maximum: 10000,
      exclusiveMaximum: false,
      minimum: 1,
      exclusiveMinimum: true,
      default: 1,
      enum: [],
      example: '',
      title: 'title001',
    },
    content: {},
  };

  let parameterData08 = {
    name: 'userId',
    in: 'path',
    description: 'description86',
    required: 'N/A',
    deprecated: false,
    allowEmptyValue: false,
    style: '',
    explode: false,
    allowReserved: false,
    examples: {
      ex1: {
        externalValue: 'ssss',
        value: 111,
      },
    },
    content: {},
  };

  let parameterData09 = {
    name: 'userId',
    in: 'path',
    description: 'description86',
    required: 'N/A',
    deprecated: false,
    allowEmptyValue: false,
    style: '',
    explode: false,
    allowReserved: false,
    examples: {
      ex1: {
        externalValue: 'ssss',
        value: 111,
      },
    },
    content: {
      'application/json': {
        schema: {
          oneOf: [
            {
              $ref: '#/components/schemas/Pet',
            },
          ],
        },
      },
      'application/xml': {
        schema: {
          oneOf: [
            {
              $ref: '#/components/schemas/Pet',
            },
          ],
        },
      },
    },
  };

  let parameter;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        parameter = new Parameter(parameterData01);
        assert.equal(Utils.deepEqual(parameterData01, parameter.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate parameter data', () => {
      it('can get error named PARAMETER_NAME_REQUIRED', () => {
        parameter = new Parameter(parameterData03);
        parameter.validate();
        assert.equal(parameter.name.validation.hasError, true);
        assert.equal(parameter.name.validation.error.summary, OAS_ERRORS.PARAMETER_NAME_REQUIRED);
      });
      it('can get error named PARAMETER_IN_RESTRICTED', () => {
        parameter = new Parameter(parameterData03);
        parameter.validate();
        assert.equal(parameter.in.validation.hasError, true);
        assert.equal(parameter.in.validation.error.summary, OAS_ERRORS.PARAMETER_IN_RESTRICTED);
      });
      it('can get error named PARAMETER_REQUIRED_MUST_BE_TRUE', () => {
        parameter = new Parameter(parameterData04);
        parameter.validate();
        assert.equal(parameter.required.validation.hasError, true);
        assert.equal(
          parameter.required.validation.error.summary,
          OAS_ERRORS.PARAMETER_REQUIRED_MUST_BE_TRUE
        );
      });
      it('can get warning named PARAMETER_ALLOWEMPTYVALUE_ONLY_FOR_QUERY', () => {
        parameter = new Parameter(parameterData05);
        parameter.validate();
        assert.equal(parameter.allowEmptyValue.validation.hasWarning, true);
        assert.equal(
          parameter.allowEmptyValue.validation.warning.summary,
          OAS_WARNINGS.PARAMETER_ALLOWEMPTYVALUE_ONLY_FOR_QUERY
        );
      });
      it('can get error named PARAMETER_STYLE_RESTRICTED', () => {
        parameter = new Parameter(parameterData051);
        parameter.validate();
        assert.equal(parameter.style.validation.hasError, true);
        assert.equal(
          parameter.style.validation.error.summary,
          OAS_ERRORS.PARAMETER_STYLE_RESTRICTED
        );
      });
      it('can get error named PARAMETER_ALLOWRESERVED_ONLY_FOR_QUERY', () => {
        parameter = new Parameter(parameterData05);
        parameter.validate();
        assert.equal(parameter.allowReserved.validation.hasWarning, true);
        assert.equal(
          parameter.allowReserved.validation.warning.summary,
          OAS_WARNINGS.PARAMETER_ALLOWRESERVED_ONLY_FOR_QUERY
        );
      });
      it('can get error named EXCLUSIVE_EXAMPLE_AND_EXAMPLES', () => {
        parameter = new Parameter(parameterData07);
        parameter.validate();
        assert.equal(parameter.validation.hasError, true);
        assert.equal(parameter.validation.error.summary, OAS_ERRORS.EXCLUSIVE_EXAMPLE_AND_EXAMPLES);
      });
      it('can get error named INVALID_PARAMETER_CONTENT(has no content entry)', () => {
        parameter = new Parameter(parameterData08);
        parameter.validate();
        assert.equal(parameter.content.validation.hasError, true);
        assert.equal(
          parameter.content.validation.error.summary,
          OAS_ERRORS.INVALID_PARAMETER_CONTENT
        );
      });
      it('can get error named INVALID_PARAMETER_CONTENT(has multiple entries)', () => {
        parameter = new Parameter(parameterData09);
        parameter.validate();
        assert.equal(parameter.content.validation.hasError, true);
        assert.equal(
          parameter.content.validation.error.summary,
          OAS_ERRORS.INVALID_PARAMETER_CONTENT
        );
      });
    });
  });
});
