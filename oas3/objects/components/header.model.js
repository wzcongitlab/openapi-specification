import { CONSTANTS } from '../../config/constants';
import { OasNode } from '../../base/oasNode';
import { StringField } from '../../fields/string.field';
import { BooleanField } from '../../fields/boolean.field';
import { AnyField } from '../../fields/any.field';
import { OasSchemaFactory } from '../common/schema/factories/oasSchemaFactory';
import { OasSchema } from '../common/schema/oasSchema.model';
import { Examples } from './examples.model';
import { PayloadContent } from '../common/payloadContent.model';
import { PARAMETER_STYLE_LIST, PARAMETER_STYLES } from '../../config/oasConfig';
import { OAS_ERRORS, OAS_WARNINGS } from '../../config/oasErrors';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';

export class Header extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'Header';
  }

  constructor(params, parent, nodeName = 'header') {
    super(params, parent, nodeName);
    this.description = new StringField(params.description, this, 'description');
    this.required = new BooleanField(params.required, this, 'required');
    this.deprecated = new BooleanField(params.deprecated, this, 'deprecated');
    this.allowEmptyValue = new BooleanField(params.allowEmptyValue, this, 'allowEmptyValue');
    this.style = new StringField(
      params.style ? params.style : CONSTANTS.unavailable,
      this,
      'style'
    );
    this.required = new BooleanField(
      typeof params.required === 'boolean' ? params.required : false,
      this,
      'required'
    );
    // 从空对象新建parameter时, 根据style设置默认值;
    if (BooleanField.isAvailableBooleanValue(params.explode)) {
      this.explode = new BooleanField(params.explode, this, 'explode');
    } else if (this.style.equal(PARAMETER_STYLES.form)) {
      this.explode = new BooleanField(true, this, 'explode');
    } else {
      this.explode = new BooleanField(false, this, 'explode');
    }
    this.allowReserved = new BooleanField(params.allowReserved, this, 'allowReserved');
    this.example = new AnyField(params.example, this, 'example');
    this.examples = new Examples(params.examples, this, 'examples');
    this.schema = params.schema
      ? OasSchemaFactory.createSchema(params.schema, this, 'schema')
      : null;
    this.content = params.content ? new PayloadContent(params.content, this, 'content') : null;
    // 是否启用content, 启用content则schema应为null对象
    if (this.schema === null && this.content === null) {
      this.schema = OasSchemaFactory.createSchema({}, this, 'schema');
      this._enableContent = false;
      return;
    }
    if (this.schema instanceof OasSchema) {
      this._enableContent = false;
      this.content = null;
      return;
    }
    this._enableContent = this.content !== null;
  }

  get enableContent() {
    return this._enableContent;
  }

  get hasContent() {
    return !!this.content;
  }

  set enableContent(value) {
    this._enableContent = value;
    if (value) {
      this.content = new PayloadContent({ 'application/json': {} }, this, 'content');
      this.schema = null;
    } else {
      this.schema = OasSchemaFactory.createSchema({}, this, 'schema');
      this.content = null;
    }
  }

  validateNode() {
    // 校验allowEmptyValue;
    if (!this.allowEmptyValue.equal(CONSTANTS.unavailable)) {
      this.allowEmptyValue.validation.update({
        hasWarning: true,
        summary: OAS_WARNINGS.PARAMETER_ALLOWEMPTYVALUE_ONLY_FOR_QUERY,
      });
    }
    // 校验style;
    if (!PARAMETER_STYLE_LIST.includes(this.style.value)) {
      this.style.validation.update({
        hasError: true,
        summary: OAS_ERRORS.PARAMETER_STYLE_RESTRICTED,
      });
    }
    // 校验allowReserved;
    if (!this.allowReserved.equal(CONSTANTS.unavailable)) {
      this.allowReserved.validation.update({
        hasWarning: true,
        summary: OAS_WARNINGS.PARAMETER_ALLOWRESERVED_ONLY_FOR_QUERY,
      });
    }
    // 校验content和schema;
    if (this.schema !== null && this.content !== null) {
      this.validation.update({
        hasError: true,
        summary: OAS_ERRORS.PARAMETER_EXCLUSIVE_CONTENT_AND_SCHEMA,
      });
    }
    // 校验example/examples的互斥性;
    if (!this.example.isEmpty && !this.examples.isEmpty) {
      this.validation.update({
        hasError: true,
        summary: OAS_ERRORS.EXCLUSIVE_EXAMPLE_AND_EXAMPLES,
      });
    }
  }
}
