import { OasNode } from '../../base/oasNode';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';
import { PayloadContent } from '../common/payloadContent.model';
import { RichTextField } from '../../fields/richText.field';
import { BooleanField } from '../../fields/boolean.field';
import { OAS_ERRORS } from '../../config/oasErrors';

export class RequestBody extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'RequestBody';
  }

  constructor(params = {}, parent = null, nodeName = 'requestBody') {
    super(params, parent, nodeName);
    this.required = new BooleanField(params.required, this, 'required');
    this.description = new RichTextField(params.description, this, 'description');
    this.content = new PayloadContent(params.content, this);
  }

  validateNode() {
    if (this.content.isEmpty) {
      this.content.validation.update({
        hasError: true,
        summary: OAS_ERRORS.REQUEST_BODY_CONTENT_REQUIRED,
      });
    }
  }
}
