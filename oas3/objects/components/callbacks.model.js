import { OasMapNode } from '../../base/oasMapNode';
import { Callback } from './callback.model';
import { Reference } from '../common/reference.model';
import { ComponentFactory } from './componentFactory';
import { OAS_COMPONENT_TYPES } from '../../config/oasConfig';

export class Callbacks extends OasMapNode {
  static get nodeObjectType() {
    return 'Callbacks';
  }

  constructor(params = {}, parent = null, nodeName = 'callbacks') {
    super(parent, nodeName);
    for (let key of Object.keys(params)) {
      this.addCallback(key, params[key]);
    }
  }

  addCallback(key, callback) {
    if (callback instanceof Callback || callback instanceof Reference) {
      callback.parent = this;
      this.addItem(key, callback);
    } else {
      let newCallback = ComponentFactory.createComponent({
        params: callback,
        parent: this,
        type: OAS_COMPONENT_TYPES.callback.name,
        nodeName: key,
      });
      this.addItem(key, newCallback);
    }
  }

  updateCallback(key, callback) {
    // callback 是一个指定callback的oas json数据或callback对象;
    if (callback instanceof Callback || callback instanceof Reference) {
      callback.parent = this;
      this.updateItem(key, callback);
      return;
    }
    this.updateItem(
      key,
      ComponentFactory.createComponent({
        params: callback,
        parent: this,
        type: OAS_COMPONENT_TYPES.callback.name,
        nodeName: key,
      })
    );
  }

  removeCallback(key) {
    this.removeItem(key);
  }
}
