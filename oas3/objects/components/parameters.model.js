import { OasMapNode } from '../../base/oasMapNode';
import { Parameter } from './parameter.model';
import { Reference } from '../common/reference.model';
import { ComponentFactory } from './componentFactory';
import { OAS_COMPONENT_TYPES } from '../../config/oasConfig';

export class Parameters extends OasMapNode {
  static get nodeObjectType() {
    return 'Parameters';
  }

  constructor(params = {}, parent = null, nodeName = 'parameters') {
    super(parent, nodeName);
    for (let key of Object.keys(params)) {
      this.addParameter(key, params[key]);
    }
  }

  addParameter(key, parameter) {
    if (parameter instanceof Parameter || parameter instanceof Reference) {
      parameter.parent = this;
      this.addItem(key, parameter);
    } else {
      let newParameter = ComponentFactory.createComponent({
        params: parameter,
        parent: this,
        type: OAS_COMPONENT_TYPES.parameter.name,
        nodeName: key,
      });
      this.addItem(key, newParameter);
    }
  }

  updateParameter(key, parameter) {
    // parameter 是一个指定parameter的oas json数据或parameter对象;
    if (parameter instanceof Parameter || parameter instanceof Reference) {
      parameter.parent = this;
      this.updateItem(key, parameter);
      return;
    }
    this.updateItem(
      key,
      ComponentFactory.createComponent({
        params: parameter,
        parent: this,
        type: OAS_COMPONENT_TYPES.parameter.name,
        nodeName: key,
      })
    );
  }

  removeParameter(key) {
    this.removeItem(key);
  }
}
