import { OasMapNode } from '../../base/oasMapNode';
import { AnyField } from '../../fields/any.field';

export class LinkParameters extends OasMapNode {
  static get nodeObjectType() {
    return 'LinkParameters';
  }

  constructor(params = {}, parent = null, nodeName = 'parameters') {
    super(parent, nodeName);
    for (let key of Object.keys(params)) {
      this.addLinkParameter(key, params[key]);
    }
  }

  addLinkParameter(key, linkParameter) {
    if (linkParameter instanceof AnyField) {
      linkParameter.parent = this;
      this.addItem(key, linkParameter);
    } else {
      let newParameter = new AnyField(linkParameter, this);
      this.addItem(key, newParameter);
    }
  }

  removeLinkParameter(key) {
    this.removeItem(key);
  }
}
