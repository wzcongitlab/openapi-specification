import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Server } from '../common/server.model';
import { Link } from './link.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Link', () => {
  let linkData01 = {
    operationRef: 'operationRef73',
    operationId: 'operationId23',
    parameters: {},
    requestBody: 'requestBody76',
    description: 'description87',
    server: {
      url: '',
      description: '',
      variables: {},
    },
  };

  let linkData02 = {
    operationRef: 'operationRef73',
    operationId: 'operationId23',
    parameters: {},
    requestBody: 'requestBody76',
    description: 'description87',
    server: {
      url: '',
      description: '',
      variables: {},
    },
  };

  let link;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        link = new Link(linkData01);
        assert.equal(Utils.deepEqual(linkData01, link.extract()), true);
      });
    });

    describe('functions', () => {
      it('can add Server', () => {
        link = new Link(linkData01);
        link.addServer({});
        assert.equal(link.server instanceof Server, true);
      });
      it('can remove Server', () => {
        link = new Link(linkData01);
        link.addServer({});
        assert.equal(link.server instanceof Server, true);
        link.removeServer();
        assert.equal(link.server === null, true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate link data', () => {
      it('can pass validation when given valid data', () => {
        link = new Link(linkData01);
        link.validate();
        assert.equal(link.validation.hasError, false);
        assert.equal(link.validation.hasWarning, false);
      });
    });
  });
});
