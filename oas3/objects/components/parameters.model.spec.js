import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Parameters } from './parameters.model';
import { Parameter } from './parameter.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Parameters', () => {
  let parametersData01 = {};

  let parametersData02 = {};

  let parameters;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        parameters = new Parameters(parametersData01);
        assert.equal(Utils.deepEqual(parametersData01, parameters.extract()), true);
      });
    });
    describe('functions', () => {
      it('can add Parameter', () => {
        parameters = new Parameters(parametersData01);
        let parameterName = 'parameter1';
        parameters.addParameter(parameterName);
        assert.equal(parameters[parameterName] instanceof Parameter, true);
      });
      it('can remove Parameter', () => {
        parameters = new Parameters(parametersData01);
        let parameterName = 'parameter1';
        parameters.addParameter(parameterName, {});
        parameters.removeParameter(parameterName);
        assert.equal(parameters.itemKeys.includes(parameterName), false);
        assert.equal(parameters[parameterName] === undefined, true);
      });
    });
  });

  describe('#validation', () => {});
});
