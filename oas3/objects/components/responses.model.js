import { OasMapNode } from '../../base/oasMapNode';
import { Response } from './response.model';
import { Reference } from '../common/reference.model';
import { ComponentFactory } from './componentFactory';
import { OAS_COMPONENT_TYPES } from '../../config/oasConfig';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';

export class Responses extends OasExtensibleNode(OasMapNode) {
  static get nodeObjectType() {
    return 'Responses';
  }

  constructor(params = {}, parent = null, nodeName = 'responses') {
    super(params, parent, nodeName);
    for (let key of Object.keys(params)) {
      this.addResponse(key, params[key]);
    }
  }

  addResponse(key, response) {
    if (response instanceof Response || response instanceof Reference) {
      response.parent = this;
      this.addItem(key, response);
    } else {
      let newResponse = ComponentFactory.createComponent({
        params: response,
        parent: this,
        type: OAS_COMPONENT_TYPES.response.name,
        nodeName: key,
      });
      this.addItem(key, newResponse);
    }
  }

  updateResponse(key, response) {
    // response 是一个指定response的oas json数据或response对象;
    if (response instanceof Response || response instanceof Reference) {
      response.parent = this;
      this.updateItem(key, response);
      return;
    }
    this.updateItem(
      key,
      ComponentFactory.createComponent({
        params: response,
        parent: this,
        type: OAS_COMPONENT_TYPES.response.name,
        nodeName: key,
      })
    );
  }

  removeResponse(key) {
    this.removeItem(key);
  }
}
