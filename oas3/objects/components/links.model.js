import { OasMapNode } from '../../base/oasMapNode';
import { Link } from './link.model';
import { Reference } from '../common/reference.model';
import { ComponentFactory } from './componentFactory';
import { OAS_COMPONENT_TYPES } from '../../config/oasConfig';

export class Links extends OasMapNode {
  static get nodeObjectType() {
    return 'Links';
  }

  constructor(params = {}, parent = null, nodeName = 'links') {
    super(parent, nodeName);
    for (let key of Object.keys(params)) {
      this.addLink(key, params[key]);
    }
  }

  addLink(key, link) {
    if (link instanceof Link || link instanceof Reference) {
      link.parent = this;
      this.addItem(key, link);
    } else {
      let newLink = ComponentFactory.createComponent({
        params: link,
        parent: this,
        type: OAS_COMPONENT_TYPES.link.name,
        nodeName: key,
      });
      this.addItem(key, newLink);
    }
  }

  updateLink(key, link) {
    // link 是一个指定link的oas json数据或link对象;
    if (link instanceof Link || link instanceof Reference) {
      link.parent = this;
      this.updateItem(key, link);
      return;
    }
    this.updateItem(
      key,
      ComponentFactory.createComponent({
        params: link,
        parent: this,
        type: OAS_COMPONENT_TYPES.link.name,
        nodeName: key,
      })
    );
  }

  removeLink(key) {
    this.removeItem(key);
  }
}
