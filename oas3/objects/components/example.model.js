import { OasNode } from '../../base/oasNode';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';
import { StringField } from '../../fields/string.field';
import { UrlField } from '../../fields/url.field';
import { AnyField } from '../../fields/any.field';
import { OAS_ERRORS } from '../../config/oasErrors';

export class Example extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'Example';
  }

  constructor(params = {}, parent, nodeName = 'header') {
    super(params, parent, nodeName);
    this.summary = new StringField(params.summary, this, 'summary');
    this.description = new StringField(params.description, this, 'description');
    // exampleValue 这个字段名在oas标准中应该是'value', 此处exampleValue已配置成等价于value的属性;
    // 参见 oas3/config/constants.oas
    this.exampleValue = new AnyField(params.value, this, 'value');
    this.externalValue = new UrlField(params.externalValue, this, 'externalValue');
  }

  set value(exampleValue) {
    this.exampleValue = exampleValue;
  }

  get value() {
    // 该get方法用于拦截value原子节点的获取, 转到this.exampleValue对象;
    return this.exampleValue;
  }

  get enableValue() {
    return this.externalValue.isEmpty || !this.value.isEmpty;
  }

  get enableExternalValue() {
    return this.value.isEmpty || !this.externalValue.isEmpty;
  }

  validateNode() {
    // 校验allowEmptyValue;
    if (!this.externalValue.isEmpty && !this.value.isEmpty) {
      this.value.validation.update({
        hasError: true,
        summary: OAS_ERRORS.EXAMPLE_VALUE_EXTERNAL_VALUE_CONFLICTED,
      });
      this.externalValue.validation.update({
        hasError: true,
        summary: OAS_ERRORS.EXAMPLE_VALUE_EXTERNAL_VALUE_CONFLICTED,
      });
    }
  }
}
