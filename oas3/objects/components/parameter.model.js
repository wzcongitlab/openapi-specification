import { OasNode } from '../../base/oasNode';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';
import { StringField } from '../../fields/string.field';
import { BooleanField } from '../../fields/boolean.field';
import { OasSchemaFactory } from '../common/schema/factories/oasSchemaFactory';
import { OasSchema } from '../common/schema/oasSchema.model';
import { AnyField } from '../../fields/any.field';
import { Examples } from './examples.model';
import { PayloadContent } from '../common/payloadContent.model';
import { CONSTANTS } from '../../config/constants';
import {
  PARAMETER_LOCATIONS,
  PARAMETER_LOCATION_LIST,
  PARAMETER_STYLES,
  PARAMETER_STYLE_LIST,
} from '../../config/oasConfig';
import { PARAMETER_LOCATIONS_STYLES } from '../../config/parameterTypeStyleMapping';
import { OAS_ERRORS, OAS_WARNINGS } from '../../config/oasErrors';

export class Parameter extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'Parameter';
  }

  constructor(params = {}, parent, nodeName = 'parameter') {
    super(params, parent, nodeName);
    this.name = new StringField(params.name, this, 'name');
    this.in = new StringField(params.in ? params.in : CONSTANTS.unavailable, this, 'in');
    this.description = new StringField(params.description, this, 'description');
    this.required = new BooleanField(params.required, this, 'required');
    this.deprecated = new BooleanField(params.deprecated, this, 'deprecated');
    this.allowEmptyValue = new BooleanField(params.allowEmptyValue, this, 'allowEmptyValue');
    this.style = new StringField(
      params.style ? params.style : CONSTANTS.unavailable,
      this,
      'style'
    );
    this.required = new BooleanField(params.required, this, 'required');
    if (BooleanField.isAvailableBooleanValue(params.explode)) {
      this.explode = new BooleanField(params.explode, this, 'explode');
    } else if (this.style.equal(PARAMETER_STYLES.form)) {
      this.explode = new BooleanField(true, this, 'explode');
    } else {
      this.explode = new BooleanField(false, this, 'explode');
    }
    this.allowReserved = new BooleanField(params.allowReserved, this, 'allowReserved');
    this.example = new AnyField(params.example, this, 'example');
    this.examples = new Examples(params.examples, this, 'examples');
    this.schema = params.schema
      ? OasSchemaFactory.createSchema(params.schema, this, 'schema')
      : null;
    this.content = params.content ? new PayloadContent(params.content, this, 'content') : null;
    // 是否启用content, 启用content则schema应为null对象
    if (this.schema === null && this.content === null) {
      this.schema = OasSchemaFactory.createSchema({}, this, 'schema');
      this._enableContent = false;
      return;
    }
    if (this.schema instanceof OasSchema) {
      this._enableContent = false;
      this.content = null;
      return;
    }
    this._enableContent = this.content !== null;
  }

  get enableContent() {
    return this._enableContent;
  }

  set enableContent(enable) {
    this._enableContent = enable;
    if (enable) {
      this.content = new PayloadContent({ 'application/json': {} }, this, 'content');
      this.schema = null;
    } else {
      this.schema = OasSchemaFactory.createSchema({}, this, 'schema');
      this.content = null;
    }
  }

  get hasContent() {
    return !!this.content;
  }

  validateNode() {
    // 校验name必填;
    if (this.name.isEmpty) {
      this.name.validation.update({
        hasError: true,
        summary: OAS_ERRORS.PARAMETER_NAME_REQUIRED,
      });
    }
    // 校验in的值范围;
    if (!PARAMETER_LOCATION_LIST.includes(this.in.value)) {
      this.in.validation.update({
        hasError: true,
        summary: OAS_ERRORS.PARAMETER_IN_RESTRICTED,
      });
    }
    // 校验required字段;
    if (this.in.equal(PARAMETER_LOCATIONS.path) && !this.required.equal(true)) {
      this.required.validation.update({
        hasError: true,
        summary: OAS_ERRORS.PARAMETER_REQUIRED_MUST_BE_TRUE,
      });
    }
    // 校验allowEmptyValue;
    if (
      !this.in.equal(PARAMETER_LOCATIONS.query) &&
      !this.allowEmptyValue.equal(CONSTANTS.unavailable)
    ) {
      this.allowEmptyValue.validation.update({
        hasWarning: true,
        summary: OAS_WARNINGS.PARAMETER_ALLOWEMPTYVALUE_ONLY_FOR_QUERY,
      });
    }
    // 校验style;
    if (!PARAMETER_STYLE_LIST.includes(this.style.value)) {
      this.style.validation.update({
        hasError: true,
        summary: OAS_ERRORS.PARAMETER_STYLE_RESTRICTED,
      });
    }
    // 校验in-style的映射关系
    if (
      !this.in.value.isEmpty &&
      PARAMETER_STYLE_LIST.includes(this.style.value) &&
      PARAMETER_LOCATIONS_STYLES[this.in.value]
    ) {
      let styleValue = this.style.value;
      let styles = PARAMETER_LOCATIONS_STYLES[this.in.value];
      if (!styles.styleOptions.some(s => s.value === styleValue)) {
        this.style.validation.update({
          hasWarning: true,
          summary: OAS_WARNINGS.PARAMETER_LOCATION_STYLE_NOT_MATCHED,
        });
      }
    }
    // 校验allowReserved;
    if (
      !this.in.equal(PARAMETER_LOCATIONS.query) &&
      !this.allowReserved.equal(CONSTANTS.unavailable)
    ) {
      this.allowReserved.validation.update({
        hasWarning: true,
        summary: OAS_WARNINGS.PARAMETER_ALLOWRESERVED_ONLY_FOR_QUERY,
      });
    }
    // 校验content和schema;
    if (this.schema !== null && this.content !== null) {
      this.validation.update({
        hasError: true,
        summary: OAS_ERRORS.PARAMETER_EXCLUSIVE_CONTENT_AND_SCHEMA,
      });
    }
    // 校验example/examples的互斥性;
    if (!this.example.isEmpty && !this.examples.isEmpty) {
      this.validation.update({
        hasError: true,
        summary: OAS_ERRORS.EXCLUSIVE_EXAMPLE_AND_EXAMPLES,
      });
    }
    // 校验content的条目数;
    if (this.content !== null && this.content.size !== 1) {
      this.content.validation.update({
        hasError: true,
        summary: OAS_ERRORS.INVALID_PARAMETER_CONTENT,
      });
    }
  }
}
