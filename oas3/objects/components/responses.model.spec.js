import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Responses } from './responses.model';
import { Response } from './response.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Responses', () => {
  let responsesData01 = {};

  let responsesData02 = {};

  let responses;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        responses = new Responses(responsesData01);
        assert.equal(Utils.deepEqual(responsesData01, responses.extract()), true);
      });
    });
    describe('functions', () => {
      it('can add Response', () => {
        responses = new Responses(responsesData01);
        let responseName = 'response1';
        responses.addResponse(responseName);
        assert.equal(responses[responseName] instanceof Response, true);
      });
      it('can remove Response', () => {
        responses = new Responses(responsesData01);
        let responseName = 'response1';
        responses.addResponse(responseName, {});
        responses.removeResponse(responseName);
        assert.equal(responses.itemKeys.includes(responseName), false);
        assert.equal(responses[responseName] === undefined, true);
      });
    });
  });

  describe('#validation', () => {});
});
