import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Callbacks } from './callbacks.model';
import { Callback } from './callback.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Callbacks', () => {
  let callbacksData01 = {
    myEvent: {
      '{$request.body#/callbackUrl}': {
        summary: '',
        description: '',
        parameters: [],
        servers: [],
      },
    },
  };

  let callbacksData02 = {};

  let callbacks;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        callbacks = new Callbacks(callbacksData01);
        console.log(callbacks.extract());
        assert.equal(Utils.deepEqual(callbacksData01, callbacks.extract()), true);
      });

      describe('functions', () => {
        it('can add Callback', () => {
          callbacks = new Callbacks(callbacksData01);
          let callbackName = 'callback1';
          callbacks.addCallback(callbackName);
          assert.equal(callbacks[callbackName] instanceof Callback, true);
        });
        it('can remove Callback', () => {
          callbacks = new Callbacks(callbacksData01);
          let callbackName = 'callback1';
          callbacks.addCallback(callbackName, {});
          callbacks.removeCallback(callbackName);
          assert.equal(callbacks.itemKeys.includes(callbackName), false);
          assert.equal(callbacks[callbackName] === undefined, true);
        });
      });
    });
  });

  describe('#validation', () => {});
});
