import { CONSTANTS } from '../../config/constants';
import { OasNode } from '../../base/oasNode';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';
import { PayloadContent } from '../common/payloadContent.model';
import { RichTextField } from '../../fields/richText.field';
import { Headers } from './headers.model';
import { Links } from './links.model';
import { OAS_ERRORS } from '../../config/oasErrors';

export class Response extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'Response';
  }

  constructor(params = {}, parent = null, nodeName = CONSTANTS.emptyStr) {
    super(params, parent, nodeName);
    this.description = new RichTextField(params.description, this, 'description');
    this.headers = new Headers(params.headers, this);
    this.content = new PayloadContent(params.content, this);
    this.links = new Links(params.links, this);
  }

  validateNode() {
    if (this.description.isEmpty) {
      this.description.validation.update({
        hasError: true,
        summary: OAS_ERRORS.RESPONSE_DESCRIPTION_REQUIRED,
      });
    }
  }
}
