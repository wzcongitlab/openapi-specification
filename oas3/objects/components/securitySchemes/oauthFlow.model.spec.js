import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { OAuthFlow } from './oauthFlow.model';
import { OAS_WARNINGS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('OAuthFlow', () => {
  let oAuthFlowData01 = {
    refreshUrl: 'refreshUrl47',
    scopes: {},
  };

  let oAuthFlowData02 = {
    refreshUrl: 'refreshUrl47',
    scopes: {
      'write:pet': '',
      'read:pet': '',
    },
  };

  let oAuthFlow;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        oAuthFlow = new OAuthFlow(oAuthFlowData01);
        assert.equal(Utils.deepEqual(oAuthFlowData01, oAuthFlow.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate OAuthFlow data', () => {
      it('can get warning named OAUTHFLOW_SCOPE_DESCRIPTION_RECOMMENDED', () => {
        oAuthFlow = new OAuthFlow(oAuthFlowData01);
        oAuthFlow.validate();
        for (let scopeDescription of oAuthFlow.scopes.items) {
          assert.equal(scopeDescription.hasWarning, true);
          assert.equal(
            scopeDescription.validation.summary,
            OAS_WARNINGS.OAUTHFLOW_SCOPE_DESCRIPTION_RECOMMENDED
          );
        }
      });
    });
  });
});
