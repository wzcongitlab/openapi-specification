import { CONSTANTS } from '../../../config/constants';
import { StringField } from '../../../fields/string.field';
import { OasNode } from '../../../base/oasNode';
import { OAS_WARNINGS } from '../../../config/oasErrors';

export class EmptySecuritySchemeModel extends OasNode {
  static get nodeObjectType() {
    return 'EmptySecuritySchemeModel';
  }

  constructor(params = {}, parent = null, nodeName = CONSTANTS.emptyStr) {
    super(parent, nodeName);
    this.type = new StringField(CONSTANTS.emptyStr, this, 'type');
  }

  validateNode() {
    if (this.type.isEmpty) {
      this.type.validation.update({
        hasError: true,
        summary: OAS_WARNINGS.SECURITY_SCHEME_TYPE_REQUIRED,
      });
    }
  }
}
