import { StringField } from '../../../fields/string.field';
import { SecurityScheme } from './securityScheme.model';
import { HTTP_SCHEMES } from '../../../config/oasConfig';
import { OAS_ERRORS } from '../../../config/oasErrors';

export class HttpSecurityScheme extends SecurityScheme {
  static get nodeObjectType() {
    return 'HttpSecurityScheme';
  }

  constructor(params = {}, parent, nodeName = 'parameter') {
    super(params, parent, nodeName);
    this.scheme = new StringField(
      params.scheme ? params.scheme : HTTP_SCHEMES.basic.name,
      this,
      'scheme'
    );
    this.bearerFormat = this.scheme.equal(HTTP_SCHEMES.bearer.name)
      ? new StringField(params.bearerFormat, this, 'bearerFormat')
      : null;
  }

  addBearerFormat() {
    this.bearerFormat = new StringField('', this, 'bearerFormat');
  }

  removeBearerFormat() {
    this.bearerFormat = null;
  }

  validateNode() {
    if (this.scheme.isEmpty) {
      this.scheme.validation.update({
        hasError: true,
        summary: OAS_ERRORS.SECURITY_HTTP_SCHEME_REQUIRED,
      });
    }
  }
}
