import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { ApiKeySecurityScheme } from './apiKeySecurityScheme.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('ApiKeySecurityScheme', () => {
  let apiKeySecuritySchemeData01 = {
    type: 'type25',
    description: 'description42',
    name: 'userName',
    in: 'query',
  };

  let apiKeySecuritySchemeData02 = {
    type: 'type25',
    description: 'description42',
    name: '',
    in: '',
  };

  let apiKeySecurityScheme;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        apiKeySecurityScheme = new ApiKeySecurityScheme(apiKeySecuritySchemeData01);
        assert.equal(
          Utils.deepEqual(apiKeySecuritySchemeData01, apiKeySecurityScheme.extract()),
          true
        );
      });
    });
  });

  describe('#validation', () => {
    describe('validate apiKeySecurityScheme data', () => {
      it('can get error named SECURITY_SCHEME_NAME_REQUIRED', () => {
        apiKeySecurityScheme = new ApiKeySecurityScheme(apiKeySecuritySchemeData02);
        apiKeySecurityScheme.validate();
        assert.equal(apiKeySecurityScheme.name.validation.hasError, true);
        assert.equal(
          apiKeySecurityScheme.name.validation.error.summary,
          OAS_ERRORS.SECURITY_SCHEME_NAME_REQUIRED
        );
      });

      it('can pass validation when given valid data', () => {
        apiKeySecurityScheme = new ApiKeySecurityScheme(apiKeySecuritySchemeData01);
        apiKeySecurityScheme.validate();
        assert.equal(apiKeySecurityScheme.validation.hasError, false);
        assert.equal(apiKeySecurityScheme.validation.error, null);
      });
    });
  });
});
