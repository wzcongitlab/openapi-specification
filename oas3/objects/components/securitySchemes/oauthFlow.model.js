import { CONSTANTS } from '../../../config/constants';
import { OasNode } from '../../../base/oasNode';
import { OasExtensibleNode } from '../../../base/oasExtensibleNode';
import { UrlField } from '../../../fields/url.field';
import { OAuthFlowScopes } from './oauthFlowScopes.model';
import { OAS_WARNINGS } from '../../../config/oasErrors';

export class OAuthFlow extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'OAuthFlow';
  }

  constructor(params = {}, parent = null, nodeName = CONSTANTS.emptyStr) {
    super(params, parent, nodeName);
    this.refreshUrl = new UrlField(params.refreshUrl, this, 'refreshUrl');
    this.scopes = new OAuthFlowScopes(params.scopes, this);
  }

  validateScopes() {
    for (let scopeDescription of this.scopes.items) {
      if (scopeDescription.isEmpty) {
        scopeDescription.validation.update({
          hasWarning: true,
          summary: OAS_WARNINGS.OAUTHFLOW_SCOPE_DESCRIPTION_RECOMMENDED,
        });
      }
    }
  }
}
