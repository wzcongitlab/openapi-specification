import { UrlField } from '../../../fields/url.field';
import { SecurityScheme } from './securityScheme.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

export class OpenIdSecurityScheme extends SecurityScheme {
  static get nodeObjectType() {
    return 'OpenIdSecurityScheme';
  }

  constructor(params = {}, parent, nodeName = 'parameter') {
    super(params, parent, nodeName);
    this.openIdConnectUrl = new UrlField(params.openIdConnectUrl, this, 'openIdConnectUrl');
  }

  validateNode() {
    if (this.openIdConnectUrl.isEmpty) {
      this.openIdConnectUrl.validation.update({
        hasError: true,
        summary: OAS_ERRORS.SECURITY_OPENIDCONNECTURL_REQUIRED,
      });
    }
  }
}
