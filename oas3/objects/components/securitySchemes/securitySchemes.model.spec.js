import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { SecuritySchemes } from './securitySchemes.model';
import { SecurityScheme } from './securityScheme.model';
import { EmptySecuritySchemeModel } from './emptySecurityScheme.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('SecuritySchemes', () => {
  let securitySchemesData01 = {};

  let securitySchemesData02 = {};

  let securitySchemes;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        securitySchemes = new SecuritySchemes(securitySchemesData01);
        assert.equal(Utils.deepEqual(securitySchemesData01, securitySchemes.extract()), true);
      });
    });
    describe('functions', () => {
      it('can add SecurityScheme', () => {
        securitySchemes = new SecuritySchemes(securitySchemesData01);
        let securityName = 'petStore_auth';
        securitySchemes.addSecurityScheme(securityName);
        assert.equal(
          securitySchemes[securityName] instanceof SecurityScheme ||
            securitySchemes[securityName] instanceof EmptySecuritySchemeModel,
          true
        );
      });
      it('can remove SecurityScheme', () => {
        securitySchemes = new SecuritySchemes(securitySchemesData01);
        let securityName = 'petStore_auth';
        securitySchemes.addSecurityScheme(securityName, {});
        securitySchemes.removeSecurityScheme(securityName);
        assert.equal(securitySchemes.itemKeys.includes(securityName), false);
        assert.equal(securitySchemes[securityName] === undefined, true);
      });
    });
  });

  describe('#validation', () => {});
});
