import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { OAuth2SecurityScheme } from './oauth2SecurityScheme.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('OAuth2SecurityScheme', () => {
  let oAuth2SecuritySchemeData01 = {
    type: 'type56',
    description: 'description89',
    flows: {
      implicit: {
        refreshUrl: '',
        authorizationUrl: 'http://petstore.swagger.io/api/oauth/dialog',
        scopes: {
          'write:pets': 'modify pets in your account',
          'read:pets': 'read your pets',
        },
      },
    },
  };

  let oAuth2SecuritySchemeData02 = {
    type: 'type56',
    description: 'description89',
    flows: {},
  };

  let oAuth2SecurityScheme;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        oAuth2SecurityScheme = new OAuth2SecurityScheme(oAuth2SecuritySchemeData01);
        assert.equal(
          Utils.deepEqual(oAuth2SecuritySchemeData01, oAuth2SecurityScheme.extract()),
          true
        );
      });
    });
  });

  describe('#validation', () => {
    describe('validate oAuth2SecurityScheme data', () => {
      it('can get error named SECURITY_OAUTH_FLOWS_REQUIRED', () => {
        oAuth2SecurityScheme = new OAuth2SecurityScheme(oAuth2SecuritySchemeData02);
        oAuth2SecurityScheme.validate();
        assert.equal(oAuth2SecurityScheme.flows.validation.hasError, true);
        assert.equal(
          oAuth2SecurityScheme.flows.validation.error.summary,
          OAS_ERRORS.SECURITY_OAUTH_FLOWS_REQUIRED
        );
      });

      it('can pass validation when given valid data', () => {
        oAuth2SecurityScheme = new OAuth2SecurityScheme(oAuth2SecuritySchemeData01);
        oAuth2SecurityScheme.validate();
        assert.equal(oAuth2SecurityScheme.validation.hasError, false);
        assert.equal(oAuth2SecurityScheme.validation.error, null);
      });
    });
  });
});
