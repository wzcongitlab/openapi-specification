import { OasMapNode } from '../../../base/oasMapNode';
import { StringField } from '../../../fields/string.field';

export class OAuthFlowScopes extends OasMapNode {
  static get nodeObjectType() {
    return 'OAuthFlowScopes';
  }

  constructor(params = {}, parent, nodeName = 'scopes') {
    super(parent, nodeName);
    for (let key of Object.keys(params)) {
      this.addScope(key, params[key]);
    }
  }

  addScope(key, scope) {
    if (scope instanceof StringField) {
      scope.parent = this;
      return this.addItem(key, scope);
    } else {
      return this.addItem(key, new StringField(scope, this, key));
    }
  }

  removeScope(key) {
    return this.removeItem(key);
  }
}
