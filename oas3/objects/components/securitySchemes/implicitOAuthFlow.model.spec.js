import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { ImplicitOAuthFlow } from './implicitOAuthFlow.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('ImplicitOAuthFlow', () => {
  let implicitOAuthFlowData01 = {
    refreshUrl: 'refreshUrl51',
    scopes: {
      'write:pets': 'modify pets in your account',
    },
    authorizationUrl: 'http://mindoc.skyinno.com/',
  };

  let implicitOAuthFlowData02 = {
    refreshUrl: 'refreshUrl51',
    scopes: {},
    authorizationUrl: '',
  };

  let implicitOAuthFlow;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        implicitOAuthFlow = new ImplicitOAuthFlow(implicitOAuthFlowData01);
        assert.equal(Utils.deepEqual(implicitOAuthFlowData01, implicitOAuthFlow.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate implicitOAuthFlow data', () => {
      it('can get error named OAUTH_FLOW_SCOPES_REQUIRED', () => {
        implicitOAuthFlow = new ImplicitOAuthFlow(implicitOAuthFlowData02);
        implicitOAuthFlow.validate();
        assert.equal(implicitOAuthFlow.scopes.validation.hasError, true);
        assert.equal(
          implicitOAuthFlow.scopes.validation.error.summary,
          OAS_ERRORS.OAUTH_FLOW_SCOPES_REQUIRED
        );
      });

      it('can get error named OAUTH_FLOW_AUTHURL_REQUIRED', () => {
        implicitOAuthFlow = new ImplicitOAuthFlow(implicitOAuthFlowData02);
        implicitOAuthFlow.validate();
        assert.equal(implicitOAuthFlow.authorizationUrl.validation.hasError, true);
        assert.equal(
          implicitOAuthFlow.authorizationUrl.validation.error.summary,
          OAS_ERRORS.OAUTH_FLOW_AUTHURL_REQUIRED
        );
      });

      it('can pass validation when given valid data', () => {
        implicitOAuthFlow = new ImplicitOAuthFlow(implicitOAuthFlowData01);
        implicitOAuthFlow.validate();
        assert.equal(implicitOAuthFlow.authorizationUrl.validation.hasError, false);
        assert.equal(implicitOAuthFlow.scopes.validation.hasError, false);
      });
    });
  });
});
