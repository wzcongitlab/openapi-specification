import { OasNode } from '../../../base/oasNode';
import { OasExtensibleNode } from '../../../base/oasExtensibleNode';
import { StringField } from '../../../fields/string.field';
import { RichTextField } from '../../../fields/richText.field';

export class SecurityScheme extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'SecurityScheme';
  }

  constructor(params = {}, parent, nodeName = 'parameter') {
    super(params, parent, nodeName);
    this.type = new StringField(params.type, this, 'type');
    this.description = new RichTextField(params.description, this, 'description');
  }
}
