import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { OpenIdSecurityScheme } from './openIdSecurityScheme.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('OpenIdSecurityScheme', () => {
  let openIdSecuritySchemeData01 = {
    type: 'type35',
    description: 'description49',
    openIdConnectUrl: 'http://mindoc.skyinno.com/docs/oas3/oas3.0.2#pathItemObject',
  };

  let openIdSecuritySchemeData02 = {
    type: 'type35',
    description: 'description49',
    openIdConnectUrl: '',
  };

  let openIdSecurityScheme;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        openIdSecurityScheme = new OpenIdSecurityScheme(openIdSecuritySchemeData01);
        assert.equal(
          Utils.deepEqual(openIdSecuritySchemeData01, openIdSecurityScheme.extract()),
          true
        );
      });
    });
  });

  describe('#validation', () => {
    describe('validate openIdSecurityScheme data', () => {
      it('can get error named SECURITY_OPENIDCONNECTURL_REQUIRED', () => {
        openIdSecurityScheme = new OpenIdSecurityScheme(openIdSecuritySchemeData02);
        openIdSecurityScheme.validate();
        assert.equal(openIdSecurityScheme.openIdConnectUrl.validation.hasError, true);
        assert.equal(
          openIdSecurityScheme.openIdConnectUrl.validation.error.summary,
          OAS_ERRORS.SECURITY_OPENIDCONNECTURL_REQUIRED
        );
      });

      it('can pass validation when given valid data', () => {
        openIdSecurityScheme = new OpenIdSecurityScheme(openIdSecuritySchemeData01);
        openIdSecurityScheme.validate();
        assert.equal(openIdSecurityScheme.openIdConnectUrl.validation.hasError, false);
      });
    });
  });
});
