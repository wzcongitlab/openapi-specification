import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { AuthorizationCodeOAuthFlow } from './authorizationCodeOAuthFlow.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('AuthorizationCodeOAuthFlow', () => {
  let authorizationCodeOAuthFlowData01 = {
    refreshUrl: 'http://mindoc.skyinno.com/',
    scopes: {
      'write:pets': 'modify pets in your account',
    },
    tokenUrl: 'http://mindoc.skyinno.com/',
    authorizationUrl: 'http://mindoc.skyinno.com/',
  };

  let authorizationCodeOAuthFlowData02 = {
    refreshUrl: 'refreshUrl44',
    scopes: {},
    tokenUrl: '',
    authorizationUrl: '',
  };

  let authorizationCodeOAuthFlow;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        authorizationCodeOAuthFlow = new AuthorizationCodeOAuthFlow(
          authorizationCodeOAuthFlowData01
        );
        assert.equal(
          Utils.deepEqual(authorizationCodeOAuthFlowData01, authorizationCodeOAuthFlow.extract()),
          true
        );
      });
    });
  });

  describe('#validation', () => {
    describe('validate authorizationCodeOAuthFlow data', () => {
      it('can get error named OAUTH_FLOW_SCOPES_REQUIRED', () => {
        authorizationCodeOAuthFlow = new AuthorizationCodeOAuthFlow(
          authorizationCodeOAuthFlowData02
        );
        authorizationCodeOAuthFlow.validate();
        assert.equal(authorizationCodeOAuthFlow.scopes.validation.hasError, true);
        assert.equal(
          authorizationCodeOAuthFlow.scopes.validation.error.summary,
          OAS_ERRORS.OAUTH_FLOW_SCOPES_REQUIRED
        );
      });

      it('can get error named OAUTH_FLOW_TOKENURL_REQUIRED', () => {
        authorizationCodeOAuthFlow = new AuthorizationCodeOAuthFlow(
          authorizationCodeOAuthFlowData02
        );
        authorizationCodeOAuthFlow.validate();
        assert.equal(authorizationCodeOAuthFlow.tokenUrl.validation.hasError, true);
        assert.equal(
          authorizationCodeOAuthFlow.tokenUrl.validation.error.summary,
          OAS_ERRORS.OAUTH_FLOW_TOKENURL_REQUIRED
        );
      });

      it('can get error named OAUTH_FLOW_AUTHURL_REQUIRED', () => {
        authorizationCodeOAuthFlow = new AuthorizationCodeOAuthFlow(
          authorizationCodeOAuthFlowData02
        );
        authorizationCodeOAuthFlow.validate();
        assert.equal(authorizationCodeOAuthFlow.authorizationUrl.validation.hasError, true);
        assert.equal(
          authorizationCodeOAuthFlow.authorizationUrl.validation.error.summary,
          OAS_ERRORS.OAUTH_FLOW_AUTHURL_REQUIRED
        );
      });

      it('can pass validation when given valid data', () => {
        authorizationCodeOAuthFlow = new AuthorizationCodeOAuthFlow(
          authorizationCodeOAuthFlowData01
        );
        authorizationCodeOAuthFlow.validate();
        assert.equal(authorizationCodeOAuthFlow.authorizationUrl.validation.hasError, false);
        assert.equal(authorizationCodeOAuthFlow.tokenUrl.validation.hasError, false);
        assert.equal(authorizationCodeOAuthFlow.scopes.validation.hasError, false);
      });
    });
  });
});
