import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { HttpSecurityScheme } from './httpSecurityScheme.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('HttpSecurityScheme', () => {
  let httpSecuritySchemeData01 = {
    type: 'http',
    description: 'description39',
    scheme: 'bearer',
    bearerFormat: 'bearerFormat65',
  };

  let httpSecuritySchemeData02 = {
    type: 'type29',
    description: 'description39',
    scheme: '',
    bearerFormat: 'bearerFormat65',
  };

  let httpSecurityScheme;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        httpSecurityScheme = new HttpSecurityScheme(httpSecuritySchemeData01);
        console.log(httpSecurityScheme.extract());
        assert.equal(Utils.deepEqual(httpSecuritySchemeData01, httpSecurityScheme.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate httpSecurityScheme data', () => {
      it('can pass validation when given valid data', () => {
        httpSecurityScheme = new HttpSecurityScheme(httpSecuritySchemeData01);
        httpSecurityScheme.validate();
        assert.equal(httpSecurityScheme.scheme.validation.hasError, false);
      });
    });
  });
});
