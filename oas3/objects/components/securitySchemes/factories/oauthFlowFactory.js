import { CONSTANTS } from '../../../../config/constants';
import { OAUTH_FLOW_TYPES } from '../../../../config/oasConfig';
import { ImplicitOAuthFlow } from '../implicitOAuthFlow.model';
import { PasswordOAuthFlow } from '../passwordOAuthFlow.model';
import { ClientCredentialsOAuthFlow } from '../clientCredentialsOAuthFlow.model';
import { AuthorizationCodeOAuthFlow } from '../authorizationCodeOAuthFlow.model';

export class OAuthFlowFactory {
  static createOAuthFlow(params, parent, nodeName = CONSTANTS.emptyStr) {
    if (nodeName === OAUTH_FLOW_TYPES.implicit) {
      return new ImplicitOAuthFlow(params, parent, nodeName);
    }
    if (nodeName === OAUTH_FLOW_TYPES.password) {
      return new PasswordOAuthFlow(params, parent, nodeName);
    }
    if (nodeName === OAUTH_FLOW_TYPES.clientCredentials) {
      return new ClientCredentialsOAuthFlow(params, parent, nodeName);
    }
    if (nodeName === OAUTH_FLOW_TYPES.authorizationCode) {
      return new AuthorizationCodeOAuthFlow(params, parent, nodeName);
    }
    return null;
  }
}
