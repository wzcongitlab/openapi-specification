import { SECURITY_SCHEME_TYPES } from '../../../../config/oasConfig';
import { ApiKeySecurityScheme } from '../apiKeySecurityScheme.model';
import { HttpSecurityScheme } from '../httpSecurityScheme.model';
import { OAuth2SecurityScheme } from '../oauth2SecurityScheme.model';
import { OpenIdSecurityScheme } from '../openIdSecurityScheme.model';
import { EmptySecuritySchemeModel } from '../emptySecurityScheme.model';

export class SecuritySchemeFactory {
  // 这一类工厂方法应保持简单, 仅根据类型创建不同的对象,
  // 具体新建对象的方式应与event的回退前进中新建对象的方式一致
  // 即 new Class(params, parent, nodeName);
  static createSecurityScheme(params = {}, parent = null, nodeName) {
    if (params.type === SECURITY_SCHEME_TYPES.apiKey.name) {
      return new ApiKeySecurityScheme(params, parent, nodeName);
    }
    if (params.type === SECURITY_SCHEME_TYPES.http.name) {
      return new HttpSecurityScheme(params, parent, nodeName);
    }
    if (params.type === SECURITY_SCHEME_TYPES.oauth2.name) {
      return new OAuth2SecurityScheme(params, parent, nodeName);
    }
    if (params.type === SECURITY_SCHEME_TYPES.openIdConnect.name) {
      return new OpenIdSecurityScheme(params, parent, nodeName);
    }
    return new EmptySecuritySchemeModel({}, parent, nodeName);
  }
}
