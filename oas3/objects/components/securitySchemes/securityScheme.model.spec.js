import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { SecurityScheme } from './securityScheme.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('SecurityScheme', () => {
  let securitySchemeData01 = {
    type: 'type2',
    description: 'description94',
  };

  let securitySchemeData02 = {
    type: 'type2',
    description: 'description94',
  };

  let securityScheme;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        securityScheme = new SecurityScheme(securitySchemeData01);
        assert.equal(Utils.deepEqual(securitySchemeData01, securityScheme.extract()), true);
      });
    });
  });

  describe('#validation', () => {});
});
