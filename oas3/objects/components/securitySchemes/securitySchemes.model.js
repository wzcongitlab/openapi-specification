import { OasMapNode } from '../../../base/oasMapNode';
import { SecurityScheme } from './securityScheme.model';
import { Reference } from '../../common/reference.model';
import { ComponentFactory } from '../componentFactory';
import { OAS_COMPONENT_TYPES } from '../../../config/oasConfig';

export class SecuritySchemes extends OasMapNode {
  static get nodeObjectType() {
    return 'SecuritySchemes';
  }

  constructor(params = {}, parent = null, nodeName = 'securitySchemes') {
    super(parent, nodeName);
    for (let key of Object.keys(params)) {
      this.addSecurityScheme(key, params[key]);
    }
  }

  addSecurityScheme(key, securityScheme) {
    if (securityScheme instanceof SecurityScheme || securityScheme instanceof Reference) {
      securityScheme.parent = this;
      this.addItem(key, securityScheme);
    } else {
      let newSecurityScheme = ComponentFactory.createComponent({
        params: securityScheme,
        parent: this,
        type: OAS_COMPONENT_TYPES.securityScheme.name,
        nodeName: key,
      });
      this.addItem(key, newSecurityScheme);
    }
  }

  updateSecurityScheme(key, securityScheme) {
    if (securityScheme instanceof SecurityScheme || securityScheme instanceof Reference) {
      securityScheme.parent = this;
      this.updateItem(key, securityScheme);
      return;
    }
    this.updateItem(
      key,
      ComponentFactory.createComponent({
        params: securityScheme,
        parent: this,
        type: OAS_COMPONENT_TYPES.securityScheme.name,
        nodeName: key,
      })
    );
  }

  removeSecurityScheme(key) {
    this.removeItem(key);
  }
}
