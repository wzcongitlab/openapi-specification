import { UrlField } from '../../../fields/url.field';
import { OAuthFlow } from './oauthFlow.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

export class ImplicitOAuthFlow extends OAuthFlow {
  static get nodeObjectType() {
    return 'ImplicitOAuthFlow';
  }

  constructor(params = {}, parent = null, nodeName = 'implicit') {
    super(params, parent, nodeName);
    this.authorizationUrl = new UrlField(params.authorizationUrl, this, 'authorizationUrl');
  }

  validateNode() {
    if (this.scopes.isEmpty) {
      this.scopes.validation.update({
        hasError: true,
        summary: OAS_ERRORS.OAUTH_FLOW_SCOPES_REQUIRED,
      });
    }
    if (this.authorizationUrl.isEmpty) {
      this.authorizationUrl.validation.update({
        hasError: true,
        summary: OAS_ERRORS.OAUTH_FLOW_AUTHURL_REQUIRED,
      });
    }
    this.validateScopes();
  }
}
