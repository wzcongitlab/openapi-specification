import { RichTextField } from '../../../fields/richText.field';
import { SecurityScheme } from './securityScheme.model';
import { OAuthFlows } from './oauthFlows.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

export class OAuth2SecurityScheme extends SecurityScheme {
  static get nodeObjectType() {
    return 'OAuth2SecurityScheme';
  }

  constructor(params = {}, parent, nodeName = 'parameter') {
    super(params, parent, nodeName);
    this.description = new RichTextField(params.description, this, 'description');
    this.flows = new OAuthFlows(params.flows, this);
  }

  validateNode() {
    if (this.flows.isEmpty) {
      this.flows.validation.update({
        hasError: true,
        summary: OAS_ERRORS.SECURITY_OAUTH_FLOWS_REQUIRED,
      });
    }
  }
}
