import { StringField } from '../../../fields/string.field';
import { SecurityScheme } from './securityScheme.model';
import { API_KEY_LOCATIONS } from '../../../config/oasConfig';
import { OAS_ERRORS } from '../../../config/oasErrors';

export class ApiKeySecurityScheme extends SecurityScheme {
  static get nodeObjectType() {
    return 'ApiKeySecurityScheme';
  }

  constructor(params, parent, nodeName = 'parameter') {
    super(params, parent, nodeName);
    this.name = new StringField(params.name, this, 'name');
    this.in = new StringField(params.in ? params.in : API_KEY_LOCATIONS.query.name, this, 'in');
  }

  validateNode() {
    if (this.name.isEmpty) {
      this.name.validation.update({
        hasError: true,
        summary: OAS_ERRORS.SECURITY_SCHEME_NAME_REQUIRED,
      });
    }
    if (this.in.isEmpty) {
      this.in.validation.update({
        hasError: true,
        summary: OAS_ERRORS.SECURITY_SCHEME_IN_REQUIRED,
      });
    }
  }
}
