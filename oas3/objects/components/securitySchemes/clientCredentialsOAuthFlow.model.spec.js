import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { ClientCredentialsOAuthFlow } from './clientCredentialsOAuthFlow.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('ClientCredentialsOAuthFlow', () => {
  let clientCredentialsOAuthFlowData01 = {
    refreshUrl: 'refreshUrl95',
    scopes: {
      'write:pets': 'modify pets in your account',
    },
    tokenUrl: 'http://mindoc.skyinno.com/',
  };

  let clientCredentialsOAuthFlowData02 = {
    refreshUrl: 'refreshUrl95',
    scopes: {},
    tokenUrl: '',
  };

  let clientCredentialsOAuthFlow;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        clientCredentialsOAuthFlow = new ClientCredentialsOAuthFlow(
          clientCredentialsOAuthFlowData01
        );
        assert.equal(
          Utils.deepEqual(clientCredentialsOAuthFlowData01, clientCredentialsOAuthFlow.extract()),
          true
        );
      });
    });
  });

  describe('#validation', () => {
    describe('validate clientCredentialsOAuthFlow data', () => {
      it('can get error named OAUTH_FLOW_SCOPES_REQUIRED', () => {
        clientCredentialsOAuthFlow = new ClientCredentialsOAuthFlow(
          clientCredentialsOAuthFlowData02
        );
        clientCredentialsOAuthFlow.validate();
        assert.equal(clientCredentialsOAuthFlow.scopes.validation.hasError, true);
        assert.equal(
          clientCredentialsOAuthFlow.scopes.validation.error.summary,
          OAS_ERRORS.OAUTH_FLOW_SCOPES_REQUIRED
        );
      });

      it('can get error named OAUTH_FLOW_TOKENURL_REQUIRED', () => {
        clientCredentialsOAuthFlow = new ClientCredentialsOAuthFlow(
          clientCredentialsOAuthFlowData02
        );
        clientCredentialsOAuthFlow.validate();
        assert.equal(clientCredentialsOAuthFlow.tokenUrl.validation.hasError, true);
        assert.equal(
          clientCredentialsOAuthFlow.tokenUrl.validation.error.summary,
          OAS_ERRORS.OAUTH_FLOW_TOKENURL_REQUIRED
        );
      });

      it('can pass validation when given valid data', () => {
        clientCredentialsOAuthFlow = new ClientCredentialsOAuthFlow(
          clientCredentialsOAuthFlowData01
        );
        clientCredentialsOAuthFlow.validate();
        assert.equal(clientCredentialsOAuthFlow.scopes.validation.hasError, false);
        assert.equal(clientCredentialsOAuthFlow.tokenUrl.validation.hasError, false);
      });
    });
  });
});
