import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { StringField } from '../../../fields/string.field';
import { OAuthFlowScopes } from './oauthFlowScopes.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('OAuthFlowScopes', () => {
  let oAuthFlowScopesData01 = {};

  let oAuthFlowScopesData02 = {};

  let oAuthFlowScopes;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        oAuthFlowScopes = new OAuthFlowScopes(oAuthFlowScopesData01);
        assert.equal(Utils.deepEqual(oAuthFlowScopesData01, oAuthFlowScopes.extract()), true);
      });
    });
    describe('functions', () => {
      it('can add scope', () => {
        oAuthFlowScopes = new OAuthFlowScopes(oAuthFlowScopesData01);
        let scopeName = 'write:pet';
        let scopeDescription = 'has permission to write pet';
        oAuthFlowScopes.addScope(scopeName, scopeDescription);
        assert.equal(oAuthFlowScopes[scopeName] instanceof StringField, true);
        assert.equal(oAuthFlowScopes[scopeName].extract(), scopeDescription);
      });
    });
  });

  describe('#validation', () => {});
});
