import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { PasswordOAuthFlow } from './passwordOAuthFlow.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('PasswordOAuthFlow', () => {
  let passwordOAuthFlowData01 = {
    refreshUrl: 'refreshUrl71',
    scopes: {
      'write:pets': 'modify pets in your account',
    },
    tokenUrl: 'http://mindoc.skyinno.com/docs/oas3/oas3.0.2#pathItemObject',
  };

  let passwordOAuthFlowData02 = {
    refreshUrl: 'refreshUrl71',
    scopes: {},
    tokenUrl: '',
  };

  let passwordOAuthFlow;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        passwordOAuthFlow = new PasswordOAuthFlow(passwordOAuthFlowData01);
        assert.equal(Utils.deepEqual(passwordOAuthFlowData01, passwordOAuthFlow.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate passwordOAuthFlow data', () => {
      it('can get error named OAUTH_FLOW_SCOPES_REQUIRED', () => {
        passwordOAuthFlow = new PasswordOAuthFlow(passwordOAuthFlowData02);
        passwordOAuthFlow.validate();
        assert.equal(passwordOAuthFlow.scopes.validation.hasError, true);
        assert.equal(
          passwordOAuthFlow.scopes.validation.error.summary,
          OAS_ERRORS.OAUTH_FLOW_SCOPES_REQUIRED
        );
      });

      it('can get error named OAUTH_FLOW_TOKENURL_REQUIRED', () => {
        passwordOAuthFlow = new PasswordOAuthFlow(passwordOAuthFlowData02);
        passwordOAuthFlow.validate();
        assert.equal(passwordOAuthFlow.tokenUrl.validation.hasError, true);
        assert.equal(
          passwordOAuthFlow.tokenUrl.validation.error.summary,
          OAS_ERRORS.OAUTH_FLOW_TOKENURL_REQUIRED
        );
      });

      it('can pass validation when given valid data', () => {
        passwordOAuthFlow = new PasswordOAuthFlow(passwordOAuthFlowData01);
        passwordOAuthFlow.validate();
        assert.equal(passwordOAuthFlow.tokenUrl.validation.hasError, false);
        assert.equal(passwordOAuthFlow.scopes.validation.hasError, false);
      });
    });
  });
});
