import chai from 'chai';
import { Utils } from '../../../../utils/utils';
import { OAUTH_FLOW_TYPES } from '../../../config/oasConfig';
import { OAuthFlows } from './oauthFlows.model';
import { OAuthFlow } from './oauthFlow.model';
import { OAS_ERRORS } from '../../../config/oasErrors';

const assert = chai.assert;

describe('OAuthFlows', () => {
  let oAuthFlowsData01 = {};

  let oAuthFlowsData02 = {};

  let oAuthFlows;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        oAuthFlows = new OAuthFlows(oAuthFlowsData01);
        assert.equal(Utils.deepEqual(oAuthFlowsData01, oAuthFlows.extract()), true);
      });
    });
    describe('functions', () => {
      it('can add OAuthFlow', () => {
        oAuthFlows = new OAuthFlows(oAuthFlowsData01);
        oAuthFlows.addOAuthFlow(OAUTH_FLOW_TYPES.clientCredentials, {});
        assert.equal(oAuthFlows[OAUTH_FLOW_TYPES.clientCredentials] instanceof OAuthFlow, true);
      });
    });
  });

  describe('#validation', () => {});
});
