import { OasMapNode } from '../../../base/oasMapNode';
import { OasExtensibleNode } from '../../../base/oasExtensibleNode';
import { OAuthFlow } from './oauthFlow.model';
import { OAuthFlowFactory } from './factories/oauthFlowFactory';
import { OAUTH_FLOW_TYPES } from '../../../config/oasConfig';

export class OAuthFlows extends OasExtensibleNode(OasMapNode) {
  static get nodeObjectType() {
    return 'OAuthFlows';
  }

  constructor(params = {}, parent, nodeName = 'flows') {
    super(params, parent, nodeName);
    for (let key of Object.keys(params)) {
      this.addOAuthFlow(key, params[key]);
    }
  }

  addOAuthFlow(key, oauthFlow) {
    if (!OAUTH_FLOW_TYPES.hasOwnProperty(key)) {
      return;
    }
    if (oauthFlow instanceof OAuthFlow) {
      oauthFlow.parent = this;
      return this.addItem(key, oauthFlow);
    } else {
      let newOAuthFlow = OAuthFlowFactory.createOAuthFlow(oauthFlow, this, key);
      return this.addItem(key, newOAuthFlow);
    }
  }

  removeOAuthFlow(key) {
    if (!OAUTH_FLOW_TYPES.hasOwnProperty(key)) {
      return;
    }
    return this.removeItem(key);
  }

  hasOAuthFlow(key) {
    return this[key] instanceof OAuthFlow;
  }
}
