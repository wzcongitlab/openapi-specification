import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Example } from './example.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Example', () => {
  let exampleData01 = {
    summary: 'summary89',
    description: 'description5',
    value: 'exampleValue17',
    externalValue: 'externalValue',
  };

  let exampleData02 = {
    summary: 'summary89',
    description: 'description5',
    value: 'exampleValue17',
    externalValue: 'externalValue',
  };

  let example;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        example = new Example(exampleData01);
        assert.equal(Utils.deepEqual(exampleData01, example.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate example data', () => {
      it('can pass validation when given valid data', () => {
        example = new Example(exampleData01);
        example.validate();
        assert.equal(example.validation.hasError, false);
        assert.equal(example.validation.hasWarning, false);
      });
    });
  });
});
