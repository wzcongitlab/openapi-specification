import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { LinkParameters } from './linkParameters.model';
import { AnyField } from '../../fields/any.field';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('LinkParameters', () => {
  let linkParametersData01 = {};

  let linkParametersData02 = {};

  let linkParameters;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        linkParameters = new LinkParameters(linkParametersData01);
        assert.equal(Utils.deepEqual(linkParametersData01, linkParameters.extract()), true);
      });
      describe('functions', () => {
        it('can add LinkParameter', () => {
          linkParameters = new LinkParameters(linkParametersData01);
          let linkParameterName = 'linkParameter1';
          linkParameters.addLinkParameter(linkParameterName);
          assert.equal(linkParameters[linkParameterName] instanceof AnyField, true);
        });
        it('can remove LinkParameter', () => {
          linkParameters = new LinkParameters(linkParametersData01);
          let linkParameterName = 'linkParameter1';
          linkParameters.addLinkParameter(linkParameterName, '');
          linkParameters.removeLinkParameter(linkParameterName);
          assert.equal(linkParameters.itemKeys.includes(linkParameterName), false);
          assert.equal(linkParameters[linkParameterName] === undefined, true);
        });
      });
    });
  });

  describe('#validation', () => {});
});
