import { CONSTANTS } from '../../config/constants';
import { Reference } from '../common/reference.model';
import { Header } from './header.model';
import { Example } from './example.model';
import { Parameter } from './parameter.model';
import { RequestBody } from './requestBody.model';
import { Response } from './response.model';
import { Callback } from './callback.model';
import { SecurityScheme } from './securitySchemes/securityScheme.model';
import { Link } from './link.model';
import { OAS_COMPONENT_TYPES } from '../../config/oasConfig';
import { OasSchemaFactory } from '../common/schema/factories/oasSchemaFactory';
import { SecuritySchemeFactory } from './securitySchemes/factories/securitySchemeFactory';

export class ComponentFactory {
  static createComponent({
    params = {},
    parent = null,
    type = 'reference',
    nodeName = CONSTANTS.emptyStr,
  } = {}) {
    if (params.hasOwnProperty('$ref') || type === 'reference') {
      return new Reference(params, parent, nodeName);
    }
    if (!OAS_COMPONENT_TYPES.hasOwnProperty(type)) {
      return;
    }
    if (type === OAS_COMPONENT_TYPES.schema.name) {
      return OasSchemaFactory.createSchema(params, parent, nodeName);
    }
    if (type === OAS_COMPONENT_TYPES.securityScheme.name) {
      return SecuritySchemeFactory.createSecurityScheme(params, parent, nodeName);
    }
    return new ComponentFactory.componentConstructors[type](params, parent, nodeName);
  }

  static get componentConstructors() {
    return {
      header: Header,
      example: Example,
      parameter: Parameter,
      requestBody: RequestBody,
      response: Response,
      link: Link,
      securityScheme: SecurityScheme,
      callback: Callback,
    };
  }
}
