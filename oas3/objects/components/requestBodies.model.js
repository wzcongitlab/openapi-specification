import { OasMapNode } from '../../base/oasMapNode';
import { RequestBody } from './requestBody.model';
import { Reference } from '../common/reference.model';
import { ComponentFactory } from './componentFactory';
import { OAS_COMPONENT_TYPES } from '../../config/oasConfig';

export class RequestBodies extends OasMapNode {
  static get nodeObjectType() {
    return 'RequestBodies';
  }

  constructor(params = {}, parent = null, nodeName = 'requestBodies') {
    super(parent, nodeName);
    for (let key of Object.keys(params)) {
      this.addRequestBody(key, params[key]);
    }
  }

  addRequestBody(key, requestBody) {
    if (requestBody instanceof RequestBody || requestBody instanceof Reference) {
      requestBody.parent = this;
      this.addItem(key, requestBody);
    } else {
      let newRequestBody = ComponentFactory.createComponent({
        params: requestBody,
        parent: this,
        type: OAS_COMPONENT_TYPES.requestBody.name,
        nodeName: key,
      });
      this.addItem(key, newRequestBody);
    }
  }

  updateRequestBody(key, requestBody) {
    // requestBody 是一个指定requestBody的oas json数据或requestBody对象;
    if (requestBody instanceof RequestBody || requestBody instanceof Reference) {
      requestBody.parent = this;
      this.updateItem(key, requestBody);
      return;
    }
    this.updateItem(
      key,
      ComponentFactory.createComponent({
        params: requestBody,
        parent: this,
        type: OAS_COMPONENT_TYPES.requestBody.name,
        nodeName: key,
      })
    );
  }

  removeRequestBody(key) {
    this.removeItem(key);
  }
}
