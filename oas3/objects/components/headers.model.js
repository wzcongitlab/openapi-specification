import { OasMapNode } from '../../base/oasMapNode';
import { Header } from './header.model';
import { Reference } from '../common/reference.model';
import { ComponentFactory } from './componentFactory';
import { OAS_COMPONENT_TYPES } from '../../config/oasConfig';
import { Encoding } from '../common/encoding.model';
import { OAS_WARNINGS } from '../../config/oasErrors';

export class Headers extends OasMapNode {
  static get nodeObjectType() {
    return 'Headers';
  }

  constructor(params = {}, parent = null, nodeName = 'headers') {
    super(parent, nodeName);
    for (let key of Object.keys(params)) {
      this.addHeader(key, params[key]);
    }
  }

  addHeader(key, header) {
    if (header instanceof Header || header instanceof Reference) {
      header.parent = this;
      this.addItem(key, header);
    } else {
      if (!!header && !header.hasOwnProperty('name')) {
        header.name = key;
      }
      let newHeader = ComponentFactory.createComponent({
        params: header,
        parent: this,
        type: OAS_COMPONENT_TYPES.header.name,
        nodeName: key,
      });
      this.addItem(key, newHeader);
    }
  }

  updateHeader(key, header) {
    // header 是一个指定header的oas json数据或header对象;
    if (header instanceof Header || header instanceof Reference) {
      header.parent = this;
      this.updateItem(key, header);
      return;
    }
    this.updateItem(
      key,
      ComponentFactory.createComponent({
        params: header,
        parent: this,
        type: OAS_COMPONENT_TYPES.header.name,
        nodeName: key,
      })
    );
  }

  removeHeader(key) {
    this.removeItem(key);
  }

  validateNode() {
    try {
      if (this.parent instanceof Encoding) {
        for (let header of this.items) {
          if (
            header instanceof Header &&
            this.parent.parent.parent.parent.parent instanceof Header
          ) {
            this.validation.update({
              hasWarning: true,
              summary: OAS_WARNINGS.HEADERS_REFERENCED_SUGGESTED,
            });
          }
        }
      }
    } catch (e) {
      // headers不是嵌套的headers.
      console.log(`[Headers.validateNode] ${e}`);
    }
  }
}
