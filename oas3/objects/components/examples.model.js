import { OasMapNode } from '../../base/oasMapNode';
import { Example } from './example.model';
import { Reference } from '../common/reference.model';
import { ComponentFactory } from './componentFactory';
import { OAS_COMPONENT_TYPES } from '../../config/oasConfig';

export class Examples extends OasMapNode {
  static get nodeObjectType() {
    return 'Examples';
  }

  constructor(params = {}, parent = null, nodeName = 'examples') {
    super(parent, nodeName);
    for (let key of Object.keys(params)) {
      this.addExample(key, params[key]);
    }
  }

  addExample(key, example) {
    if (example instanceof Example || example instanceof Reference) {
      example.parent = this;
      this.addItem(key, example);
    } else {
      let newExample = ComponentFactory.createComponent({
        params: example,
        parent: this,
        type: OAS_COMPONENT_TYPES.example.name,
        nodeName: key,
      });
      this.addItem(key, newExample);
    }
  }

  updateExample(key, example) {
    // example 是一个指定example的oas json数据或example对象;
    if (example instanceof Example || example instanceof Reference) {
      example.parent = this;
      this.updateItem(key, example);
      return;
    }
    this.updateItem(
      key,
      ComponentFactory.createComponent({
        params: example,
        parent: this,
        type: OAS_COMPONENT_TYPES.example.name,
        nodeName: key,
      })
    );
  }

  removeExample(key) {
    this.removeItem(key);
  }
}
