import { OasMapNode } from '../../base/oasMapNode';
import { JsonSchema } from '../common/schema/jsonSchema.model';
import { Reference } from '../common/reference.model';
import { ComponentFactory } from './componentFactory';
import { OAS_COMPONENT_TYPES } from '../../config/oasConfig';

export class Schemas extends OasMapNode {
  static get nodeObjectType() {
    return 'Schemas';
  }

  constructor(params = {}, parent = null, nodeName = 'schemas') {
    super(parent, nodeName);
    for (let key of Object.keys(params)) {
      this.addSchema(key, params[key]);
    }
  }

  addSchema(key, schema) {
    if (schema instanceof JsonSchema || schema instanceof Reference) {
      schema.parent = this;
      this.addItem(key, schema);
      return;
    }
    this.addItem(
      key,
      ComponentFactory.createComponent({
        params: schema,
        parent: this,
        type: OAS_COMPONENT_TYPES.schema.name,
        nodeName: key,
      })
    );
  }

  updateSchema(key, schema) {
    // schema 是一个指定schema的oas json数据或schema对象;
    if (schema instanceof JsonSchema || schema instanceof Reference) {
      schema.parent = this;
      this.updateItem(key, schema);
      return;
    }
    this.updateItem(
      key,
      ComponentFactory.createComponent({
        params: schema,
        parent: this,
        type: OAS_COMPONENT_TYPES.schema.name,
        nodeName: key,
      })
    );
  }

  removeSchema(key) {
    this.removeItem(key);
  }
}
