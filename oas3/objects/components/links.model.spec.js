import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Links } from './links.model';
import { Link } from './link.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Links', () => {
  let linksData01 = {};

  let linksData02 = {};

  let links;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        links = new Links(linksData01);
        assert.equal(Utils.deepEqual(linksData01, links.extract()), true);
      });
    });
    describe('functions', () => {
      it('can add Link', () => {
        links = new Links(linksData01);
        let linkName = 'link1';
        links.addLink(linkName);
        assert.equal(links[linkName] instanceof Link, true);
      });
      it('can remove Link', () => {
        links = new Links(linksData01);
        let linkName = 'link1';
        links.addLink(linkName, {});
        links.removeLink(linkName);
        assert.equal(links.itemKeys.includes(linkName), false);
        assert.equal(links[linkName] === undefined, true);
      });
    });
  });

  describe('#validation', () => {});
});
