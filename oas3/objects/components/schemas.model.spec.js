import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Schemas } from './schemas.model';
import { OasSchema } from '../common/schema/oasSchema.model';
import { OAS_ERRORS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Schemas', () => {
  let schemasData01 = {};

  let schemasData02 = {};

  let schemas;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        schemas = new Schemas(schemasData01);
        assert.equal(Utils.deepEqual(schemasData01, schemas.extract()), true);
      });
    });
    describe('functions', () => {
      it('can add Schema', () => {
        schemas = new Schemas(schemasData01);
        let schemaName = 'schema1';
        schemas.addSchema(schemaName, { type: 'string' });
        assert.equal(schemas[schemaName] instanceof OasSchema, true);
      });
      it('can remove Schema', () => {
        schemas = new Schemas(schemasData01);
        let schemaName = 'schema1';
        schemas.addSchema(schemaName, { type: 'string' });
        schemas.removeSchema(schemaName);
        assert.equal(schemas.itemKeys.includes(schemaName), false);
        assert.equal(schemas[schemaName] === undefined, true);
      });
    });
  });

  describe('#validation', () => {});
});
