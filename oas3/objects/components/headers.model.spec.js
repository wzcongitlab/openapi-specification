import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Headers } from './headers.model';
import { Header } from './header.model';
import { OAS_WARNINGS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Headers', () => {
  let headersData01 = {};

  let headersData02 = {
    header0: {
      description: 'Header',
      style: 'simple',
      content: {
        'application/json': {
          schema: {
            type: 'string',
            format: 'byte',
          },
          encoding: {
            encode0: {
              contentType: 'sdfsf',
              headers: {
                header0: {
                  $ref: '#/components/headers/header1',
                },
                header1: {
                  description: 'Header',
                  style: 'simple',
                },
              },
            },
          },
        },
      },
    },
  };

  let headers;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        headers = new Headers(headersData01);
        assert.equal(Utils.deepEqual(headersData01, headers.extract()), true);
      });
      describe('functions', () => {
        it('can add Header', () => {
          headers = new Headers(headersData01);
          let headerName = 'header1';
          headers.addHeader(headerName);
          assert.equal(headers[headerName] instanceof Header, true);
        });
        it('can remove Header', () => {
          headers = new Headers(headersData01);
          let headerName = 'header1';
          headers.addHeader(headerName, {});
          headers.removeHeader(headerName);
          assert.equal(headers.itemKeys.includes(headerName), false);
          assert.equal(headers[headerName] === undefined, true);
        });
      });
    });
  });

  describe('#validation', () => {
    describe('validate headers', () => {
      it('can get warning named HEADERS_REFERENCED_SUGGESTED', () => {
        headers = new Headers(headersData02);
        headers.validate();
        let nestedHeaders = headers.header0.content['application/json'].encoding.encode0.headers;
        assert.equal(nestedHeaders.validation.hasWarning, true);
        assert.equal(
          nestedHeaders.validation.warning.summary,
          OAS_WARNINGS.HEADERS_REFERENCED_SUGGESTED
        );
      });
    });
  });
});
