import { OasNode } from '../../base/oasNode';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';
import { Schemas } from './schemas.model';
import { Responses } from './responses.model';
import { Examples } from './examples.model';
import { Parameters } from './parameters.model';
import { RequestBodies } from './requestBodies.model';
import { Headers } from './headers.model';
import { SecuritySchemes } from './securitySchemes/securitySchemes.model';
import { Links } from './links.model';
import { Callbacks } from './callbacks.model';

export class Components extends OasExtensibleNode(OasNode) {
  static get nodeObjectType() {
    return 'Components';
  }

  constructor(params = {}, parent = null, nodeName = 'components') {
    super(params, parent, nodeName);
    this.schemas = new Schemas(params.schemas, this);
    this.responses = new Responses(params.responses, this);
    this.parameters = new Parameters(params.parameters, this);
    this.examples = new Examples(params.examples, this);
    this.requestBodies = new RequestBodies(params.requestBodies, this);
    this.headers = new Headers(params.headers, this);
    this.securitySchemes = new SecuritySchemes(params.securitySchemes, this);
    this.links = new Links(params.links, this);
    this.callbacks = new Callbacks(params.callbacks, this);
  }
}
