import { OasMapNode } from '../../base/oasMapNode';
import { OasExtensibleNode } from '../../base/oasExtensibleNode';
import { PathFactory } from '../paths/pathFactory';

export class Callback extends OasExtensibleNode(OasMapNode) {
  static get nodeObjectType() {
    return 'Callback';
  }

  constructor(params = {}, parent = null, nodeName = 'callback') {
    super(params, parent, nodeName);
    for (let key of Object.keys(params)) {
      this.addPathItem(key, params[key]);
    }
  }

  addPathItem(key, params) {
    this.addItem(key, PathFactory.createPathItem(params, this, key));
  }

  updatePathItem(key, params) {
    this.updateItem(key, PathFactory.createPathItem(params, this, key));
  }

  removePathItem(key) {
    this.removeItem(key);
  }
}
