import chai from 'chai';
import { Utils } from '../../../utils/utils';
import { Header } from './header.model';
import { OAS_ERRORS, OAS_WARNINGS } from '../../config/oasErrors';

const assert = chai.assert;

describe('Header', () => {
  let headerData01 = {
    description: 'description0',
    required: false,
    deprecated: false,
    allowEmptyValue: false,
    style: 'header',
    explode: false,
    allowReserved: false,
    schema: {
      example: '',
      type: 'string',
      description: '',
      pattern: '',
      default: '',
      enum: [],
      title: 'title001',
      nullable: false,
      readOnly: false,
      writeOnly: false,
      deprecated: false,
    },
    example: 'example27',
    examples: {},
  };

  let headerData02 = {
    description: 'description0',
    required: false,
    deprecated: false,
    allowEmptyValue: false,
    style: 'gdfse',
    explode: false,
    allowReserved: false,
    schema: {
      example: '',
      type: '',
      format: '',
      description: '',
      pattern: '',
      default: '',
      enum: [],
      title: 'title001',
    },
    example: 'example27',
    examples: {},
  };

  let headerData03 = {
    description: 'description0',
    required: false,
    deprecated: false,
    allowEmptyValue: false,
    style: 'dfgdg',
    explode: false,
    allowReserved: false,
    schema: {
      example: '',
      type: '',
      format: '',
      description: '',
      pattern: '',
      default: '',
      enum: [],
      title: 'title001',
    },
    content: {},
    example: 'example27',
    examples: {},
  };

  let headerData07 = {
    description: 'description0',
    required: false,
    deprecated: false,
    allowEmptyValue: false,
    style: 'dfgdg',
    explode: false,
    allowReserved: false,
    schema: {
      example: '',
      type: '',
      format: '',
      description: '',
      pattern: '',
      default: '',
      enum: [],
      title: 'title001',
    },
    content: {},
    example: 'example9',
    examples: {
      ex1: {
        externalValue: 'ssss',
        value: 111,
      },
    },
  };

  let header;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        header = new Header(headerData01);
        assert.equal(Utils.deepEqual(headerData01, header.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate header data', () => {
      it('can get warning named PARAMETER_ALLOWEMPTYVALUE_ONLY_FOR_QUERY', () => {
        header = new Header(headerData03);
        header.validate();
        assert.equal(header.allowEmptyValue.validation.hasWarning, true);
        assert.equal(
          header.allowEmptyValue.validation.warning.summary,
          OAS_WARNINGS.PARAMETER_ALLOWEMPTYVALUE_ONLY_FOR_QUERY
        );
      });
      it('can get error named PARAMETER_STYLE_RESTRICTED', () => {
        header = new Header(headerData02);
        header.validate();
        assert.equal(header.style.validation.hasError, true);
        assert.equal(header.style.validation.error.summary, OAS_ERRORS.PARAMETER_STYLE_RESTRICTED);
      });
      it('can get error named PARAMETER_ALLOWRESERVED_ONLY_FOR_QUERY', () => {
        header = new Header(headerData02);
        header.validate();
        assert.equal(header.allowReserved.validation.hasWarning, true);
        assert.equal(
          header.allowReserved.validation.warning.summary,
          OAS_WARNINGS.PARAMETER_ALLOWRESERVED_ONLY_FOR_QUERY
        );
      });
      it('can get error named PARAMETER_EXCLUSIVE_CONTENT_AND_SCHEMA', () => {
        header = new Header(headerData03);
        header.validate();
        assert.equal(header.validation.hasError, true);
        assert.equal(
          header.validation.error.summary,
          OAS_ERRORS.PARAMETER_EXCLUSIVE_CONTENT_AND_SCHEMA
        );
      });
      it('can get error named EXCLUSIVE_EXAMPLE_AND_EXAMPLES', () => {
        header = new Header(headerData07);
        header.validate();
        assert.equal(header.validation.hasError, true);
        assert.equal(header.validation.error.summary, OAS_ERRORS.EXCLUSIVE_EXAMPLE_AND_EXAMPLES);
      });
    });
  });
});
