import { Utils } from '../../utils/utils';
import { CONSTANTS } from '../config/constants';
import { Validation } from '../validation/validation';

// 所有oas node 的基类
export class OasNode {
  static get nodeObjectType() {
    return 'OasNode';
  }

  constructor(parent = null, nodeName = CONSTANTS.emptyStr) {
    this._parent = parent;
    this._oasNodeId = Utils.newGuid();
    let nodeType = typeof nodeName;
    if (
      nodeType === CONSTANTS.jsonFieldTypes.string ||
      nodeType === CONSTANTS.jsonFieldTypes.number
    ) {
      this._nodeName = nodeName.toString();
    }
    this._path = CONSTANTS.emptyStr;
    this._documentation = null;
    this._validation = new Validation({}, this);
  }

  get oasObjectType() {
    return this.constructor.nodeObjectType;
  }

  get oasNodeId() {
    return this._oasNodeId;
  }

  set oasNodeId(id) {
    this._oasNodeId = id;
  }

  get nodeName() {
    return this._nodeName;
  }

  set nodeName(value) {
    this._nodeName = value;
  }

  get path() {
    if (this.parent == null) {
      return this.nodeName;
    }
    if (this.parent.hasOwnProperty(CONSTANTS.oas.valueProp) && this.parent.value instanceof Array) {
      return (
        this.parent.path +
        CONSTANTS.oas.pathSep +
        CONSTANTS.oas.valueProp +
        CONSTANTS.oas.pathSep +
        this.parent.value.indexOf(this)
      );
    }
    return this.parent.path + CONSTANTS.oas.pathSep + this.nodeName;
  }

  get pathKey() {
    return this.path.replace(CONSTANTS.oas.pathSepRegex, CONSTANTS.oas.pathKeySep);
  }

  get oasNodePath() {
    if (this.parent == null) {
      return this.nodeName;
    }
    if (this.parent.hasOwnProperty(CONSTANTS.oas.valueProp) && this.parent.value instanceof Array) {
      return this.parent.oasNodePath + CONSTANTS.oas.pathSep + this.parent.value.indexOf(this);
    }
    return this.parent.oasNodePath + CONSTANTS.oas.pathSep + this.nodeName;
  }

  set path(value) {
    this._path = value;
  }

  get parent() {
    return this._parent;
  }

  set parent(value) {
    this._parent = value;
  }

  get documentation() {
    return this._documentation;
  }

  set documentation(value) {
    this._documentation = value;
  }

  get validation() {
    return this._validation;
  }

  set validation(value) {
    if (value instanceof Validation) {
      this._validation = value;
    }
  }

  getNodeByAbsPath(nodeAbsPath) {
    try {
      if (this.nodeName.length > 0) {
        let rootNodeName = this.nodeName;
        let data = {};
        data[rootNodeName] = this;
        return Utils.getPropByPath(data, nodeAbsPath, CONSTANTS.oas.pathSep);
      }
    } catch (e) {
      console.log(`[Exception][getNodeByPath]${e}`);
      return null;
    }
    return null;
  }

  getNodeByRelPath(nodeRelPath) {
    let absPath = `${CONSTANTS.oas.rootNodeName}${CONSTANTS.oas.pathSep}${nodeRelPath}`;
    return this.getNodeByAbsPath(absPath);
  }

  setNodeByAbsPath(nodeAbsPath, value) {
    try {
      let propRelPath = nodeAbsPath.substring(
        nodeAbsPath.indexOf(CONSTANTS.oas.pathSep) + CONSTANTS.oas.pathSepLength
      );
      let originalNode = this.getNodeByAbsPath(nodeAbsPath);
      if (originalNode instanceof OasNode) {
        value.parent = originalNode.parent;
      } else {
        value.parent = this.getNodeByAbsPath(
          nodeAbsPath.substring(0, nodeAbsPath.lastIndexOf(CONSTANTS.oas.pathSep))
        );
      }
      Utils.setPropByPath(this, value, propRelPath, CONSTANTS.oas.pathSep);
      return this;
    } catch (e) {
      console.log(`[Exception][setPropByPath]${e}`);
      return this;
    }
  }

  extract(withNullNodes = false) {
    try {
      if (withNullNodes === true) {
        // 该分支仅用于tools/generateOasObjectUnitTests.js工具生成oas对象的单元测试;
        // 准确性没有持续维护, 不应用于系统业务中;
        return extractOasNodeWithNullNodes(this);
      }
      return extractOasNode(this);
    } catch (e) {
      console.log(`[Exception][extract]${e}`);
      return null;
    }
  }

  extractSegmentation(fieldNames = [], reversed = false) {
    const deleteFiled = (obj, field) => {
      try {
        if (obj instanceof Array) {
          obj = obj.splice(field, 1);
        } else {
          delete obj[field];
        }
      } catch (e) {
        console.log(`[Exception][extractSegmentation][deleteFiled]${e}`);
      }
      return obj;
    };
    let data = this.extract();
    let target;
    if (!(data instanceof Object)) {
      return data;
    }
    if (data instanceof Array) {
      target = reversed ? data : [];
    } else {
      target = reversed ? data : {};
    }
    if (fieldNames instanceof Array) {
      for (let f of fieldNames) {
        if (data.hasOwnProperty(f)) {
          reversed ? (target = deleteFiled(target, f)) : (target[f] = data[f]);
        }
      }
    }
    return target;
  }

  validate() {
    let redirectNodeValue = node => {
      if (
        node.hasOwnProperty(CONSTANTS.oas.valueProp) &&
        node[CONSTANTS.oas.valueProp] instanceof Array
      ) {
        return node[CONSTANTS.oas.valueProp];
      } else {
        return node;
      }
    };
    let chainedValidate = oasNode => {
      let nodeValue = redirectNodeValue(oasNode);
      let execBasicValidation = node => {
        if (node instanceof OasNode && typeof node.validate === 'function') {
          node.validate();
        }
      };
      let execNodeValidation = node => {
        if (node instanceof OasNode && typeof node.validateNode === 'function') {
          node.validateNode();
        }
      };
      let execValidation = node => {
        execBasicValidation(node);
        execNodeValidation(node);
      };

      for (let k of Object.keys(nodeValue)) {
        if (!k.startsWith(CONSTANTS.oas.reservedPropPrefix) && nodeValue[k] instanceof OasNode) {
          execNodeValidation(nodeValue[k]);
          let subNodeValue = redirectNodeValue(nodeValue[k]);
          if (subNodeValue instanceof Array) {
            subNodeValue.forEach(node => {
              execValidation(node);
            });
          }
          execBasicValidation(subNodeValue);
        }
      }
      execNodeValidation(oasNode);
    };
    return chainedValidate(this);
  }
}

const isUnavailableField = field => {
  try {
    if (!field.hasOwnProperty(CONSTANTS.oas.valueProp)) {
      return false;
    }
    return field.value === CONSTANTS.unavailable || field.value == undefined;
  } catch (ex) {
    return false;
  }
};

// 提取oas node对象中有晓得oas 数据, 器结构和oas3 的结构一致, 且不包括null值的节点
const extractOasNode = obj =>
  Object.keys(obj)
    .filter(
      k =>
        !CONSTANTS.oas.uiReservedProps.includes(k) && obj[k] !== null && !isUnavailableField(obj[k])
    )
    .reduce((newObj, k) => {
      let node = obj[k];
      let oasNodeName = k;
      // 处理等价于value的oas对象属性;
      if (CONSTANTS.oas.equivalentValueProps.includes(oasNodeName)) {
        oasNodeName = CONSTANTS.oas.valueProp;
      }
      if (
        obj.constructor.nodeObjectType === CONSTANTS.oas.anyFieldType &&
        obj._type === CONSTANTS.jsonFieldTypes.object
      ) {
        // 添加parse try catch;
        return obj.value;
      }
      if (Utils.isPrimitive(node)) {
        if (
          obj.constructor.nodeObjectType === CONSTANTS.oas.booleanFieldType ||
          (obj.constructor.nodeObjectType === CONSTANTS.oas.anyFieldType &&
            obj._type === CONSTANTS.jsonFieldTypes.boolean)
        ) {
          if (obj.value === CONSTANTS.unavailable) {
            return undefined;
          }
          return obj.value === CONSTANTS.trueAsString;
        }
        return node;
      }
      if (node instanceof Array) {
        let temp = extractOasNode(node);
        temp.length = node.length;
        return Array.prototype.slice.call(temp);
      }
      let nodeValue;
      // 将'true'转为boolean值true
      if (node.constructor.nodeObjectType === CONSTANTS.oas.anyFieldType) {
        if (node._type === CONSTANTS.jsonFieldTypes.boolean) {
          nodeValue = node.value === CONSTANTS.trueAsString;
        } else if (node._type === CONSTANTS.jsonFieldTypes.object) {
          nodeValue = node.value;
        } else {
          nodeValue = node.hasOwnProperty(CONSTANTS.oas.valueProp) ? node.value : node;
        }
        return Object.assign(newObj, {
          [oasNodeName]: nodeValue,
        });
      } else if (node.constructor.nodeObjectType === CONSTANTS.oas.booleanFieldType) {
        nodeValue = node.value === CONSTANTS.trueAsString;
      } else {
        nodeValue = node.hasOwnProperty(CONSTANTS.oas.valueProp) ? node.value : node;
      }
      if (nodeValue instanceof Array) {
        let temp = extractOasNode(nodeValue);
        temp.length = nodeValue.length;
        return Object.assign(newObj, {
          [oasNodeName]: Array.prototype.slice.call(temp),
        });
      }
      if (nodeValue instanceof OasNode) {
        return Object.assign(newObj, {
          [oasNodeName]: extractOasNode(nodeValue),
        });
      }
      return Object.assign(newObj, {
        [oasNodeName]: nodeValue,
      });
    }, {});

// 提取oas node对象中有晓得oas 数据, 器结构和oas3 的结构一致, 且包括null值的节点
// 稳定性和正确性没有持续维护, 仅应用于测试用例生成;
const extractOasNodeWithNullNodes = obj =>
  Object.keys(obj)
    .filter(k => !k.startsWith(CONSTANTS.oas.reservedPropPrefix))
    .reduce((newObj, k) => {
      if (obj[k] == null) {
        return Object.assign(newObj, {
          [k]: null,
        });
      }
      if (Utils.isPrimitive(obj[k])) {
        return obj[k];
      }
      if (obj[k] instanceof Array) {
        let temp = extractOasNode(obj[k]);
        temp.length = obj[k].length;
        return Array.prototype.slice.call(temp);
      }
      let node = obj[k];
      let nodeValue = node.hasOwnProperty(CONSTANTS.oas.valueProp) ? node.value : node;
      if (nodeValue instanceof Array) {
        let temp = extractOasNode(nodeValue);
        temp.length = nodeValue.length;
        return Object.assign(newObj, {
          [k]: Array.prototype.slice.call(temp),
        });
      }
      if (nodeValue instanceof OasNode) {
        return Object.assign(newObj, {
          [k]: extractOasNode(nodeValue),
        });
      }
      return Object.assign(newObj, {
        [k]: nodeValue,
      });
    }, {});
