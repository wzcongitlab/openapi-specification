import { CONSTANTS } from '../config/constants';
import { Utils } from '../../utils/utils';
import { OasNode } from './oasNode';
// map类型如oas node, variables, responses, properties等oas节点类的基类
export class OasMapNode extends OasNode {
  static get nodeObjectType() {
    return 'OasMapNode';
  }

  constructor(parent = null, nodeName = CONSTANTS.emptyStr) {
    super(parent, nodeName);
    this._itemKeys = [];
  }

  get items() {
    return this.itemKeys.map(k => this[k]);
  }

  get itemKeys() {
    return this._itemKeys.sort();
  }

  get size() {
    return this._itemKeys.length;
  }

  get isEmpty() {
    return this.size === 0;
  }

  getItem(key) {
    if (this.hasOwnProperty(key)) {
      return this[key];
    }
  }

  updateItem(key, item) {
    if (!Utils.isNotEmptyString(key)) {
      return this;
    }
    if (this.hasOwnProperty(key) && item instanceof OasNode) {
      this[key] = item;
      return this;
    }
  }

  addItem(key, item) {
    if (!Utils.isNotEmptyString(key)) {
      return;
    }
    if (!this.hasOwnProperty(key) && item instanceof OasNode) {
      // item参数应该是基类为OasNode的对象(例如variable, response等)
      this[key] = new item.constructor(item.extract(), this, key);
      this.addItemKey(key);
    }
    return this;
  }

  removeItem(key) {
    if (!Utils.isNotEmptyString(key)) {
      return this;
    }
    if (this.hasOwnProperty(key)) {
      delete this[key];
      this.removeItemKey(key);
      return this;
    }
  }

  addItemKey(key) {
    this.itemKeys.push(key);
  }

  removeItemKey(key) {
    let keyIndex = this.itemKeys.indexOf(key);
    if (keyIndex > -1) {
      this.itemKeys.splice(keyIndex, 1);
    }
  }

  changeItemKey(oldKey, newKey) {
    if (!Utils.isNotEmptyString(oldKey)) {
      return;
    }
    if (this.hasOwnProperty(oldKey) && this[oldKey] instanceof OasNode) {
      let MapItemConstructor = this[oldKey].constructor;
      let oldKeyPosition = this.itemKeys.indexOf(oldKey);
      let mapItemData = this[oldKey].extract();
      this[newKey] = new MapItemConstructor(mapItemData, this, newKey);
      this.itemKeys[oldKeyPosition] = newKey;
      delete this[oldKey];
      let mapNodeData = this.extract();
      let ItemConstructorMap = {};
      for (let key of this.itemKeys) {
        ItemConstructorMap[key] = this[key].constructor;
        delete this[key];
      }
      for (let key of this.itemKeys) {
        this[key] = new ItemConstructorMap[key](mapNodeData[key], this, key);
      }
    }
  }
}
