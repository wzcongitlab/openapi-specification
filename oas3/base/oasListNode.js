import { CONSTANTS } from '../config/constants';
// list类型的oas node, servers, externalDocs, tags, parameters等节点类的基类
import { OasNode } from './oasNode';

export class OasListNode extends OasNode {
  static get nodeObjectType() {
    return 'OasListNode';
  }

  constructor(parent = null, nodeName = CONSTANTS.emptyStr) {
    super(parent, nodeName);
    this.value = [];
  }

  get items() {
    return this.value;
  }

  get length() {
    return this.value.length;
  }

  get isEmpty() {
    return this.length === 0;
  }

  addItem(item) {
    this.value.push(item);
    return this.value;
  }

  removeItem(index) {
    this.value.splice(index, 1);
    return this.value;
  }

  clear() {
    this.value = [];
  }
}
