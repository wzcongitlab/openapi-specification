import { CONSTANTS } from '../config/constants';
import { Utils } from '../../utils/utils';
import { AnyField } from '../fields/any.field';
import { OasNode } from './oasNode';
import { OasMapNode } from './oasMapNode';

// 传入可扩展对象的超类;
export const OasExtensibleNode = SupperClass => {
  if (!(typeof SupperClass === 'function')) {
    throw new Error('OasExtensibleNode方法的参数必须是function类型.');
  }

  if (!(SupperClass === OasNode || SupperClass === OasMapNode)) {
    throw new Error('OasExtensibleNode方法的参数必须是OasNode或OasMapNode.');
  }

  return class extends SupperClass {
    static get nodeObjectType() {
      return 'OasExtensibleNode';
    }

    constructor(params, parent = null, nodeName = CONSTANTS.emptyStr) {
      super(parent, nodeName);
      this._extensiveProps = [];
      for (let prop of Object.keys(params)) {
        if (CONSTANTS.oas.extensivePropRegex.test(prop)) {
          this.addExtension(prop, params[prop]);
        }
      }
    }

    addExtensiveProp(key) {
      this._extensiveProps.push(key);
    }

    get extensiveProps() {
      return this._extensiveProps;
    }

    addExtension(key, value) {
      if (!Utils.isNotEmptyString(key) || this.hasOwnProperty(key)) {
        return this;
      }
      if (!CONSTANTS.oas.extensivePropRegex.test(key)) {
        return this;
      }
      if (value instanceof AnyField) {
        this[key] = new AnyField(value.extract(), this, key);
        this.addExtensiveProp(key);
        return this;
      }
      if (typeof value !== 'undefined') {
        this[key] = new AnyField(value, this, key);
        this.addExtensiveProp(key);
        return this;
      }
      return this;
    }

    removeExtension(key) {
      if (!Utils.isNotEmptyString(key)) {
        return this;
      }
      if (!CONSTANTS.oas.extensivePropRegex.test(key)) {
        return this;
      }
      if (this.hasOwnProperty(key)) {
        delete this[key];
        let keyIndex = this._extensiveProps.indexOf(key);
        if (keyIndex > -1) {
          this._extensiveProps.splice(keyIndex, 1);
        }
        return this;
      }
    }

    clearExtensiveProps() {
      for (let extensionProp of this.extensiveProps) {
        this.removeExtension(extensionProp);
      }
    }
  };
};
