import chai from 'chai';
import { Utils } from '../../utils/utils';
import { ExampleValueField } from './exampleValue.field';
import { OAS_ERRORS } from '../config/oasErrors';

const assert = chai.assert;

describe('ExampleValueField', () => {
  let exampleValueFieldData01 = 'userName1';

  let exampleValueFieldData02 = '';

  let exampleValueField;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        exampleValueField = new ExampleValueField(exampleValueFieldData01);
        assert.equal(Utils.deepEqual(exampleValueFieldData01, exampleValueField.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate exampleValueField data', () => {
      it('can pass validation when given valid data', () => {
        exampleValueField = new ExampleValueField(exampleValueFieldData01);
        exampleValueField.validate();
        assert.equal(exampleValueField.validation.hasError, false);
        assert.equal(exampleValueField.validation.hasWarning, false);
      });
    });
  });
});
