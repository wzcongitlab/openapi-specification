import { CONSTANTS } from '../config/constants';
import { OasField } from './field';
import { Utils } from '../../utils/utils';

export class StringField extends OasField {
  static get nodeObjectType() {
    return 'StringField';
  }

  constructor(value = CONSTANTS.emptyStr, parent = null, fieldName = CONSTANTS.emptyStr) {
    super(parent, fieldName);
    this.value = value.toString();
  }

  equal(otherValue) {
    return this.value === otherValue;
  }

  get isEmpty() {
    return !Utils.isNotEmptyString(this.value);
  }

  get length() {
    return this.value.length;
  }

  validate() {}
}
