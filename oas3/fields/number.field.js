import { CONSTANTS } from '../config/constants';
import { OAS_ERRORS } from '../config/oasErrors';
import { NumerableField } from './numerableField';

export class NumberField extends NumerableField {
  static get nodeObjectType() {
    return 'NumberField';
  }

  constructor(value, parent = null, fieldName = CONSTANTS.emptyStr) {
    super(value, parent, fieldName);
  }

  validate() {
    if (this.value === undefined) {
      return;
    }
    if (!(typeof this.value === 'number')) {
      this.validation.update({
        hasError: true,
        summary: OAS_ERRORS.INVALID_NUMBER,
      });
    }
  }
}
