import { CONSTANTS } from '../config/constants';
import { StringField } from './string.field';

export class RuntimeExpressionField extends StringField {
  // [TODO] 输入框可以设计成单行输入框,右侧可辅助按钮,帮助弹出编辑并插入expression
  // 可详细设计expression的自动填充或者选择功能
  // 也可点击切换成多行文本, 帮助输入json, 硬编码的值
  // 参考 https://swagger.io/docs/specification/links/
  static get nodeObjectType() {
    return 'RuntimeExpressionField';
  }

  constructor(value = CONSTANTS.emptyStr, parent = null, fieldName = 'expression') {
    super(value, parent, fieldName);
  }
}
