import chai from 'chai';
import { Utils } from '../../utils/utils';
import { NumberField } from './number.field';
import { OAS_ERRORS } from '../config/oasErrors';

const assert = chai.assert;

describe('NumberField', () => {
  let numberFieldData01 = 1.011;

  let numberFieldData02 = 'dsfgdsgf';

  let numberField;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        numberField = new NumberField(numberFieldData01);
        assert.equal(Utils.deepEqual(numberFieldData01, numberField.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate numberField data', () => {
      it('can get error named INVALID_NUMBER', () => {
        numberField = new NumberField(numberFieldData02);
        numberField.validate();
        assert.equal(numberField.validation.hasError, true);
        assert.equal(numberField.validation.error.summary, OAS_ERRORS.INVALID_NUMBER);
      });

      it('can pass validation when given valid data', () => {
        numberField = new NumberField(numberFieldData01);
        numberField.validate();
        assert.equal(numberField.validation.hasError, false);
        assert.equal(numberField.validation.hasWarning, false);
      });
    });
  });
});
