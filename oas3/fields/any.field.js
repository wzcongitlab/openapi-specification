import { Utils } from '../../utils/utils';
import { CONSTANTS } from '../config/constants';
import { OasField } from './field';

export class AnyField extends OasField {
  static get nodeObjectType() {
    return 'AnyField';
  }

  constructor(value = CONSTANTS.emptyStr, parent = null, fieldName = '', type) {
    super(parent, fieldName);

    this._type = undefined;
    this.value = undefined;
    this.setValue(value, type);
  }

  get type() {
    return this._type;
  }

  set type(valueType) {
    this._type = valueType;
  }

  setValue(value, type) {
    if (value === undefined) {
      return this;
    }
    let valueType = typeof value;
    this._type = type || valueType;
    /* eslint-disable */
    switch (this._type) {
      case CONSTANTS.jsonFieldTypes.string:
        this.value = value.toString();
        break;
      case CONSTANTS.jsonFieldTypes.number:
        if (valueType === CONSTANTS.jsonFieldTypes.number) {
          this.value = value;
        } else {
          this.value = 0;
        }
        break;
      case CONSTANTS.jsonFieldTypes.boolean:
        if (value === CONSTANTS.trueAsString || value === CONSTANTS.falseAsString) {
          this.value = value;
        } else if (valueType === CONSTANTS.jsonFieldTypes.boolean) {
          this.value = value.toString();
        } else {
          this.value = CONSTANTS.falseAsString;
        }
        break;
      case CONSTANTS.jsonFieldTypes.object:
        if (valueType === CONSTANTS.jsonFieldTypes.object) {
          this.value = JSON.parse(JSON.stringify(value));
        } else {
          this.value = {};
        }
        break;
      default:
        break;
    }
    /* eslint-disable */
    return this;
  }

  equal(otherValue) {
    /* eslint-disable */
    switch (this._type) {
      case CONSTANTS.jsonFieldTypes.string:
        return this.value === otherValue;
      case CONSTANTS.jsonFieldTypes.number:
        return this.value === otherValue;
      case CONSTANTS.jsonFieldTypes.boolean:
        return this.value === otherValue.toString();
      case CONSTANTS.jsonFieldTypes.object:
        return Utils.deepEqual(this.value, otherValue);
      default:
        return false;
    }
    /* eslint-disable */
  }

  get isEmpty() {
    /* eslint-disable */
    switch (this._type) {
      case CONSTANTS.jsonFieldTypes.string:
        return !Utils.isNotEmptyString(this.value);
      default:
        return false;
    }
    /* eslint-disable */
  }

  get hasValue() {
    return this.value !== undefined;
  }

  lessThan(otherValue) {
    if (this._type === CONSTANTS.jsonFieldTypes.number) {
      return this.value < otherValue;
    }
    return false;
  }

  greaterThan(otherValue) {
    if (this._type === CONSTANTS.jsonFieldTypes.number) {
      return this.value > otherValue;
    }
    return false;
  }

  lessThanOrEqual(otherValue) {
    if (this._type === CONSTANTS.jsonFieldTypes.number) {
      return this.value <= otherValue;
    }
    return false;
  }

  greaterThanOrEqual(otherValue) {
    if (this._type === CONSTANTS.jsonFieldTypes.number) {
      return this.value >= otherValue;
    }
    return false;
  }
}
