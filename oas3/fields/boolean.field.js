import { CONSTANTS } from '../config/constants';
import { OasField } from './field';

export class BooleanField extends OasField {
  static get nodeObjectType() {
    return 'BooleanField';
  }

  constructor(value = CONSTANTS.falseAsString, parent = null, fieldName = CONSTANTS.emptyStr) {
    super(parent, fieldName);
    if (value === CONSTANTS.trueAsString || value === CONSTANTS.falseAsString) {
      this.value = value;
    } else if (typeof value === 'boolean') {
      this.value = value.toString();
    } else {
      this.value = CONSTANTS.falseAsString;
    }
  }

  equal(otherValue) {
    return this.value === otherValue.toString();
  }

  static isAvailableBooleanValue(value) {
    return (
      typeof value === 'boolean' ||
      value === CONSTANTS.trueAsString ||
      value === CONSTANTS.falseAsString
    );
  }
}
