import { CONSTANTS } from '../config/constants';
import { OasNode } from '../base/oasNode';

export class OasField extends OasNode {
  static get nodeObjectType() {
    throw new Error(
      'nodeObjectType function should be defined as static method with all Oas Field classes.'
    );
  }

  constructor(parent = null, fieldName = CONSTANTS.emptyStr) {
    super(parent, fieldName);
  }
}
