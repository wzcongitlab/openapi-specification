import { CONSTANTS } from '../config/constants';
import { StringField } from './string.field';

export class RichTextField extends StringField {
  static get nodeObjectType() {
    return 'RichTextField';
  }

  constructor(value = CONSTANTS.emptyStr, parent = null, fieldName = CONSTANTS.emptyStr) {
    super(value, parent, fieldName);
  }
}
