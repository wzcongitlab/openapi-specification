import chai from 'chai';
import { Utils } from '../../utils/utils';
import { UrlField } from './url.field';
import { OAS_ERRORS } from '../config/oasErrors';

const assert = chai.assert;

describe('UrlField', () => {
  let urlFieldName = 'url';
  let urlData01 = 'http://www.kato.com';
  let urlData02 = 'ddedfchnbwesjd';
  let urlField;

  describe('#fields', () => {
    describe('data extraction', () => {
      before(() => {
        urlField = new UrlField(urlData01, null, urlFieldName);
      });
      it('can extract correct data.', () => {
        assert.equal(Utils.deepEqual(urlField.extract(), urlData01), true);
      });
    });

    describe('validation', () => {
      it('can get error named BAD_URL', () => {
        urlField = new UrlField(urlData02, null, urlFieldName);
        urlField.validate();
        assert.equal(urlField.validation.hasError, true);
        assert.equal(urlField.validation.error.summary, OAS_ERRORS.BAD_URL);
      });
      it('can pass validation when given valid url', () => {
        urlField = new UrlField(urlData01, null, urlFieldName);
        urlField.validate();
        assert.equal(urlField.validation.hasError, false);
        assert.equal(urlField.validation.error, null);
      });
    });
  });
});
