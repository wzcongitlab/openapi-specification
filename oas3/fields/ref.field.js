import { CONSTANTS } from '../config/constants';
import { StringField } from './string.field';

export class RefField extends StringField {
  static get nodeObjectType() {
    return 'RefField';
  }

  constructor(value = CONSTANTS.notValid, parent = null, fieldName = '$ref') {
    super(value, parent, fieldName);
  }
}
