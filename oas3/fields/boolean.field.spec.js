import chai from 'chai';
import { Utils } from '../../utils/utils';
import { BooleanField } from './boolean.field';
import { OAS_ERRORS } from '../config/oasErrors';

const assert = chai.assert;

describe('BooleanField', () => {
  let booleanFieldData01 = true;

  let booleanFieldData02 = false;

  let booleanField;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data(true >> true)', () => {
        booleanField = new BooleanField(true);
        assert.equal(Utils.deepEqual(true, booleanField.extract()), true);
      });
      it('can extract correct data(false >> false)', () => {
        booleanField = new BooleanField(false);
        assert.equal(Utils.deepEqual(false, booleanField.extract()), true);
      });
      it('can extract correct data("true" >> true)', () => {
        booleanField = new BooleanField('true');
        assert.equal(Utils.deepEqual(true, booleanField.extract()), true);
      });
      it('can extract correct data("false" >> false)', () => {
        booleanField = new BooleanField('false');
        assert.equal(Utils.deepEqual(false, booleanField.extract()), true);
      });
      it('can extract correct data("any"(any) - false)', () => {
        booleanField = new BooleanField('any');
        assert.equal(Utils.deepEqual(false, booleanField.extract()), true);
      });
      it('can extract correct data("N/A"(other) - false)', () => {
        booleanField = new BooleanField('N/A');
        assert.equal(Utils.deepEqual(false, booleanField.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate booleanField data', () => {});
  });
});
