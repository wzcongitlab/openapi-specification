import { CONSTANTS } from '../config/constants';
import { OAS_ERRORS } from '../config/oasErrors';
import { Utils } from '../../utils/utils';
import { StringField } from './string.field';

export class EmailField extends StringField {
  static get nodeObjectType() {
    return 'EmailField';
  }

  constructor(value = CONSTANTS.emptyStr, parent = null, fieldName = 'email') {
    super(value, parent, fieldName);
  }

  validate() {
    if (this.isEmpty) {
      return;
    }
    if (!Utils.isValidEMail(this.value)) {
      this.validation.update({
        hasError: true,
        summary: OAS_ERRORS.BAD_EMAIL,
      });
    }
  }
}
