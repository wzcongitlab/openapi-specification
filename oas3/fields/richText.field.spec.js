import chai from 'chai';
import { Utils } from '../../utils/utils';
import { RichTextField } from './richText.field';
import { OAS_ERRORS } from '../config/oasErrors';

const assert = chai.assert;

describe('RichTextField', () => {
  let richTextFieldData01 = '';

  let richTextFieldData02 = '';

  let richTextField;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        richTextField = new RichTextField(richTextFieldData01);
        assert.equal(Utils.deepEqual(richTextFieldData01, richTextField.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate richTextField data', () => {
      it('can pass validation when given valid data', () => {
        richTextField = new RichTextField(richTextFieldData01);
        richTextField.validate();
        assert.equal(richTextField.validation.hasError, false);
        assert.equal(richTextField.validation.hasWarning, false);
      });
    });
  });
});
