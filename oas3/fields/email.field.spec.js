import chai from 'chai';
import { Utils } from '../../utils/utils';
import { EmailField } from './email.field';
import { OAS_ERRORS } from '../config/oasErrors';

const assert = chai.assert;

describe('EmailField', () => {
  let emailFieldName = 'email';
  let emailData01 = 'abc@fiberhome.com';
  let emailData02 = 'ddedfchnbwesjd';
  let emailField;

  describe('#fields', () => {
    describe('data extraction', () => {
      before(() => {
        emailField = new EmailField(emailData01, null, emailFieldName);
      });
      it('can extract correct data.', () => {
        assert.equal(Utils.deepEqual(emailField.extract(), emailData01), true);
      });
    });

    describe('validation', () => {
      it('can get error named BAD_EMAIL', () => {
        emailField = new EmailField(emailData02, null, emailFieldName);
        emailField.validate();
        assert.equal(emailField.validation.hasError, true);
        assert.equal(emailField.validation.error.summary, OAS_ERRORS.BAD_EMAIL);
      });
      it('can pass validation when given valid email', () => {
        emailField = new EmailField(emailData01, null, emailFieldName);
        emailField.validate();
        assert.equal(emailField.validation.hasError, false);
        assert.equal(emailField.validation.hasWarning, false);
      });
    });
  });
});
