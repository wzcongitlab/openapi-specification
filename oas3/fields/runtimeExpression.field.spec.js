import chai from 'chai';
import { Utils } from '../../utils/utils';
import { RuntimeExpressionField } from './runtimeExpression.field';
import { OAS_ERRORS } from '../config/oasErrors';

const assert = chai.assert;

describe('RuntimeExpressionField', () => {
  let runtimeExpressionFieldData01 = '';

  let runtimeExpressionFieldData02 = '';

  let runtimeExpressionField;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        runtimeExpressionField = new RuntimeExpressionField(runtimeExpressionFieldData01);
        assert.equal(
          Utils.deepEqual(runtimeExpressionFieldData01, runtimeExpressionField.extract()),
          true
        );
      });
    });
  });

  describe('#validation', () => {
    describe('validate runtimeExpressionField data', () => {
      it('can pass validation when given valid data', () => {
        runtimeExpressionField = new RuntimeExpressionField(runtimeExpressionFieldData01);
        runtimeExpressionField.validate();
        assert.equal(runtimeExpressionField.validation.hasError, false);
        assert.equal(runtimeExpressionField.validation.hasWarning, false);
      });
    });
  });
});
