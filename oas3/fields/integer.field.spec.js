import chai from 'chai';
import { Utils } from '../../utils/utils';
import { IntegerField } from './integer.field';
import { OAS_ERRORS } from '../config/oasErrors';

const assert = chai.assert;

describe('IntegerField', () => {
  let integerFieldData01 = 5;

  let integerFieldData02 = 'dfredgt';

  let integerFieldData03 = 3.1415;

  let integerField;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        integerField = new IntegerField(integerFieldData01);
        assert.equal(Utils.deepEqual(integerFieldData01, integerField.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate integerField data', () => {
      it('can get error named INVALID_INTEGER(string)', () => {
        integerField = new IntegerField(integerFieldData02);
        integerField.validate();
        assert.equal(integerField.validation.hasError, true);
        assert.equal(integerField.validation.error.summary, OAS_ERRORS.INVALID_INTEGER);
      });
      it('can get error named INVALID_INTEGER(float)', () => {
        integerField = new IntegerField(integerFieldData03);
        integerField.validate();
        assert.equal(integerField.validation.hasError, true);
        assert.equal(integerField.validation.error.summary, OAS_ERRORS.INVALID_INTEGER);
      });

      it('can pass validation when given valid data', () => {
        integerField = new IntegerField(integerFieldData01);
        integerField.validate();
        assert.equal(integerField.validation.hasError, false);
        assert.equal(integerField.validation.error, null);
      });
    });
  });
});
