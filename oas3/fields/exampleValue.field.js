import { CONSTANTS } from '../config/constants';
import { StringField } from './string.field';

export class ExampleValueField extends StringField {
  static get nodeObjectType() {
    return 'ExampleValueField';
  }

  constructor(value = CONSTANTS.emptyStr, parent = null, fieldName = 'example') {
    super(value, parent, fieldName);
  }
}
