import chai from 'chai';
import { Utils } from '../../utils/utils';
import { StringField } from './string.field';
import { OAS_ERRORS } from '../config/oasErrors';

const assert = chai.assert;

describe('StringField', () => {
  let stringFieldData01 = '';

  let stringFieldData02 = '';

  let stringField;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        stringField = new StringField(stringFieldData01);
        assert.equal(Utils.deepEqual(stringFieldData01, stringField.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate stringField data', () => {
      it('can pass validation when given valid data', () => {
        stringField = new StringField(stringFieldData01);
        stringField.validate();
        assert.equal(stringField.validation.hasError, false);
        assert.equal(stringField.validation.hasWarning, false);
      });
    });
  });
});
