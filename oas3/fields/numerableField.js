import { CONSTANTS } from '../config/constants';
import { OasField } from './field';

export class NumerableField extends OasField {
  static get nodeObjectType() {
    return 'NumerableField';
  }

  constructor(value, parent = null, fieldName = CONSTANTS.emptyStr) {
    super(parent, fieldName);
    this.value = value;
  }

  get hasValue() {
    return this.value !== undefined;
  }

  equal(otherValue) {
    return this.value === otherValue;
  }

  lessThan(otherValue) {
    return this.value < otherValue;
  }

  greaterThan(otherValue) {
    return this.value > otherValue;
  }

  lessThanOrEqual(otherValue) {
    return this.value <= otherValue;
  }

  greaterThanOrEqual(otherValue) {
    return this.value >= otherValue;
  }
}
