import chai from 'chai';
import { Utils } from '../../utils/utils';
import { AnyField } from './any.field';
import { OAS_ERRORS } from '../config/oasErrors';

const assert = chai.assert;

describe('anyField', () => {
  let anyFieldData01 = 'userName1';

  let anyFieldData02 = 123;

  let anyFieldData03 = true;

  let anyFieldData04 = {
    a: 1,
    b: 'sdsdd',
    c: false,
  };

  let anyFieldData05 = [
    1,
    2,
    3,
    {
      a: 11,
      b: '344ds',
    },
  ];

  let anyFieldData06 = 'dsfds';

  let anyFieldData07;

  let anyField;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data with string', () => {
        anyField = new AnyField(anyFieldData01, null, '', 'string');
        assert.equal(Utils.deepEqual(anyFieldData01, anyField.extract()), true);
      });

      it('can extract correct data with number', () => {
        anyField = new AnyField(anyFieldData02, null, '', 'number');
        assert.equal(Utils.deepEqual(anyFieldData02, anyField.extract()), true);
      });

      it('can extract correct data with boolean', () => {
        anyField = new AnyField(anyFieldData03, null, '', 'boolean');
        assert.equal(Utils.deepEqual(anyFieldData03, anyField.extract()), true);
      });

      it('can extract correct data with object', () => {
        anyField = new AnyField(anyFieldData04, null, '', 'object');
        assert.equal(Utils.deepEqual(anyFieldData04, anyField.extract()), true);
      });

      it('can extract correct data with array object', () => {
        anyField = new AnyField(anyFieldData05, null, '');
        assert.equal(Utils.deepEqual(anyFieldData05, anyField.extract()), true);
      });

      it('can change value with object and extract correct data', () => {
        anyField = new AnyField(anyFieldData04, null, '', 'object');
        let newValue = {
          a: 3,
        };
        anyField.setValue(newValue);
        assert.equal(Utils.deepEqual(newValue, anyField.extract()), true);
      });

      it('can change value with string and extract correct data', () => {
        anyField = new AnyField(anyFieldData04, null, '', 'object');
        let newValue = 'ssss';
        anyField.setValue(newValue);
        assert.equal(Utils.deepEqual(newValue, anyField.extract()), true);
      });

      it('can apply type param, prior to automatic data type(string vs object).', () => {
        anyField = new AnyField(anyFieldData06, null, '', 'object');
        console.log('apply type param, prior to automatic data type.');
        assert.equal(Utils.deepEqual({}, anyField.extract()), true);
      });

      it('can apply type param, prior to automatic data type(number vs string).', () => {
        anyField = new AnyField(2, null, '', 'string');
        console.log('apply type param, prior to automatic data type.');
        assert.equal(Utils.deepEqual('2', anyField.extract()), true);
      });

      it('can apply type param, prior to automatic data type(object vs boolean).', () => {
        anyField = new AnyField({ a: 1 }, null, '', 'boolean');
        console.log('apply type param, prior to automatic data type.');
        assert.equal(Utils.deepEqual(false, anyField.extract()), true);
      });
    });
  });
});
