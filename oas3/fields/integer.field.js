import { CONSTANTS } from '../config/constants';
import { Utils } from '../../utils/utils';
import { OAS_ERRORS } from '../config/oasErrors';
import { NumerableField } from './numerableField';

export class IntegerField extends NumerableField {
  static get nodeObjectType() {
    return 'IntegerField';
  }

  constructor(value, parent = null, fieldName = CONSTANTS.emptyStr) {
    super(value, parent, fieldName);
  }

  validate() {
    if (this.hasValue && !Utils.isInteger(this.value)) {
      this.validation.update({
        hasError: true,
        summary: OAS_ERRORS.INVALID_INTEGER,
      });
    }
  }
}
