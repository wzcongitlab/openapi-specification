import chai from 'chai';
import { Utils } from '../../utils/utils';
import { RefField } from './ref.field';
import { OAS_ERRORS } from '../config/oasErrors';

const assert = chai.assert;

describe('RefField', () => {
  let refFieldData01 = '';

  let refFieldData02 = '';

  let refField;

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        refField = new RefField(refFieldData01);
        assert.equal(Utils.deepEqual(refFieldData01, refField.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate refField data', () => {
      it('can pass validation when given valid data', () => {
        refField = new RefField(refFieldData01);
        refField.validate();
        assert.equal(refField.validation.hasError, false);
        assert.equal(refField.validation.hasWarning, false);
      });
    });
  });
});
