import { CONSTANTS } from '../config/constants';
import { OAS_ERRORS } from '../config/oasErrors';
import { Utils } from '../../utils/utils';
import { StringField } from './string.field';

export class UrlField extends StringField {
  static get nodeObjectType() {
    return 'UrlField';
  }

  constructor(value = CONSTANTS.emptyStr, parent = null, fieldName = 'url') {
    super(value, parent, fieldName);
  }

  validate() {
    if (this.isEmpty) {
      return;
    }
    if (!Utils.isValidUrl(this.value)) {
      this.validation.update({
        hasError: true,
        summary: OAS_ERRORS.BAD_URL,
      });
    }
  }
}
