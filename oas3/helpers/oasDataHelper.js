import { CONSTANTS } from '../config/constants';
import { OAS_OBJECT_TYPES, OPERATION_TYPES, SECURITY_SCHEME_TYPES } from '../config/oasConfig';
import { Utils } from '../../utils/utils';

export class OasDataHelper {
  static extractOasBasicsData(oasDocValue) {
    try {
      delete oasDocValue.paths;
      delete oasDocValue.components;
    } catch (e) {
      console.log('[OasDataHelper.extractOasBasicsData]', e);
    }
    return oasDocValue;
  }

  static getObjType(nodeAbsPath) {
    let nodeName = nodeAbsPath.split(CONSTANTS.oas.pathSep)[1];
    for (let t of Object.keys(OAS_OBJECT_TYPES)) {
      if (OAS_OBJECT_TYPES[t].nodeNames.includes(nodeName)) {
        return OAS_OBJECT_TYPES[t].name;
      }
    }
    if (CONSTANTS.oas.extensivePropRegex.test(nodeName)) {
      return OAS_OBJECT_TYPES.other.name;
    }
    return OAS_OBJECT_TYPES.other.name;
  }

  static getNodeNamesByObjectType(objectType) {
    for (let t of Object.keys(OAS_OBJECT_TYPES)) {
      if (OAS_OBJECT_TYPES[t].name === objectType) {
        return OAS_OBJECT_TYPES[t].nodeNames;
      }
    }
    return null;
  }

  static convertPathToPathKey(oasNodePath) {
    return oasNodePath.replace(CONSTANTS.oas.pathSepRegex, CONSTANTS.oas.pathKeySep);
  }

  static getOperationIds(oasData) {
    // 获取oas文档中所有的operationId.
    let documentIds = [];
    if (!oasData.paths) {
      return documentIds;
    }
    for (let path of Object.values(oasData.paths)) {
      for (let prop of Object.keys(path)) {
        if (Object.keys(OPERATION_TYPES).includes(prop) && path[prop]) {
          if (Utils.isNotEmptyString(path[prop].operationId)) {
            documentIds.push({ value: path[prop].operationId, isUsed: false });
          }
        }
      }
    }
    return documentIds;
  }

  // 已经在组件中获取了
  static getDocumentTags(oasData) {
    // 获取oas文档的tags.
    let tags = [];
    if (!oasData.info || !oasData.info.tags) {
      return tags;
    }
    for (let tag of oasData.info.tags) {
      if (Utils.isNotEmptyString(tag.name)) {
        tags.push(tag.name);
      }
    }
    return tags;
  }

  static getSecuritySchemes(oasData) {
    // 获取oas文档的SecuritySchemes(数组).
    let securitySchemes = [];
    if (!oasData.components || !oasData.components.securitySchemes) {
      return securitySchemes;
    }
    let securitySchemesData = oasData.components.securitySchemes;

    for (let keyName of Object.keys(securitySchemesData)) {
      let securitySchemeData = securitySchemesData[keyName];
      let scopes = [];

      // 判断 securityScheme 的类型
      if (securitySchemeData.type === SECURITY_SCHEME_TYPES.oauth2.name) {
        let flows = securitySchemeData.flows;
        let flowItems = Object.values(flows);
        while (flowItems.length > 0) {
          // flows的key可能是x-开头的扩展; 其不存在scopes字段;
          let typeOfScopes = typeof flowItems[0].scopes;
          if (typeOfScopes === CONSTANTS.jsonFieldTypes.object) {
            let scopeNames = Object.keys(flowItems[0].scopes);
            scopeNames.forEach(v => {
              scopes.push({ key: v });
            });
          }
          flowItems.shift();
        }
      }

      let securityScheme = {
        key: keyName,
        scopes,
        type: securitySchemeData.type,
      };
      securitySchemes.push(securityScheme);
    }

    return securitySchemes;
  }
}
