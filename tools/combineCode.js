/* eslint-disable */

var fs = require('fs');
var targetFilePath = './oas3/oas3.all1.js';
var oas3Folder = '../../../../routes';
var modelSuffix = '.model.js';
var fieldSuffix = '.field.js';
var specSuffix = '.spec.js';
var jsSuffix = '.js';
var lessSuffix = '.less';

var targetContent = '';

function readModulesRecursively(folder) {
  var paths = [];
  var readDir = function(dirPath) {
    fs.readdir(dirPath, function(err, fileNames) {
      var subPaths = fileNames.map(function(fileName) {
        return dirPath + '/' + fileName;
      });
      subPaths.forEach(function(path) {
        fs.stat(path, function(err, stat) {
          if (stat.isDirectory()) {
            readDir(path);
          } else {
            // 跳过node_modules文件夹
            if (path.indexOf('node_modules') < 0) {
              // 输出如下文件
              if (
                path.endsWith(modelSuffix) ||
                path.endsWith(fieldSuffix) ||
                path.endsWith(specSuffix) ||
                path.endsWith(jsSuffix) ||
                path.endsWith(lessSuffix)
              ) {
                fs.readFile(path, { flag: 'r+', encoding: 'utf8' }, function(err, fileContent) {
                  targetContent += '### *' + path + '*';
                  targetContent += '\n\n';
                  targetContent += '`' + fileContent + '`';
                  targetContent += '\n\n\n';
                });
              }
            }
          }
        });
      });
    });
  };
  readDir(folder);
  return paths;
}

readModulesRecursively(oas3Folder);

setTimeout(function() {
  fs.writeFile(targetFilePath, targetContent, { flag: 'w' }, function(err) {
    if (err) {
      console.error(err);
    } else {
      console.log('更新成功');
    }
  });
}, 15000);

/* eslint-disable */
