/* eslint-disable */

var fs = require('fs');
var oasObjectsFolder = './oas3/objects';
var oasFieldsFolder = './oas3/fields';
var modelSuffix = '.model.js';
var fieldSuffix = '.field.js';

import { OAS_NODE_TYPES } from '../oas3/nodeTypes';

function generateModulePath(parentPathDepth, absModulePath) {
  let result = '';
  for (let index = 0; index < parentPathDepth; ++index) {
    result += '../';
  }
  result += absModulePath;
  return result;
}

function generateTestData(modelName) {
  let mockData = new OAS_NODE_TYPES[modelName]().extract(true);
  for (let key of Object.keys(mockData)) {
    if (typeof mockData[key] === 'string' && mockData[key].length === 0) {
      mockData[key] = key + Math.floor(Math.random() * 100);
    }
    if (typeof mockData[key] === 'number') {
      mockData[key] = mockData[key] + 2;
    }
  }
  return JSON.stringify(mockData, null, 2);
}

function readModulesRecursively(folder) {
  var paths = [];
  var readDir = function(dirPath) {
    fs.readdir(dirPath, function(err, fileNames) {
      var subPaths = fileNames.map(function(fileName) {
        return {
          path: dirPath + '/' + fileName,
          moduleName: fileName.replace('.js', ''),
        };
      });
      subPaths.forEach(function(path) {
        fs.stat(path.path, function(err, stat) {
          if (stat.isDirectory()) {
            readDir(path.path);
          } else {
            if (path.path.endsWith(modelSuffix) || path.path.endsWith(fieldSuffix)) {
              let targetSpecPath = path.path.replace('.js', '.spec.js');
              fs.stat(targetSpecPath, function(err, stat) {
                if (!stat) {
                  if (true || path.moduleName === 'pathItem.model') {
                    fs.readFile(path.path, { flag: 'r+', encoding: 'utf8' }, function(
                      err,
                      fileContent
                    ) {
                      var regex = /static\s+get\s+nodeObjectType\(\)\s\{\s*return\s*'\w+/gm;
                      var m;
                      while ((m = regex.exec(fileContent)) !== null) {
                        if (m.index === regex.lastIndex) {
                          regex.lastIndex++;
                        }
                        m.forEach((match, groupIndex) => {
                          var objClassName = match.substr(match.lastIndexOf("'") + 1);
                          let modelName = objClassName;
                          let moduleName = path.moduleName;
                          let camelModelInst =
                            objClassName[0].toLowerCase() + objClassName.substr(1);
                          let testData = generateTestData(modelName);
                          let parentPathDepth = targetSpecPath.split('/').length - 2 - 1;
                          let nodeTypesModulePath = generateModulePath(
                            parentPathDepth,
                            'nodeTypes'
                          );
                          let utilsModulePath = generateModulePath(
                            parentPathDepth,
                            '../utils/utils'
                          );
                          let oasErrorsModulePath = generateModulePath(
                            parentPathDepth,
                            'config/oasErrors'
                          );

                          let specFileContent = `import chai from 'chai';
import { Utils } from '${utilsModulePath}';
import { ${modelName} } from './${moduleName}';
import { OAS_ERRORS } from '${oasErrorsModulePath}';

const assert = chai.assert;

describe('${modelName}', () => {
  let ${camelModelInst}Data01 = ${testData};

  let ${camelModelInst}Data02 = ${testData};

  let ${camelModelInst};

  describe('#fields', () => {
    describe('data extraction', () => {
      it('can extract correct data', () => {
        ${camelModelInst} = new ${modelName}(${camelModelInst}Data01);
        assert.equal(Utils.deepEqual(${camelModelInst}Data01, ${camelModelInst}.extract()), true);
      });
    });
  });

  describe('#validation', () => {
    describe('validate ${camelModelInst} data', () => {
      // [TODO] 完善校验逻辑失败的单元测试;
      it('can get error named ERROR', () => {
        ${camelModelInst} = new ${modelName}(${camelModelInst}Data02);
        ${camelModelInst}.validate();
        assert.equal(${camelModelInst}.validation.hasError, true);
        assert.equal(OAS_ERRORS.ERROR, OAS_ERRORS.ERROR);
      });

      // [TODO] 完善校验逻辑成功的单元测试;
      it('can pass validation when given valid data', () => {
        ${camelModelInst} = new ${modelName}(${camelModelInst}Data01);
        ${camelModelInst}.validate();
        assert.equal(${camelModelInst}.validation.hasError, false);
        assert.equal(${camelModelInst}.validation.error, null);
      });
    });
  });
});
`;
                          fs.writeFile(targetSpecPath, specFileContent, { flag: 'w' }, function(
                            err
                          ) {
                            if (err) {
                              console.error(err);
                            } else {
                              console.log(targetSpecPath + ' 写入成功');
                            }
                          });
                        });
                      }
                    });
                  }
                } else {
                  console.log(targetSpecPath + ' 已经存在.');
                }
              });
            }
          }
        });
      });
    });
  };
  readDir(folder);
  return paths;
}

readModulesRecursively(oasFieldsFolder);
readModulesRecursively(oasObjectsFolder);

/* eslint-disable */
