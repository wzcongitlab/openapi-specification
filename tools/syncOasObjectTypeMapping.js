/* eslint-disable */

var fs = require('fs');
var targetFilePath = './oas3/nodeTypes.js';
var oasObjectsFolder = './oas3/objects';
var oasFieldsFolder = './oas3/fields';
var modelSuffix = '.model.js';
var fieldSuffix = '.field.js';
var importsPart = '';
var exportPart = 'export const OAS_NODE_TYPES = {\n';

function createImportModulePath(filePath) {
  return filePath.replace('.js', '').replace('oas3/', '');
}

function createImportStr(filePath, objClassName) {
  var modulePath = createImportModulePath(filePath);
  return 'import { ' + objClassName + " } from '" + modulePath + "';\n";
}

function readModulesRecursively(folder) {
  var paths = [];
  var readDir = function(dirPath) {
    fs.readdir(dirPath, function(err, fileNames) {
      var subPaths = fileNames.map(function(fileName) {
        return dirPath + '/' + fileName;
      });
      subPaths.forEach(function(path) {
        fs.stat(path, function(err, stat) {
          if (stat.isDirectory()) {
            readDir(path);
          } else {
            if (path.endsWith(modelSuffix) || path.endsWith(fieldSuffix)) {
              fs.readFile(path, { flag: 'r+', encoding: 'utf8' }, function(err, fileContent) {
                var regex = /static\s+get\s+nodeObjectType\(\)\s\{\s*return\s*'\w+/gm;
                var m;
                while ((m = regex.exec(fileContent)) !== null) {
                  if (m.index === regex.lastIndex) {
                    regex.lastIndex++;
                  }
                  m.forEach((match, groupIndex) => {
                    var objClassName = match.substr(match.lastIndexOf("'") + 1);
                    importsPart += createImportStr(path, objClassName);
                    exportPart += '   ' + objClassName + ',\n';
                  });
                }
              });
            }
          }
        });
      });
    });
  };
  readDir(folder);
  return paths;
}

readModulesRecursively(oasFieldsFolder);
readModulesRecursively(oasObjectsFolder);

setTimeout(function() {
  importsPart += '\n';
  exportPart += '};';
  fs.writeFile(targetFilePath, importsPart + exportPart, { flag: 'w' }, function(err) {
    if (err) {
      console.error(err);
    } else {
      console.log('更新成功');
    }
  });
}, 2000);

/* eslint-disable */
