export const petStoreOas = {
  openapi: '3.0.1',
  info: {
    description:
      'This is a sample server Petstore server. For this sample, you can use the api key `special-key` to test the authorization filters.',
    license: {
      name: 'Apache-2.0',
      url: 'http://www.apache.org/licenses/LICENSE-2.0.html',
    },
    title: 'OpenAPI Petstore',
    version: '1.0.0',
    contact: {
      name: 'Wangzc',
      email: '',
      url: 'http://www.apache.org/licenses',
    },
    termsOfService: 'http://www.apache.org/licenses/LICENSE-2.0.html',
  },
  servers: [
    {
      url: 'http://petstore.swagger.io/v2',
      description: 'v2 server',
      variables: {
        version: {
          default: 'v1',
          description: 'version',
          enum: ['v1', 'v2'],
        },
      },
    },
    {
      url: '',
      description: 'v2 server',
      variables: {
        version: {
          default: 'v1',
          description: 'version',
          enum: ['v1', 'v2'],
        },
      },
    },
  ],
  externalDocs: {
    url: 'ss',
    description: 'description',
  },
  tags: [
    {
      description: 'Everything about your Pets',
      name: 'pet',
      externalDocs: {
        url: 'http://localhost://pettag/detail',
        description: 'pet dog',
      },
    },
    {
      description: 'Access to Petstore orders',
      name: 'store',
      externalDocs: {
        url: 'http://localhost://store/detail',
        description: 'store store',
      },
    },
    {
      description: 'Operations about user',
      name: 'user',
    },
  ],
  paths: {
    '/petTag': {
      post: {
        operationId: 'create_pet_tag',
        responses: {
          '200': {
            description: 'create pet tag',
            content: {
              'application/json': {
                examples: {
                  tagName: {
                    description: 'pet tag as a string',
                  },
                },
                schema: {
                  type: 'object',
                  properties: {
                    tagId: {
                      type: 'string',
                      example: 'sdfdf0-sddsads-ssdsds',
                      format: 'uuid',
                    },
                    tagYY: {
                      type: 'string',
                      example: 'sdfdf0-sddsads-ssdsds',
                    },
                    tagName: {
                      type: 'string',
                      example: 'dog',
                      format: 'byte',
                    },
                  },
                },
              },
            },
          },
        },
        security: [
          {
            api_key: [],
          },
        ],
        servers: [
          {
            url: 'http://petstore.swagger.io/v2',
            description: 'v2 server',
            variables: {
              version: {
                default: 'v1',
                description: 'version',
                enum: ['v1', 'v2'],
              },
            },
          },
        ],
      },
    },
    '/pet': {
      post: {
        operationId: 'add_pet',
        requestBody: {
          content: {
            'application/json': {
              schema: {
                $ref: '#/components/schemas/Pet',
              },
            },
            'application/xml': {
              schema: {
                $ref: '#/components/schemas/Pet',
              },
            },
          },
          description: 'Pet object that needs to be added to the store',
          required: true,
        },
        responses: {
          '405': {
            content: {},
            description: 'Invalid input',
          },
        },
        security: [
          {
            petstore_auth: ['write:pets', 'read:pets'],
          },
          {
            api_key: [],
          },
        ],
        summary: 'Add a new pet to the store',
        tags: ['pet'],
        'x-openapi-router-controllers': 'openapi_server.controllers.pet_controller',
      },
      put: {
        operationId: 'update_pet',
        requestBody: {
          content: {
            'application/json': {
              schema: {
                oneOf: [
                  {
                    $ref: '#/components/schemas/Pet',
                  },
                ],
              },
            },
            'application/xml': {
              schema: {
                allOf: [
                  {
                    $ref: '#/components/schemas/Pet',
                  },
                  {
                    type: 'object',
                    properties: {
                      id: {
                        type: 'string',
                        format: 'string',
                      },
                    },
                  },
                ],
              },
            },
          },
          description: 'Pet object that needs to be added to the store',
          required: true,
        },
        responses: {
          '400': {
            $ref: '#/components/responses/not_found',
          },
          '404': {
            content: {},
            description: 'Pet not found',
          },
          '405': {
            content: {},
            description: 'Validation exception',
          },
        },
        security: [
          {
            petstore_auth: ['write:pets', 'read:pets'],
          },
        ],
        summary: 'Update an existing pet',
        tags: ['pet'],
        'x-openapi-router-controller': 'openapi_server.controllers.pet_controller',
      },
    },
    '/pet/findByStatus': {
      get: {
        description: 'Multiple status values can be provided with comma separated strings',
        operationId: 'find_pets_by_status',
        parameters: [
          {
            description: 'Status values that need to be considered for filter',
            explode: false,
            in: 'query',
            name: 'status',
            required: true,
            schema: {
              items: {
                default: 'available',
                enum: ['available', 'pending', 'sold'],
                type: 'string',
              },
              type: 'array',
            },
            style: 'form',
          },
        ],
        responses: {
          '200': {
            content: {
              'application/xml': {
                schema: {
                  items: {
                    $ref: '#/components/schemas/Pet',
                  },
                  type: 'array',
                },
              },
              'application/json': {
                schema: {
                  items: {
                    $ref: '#/components/schemas/Pet',
                  },
                  type: 'array',
                },
              },
            },
            description: 'successful operation',
          },
          '400': {
            content: {},
            description: 'Invalid status value',
          },
        },
        security: [
          {
            petstore_auth: ['write:pets', 'read:pets'],
          },
        ],
        summary: 'Finds Pets by status',
        tags: ['pet'],
        'x-openapi-router-controller': 'openapi_server.controllers.pet_controller',
        servers: [
          {
            url: 'http://petstore.swagger.io/v2',
            description: 'v2 server',
            variables: {
              version: {
                default: 'v1',
                description: 'version',
                enum: ['v1', 'v2'],
              },
            },
          },
        ],
      },
    },
    '/pet/findByTags': {
      summary: 'findByTag',
      description: 'find Pet by tags',
      get: {
        deprecated: true,
        description:
          'Multiple tags can be provided with comma separated strings. Use tag1, tag2, tag3 for testing.',
        operationId: 'find_pets_by_tags',
        parameters: [
          {
            description: 'Tags to filter by',
            explode: false,
            in: 'query',
            name: 'tags',
            required: true,
            schema: {
              items: {
                type: 'string',
              },
              type: 'array',
            },
            style: 'form',
          },
        ],
        responses: {
          '200': {
            content: {
              'application/xml': {
                schema: {
                  items: {
                    $ref: '#/components/schemas/Pet',
                  },
                  type: 'array',
                },
              },
              'application/json': {
                schema: {
                  items: {
                    $ref: '#/components/schemas/Pet',
                  },
                  type: 'array',
                },
              },
            },
            description: 'successful operation',
          },
          '400': {
            content: {},
            description: 'Invalid tag value',
          },
        },
        security: [
          {
            petstore_auth: ['write:pets', 'read:pets'],
          },
        ],
        summary: 'Finds Pets by tags',
        tags: ['pet'],
        'x-openapi-router-controller': 'openapi_server.controllers.pet_controller',
      },
    },
    '/pet/{petId}': {
      $ref: '#/sss',
      summary: 'operate pet by id',
      description: 'operate pet by id',
      delete: {
        operationId: 'delete_pet',
        parameters: [
          {
            in: 'header',
            name: 'api_key',
            schema: {
              type: 'string',
            },
          },
          {
            description: 'Pet id to delete',
            in: 'path',
            name: 'petId',
            required: true,
            schema: {
              format: 'int64',
              type: 'integer',
            },
          },
        ],
        responses: {
          '400': {
            content: {},
            description: 'Invalid pet value',
          },
        },
        security: [
          {
            petstore_auth: ['write:pets', 'read:pets'],
          },
        ],
        summary: 'Deletes a pet',
        tags: ['pet'],
        'x-openapi-router-controller': 'openapi_server.controllers.pet_controller',
      },
      get: {
        description: 'Returns a single pet',
        operationId: 'get_pet_by_id',
        parameters: [
          {
            description: 'ID of pet to return',
            in: 'path',
            name: 'petId',
            required: true,
            schema: {
              format: 'int64',
              type: 'integer',
            },
          },
        ],
        responses: {
          '200': {
            content: {
              'application/xml': {
                schema: {
                  $ref: '#/components/schemas/Pet',
                },
              },
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/Pet',
                },
              },
            },
            description: 'successful operation',
          },
          '400': {
            content: {},
            description: 'Invalid ID supplied',
          },
          '404': {
            content: {},
            description: 'Pet not found',
          },
        },
        security: [
          {
            api_key: [],
          },
        ],
        summary: 'Find pet by ID',
        tags: ['pet'],
        'x-openapi-router-controller': 'openapi_server.controllers.pet_controller',
      },
      post: {
        operationId: 'update_pet_with_form',
        parameters: [
          {
            name: 'user_info',
            in: 'header',
            description: 'pwt user info',
            content: {
              'application/json': {
                schema: {
                  properties: {
                    userName: {
                      example: 'userA',
                    },
                    token: {
                      type: 'string',
                      example: 'dsfewkufiew',
                    },
                  },
                },
              },
            },
          },
          {
            description: 'ID of pet that needs to be updated',
            in: 'path',
            name: 'petId',
            required: true,
            schema: {
              format: 'int64',
              type: 'integer',
            },
          },
        ],
        requestBody: {
          content: {
            'application/x-www-form-urlencoded': {
              schema: {
                type: 'object',
                properties: {
                  name: {
                    description: 'Updated name of the pet',
                    type: 'string',
                  },
                  status: {
                    description: 'Updated status of the pet',
                    type: 'string',
                  },
                  pet: {
                    type: 'object',
                    properties: {
                      name: {
                        description: 'Updated name of the pet',
                        type: 'string',
                      },
                      status: {
                        description: 'Updated status of the pet',
                        type: 'string',
                      },
                      pet: {
                        type: 'object',
                        properties: {
                          name: {
                            description: 'Updated name of the pet',
                            type: 'string',
                          },
                          status: {
                            description: 'Updated status of the pet',
                            type: 'string',
                            enum: ['available', 'soldout'],
                          },
                        },
                      },
                    },
                  },
                },
                minProperties: 2,
              },
            },
            'text/plain': {
              schema: {
                format: 'int64',
                maximum: 5,
                minimum: 1,
                type: 'integer',
              },
            },
          },
        },
        responses: {
          '405': {
            content: {},
            description: 'Invalid input',
          },
        },
        security: [
          {
            petstore_auth: ['write:pets', 'read:pets'],
          },
        ],
        summary: 'Updates a pet in the store with form data',
        tags: ['pet'],
        'x-openapi-router-controller': 'openapi_server.controllers.pet_controller',
      },
    },
    '/pet/{petId}/uploadImage': {
      post: {
        operationId: 'upload_file',
        parameters: [
          {
            description: 'ID of pet to update',
            in: 'path',
            name: 'petId',
            required: true,
            schema: {
              format: 'int64',
              type: 'integer',
            },
          },
        ],
        requestBody: {
          content: {
            'multipart/form-data': {
              schema: {
                properties: {
                  additionalMetadata: {
                    description: 'Additional data to pass to server',
                    type: 'string',
                  },
                  file: {
                    description: 'file to upload',
                    format: 'binary',
                    type: 'string',
                  },
                },
              },
            },
          },
        },
        responses: {
          '200': {
            content: {
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/Pet',
                },
              },
            },
            description: 'successful operation',
          },
        },
        security: [
          {
            petstore_auth: ['write:pets', 'read:pets'],
          },
        ],
        summary: 'uploads an image',
        tags: ['pet'],
        'x-openapi-router-controller': 'openapi_server.controllers.pet_controller',
      },
    },
    '/store/inventory': {
      get: {
        description: 'Returns a map of status codes to quantities',
        operationId: 'get_inventory',
        responses: {
          '200': {
            content: {
              'application/json': {
                schema: {
                  additionalProperties: {
                    format: 'int32',
                    type: 'integer',
                  },
                  type: 'object',
                },
              },
            },
            description: 'successful operation',
          },
        },
        security: [
          {
            api_key: [],
          },
          {
            session_id: [],
          },
        ],
        summary: 'Returns pet inventories by status',
        tags: ['store'],
        'x-openapi-router-controller': 'openapi_server.controllers.store_controller',
      },
    },
    '/store/order': {
      post: {
        operationId: 'place_order',
        requestBody: {
          content: {
            '*/*': {
              schema: {
                $ref: '#/components/schemas/Order',
              },
            },
          },
          description: 'order placed for purchasing the pet',
          required: true,
        },
        responses: {
          '200': {
            content: {
              'application/xml': {
                schema: {
                  $ref: '#/components/schemas/Order',
                },
              },
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/Order',
                },
              },
            },
            description: 'successful operation',
          },
          '400': {
            content: {},
            description: 'Invalid Order',
          },
        },
        summary: 'Place an order for a pet',
        tags: ['store'],
        'x-openapi-router-controller': 'openapi_server.controllers.store_controller',
      },
    },
    '/store/order/{orderId}': {
      delete: {
        description:
          'For valid response try integer IDs with value < 1000. Anything above 1000 or nonintegers will generate API errors',
        operationId: 'delete_order',
        parameters: [
          {
            description: 'ID of the order that needs to be deleted',
            in: 'path',
            name: 'orderId',
            required: true,
            schema: {
              type: 'string',
            },
          },
        ],
        responses: {
          '400': {
            content: {},
            description: 'Invalid ID supplied',
          },
          '404': {
            content: {},
            description: 'Order not found',
          },
        },
        summary: 'Delete purchase order by ID',
        tags: ['store'],
        'x-openapi-router-controller': 'openapi_server.controllers.store_controller',
      },
      get: {
        description:
          'For valid response try integer IDs with value <= 5 or > 10. Other values will generated exceptions',
        operationId: 'get_order_by_id',
        parameters: [
          {
            description: 'ID of pet that needs to be fetched',
            in: 'path',
            name: 'orderId',
            required: true,
            schema: {
              format: 'int64',
              maximum: 5,
              minimum: 1,
              type: 'integer',
            },
          },
        ],
        responses: {
          '200': {
            content: {
              'application/xml': {
                schema: {
                  $ref: '#/components/schemas/Order',
                },
              },
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/Order',
                },
              },
            },
            description: 'successful operation',
          },
          '400': {
            content: {},
            description: 'Invalid ID supplied',
          },
          '404': {
            content: {},
            description: 'Order not found',
          },
        },
        summary: 'Find purchase order by ID',
        tags: ['store'],
        'x-openapi-router-controller': 'openapi_server.controllers.store_controller',
      },
    },
    '/user': {
      post: {
        description: 'This can only be done by the logged in user.',
        operationId: 'create_user',
        requestBody: {
          content: {
            '*/*': {
              schema: {
                $ref: '#/components/schemas/User',
              },
            },
          },
          description: 'Created user object',
          required: true,
        },
        responses: {
          default: {
            content: {},
            description: 'successful operation',
          },
        },
        summary: 'Create user',
        tags: ['user'],
        'x-openapi-router-controller': 'openapi_server.controllers.user_controller',
      },
    },
    '/user/createWithArray': {
      post: {
        operationId: 'create_users_with_array_input',
        requestBody: {
          content: {
            '*/*': {
              schema: {
                items: {
                  $ref: '#/components/schemas/User',
                },
                type: 'array',
              },
            },
          },
          description: 'List of user object',
          required: true,
        },
        responses: {
          default: {
            content: {},
            description: 'successful operation',
          },
        },
        summary: 'Creates list of users with given input array',
        tags: ['user'],
        'x-openapi-router-controller': 'openapi_server.controllers.user_controller',
      },
    },
    '/user/createWithList': {
      post: {
        operationId: 'create_users_with_list_input',
        requestBody: {
          content: {
            '*/*': {
              example: {
                value: 123,
                title: 'abc',
              },
              schema: {
                items: {
                  $ref: '#/components/schemas/User',
                },
                type: 'array',
              },
            },
          },
          description: 'List of user object',
          required: true,
        },
        responses: {
          default: {
            content: {},
            description: 'successful operation',
          },
        },
        summary: 'Creates list of users with given input array',
        tags: ['user'],
        'x-openapi-router-controller': 'openapi_server.controllers.user_controller',
      },
    },
    '/user/login': {
      get: {
        operationId: 'login_user',
        parameters: [
          {
            description: 'The user name for login',
            in: 'query',
            name: 'username',
            required: true,
            schema: {
              type: 'string',
            },
          },
          {
            description: 'The password for login in clear text',
            in: 'query',
            name: 'password',
            required: true,
            schema: {
              type: 'string',
            },
          },
        ],
        responses: {
          '200': {
            content: {
              'application/xml': {
                schema: {
                  type: 'string',
                },
              },
              'application/json': {
                schema: {
                  type: 'string',
                },
              },
            },
            description: 'successful operation',
            headers: {
              'X-Rate-Limit': {
                description: 'calls per hour allowed by the user',
                schema: {
                  format: 'int32',
                  type: 'integer',
                },
              },
              'X-Expires-After': {
                description: 'date in UTC when toekn expires',
                schema: {
                  format: 'date-time',
                  type: 'string',
                },
              },
            },
          },
          '400': {
            content: {},
            description: 'Invalid username/password supplied',
          },
        },
        summary: 'Logs user into the system',
        tags: ['user'],
        'x-openapi-router-controller': 'openapi_server.controllers.user_controller',
      },
    },
    '/user/logout': {
      get: {
        operationId: 'logout_user',
        responses: {
          default: {
            content: {},
            description: 'successful operation',
          },
        },
        summary: 'Logs out current logged in user session',
        tags: ['user', 'pet'],
        'x-openapi-router-controller': 'openapi_server.controllers.user_controller',
      },
    },
    '/user/{username}': {
      delete: {
        description: 'This can only be done by the logged in user.',
        operationId: 'delete_user',
        parameters: [
          {
            description: 'The name that needs to be deleted',
            in: 'path',
            name: 'username',
            required: true,
            schema: {
              type: 'string',
            },
          },
        ],
        responses: {
          '400': {
            content: {},
            description: 'Invalid username supplied',
          },
          '404': {
            content: {},
            description: 'User not found',
          },
        },
        summary: 'Delete user',
        tags: ['user'],
        'x-openapi-router-controller': 'openapi_server.controllers.user_controller',
      },
      get: {
        operationId: 'get_user_by_name',
        parameters: [
          {
            description: 'The name that needs to be fetched. Use user1 for testing.',
            in: 'path',
            name: 'username',
            required: true,
            schema: {
              type: 'string',
            },
          },
        ],
        responses: {
          '200': {
            content: {
              'application/xml': {
                schema: {
                  $ref: '#/components/schemas/User',
                },
              },
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/User',
                },
              },
            },
            description: 'successful operation',
          },
          '400': {
            content: {},
            description: 'Invalid username supplied',
          },
          '404': {
            content: {},
            description: 'User not found',
          },
        },
        summary: 'Get user by user name',
        tags: ['user'],
        'x-openapi-router-controller': 'openapi_server.controllers.user_controller',
      },
      put: {
        description: 'This can only be done by the logged in user.',
        operationId: 'update_user',
        parameters: [
          {
            description: 'name that need to be deleted',
            in: 'path',
            name: 'username',
            required: true,
            schema: {
              type: 'string',
            },
          },
        ],
        requestBody: {
          content: {
            '*/*': {
              schema: {
                $ref: '#/components/schemas/User',
              },
            },
          },
          description: 'Updated user object',
          required: true,
        },
        responses: {
          '400': {
            content: {},
            description: 'Invalid user supplied',
          },
          '404': {
            content: {},
            description: 'User not found',
          },
        },
        summary: 'Updated user',
        tags: ['user'],
        'x-openapi-router-controller': 'openapi_server.controllers.user_controller',
      },
    },
  },
  components: {
    schemas: {
      OrderStatus: {
        description: 'Order Status',
        enum: ['placed', 'approved', 'delivered'],
        type: 'string',
      },
      Order: {
        description: 'An order for a pets from the pet store',
        example: {
          petId: 6,
          quantity: 1,
          id: 0,
          shipDate: '2000-01-23T04:56:07.000Z',
          complete: false,
          status: 'placed',
        },
        properties: {
          id: {
            format: 'int64',
            type: 'integer',
          },
          petId: {
            format: 'int64',
            type: 'integer',
          },
          quantity: {
            format: 'int32',
            type: 'integer',
          },
          shipDate: {
            format: 'date-time',
            type: 'string',
          },
          status: {
            $ref: '#/components/schemas/OrderStatus',
          },
          complete: {
            default: false,
            type: 'boolean',
          },
        },
        title: 'Pet Order',
        type: 'object',
        xml: {
          name: 'Order',
        },
      },
      Test: {
        nullable: false,
        readOnly: false,
        writeOnly: false,
        deprecated: false,
        example: '',
        oneOf: [
          {
            $ref: '#/components/schemas/Peta',
          },
          {
            nullable: false,
            readOnly: false,
            writeOnly: false,
            deprecated: false,
            example: '',
            type: 'object',
            description: '',
            maxProperties: null,
            minProperties: null,
            required: [],
            properties: {
              id: {
                nullable: false,
                readOnly: false,
                writeOnly: false,
                deprecated: false,
                example: '',
                not: {
                  nullable: false,
                  readOnly: false,
                  writeOnly: false,
                  deprecated: false,
                  example: '',
                  type: 'integer',
                  description: '',
                  format: '',
                  multipleOf: null,
                  maximum: null,
                  exclusiveMaximum: false,
                  minimum: null,
                  exclusiveMinimum: false,
                  default: null,
                  enum: [],
                },
              },
            },
          },
        ],
      },
      Category: {
        description: 'A category for a pet',
        example: {
          name: 'name',
          id: 6,
        },
        properties: {
          id: {
            format: 'int64',
            type: 'integer',
          },
          name: {
            type: 'string',
          },
          info: {
            description: 'A tag for a pet',
            example: {
              name: 'name',
              id: 1,
            },
            maxProperties: 10,
            minProperties: 2,
            properties: {
              id: {
                format: 'int64',
                type: 'integer',
              },
              name: {
                type: 'string',
              },
            },
            title: 'Pet Tag',
            type: 'object',
            xml: {
              name: 'Tag',
            },
          },
        },
        title: 'Pet category',
        type: 'object',
        xml: {
          name: 'Category',
        },
      },
      User: {
        description: 'A User who is purchasing from the pet store',
        example: {
          firstName: 'firstName',
          lastName: 'lastName',
          password: 'password',
          userStatus: 6,
          phone: 'phone',
          id: 0,
          email: 'email',
          username: 'username',
        },
        properties: {
          id: {
            format: 'int64',
            type: 'integer',
          },
          username: {
            type: 'string',
          },
          firstName: {
            type: 'string',
          },
          lastName: {
            type: 'string',
          },
          email: {
            type: 'string',
          },
          password: {
            type: 'string',
          },
          phone: {
            type: 'string',
          },
          userStatus: {
            description: 'User Status',
            format: 'int32',
            type: 'integer',
          },
        },
        title: 'a User',
        type: 'object',
        xml: {
          name: 'User',
        },
      },
      Tag: {
        description: 'A tag for a pet',
        example: {
          name: 'name',
          id: 1,
        },
        properties: {
          id: {
            format: 'int64',
            type: 'integer',
          },
          name: {
            type: 'string',
          },
        },
        title: 'Pet Tag',
        type: 'object',
        xml: {
          name: 'Tag',
        },
      },
      Pet: {
        description: 'A pet for sale in the pet store',
        example: {
          photoUrls: ['photoUrls', 'photoUrls'],
          name: 'doggie',
          id: 0,
          category: {
            name: 'name',
            id: 6,
          },
          tags: [
            {
              name: 'name',
              id: 1,
            },
            {
              name: 'name',
              id: 1,
            },
          ],
          status: 'available',
        },
        properties: {
          id: {
            format: 'int64',
            type: 'integer',
          },
          category: {
            $ref: '#/components/schemas/Category',
          },
          name: {
            example: 'doggie',
            type: 'string',
          },
          photoUrls: {
            items: {
              type: 'string',
              maxLength: 10,
            },
            type: 'array',
            xml: {
              name: 'photoUrl',
              wrapped: true,
            },
          },
          tags: {
            items: {
              $ref: '#/components/schemas/Tag',
            },
            type: 'array',
            xml: {
              name: 'tag',
              wrapped: true,
            },
          },
          status: {
            description: 'pet status in the store',
            enum: ['available', 'pending', 'sold'],
            type: 'string',
          },
        },
        required: ['name', 'photoUrls'],
        title: 'a Pet',
        type: 'object',
        xml: {
          name: 'Pet',
        },
      },
      ApiResponse: {
        description: 'Describes the result of uploading an image resource',
        example: {
          code: 0,
          type: 'type',
          message: 'message',
        },
        properties: {
          code: {
            format: 'int32',
            type: 'integer',
          },
          type: {
            type: 'string',
          },
          message: {
            type: 'string',
          },
        },
        title: 'An uploaded response',
        type: 'object',
      },
    },
    securitySchemes: {
      petstore_auth: {
        flows: {
          implicit: {
            authorizationUrl: 'http://petstore.swagger.io/api/oauth/dialog',
            scopes: {
              'write:pets': 'modify pets in your account',
              'read:pets': 'read your pets',
            },
          },
        },
        type: 'oauth2',
      },
      api_key: {
        in: 'header',
        name: 'api_key',
        type: 'apiKey',
      },
      session_id: {
        in: 'query',
        name: 'session_id',
        type: 'apiKey',
      },
    },
    responses: {
      not_found: {
        content: {},
        description: 'Invalid input',
      },
    },
    headers: {
      pet_tag: {
        description: 'pet tag',
        required: true,
        schema: {
          type: 'integer',
        },
      },
    },
    parameters: {
      userName: {
        description: 'The user name for login',
        in: 'query',
        name: 'username',
        required: true,
        schema: {
          type: 'string',
        },
      },
    },
  },
  'x-1': 1,
  'x-2': false,
  'x-3': 'hello',
  'x-4': {
    a: 's',
    b: 1,
  },
  'x-5': [1, 2, 3],
};

export const petStoreOas2 = {
  openapi: '3.0.1',
  info: {
    description:
      'This is a sample server Petstore server. For this sample, you can use the api key `special-key` to test the authorization filters.',
    license: {
      name: 'Apache-2.0',
      url: 'http://www.apache.org/licenses/LICENSE-2.0.html',
    },
    title: 'OpenAPI Petstore',
    version: '1.0.0',
    contact: {
      name: 'Wangzc',
      email: '123@fiberhome.com',
      url: 'http://www.apache.org/licenses',
    },
    termsOfService: 'http://www.apache.org/licenses/LICENSE-2.0.html',
  },
  servers: [
    {
      url: 'http://petstore.swagger.io/v2',
      description: 'v2 server',
      variables: {
        version: {
          default: 'v1',
          description: 'version',
          enum: ['v1', 'v2'],
        },
      },
    },
  ],
  externalDocs: {
    url: 'ss',
    description: 'description',
  },
  tags: [
    {
      description: 'Everything about your Pets',
      name: 'pet',
      externalDocs: {
        url: 'http://localhost://pettag/detail',
        description: 'pet dog',
      },
    },
    {
      description: 'Access to Petstore orders',
      name: 'store',
      externalDocs: {
        url: 'http://localhost://store/detail',
        description: 'store store',
      },
    },
    {
      description: 'Operations about user',
      name: 'user',
    },
  ],
};

export const petStoreOas3 = {
  openapi: '3.0.2',
  info: {
    description:
      'This is a sample server Petstore server. For this sample, you can use the api key `special-key` to test the authorization filters.',
    title: 'pet store',
    version: '1.0.1',
    termsOfService: 'trtd',
    contact: {
      name: '',
      url: 'ddd',
    },
    license: {
      name: 'Apache-2.0',
      url: 'sss.com',
    },
  },
  servers: [
    {
      url: '123',
      variables: {
        version: {
          default: 'v111',
          enum: ['v1', 'v2'],
        },
      },
    },
    {
      url: '1223{version}',
      variables: {
        version: {
          default: 'v1',
          enum: ['v1', 'v2'],
        },
      },
    },
  ],
  tags: [
    {
      name: 'pet',
      description: 'Everything about your Pets',
    },
    {
      name: 'store',
      description: 'Access to Petstore orders',
    },
    {
      name: 'user',
      description: 'Operations about user',
    },
  ],
  paths: {
    '/pet': {
      post: {
        tags: ['pet'],
        summary: 'Add a new pet to the store',
        description: '',
        operationId: 'addPet',
        responses: {
          '405': {
            description: 'Invalid input',
          },
        },
        security: [
          {
            petstore_auth: ['write:pets', 'read:pets'],
          },
        ],
        requestBody: {
          $ref: '#/components/requestBodies/Pet',
        },
      },
      put: {
        tags: ['pet'],
        summary: 'Update an existing pet',
        description: '',
        operationId: 'updatePet',
        responses: {
          '400': {
            description: 'Invalid ID supplied',
          },
          '404': {
            description: 'Pet not found',
          },
          '405': {
            description: 'Validation exception',
          },
        },
        security: [
          {
            petstore_auth: ['write:pets', 'read:pets'],
          },
        ],
        requestBody: {
          $ref: '#/components/requestBodies/Pet',
        },
      },
    },
    '/pet/findByStatus': {
      post: {
        operationId: 'findPetsByStatus',
        responses: {
          '200': {
            description: 'post',
            content: {
              'application/xml': {
                schema: {
                  oneOf: [
                    {
                      $ref: '#/components/schemas/Peta',
                    },
                    {
                      type: 'object',
                      properties: {
                        id: {
                          type: 'string',
                          not: {
                            type: 'integer',
                          },
                        },
                      },
                    },
                  ],
                  additionalProperties: {
                    type: 'string',
                  },
                },
              },
            },
          },
        },
      },
      get: {
        tags: ['pet'],
        summary: 'Finds Pets by status',
        description: 'Multiple status values can be provided with comma separated strings',
        operationId: 'findPetsByStatus',
        parameters: [
          {
            name: 'status',
            in: 'query',
            description: 'Status values that need to be considered for filter',
            required: true,
            style: 'form',
            explode: false,
            schema: {
              type: 'array',
              items: {
                type: 'string',
                enum: ['available', 'pending', 'sold'],
                default: 'available',
              },
            },
          },
        ],
        responses: {
          '200': {
            description: 'successful operation',
            content: {
              'application/xml': {
                schema: {
                  type: 'array',
                  items: {
                    $ref: '#/components/schemas/Pet',
                  },
                },
              },
              'application/json': {
                schema: {
                  type: 'array',
                  items: {
                    $ref: '#/components/schemas/Pet',
                  },
                },
              },
            },
          },
          '400': {
            description: 'Invalid status value',
          },
        },
        security: [
          {
            petstore_auth: ['write:pets', 'read:pets'],
          },
        ],
      },
    },
    '/pet/findByTags': {
      get: {
        tags: ['pet'],
        summary: 'Finds Pets by tags',
        description:
          'Multiple tags can be provided with comma separated strings. Use tag1, tag2, tag3 for testing.',
        operationId: 'findPetsByTags',
        parameters: [
          {
            name: 'tags',
            in: 'query',
            description: 'Tags to filter by',
            required: true,
            style: 'form',
            explode: false,
            schema: {
              type: 'array',
              items: {
                type: 'string',
              },
            },
          },
        ],
        responses: {
          '200': {
            description: 'successful operation',
            content: {
              'application/xml': {
                schema: {
                  type: 'array',
                  items: {
                    $ref: '#/components/schemas/Pet',
                  },
                },
              },
              'application/json': {
                schema: {
                  type: 'array',
                  items: {
                    $ref: '#/components/schemas/Pet',
                  },
                },
              },
            },
          },
          '400': {
            description: 'Invalid tag value',
          },
        },
        security: [
          {
            petstore_auth: ['write:pets', 'read:pets'],
          },
        ],
        deprecated: true,
      },
    },
    '/pet/{petId}': {
      get: {
        tags: ['pet'],
        summary: 'Find pet by ID',
        description:
          '# OpenAPI Core Library (Typescript)  ## What is it? This project is a library, written in Typescript, to read and manipulate [OpenAPI](https://www.openapis.org/) specification documents.',
        operationId: 'getPetById',
        parameters: [
          {
            name: 'petId',
            in: 'path',
            description: 'ID of pet to return',
            required: true,
            schema: {
              type: 'string',
              format: 'password',
              maxLength: 10,
              minimum: 100,
            },
          },
        ],
        responses: {
          '200': {
            description: 'successful operation',
            content: {
              'application/xml': {
                schema: {
                  $ref: '#/components/schemas/Pet',
                },
              },
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/Pet',
                  type: 'object',
                  properties: {
                    additionalMetadata: {
                      description: 'Additional data to pass to server',
                      type: 'string',
                      enum: [2, 1],
                    },
                  },
                },
              },
            },
          },
          '400': {
            description: 'Invalid ID supplied',
          },
          '404': {
            description: 'Pet not found',
          },
        },
        security: [
          {
            api_key: [],
          },
        ],
      },
      post: {
        tags: ['pet'],
        summary: 'Updates a pet in the store with form data',
        description: '',
        operationId: 'updatePetWithForm',
        parameters: [
          {
            name: 'petId',
            in: 'path',
            description: 'ID of pet that needs to be updated',
            required: true,
            schema: {
              type: 'integer',
              format: 'int64',
            },
          },
        ],
        responses: {
          '405': {
            description: 'Invalid input',
          },
        },
        security: [
          {
            petstore_auth: ['write:pets', 'read:pets'],
          },
        ],
        requestBody: {
          content: {
            'application/x-www-form-urlencoded': {
              schema: {
                type: 'object',
                properties: {
                  name: {
                    description: 'Updated name of the pet',
                    type: 'string',
                  },
                  status: {
                    description: 'Updated status of the pet',
                    type: 'string',
                  },
                },
              },
            },
          },
        },
      },
      delete: {
        tags: ['pet'],
        summary: 'Deletes a pet',
        description: '',
        operationId: 'deletePet',
        parameters: [
          {
            name: 'api_key',
            in: 'header',
            required: false,
            schema: {
              type: 'string',
              enum: [1, 2],
            },
          },
          {
            name: 'petId',
            in: 'path',
            description: 'Pet id to delete',
            required: true,
            schema: {
              type: 'integer',
              format: 'int64',
              default: 'dsddd',
              enum: ['1rfreft', 2],
            },
          },
        ],
        responses: {
          '400': {
            description: 'Invalid pet value',
          },
        },
        security: [
          {
            petstore_auth: ['write:pets', 'read:pets'],
          },
        ],
      },
    },
    '/pet/{petId}/uploadImage': {
      post: {
        tags: ['pet'],
        summary: 'uploads an image',
        description: '',
        operationId: 'uploadFile',
        parameters: [
          {
            name: 'petId',
            in: 'path',
            description: 'ID of pet to update',
            required: true,
            schema: {
              type: 'integer',
              format: 'int64',
            },
          },
        ],
        responses: {
          '200': {
            description: 'successful operation',
            content: {
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/ApiResponse',
                },
              },
            },
          },
        },
        security: [
          {
            petstore_auth: ['write:pets', 'read:pets'],
          },
        ],
        requestBody: {
          content: {
            'multipart/form-data': {
              schema: {
                type: 'object',
                properties: {
                  additionalMetadata: {
                    description: 'Additional data to pass to server',
                    type: 'string',
                  },
                  file: {
                    description: 'file to upload',
                    type: 'string',
                    format: 'binary',
                  },
                },
              },
            },
          },
        },
      },
    },
    '/store/inventory': {
      get: {
        tags: ['store'],
        summary: 'Returns pet inventories by status',
        description: 'Returns a map of status codes to quantities',
        operationId: 'getInventory',
        responses: {
          '200': {
            description: 'successful operation',
            content: {
              'application/json': {
                schema: {
                  type: 'object',
                  additionalProperties: {
                    type: 'integer',
                    format: 'int32',
                  },
                },
              },
            },
          },
        },
        security: [
          {
            api_key: [],
          },
        ],
      },
    },
    '/store/order': {
      post: {
        tags: ['store'],
        summary: 'Place an order for a pet',
        description: '',
        operationId: 'placeOrder',
        responses: {
          '200': {
            description: 'successful operation',
            content: {
              'application/xml': {
                schema: {
                  $ref: '#/components/schemas/Order',
                },
              },
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/Order',
                },
              },
            },
          },
          '400': {
            description: 'Invalid Order',
          },
        },
        requestBody: {
          content: {
            'application/json': {
              schema: {
                $ref: '#/components/schemas/Order',
              },
            },
          },
          description: 'order placed for purchasing the pet',
          required: true,
        },
      },
    },
    '/store/order/{orderId}': {
      get: {
        tags: ['store'],
        summary: 'Find purchase order by ID',
        description:
          'For valid response try integer IDs with value <= 5 or > 10. Other values will generated exceptions',
        operationId: 'getOrderById',
        parameters: [
          {
            name: 'orderId',
            in: 'path',
            description: 'ID of pet that needs to be fetched',
            required: true,
            schema: {
              type: 'integer',
              format: 'int64',
              minimum: 1,
              maximum: 5,
            },
          },
        ],
        responses: {
          '200': {
            description: 'successful operation',
            content: {
              'application/xml': {
                schema: {
                  $ref: '#/components/schemas/Order',
                },
              },
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/Order',
                },
              },
            },
          },
          '400': {
            description: 'Invalid ID supplied',
          },
          '404': {
            description: 'Order not found',
          },
        },
      },
      delete: {
        tags: ['store'],
        summary: 'Delete purchase order by ID',
        description:
          'For valid response try integer IDs with value < 1000. Anything above 1000 or nonintegers will generate API errors',
        operationId: 'deleteOrder',
        parameters: [
          {
            name: 'orderId',
            in: 'path',
            description: 'ID of the order that needs to be deleted',
            required: true,
            schema: {
              type: 'string',
            },
          },
        ],
        responses: {
          '400': {
            description: 'Invalid ID supplied',
          },
          '404': {
            description: 'Order not found',
          },
        },
      },
    },
    '/user': {
      post: {
        tags: ['user'],
        summary: 'Create user',
        description: 'This can only be done by the logged in user.',
        operationId: 'createUser',
        responses: {
          default: {
            description: 'successful operation',
          },
        },
        requestBody: {
          content: {
            'application/json': {
              schema: {
                $ref: '#/components/schemas/User',
              },
            },
          },
          description: 'Created user object',
          required: true,
        },
      },
    },
    '/user/createWithArray': {
      post: {
        tags: ['user'],
        summary: 'Creates list of users with given input array',
        description: '',
        operationId: 'createUsersWithArrayInput',
        responses: {
          default: {
            description: 'successful operation',
          },
        },
        requestBody: {
          $ref: '#/components/requestBodies/UserArray',
        },
      },
    },
    '/user/createWithList': {
      post: {
        tags: ['user'],
        summary: 'Creates list of users with given input array',
        description: '',
        operationId: 'createUsersWithListInput',
        responses: {
          default: {
            description: 'successful operation',
          },
        },
        requestBody: {
          $ref: '#/components/requestBodies/UserArray',
        },
      },
    },
    '/user/login': {
      get: {
        tags: ['user'],
        summary: 'Logs user into the system',
        description: '',
        operationId: 'loginUser',
        parameters: [
          {
            name: 'username',
            in: 'query',
            description: 'The user name for login',
            required: true,
            schema: {
              type: 'string',
            },
          },
          {
            name: 'password',
            in: 'query',
            description: 'The password for login in clear text',
            required: true,
            schema: {
              type: 'string',
            },
          },
        ],
        responses: {
          '200': {
            description: 'successful operation',
            headers: {
              'X-Rate-Limit': {
                description: 'calls per hour allowed by the user',
                schema: {
                  type: 'integer',
                  format: 'int32',
                },
              },
              'X-Expires-After': {
                description: 'date in UTC when toekn expires',
                schema: {
                  type: 'string',
                  format: 'date-time',
                },
              },
            },
            content: {
              'application/xml': {
                schema: {
                  type: 'string',
                },
              },
              'application/json': {
                schema: {
                  type: 'string',
                },
              },
            },
          },
          '400': {
            description: 'Invalid username/password supplied',
          },
        },
      },
    },
    '/user/logout': {
      get: {
        tags: ['user'],
        summary: 'Logs out current logged in user session',
        description: '',
        operationId: 'logoutUser',
        responses: {
          default: {
            description: 'successful operation',
          },
        },
      },
    },
    '/user/{username}': {
      get: {
        tags: ['user'],
        summary: 'Get user by user name',
        description: '',
        operationId: 'getUserByName',
        parameters: [
          {
            name: 'username',
            in: 'path',
            description: 'The name that needs to be fetched. Use user1 for testing.',
            required: true,
            schema: {
              $ref: '#/components/schemas/UserId',
            },
          },
        ],
        responses: {
          '200': {
            description: 'successful operation',
            content: {
              'application/xml': {
                schema: {
                  $ref: '#/components/schemas/User',
                },
              },
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/User',
                },
              },
            },
          },
          '400': {
            description: 'Invalid username supplied',
          },
          '404': {
            description: 'User not found',
          },
        },
      },
      put: {
        tags: ['user'],
        summary: 'Updated user',
        description: 'This can only be done by the logged in user.',
        operationId: 'updateUser',
        parameters: [
          {
            name: 'username',
            in: 'path',
            description: 'name that need to be deleted',
            required: true,
            schema: {
              type: 'string',
              example: 'd',
            },
          },
        ],
        responses: {
          '400': {
            description: 'Invalid user supplied',
          },
          '404': {
            description: 'User not found',
          },
        },
        requestBody: {
          content: {
            'application/json': {
              schema: {
                $ref: '#/components/schemas/User',
              },
            },
          },
          description: 'Updated user object',
          required: true,
        },
      },
      delete: {
        tags: ['user'],
        summary: 'Delete user',
        description: 'This can only be done by the logged in user.',
        operationId: 'deleteUser',
        parameters: [
          {
            name: 'username',
            in: 'path',
            description: 'The name that needs to be deleted',
            required: true,
            schema: {
              type: 'string',
            },
          },
        ],
        responses: {
          '400': {
            description: 'Invalid username supplied',
          },
          '404': {
            description: 'User not found',
          },
        },
      },
    },
  },
  externalDocs: {
    description: 'Find out more about Swagger',
    url: 'trtd',
  },
  components: {
    requestBodies: {
      UserArray: {
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: {
                $ref: '#/components/schemas/User',
              },
              maxItems: 10,
              minItems: 1,
              uniqueItems: false,
            },
          },
        },
        description: 'List of user object',
        required: true,
      },
      Pet: {
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/Pet',
            },
          },
          'application/xml': {
            schema: {
              $ref: '#/components/schemas/Pet',
            },
          },
        },
        description: 'Pet object that needs to be added to the store',
        required: true,
      },
    },
    securitySchemes: {
      petstore_auth: {
        type: 'oauth2',
        flows: {
          implicit: {
            authorizationUrl: 'ddd',
            scopes: {
              'write:pets': 'modify pets in your account',
              'read:pets': 'read your pets',
            },
          },
        },
      },
      api_key: {
        type: 'apiKey',
        name: 'api_key',
        in: 'header',
      },
      store_auth1: {
        type: 'oauth2',
        flows: {
          implicit: {
            authorizationUrl: 'https://example.com/api/oauth/dialog',
            scopes: {
              'write:pets': 'modify pets in your account',
              'read:pets': 'read your pets',
            },
          },
          authorizationCode: {
            authorizationUrl: 'https://example.com/api/oauth/dialog',
            tokenUrl: 'https://example.com/api/oauth/token',
            scopes: {
              'write:pets': 'modify pets in your account',
              'read:pets': 'read your pets',
            },
          },
        },
      },
    },
    schemas: {
      UserId: {
        not: {
          type: 'integer',
        },
      },
      Order: {
        title: 'Pet Order',
        description: 'An order for a pets from the pet store',
        type: 'object',
        properties: {
          id: {
            type: 'integer',
            format: 'int64',
          },
          petId: {
            type: 'integer',
            format: 'int64',
          },
          quantity: {
            type: 'integer',
            format: 'int32',
            description: 'quantity of order',
            multipleOf: 1,
            maximum: 10000,
            exclusiveMaximum: false,
            minimum: 1,
            exclusiveMinimum: true,
            default: 1,
          },
          shipDate: {
            type: 'string',
            format: 'date-time',
          },
          status: {
            type: 'string',
            description: 'Order Status',
            enum: ['placed', 'approved', 'delivered'],
          },
          complete: {
            description: 'if the order is completed',
            type: 'boolean',
            default: false,
          },
        },
        xml: {
          name: 'Order',
        },
      },
      Category: {
        title: 'Pet category',
        description: 'A category for a pet',
        type: 'object',
        properties: {
          id: {
            type: 'integer',
            format: 'int64',
          },
          name: {
            type: 'string',
          },
        },
        xml: {
          name: 'Category',
        },
      },
      User: {
        title: 'a User',
        description: 'A User who is purchasing from the pet store',
        type: 'object',
        properties: {
          id: {
            type: 'integer',
            format: 'int64',
          },
          username: {
            type: 'string',
          },
          firstName: {
            type: 'string',
          },
          lastName: {
            type: 'string',
          },
          email: {
            type: 'string',
          },
          password: {
            type: 'string',
          },
          phone: {
            type: 'string',
          },
          userStatus: {
            type: 'integer',
            format: 'int32',
            description: 'User Status',
          },
        },
        xml: {
          name: 'User',
        },
      },
      Tag: {
        title: 'Pet Tag',
        description: 'A tag for a pet',
        type: 'object',
        properties: {
          id: {
            type: 'integer',
            format: 'int64',
          },
          name: {
            type: 'string',
          },
        },
        xml: {
          name: 'Tag',
        },
      },
      Test: {
        nullable: false,
        readOnly: false,
        writeOnly: false,
        deprecated: false,
        example: '',
        oneOf: [
          {
            $ref: '#/components/schemas/Peta',
          },
          {
            nullable: false,
            readOnly: false,
            writeOnly: false,
            deprecated: false,
            example: '',
            type: 'object',
            description: '',
            maxProperties: null,
            minProperties: null,
            required: [],
            properties: {
              id: {
                nullable: false,
                readOnly: false,
                writeOnly: false,
                deprecated: false,
                example: '',
                not: {
                  nullable: false,
                  readOnly: false,
                  writeOnly: false,
                  deprecated: false,
                  example: '',
                  type: 'integer',
                  description: '',
                  format: '',
                  multipleOf: null,
                  maximum: null,
                  exclusiveMaximum: false,
                  minimum: null,
                  exclusiveMinimum: false,
                  default: null,
                  enum: [],
                },
              },
            },
          },
        ],
      },
      Pet: {
        title: 'a Pet',
        description: 'A pet for sale in the pet store',
        type: 'object',
        required: ['name', 'photoUrls'],
        properties: {
          id: {
            type: 'integer',
            format: 'int64',
          },
          category: {
            $ref: '#/components/schemas/Category',
          },
          name: {
            type: 'string',
            example: 'doggie',
          },
          photoUrls: {
            type: 'array',
            xml: {
              name: 'photoUrl',
              wrapped: true,
            },
            items: {
              type: 'string',
            },
          },
          tags: {
            type: 'array',
            xml: {
              name: 'tag',
              wrapped: true,
            },
            items: {
              $ref: '#/components/schemas/Tag',
            },
          },
          status: {
            type: 'string',
            description: 'pet status in the store',
            enum: ['available', 'pending', 'sold'],
          },
        },
        xml: {
          name: 'Pet',
        },
      },
      ApiResponse: {
        title: 'An uploaded response',
        description: 'Describes the result of uploading an image resource',
        type: 'object',
        properties: {
          code: {
            type: 'integer',
            format: 'int32',
          },
          type: {
            oneOf: [
              {
                type: 'string',
              },
              {
                type: 'integer',
              },
            ],
          },
          message: {
            not: {
              type: 'string',
            },
          },
        },
      },
      Peta: {
        type: 'object',
        required: ['pet_type'],
        properties: {
          pet_type: {
            type: 'string',
          },
        },
        discriminator: {
          propertyName: 'pet_type',
          mapping: {
            cachorro: 'Dog',
          },
        },
      },
      Cat: {
        allOf: [
          {
            $ref: '#/components/schemas/Peta',
          },
          {
            $ref: '#/components/schemas/Pet',
          },
          {
            type: 'object',
            properties: {
              name: {
                type: 'string',
              },
            },
          },
        ],
      },
      Dog: {
        allOf: [
          {
            $ref: '#/components/schemas/Peta',
          },
          {
            type: 'object',
            properties: {
              bark: {
                type: 'string',
              },
            },
          },
        ],
        example: 'sss',
      },
      Lizard: {
        allOf: [
          {
            $ref: '#/components/schemas/Peta',
          },
          {
            type: 'object',
            properties: {
              lovesRocks: {
                type: 'boolean',
              },
            },
          },
        ],
      },
    },
    links: {
      GetUserByUserId: {
        operationRef: '#/paths/~1users~1{userId}/get',
        parameters: {
          userId: '$response.body#/id',
        },
      },
    },
    callbacks: {
      myEvent: {
        '{$request.body#/callbackUrl}': {
          post: {
            requestBody: {
              required: true,
              content: {
                'application/json': {
                  schema: {
                    type: 'object',
                    properties: {
                      message: {
                        type: 'string',
                        example: 'Some event happened',
                      },
                    },
                    required: ['message'],
                  },
                },
              },
            },
            responses: {
              '200': {
                description: 'Your server returns this code if it accepts the callback',
              },
            },
          },
        },
      },
    },
    examples: {
      tagName: {
        description: 'pet tag as a string',
        externalValue: 'tag3',
        value: 'Value',
      },
    },
  },
};
