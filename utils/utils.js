export class Utils {
  static isPrimitive(arg) {
    let type = typeof arg;
    return arg == null || (type !== 'object' && type !== 'function');
  }

  static isInteger(number) {
    if (!number.toString) {
      return false;
    }
    let numberStr = number.toString();
    return /^\d+$/g.test(numberStr);
  }

  static isNotEmptyString(str) {
    return (typeof str === 'string' || str instanceof String) && str.length > 0;
  }

  static isValidVersionNumber(versionStr) {
    // 参考 https://stackoverflow.com/questions/82064/a-regex-for-version-number-parsing
    const versionNumberRegex = /^(\d+\.)?(\d+\.)?(\*|\d+)$/gm;
    if (!Utils.isNotEmptyString(versionStr)) {
      return false;
    }
    return versionNumberRegex.test(versionStr);
  }

  static loadUri(value) {
    // 参考 https://www.npmjs.com/package/valid-url
    // 符合RFC 3986标准
    let splitUri = uri => {
      return uri.match(/(?:([^:\/?#]+):)?(?:\/\/([^\/?#]*))?([^?#]*)(?:\?([^#]*))?(?:#(.*))?/);
    };
    if (!value) {
      return;
    }
    // check for illegal characters
    if (/[^a-z0-9\:\/\?\#\[\]\@\!\$\&\'\(\)\*\+\,\;\=\.\-\_\~\%]/i.test(value)) {
      return;
    }

    // check for hex escapes that aren't complete
    if (/%[^0-9a-f]/i.test(value)) {
      return;
    }
    if (/%[0-9a-f](:?[^0-9a-f]|$)/i.test(value)) {
      return;
    }

    let splitted = [];
    let scheme = '';
    let authority = '';
    let path = '';
    let query = '';
    let fragment = '';
    let out = '';

    // from RFC 3986
    splitted = splitUri(value);
    scheme = splitted[1];
    authority = splitted[2];
    path = splitted[3];
    query = splitted[4];
    fragment = splitted[5];

    // scheme and path are required, though the path can be empty
    if (!(scheme && scheme.length && path.length >= 0)) {
      return;
    }

    // if authority is present, the path must be empty or begin with a /
    if (authority && authority.length) {
      if (!(path.length === 0 || /^\//.test(path))) {
        return;
      }
    } else if (/^\/\//.test(path)) {
      // if authority is not present, the path must not start with //
      return;
    }

    // scheme must begin with a letter, then consist of letters, digits, +, ., or -
    if (!/^[a-z][a-z0-9\+\-\.]*$/.test(scheme.toLowerCase())) {
      return;
    }

    // re-assemble the URL per section 5.3 in RFC 3986
    out += `${scheme}:`;
    if (authority && authority.length) {
      out += `//${authority}`;
    }

    out += path;

    if (query && query.length) {
      out += `?${query}`;
    }

    if (fragment && fragment.length) {
      out += `#${fragment}`;
    }

    return out;
  }

  static isValidUrl(urlStr) {
    if (!Utils.isNotEmptyString(urlStr)) {
      return false;
    }
    return !!Utils.loadUri(urlStr);
  }

  static isValidEMail(emailStr) {
    // 符合html5的email验证正则
    // https://trac.webkit.org/browser/trunk/Source/WebCore/html/EmailInputType.cpp?rev=86298
    // 参考: https://stackoverflow.com/questions/7786058/find-the-regex-used-by-html5-forms-for-validation
    const emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@[a-z0-9-]+(\.[a-z0-9-]+)*/gm;
    if (!Utils.isNotEmptyString(emailStr)) {
      return false;
    }
    return emailRegex.test(emailStr);
  }

  static newGuid() {
    let s4 = () => {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    };
    return `${s4() + s4()}-${s4()}-${s4()}-${s4()}-${s4()}${s4()}${s4()}`;
  }

  static deepEqual(objX, objY) {
    const recursiveEqual = (x, y) => {
      if (x === y) {
        return true;
      } else if (typeof x == 'object' && x != null && (typeof y == 'object' && y != null)) {
        if (Object.keys(x).length != Object.keys(y).length) {
          return false;
        }
        for (let key of Object.keys(x)) {
          if (y.hasOwnProperty(key)) {
            if (!recursiveEqual(x[key], y[key])) {
              return false;
            }
          } else {
            return false;
          }
        }
        return true;
      } else {
        return false;
      }
    };
    return recursiveEqual(objX, objY);
  }

  static getPropByPath(obj, propPath, pathSeparator = '.') {
    try {
      let tokens = propPath.split(pathSeparator);
      return tokens.reduce((prev, curr) => {
        if (typeof prev === 'undefined') {
          return prev;
        }
        return prev[curr];
      }, obj);
    } catch (e) {
      console.log(`[Exception][getPropByPath]${e}`);
      return null;
    }
  }

  static setPropByPath(obj, value, propPath, pathSeparator = '.') {
    try {
      let index;
      let splitPath = propPath.split(pathSeparator);
      let targetNode = obj;
      for (index = 0; index < splitPath.length - 1; index++) {
        targetNode = targetNode[splitPath[index]];
      }
      // delete targetNode[splitPath[index]];
      targetNode[splitPath[index]] = value;
    } catch (e) {
      console.log(`[Exception][setPropByPath]${e}`);
    }
  }
}
